﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class ExpenseListViewModel
    {
        public int ID { get; set; }
        public String Explaination { get; set; }
        public DateTime bill_Date { get; set; }
        public String bill_Num { get; set; }
        public Decimal TotalAmount { get; set; }
        public Decimal total_VAT { get; set; }
        public String paymentStatus { get; set; }
        public DateTime payment_Date { get; set; }
        public String supplierName { get; set; }
    }
}
