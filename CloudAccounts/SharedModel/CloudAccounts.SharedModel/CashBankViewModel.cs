﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class CashBankViewModel
    {
        public int CashBankId { get; set; }
        public string AccountName { get; set; }
        public string CurrencyName { get; set; }
        public decimal CurrencyRate { get; set; }
        public decimal? OpeningBalance { get; set; }
        public DateTime? OpeningBalanceDate { get; set; }
        public string BankName { get; set; }
        public string Branch { get; set; }
        public string AccountNumber { get; set; }
        public string IBAN { get; set; }
        public string AccountType { get; set; }             //0001 for and 0002 for bank
        public string AccountCode { get; set; }             //the code which is genertaed in the Account table
        public List<GLDetailViewModel> GLDetails { get; set; }
    }
}
