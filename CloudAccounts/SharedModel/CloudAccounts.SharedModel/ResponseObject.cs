﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class ResponseObject
    {
        public string ErrorMessage { get; set; }
        public string ResponseCode { get; set; }
        public bool IsSuccess { get; set; }
        public object ResultData { get; set; }
    }
}
