﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class RegistrationViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
        public string IndustryType { get; set; }
        public string FeatureType { get; set; }
        public string CashBankTransaction { get; set; }
        public string SalesTransaction { get; set; }
        public string ExpenseTransaction { get; set; }
        public string EmployeesTransaction { get; set; }

    }
}
