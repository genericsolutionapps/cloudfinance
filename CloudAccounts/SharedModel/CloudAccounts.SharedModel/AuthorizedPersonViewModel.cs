﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class AuthorizedPersonViewModel
    {
        public int APId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Notes { get; set; }
        public int CompanyId { get; set; }
        public int CustomerId { get; set; }
    }
}
