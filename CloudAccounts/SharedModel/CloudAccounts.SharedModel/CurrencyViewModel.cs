﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class CurrencyViewModel
    {
        public int curr_ID { get; set; }
        public String code { get; set; }
        public String title { get; set; }
        public String symbol { get; set; }
    }
}
