﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class GLDetailViewModel
    {
        public int GLDetailId { get; set; }
        public int GLHeaderId { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string Explanation { get; set; }
        public string AccountCode { get; set; }
        public DateTime Date { get; set; }
        public decimal Balance { get; set; }                  //used to maintain balance of an account after this transaction
        //public string GeneralVoucherType { get; set; }      //used when creating a gv to know whether it is for sale or expense
        public string TransactionType { get; set; }           //whether this transaction is opening bal, sales, expense etc
    }
}
