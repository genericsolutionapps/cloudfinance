﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Data;

namespace CloudAccounts.SharedModel
{
    public class SupplierViewModel
    {
        public int supplierID { get; set; }
        public String CompanyName { get; set; }
        public String shortName { get; set; }
        public String category { get; set; }
        public String emailAddress { get; set; }
        public String phoneNum { get; set; }
        public String faxNum { get; set; }
        public String IBAN_Num { get; set; }
        public String openAddress { get; set; }
        public String district { get; set; }
        public String province { get; set; }
        public String tour { get; set; }
        public String vkn_tckn { get; set; }
        public String taxAdministration { get; set; }
        //public String TC_Identification { get; set; }
        public String tc_identification { get; set; }
        //public bool openingBal { get; set; }

        public String currencySymbol { get; set; }
        public DateTime companyDate { get; set; }
        //public String companyDate { get; set; }
        public Decimal price { get; set; }
        public String msg { get; set; }

        /*,\"supplierDetail\":[
         *  if vkn-tckn and tax administration is given then tour is legal entity
         *  if TC_identification is given then tour is real person
         *
         *  For Real Person no supplier detail will be provided
         * 
         */

        //public List<SupplierDetailVM> SupplierDetailList { get; set; }
        //public SupplierHeader SupplierHeader { get; set; }
        //public supplierDetail SupplierDetail { get; set; }
        public List<supplierDetail> supplierDetail { get; set; }
    }
}
