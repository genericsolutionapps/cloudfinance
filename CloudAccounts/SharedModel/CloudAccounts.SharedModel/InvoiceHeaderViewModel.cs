﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class InvoiceHeaderViewModel
    {
        public List<InvoiceDetailViewModel> InvoiceDetailCollection { get; set; }
        public int InvoiceHeaderId { get; set; }
        public int CompanyId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalAllocatedAmount { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public decimal CurrencyRate { get; set; }
        public decimal BaseRate { get; set; }
        public string VoucherType { get; set; }         //0001 for invoice          0002 for bill
        public int CurrencyId { get; set; }

        //property for VAT report
        public decimal TotalVAT { get; set; }

        //property to be used on ProductInformation page
        public int ProductQuantity { get; set; }
    }
}
