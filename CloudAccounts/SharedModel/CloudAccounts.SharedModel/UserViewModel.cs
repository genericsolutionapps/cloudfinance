﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class UserViewModel
    {
        //public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserStatusTypeCode { get; set; }
        public string Password { get; set; }
        public string CashBankTransaction { get; set; }
        public string SalesTransaction { get; set; }
        public string ExpenseTransaction { get; set; }
        public string EmployeesTransaction { get; set; }

    }
}
