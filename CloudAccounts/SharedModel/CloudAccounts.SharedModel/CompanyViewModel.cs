﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class CompanyViewModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CommercialTitle { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Sector { get; set; }
        public string Address { get; set; }
        public string TaxInformation { get; set; }

        //Added By Noor ...
        public string District { get; set; }
        public string Province { get; set; }
        public string Wire { get; set; }
        public string VD { get; set; }

        public string Fax { get; set; }

    }
}
