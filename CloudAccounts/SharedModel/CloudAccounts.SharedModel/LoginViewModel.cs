﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class LoginViewModel
    {
        public int User_ID { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public bool Is_Active { get; set; }
    }
}
