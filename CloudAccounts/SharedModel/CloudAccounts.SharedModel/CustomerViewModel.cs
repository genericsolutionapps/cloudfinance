﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyTitle { get; set; }
        public string ShortName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string IBANNumber { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string CustomerType { get; set; }                            //since we have BusinessPartnerType now, this is prolly of no use
        public string VKN { get; set; }
        public string Tax { get; set; }
        public string Address { get; set; }
        public string CategoryName { get; set; }
        public Nullable<System.DateTime> OpeningBalanceDate { get; set; }
        public Nullable<decimal> OpeningBalanceAmount { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public decimal TotalInvoicedAmount { get; set; }
        public decimal TotalPayable { get; set; }
        public decimal TotalRemainingAmount { get; set; }                   //Invoice
        public decimal RemainingPayable { get; set; }                       //Bill
        public string BusinessPartnerType { get; set; }                     //0001 for customer, 0002 for supplier

        public List<AuthorizedPersonViewModel> AuthorizedPersonsCollection { get; set; }
    }
}
