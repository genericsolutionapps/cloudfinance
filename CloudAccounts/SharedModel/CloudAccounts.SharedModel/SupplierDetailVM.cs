﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class SupplierDetailVM
    {
        public int SupplierDetail_ID { get; set; }
        public int SupplierHeader_ID { get; set; }
        public String supplierName { get; set; }
        public String email { get; set; }
        public String telephone { get; set; }
        public String Notes { get; set; }
    }
}
