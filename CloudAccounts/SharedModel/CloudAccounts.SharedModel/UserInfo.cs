﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string SessionId { get; set; }
        public string Email { get; set; }

    }
}
