﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class InvoiceDetailViewModel
    {
        public int InvoiceDetailID { get; set; }
        public int InvoiceHeaderID { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal DiscountPerc { get; set; }
        public decimal Amount { get; set; }
        public decimal TaxPerc { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string ProductName { get; set; }
        public string BusinessPartnerName { get; set; }
        public DateTime Date { get; set; }                      //property used on stock history page
        public string VoucherType { get; set; }                 //property used on stock history page
    }
}
