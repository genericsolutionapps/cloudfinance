﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string IdentificationNumber { get; set; }
        public string IBAN { get; set; }
        public int CategoryId { get; set; }
        public int CompanyId { get; set; }
        public string AccountCode { get; set; }
        public decimal EmployeeBalance { get; set; }
    }
}
