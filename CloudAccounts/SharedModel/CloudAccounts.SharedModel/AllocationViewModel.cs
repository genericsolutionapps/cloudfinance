﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class AllocationViewModel
    {
        public int AllocationId { get; set; }
        public DateTime Date { get; set; }

        //if the allocation mode is cash
        public DateTime? ExpiryDate { get; set; }

        //name of an account, eg safe account
        public string Account { get; set; }
        public decimal Amount { get; set; }
        public string Explanation { get; set; }

        //not sure but prolly to distinguish between invoice and bill
        //so 0001 for invoice
        public string AllocationType { get; set; }

        //0001 for cash         0002 for bank
        public string AllocationMode { get; set; }
        public int? InvoiceHeaderId { get; set; }
        public int? CustomerId { get; set; }
        public List<int> InvoiceHeaderIdList { get; set; }

        public string VoucherType { get; set; }

        //used on cash bank report
        public string BusinessPartnerName { get; set; }
    }
}
