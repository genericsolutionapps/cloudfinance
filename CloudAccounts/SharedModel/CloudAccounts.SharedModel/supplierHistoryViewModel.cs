﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudAccounts.SharedModel
{
    public class supplierHistoryViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string supplierName { get; set; }
        public string customerName { get; set; }
        public List<SelectListItem> Notes { get; set; }
        public DateTime date { get; set; }
        public bool containFile { get; set; }

    }
}
