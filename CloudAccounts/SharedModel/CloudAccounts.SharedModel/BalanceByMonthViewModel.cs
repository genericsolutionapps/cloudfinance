﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class BalanceByMonthViewModel
    {
        public long Ticks { get; set; }
        public decimal Balance { get; set; }
    }
}
