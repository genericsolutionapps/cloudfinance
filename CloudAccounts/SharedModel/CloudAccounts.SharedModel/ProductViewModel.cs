﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.SharedModel
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Barcode { get; set; }
        public string AccountCode { get; set; }
        public int CategoryId { get; set; }
        public int StartingQuantity { get; set; }
        public string Unit { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal PurchaseCurrencyRate { get; set; }
        public int PurchaseCurrencyId { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal SalesCurrencyRate { get; set; }
        public int SalesCurrencyId { get; set; }
        public decimal KDVPerc { get; set; }
        public decimal BaseRate { get; set; }
        public int CurrentQuantity { get; set; }
        public int TotalStockIn { get; set; }                   //Used on Expense report
        public decimal TotalPurchasingAmount { get; set; }      //Used on Expense report
    }
}
