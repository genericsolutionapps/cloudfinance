﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IInvoiceHeaderRepository : IRepository<InvoiceHeader>
    {

    }


    public class InvoiceHeaderRepository : RepositoryBase<InvoiceHeader>, IInvoiceHeaderRepository
    {
        public InvoiceHeaderRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }

    }

}
