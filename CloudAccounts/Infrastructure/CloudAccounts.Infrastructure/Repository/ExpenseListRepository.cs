﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Data;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IExpenseListRepository : IRepository<ExpenseList>
    {

    }

    public class ExpenseListRepository : RepositoryBase<ExpenseList>, IExpenseListRepository
    {
        public ExpenseListRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
