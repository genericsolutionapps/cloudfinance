﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IAuthorizedPersonRepository : IRepository<AuthorizedPerson>
    {

    }

    public class AuthorizedPersonRepository : RepositoryBase<AuthorizedPerson>, IAuthorizedPersonRepository
    {
        public AuthorizedPersonRepository(IDatabaseFactory databaseFactory)
        : base(databaseFactory)
        {


        }
    }
}
