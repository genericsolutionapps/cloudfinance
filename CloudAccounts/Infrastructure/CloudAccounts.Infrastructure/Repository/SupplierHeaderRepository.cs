﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface ISupplierHeaderRepository : IRepository<SupplierHeader>
    {

    }

    public class SupplierHeaderRepository : RepositoryBase<SupplierHeader>, ISupplierHeaderRepository
    {
        public SupplierHeaderRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }
    }
}
