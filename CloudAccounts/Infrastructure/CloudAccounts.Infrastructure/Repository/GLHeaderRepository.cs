﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IGLHeaderRepository : IRepository<GLHeader>
    {

    }

    public class GLHeaderRepository : RepositoryBase<GLHeader>, IGLHeaderRepository
    {
        public GLHeaderRepository(IDatabaseFactory databaseFactory)
        : base(databaseFactory)
        {

        }
    }
}
