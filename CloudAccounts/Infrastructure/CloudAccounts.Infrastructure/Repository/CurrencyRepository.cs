﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Data;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface ICurrencyRepository : IRepository<currency>
    {

    }

    public class CurrencyRepository : RepositoryBase<currency>, ICurrencyRepository
    {
        public CurrencyRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

    }
}
