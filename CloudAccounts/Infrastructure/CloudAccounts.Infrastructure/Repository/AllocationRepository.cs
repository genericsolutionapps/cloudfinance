﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IAllocationRepository : IRepository<Allocation>
    {

    }

    public class AllocationRepository : RepositoryBase<Allocation>, IAllocationRepository
    {
        public AllocationRepository(IDatabaseFactory databaseFactory)
        : base(databaseFactory)
        {

        }
    }
}
