﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IInvoiceDetailRepository : IRepository<InvoiceDetail>
    {

    }


    public class InvoiceDetailRepository : RepositoryBase<InvoiceDetail>, IInvoiceDetailRepository
    {
        public InvoiceDetailRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }

    }
}
