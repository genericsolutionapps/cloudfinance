﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }
    }
    public interface IUserRepository : IRepository<User>
    {

    }
}
