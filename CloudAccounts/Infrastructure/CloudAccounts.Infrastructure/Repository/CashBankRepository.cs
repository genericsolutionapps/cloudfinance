﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface ICashBankRepository : IRepository<CashBank>
    {

    }

    public class CashBankRepository : RepositoryBase<CashBank>, ICashBankRepository
    {
        public CashBankRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
