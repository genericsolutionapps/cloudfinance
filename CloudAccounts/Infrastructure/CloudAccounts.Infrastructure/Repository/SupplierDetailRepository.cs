﻿using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface ISupplierDetailRepository : IRepository<supplierDetail>
    {

    }

    public class SupplierDetailRepository : RepositoryBase<supplierDetail>, ISupplierDetailRepository
    {
        public SupplierDetailRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }
    }
}
