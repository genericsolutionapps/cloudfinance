﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IGLDetailRepository : IRepository<GLDetail>
    {

    }

    public class GLDetailRepository : RepositoryBase<GLDetail>, IGLDetailRepository
    {
        public GLDetailRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
