﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface ICategoriesRepository : IRepository<Category>
    {

    }

    public class CategoriesRepository : RepositoryBase<Category>, ICategoriesRepository
    {
        public CategoriesRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }

    }


}
