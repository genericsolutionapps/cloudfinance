﻿using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Repository
{
    public interface IAccountRepository : IRepository<Account>
    {

    }

    public class AccountRepository : RepositoryBase<Account>, IAccountRepository
    {
        public AccountRepository(IDatabaseFactory databaseFactory)
        : base(databaseFactory)
        {

        }
    }
}
