﻿using CloudAccounts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Common
{
    public class UnitOfWork : IUnitofWork
    {
        private readonly IDatabaseFactory databaseFactory;

        private CloudAccountsDbEntities dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;

        }

        protected CloudAccountsDbEntities DataContext
        {
            get { return dataContext ?? (dataContext = databaseFactory.Get()); }
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }



        public IDatabaseFactory instance
        {
            get
            {
                return databaseFactory;
            }

        }
        public CloudAccountsDbEntities db
        {
            get
            {
                { return dataContext ?? (dataContext = databaseFactory.Get()); }
            }

        }
    }
}
