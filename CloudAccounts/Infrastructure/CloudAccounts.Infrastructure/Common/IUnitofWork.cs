﻿using CloudAccounts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Common
{
    public interface IUnitofWork
    {
        IDatabaseFactory instance { get; }
        CloudAccountsDbEntities db { get; }
        void Commit();
    }
}
