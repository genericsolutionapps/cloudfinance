﻿using CloudAccounts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Infrastructure.Common
{
    public class DatabaseFactory: Disposable, IDatabaseFactory
    {
        private CloudAccountsDbEntities dataContext;
        public CloudAccountsDbEntities Get()
        {
            return dataContext ?? (dataContext = new CloudAccountsDbEntities());
        }

        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext = null;
        }
    }
}
