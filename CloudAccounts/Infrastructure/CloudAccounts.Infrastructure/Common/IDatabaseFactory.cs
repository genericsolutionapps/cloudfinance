﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Data;

namespace CloudAccounts.Infrastructure.Common
{
    public interface IDatabaseFactory : IDisposable
    {
        CloudAccountsDbEntities Get();
    }
}
