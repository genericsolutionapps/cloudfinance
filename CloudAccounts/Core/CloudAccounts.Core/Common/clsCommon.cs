﻿using CloudAccounts.Enums.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudAccounts.Core.Common
{
   public class clsCommon
    {
       public static string GetStringValue(Enum value)
       {
           string output = null;

           Type type = value.GetType();

           FieldInfo fi = type.GetField(value.ToString());

           StringValue[] attrs =
              fi.GetCustomAttributes(typeof(StringValue),
                                      false) as StringValue[];
           if (attrs.Length > 0)
           {
               output = attrs[0].Value;
           }

           return output;
       }

       public static string GetStringDescription(Enum value)
       {
           string output = null;

           Type type = value.GetType();

           FieldInfo fi = type.GetField(value.ToString());

           StringDescription[] attrs =
              fi.GetCustomAttributes(typeof(StringDescription),
                                      false) as StringDescription[];
           if (attrs.Length > 0)
           {
               output = attrs[0].Value;
           }

           return output;
       }

       public static List<SelectListItem> GetSelectedItemUsingEnum(Enum enumModel)
       {
           List<SelectListItem> list = new List<SelectListItem>();
           foreach (Enum item in Enum.GetValues(enumModel.GetType()))
           {
               list.Add(new SelectListItem() { Text = item.ToString(), Value = GetStringValue(item) });
           }

           return list;
       }
    }
}
