﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Core.Attributes
{
    public class StringDescription: System.Attribute
    {
         private string _value;

         public StringDescription(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
