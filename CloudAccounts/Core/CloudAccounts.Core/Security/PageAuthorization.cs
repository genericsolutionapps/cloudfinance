﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//using System.Web.Mvc;

namespace CloudAccounts.Core.Security
{
    public class PageAuthorization
    {

        public static Boolean IsAuthenticate(HttpContextBase context, List<string> forms)
        {
            string FormName = ReturnFormName(context.Request.RawUrl);
            if (forms.Contains(FormName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static string ReturnFormName(string url)
        {
            string[] link = url.Split('/');
            return link[1];
        }
    }
}
