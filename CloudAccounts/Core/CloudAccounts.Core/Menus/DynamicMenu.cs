﻿using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace CloudAccounts.Core.Menus
{
    public class DynamicMenu
    {
        //private IApplicationMenuRepository applicationMenuRepository;
        //private IRolePrivilegesRepository rolePrivilegesRepository;
        private IUnitofWork unitofWork;

        public DynamicMenu()
        {
            this.unitofWork = new UnitOfWork(new DatabaseFactory());
            //this.applicationMenuRepository = new ApplicationMenuRepository(unitofWork.instance);
            //this.rolePrivilegesRepository = new RolePrivilegesRepository(unitofWork.instance);

        }

        public string GenerateSideBar(int role_ID)
        {
            //var allMenu = applicationMenuRepository.GetAll();

            DataTable dt_appMenu = new DataTable();
            dt_appMenu.Columns.Add("intID");
            dt_appMenu.Columns.Add("NodeName");
            dt_appMenu.Columns.Add("NodeUrl");
            dt_appMenu.Columns.Add("ParentNode");
            dt_appMenu.Columns.Add("Form_ID");
            dt_appMenu.Columns.Add("Seq");
            dt_appMenu.Columns.Add("Is_Active");
            dt_appMenu.Columns.Add("Created_By");
            dt_appMenu.Columns.Add("Created_ON");

            //foreach (var item in allMenu)
            //{
            //    dt_appMenu.Rows.Add(item.intID, item.NodeName, item.NodeUrl, item.ParentNode, item.Form_ID, item.Seq, item.Is_Active, item.Created_By, item.Created_On);
            //}

            //var roleDetail = (from details in unitofWork.db.Role_Privileges
            //                  where details.Role_ID == role_ID
            //                  select details).ToList();


            DataTable dt_roleDetail = new DataTable();
            dt_roleDetail.Columns.Add("Role_ID");
            dt_roleDetail.Columns.Add("Form_ID");
            dt_roleDetail.Columns.Add("Is_Insert");
            dt_roleDetail.Columns.Add("Is_Modify");
            dt_roleDetail.Columns.Add("Is_Delete");
            dt_roleDetail.Columns.Add("Is_View");

            //foreach (var item in roleDetail)
            //{

            //    dt_roleDetail.Rows.Add(item.Role_ID, item.Form_ID, item.Is_Insert, item.Is_Update, item.Is_Delete, item.Is_View);

            //}

            DataTable dt_AllowedForms = new DataTable();
            //dt_AllowedForms.Columns.Add("ID");
            dt_AllowedForms.Columns.Add("Role_ID");
            dt_AllowedForms.Columns.Add("Form_ID");
            dt_AllowedForms.Columns.Add("Is_Insert");
            dt_AllowedForms.Columns.Add("Is_Update");
            dt_AllowedForms.Columns.Add("Is_View");
            dt_AllowedForms.Columns.Add("Is_Delete");

            //for (int i = 0; i < dt_roleDetail.Rows.Count; i++)
            //{
            //    if (dt_roleDetail.Rows[i]["Is_Insert"].ToString() == "True" || dt_roleDetail.Rows[i]["Is_Modify"].ToString() == "True" || dt_roleDetail.Rows[i]["Is_View"].ToString() == "True" || dt_roleDetail.Rows[i]["Is_Delete"].ToString() == "True")
            //    {
            //        dt_AllowedForms.Rows.Add(dt_roleDetail.Rows[i]["Role_ID"], dt_roleDetail.Rows[i]["Form_ID"], dt_roleDetail.Rows[i]["Is_Insert"], dt_roleDetail.Rows[i]["Is_Modify"], dt_roleDetail.Rows[i]["Is_View"], dt_roleDetail.Rows[i]["Is_Delete"]);
            //    }
            //}

            string output_Menu1 = string.Empty;
            //DataTable dt_Menu = new DataTable();
            //dt_Menu = GenerateTable(dt_appMenu, dt_AllowedForms);
            //HtmlGenericControl Menu1 = new HtmlGenericControl("ul");
            //if (dt_Menu != null)
            //{

            //    Menu1 = GenerateHtml_SideBar(dt_Menu);
            //    StringBuilder generatedHtml = new StringBuilder();
            //    HtmlTextWriter htw = new HtmlTextWriter(new StringWriter(generatedHtml));
            //    Menu1.RenderControl(htw);
            //    output_Menu1 = generatedHtml.ToString();


            //}
            return output_Menu1;


        }

        private DataTable GenerateTable(DataTable dt_appMenu, DataTable dt_allowedForms)
        {
            int Parent_Node = 0;


            DataTable dt_Menu = new DataTable();
            dt_Menu.Columns.Add("MenuItemID");
            dt_Menu.Columns.Add("MenuItemName");
            dt_Menu.Columns.Add("MenuItemURL");
            dt_Menu.Columns.Add("MenuItemParentID");




            for (int i = 0; i < dt_allowedForms.Rows.Count; i++)
            {
                bool IsExist = false;
                int count = 0;
                foreach (DataRow dr in dt_appMenu.Rows)
                {
                    if (dt_appMenu.Rows[count]["Form_ID"].ToString() != "")
                    {
                        if (Convert.ToInt32(dt_allowedForms.Rows[i]["Form_ID"]) == Convert.ToInt32(dt_appMenu.Rows[count]["Form_ID"]))
                        {
                            IsExist = true;
                            int NodeID = Convert.ToInt32(dt_appMenu.Rows[count]["intID"]);
                            string NodeName = dt_appMenu.Rows[count]["NodeName"].ToString();
                            string NodeUrl = dt_appMenu.Rows[count]["NodeUrl"].ToString();
                            int ParentNode = Convert.ToInt32(dt_appMenu.Rows[count]["ParentNode"]);

                            dt_Menu.Rows.Add(NodeID, NodeName, NodeUrl, ParentNode);
                            break;
                        }

                    }
                    count++;
                }
                if (IsExist == true)
                {
                    int Parentcheck = 0;
                    for (int k = 0; k < dt_Menu.Rows.Count; k++)
                    {
                        if (Convert.ToInt32(dt_appMenu.Rows[count]["ParentNode"]) == Convert.ToInt32(dt_Menu.Rows[k]["MenuItemParentID"]))
                        {
                            Parentcheck = Parentcheck + 1;
                        }

                    }

                    if (Parentcheck < 2)
                    {
                        Parent_Node = Convert.ToInt32(dt_appMenu.Rows[count]["ParentNode"]);
                        int NodeID = Convert.ToInt32(dt_appMenu.Rows[count]["ParentNode"]);

                        for (int l = 0; l < dt_appMenu.Rows.Count; l++)
                        {
                            if (NodeID == Convert.ToInt32(dt_appMenu.Rows[l]["intID"]))
                            {
                                string NodeName = dt_appMenu.Rows[l]["NodeName"].ToString();
                                string NodeUrl = dt_appMenu.Rows[l]["NodeUrl"].ToString();
                                int ParentNode = Convert.ToInt32(dt_appMenu.Rows[l]["ParentNode"]);
                                dt_Menu.Rows.Add(NodeID, NodeName, NodeUrl, ParentNode);
                                break;
                            }
                        }

                    }

                }

            }
            DataTable dtk = new DataTable();

            return dt_Menu;


        }

        private HtmlGenericControl GenerateHtml_SideBar(DataTable dt_Menu)
        {
            DataRow[] dr1 = dt_Menu.Select("MenuItemParentID = 0");


            HtmlGenericControl Menu = new HtmlGenericControl("ul");
            Menu.Attributes.Add("class", "sidebar-menu");
            Menu.Attributes.Add("id", "nav-accordion");

            HtmlGenericControl lifirst = new HtmlGenericControl("li");
            lifirst.Attributes.Add("class", "active");


            HtmlGenericControl atagfirst = new HtmlGenericControl("a");
            atagfirst.Attributes.Add("href", "/Dashboard");




            HtmlGenericControl spanfirst = new HtmlGenericControl("span");
            spanfirst.InnerText = "Dashboard";
            atagfirst.Controls.Add(spanfirst);
            lifirst.Controls.Add(atagfirst);

            Menu.Controls.Add(lifirst);



            for (int i = 0; i < dr1.Count(); i++)
            {

                HtmlGenericControl li_1 = new HtmlGenericControl("li");
                li_1.Attributes.Add("class", "sub-menu");
               

                HtmlGenericControl atag = new HtmlGenericControl("a");
                atag.Attributes.Add("href", "#");




                HtmlGenericControl span_1 = new HtmlGenericControl("span");
                span_1.InnerText = dr1[i]["MenuItemName"].ToString();
                atag.Controls.Add(span_1);


                li_1.Controls.Add(atag);

                HtmlGenericControl ul = new HtmlGenericControl("ul");
                ul.Attributes.Add("class", "sub");

                int Count = 0;
                foreach (DataRow row in dt_Menu.Rows)
                {

                    if (Convert.ToInt32(Convert.ToInt32(dr1[i]["MenuItemID"])) == Convert.ToInt32(dt_Menu.Rows[Count]["MenuItemParentID"]))
                    {
                        HtmlGenericControl li1 = new HtmlGenericControl("li");
                        HtmlGenericControl a = new HtmlGenericControl("a");
                        a.Attributes.Add("href", row["MenuItemURL"].ToString());
                        a.InnerText = row["MenuItemName"].ToString();

                        li1.Controls.Add(a);
                        ul.Controls.Add(li1);
                    }
                    Count++;
                }
                li_1.Controls.Add(ul);
                Menu.Controls.Add(li_1);

            }

            return Menu;
        }
    }
}
