﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IProductService
    {
        ResponseObject Create(ProductViewModel model, int userId);
        List<ProductViewModel> GetAllProducts(int userId);
        ProductViewModel GetProductById(int prodcutId, int userId);
        bool Edit(ProductViewModel viewModel, int userId);
        bool Delete(int productId);
    }

    public class ProductService : IProductService
    {
        private ProductAdapter productAdapter;

        public ProductService()
        {
            productAdapter = new ProductAdapter();
        }

        public ResponseObject Create(ProductViewModel model, int userId)
        {
            try
            {
                return productAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductViewModel> GetAllProducts(int userId)
        {
            return productAdapter.GetAllProducts(userId);
        }
        public ProductViewModel GetProductById(int productId, int userId)
        {
            return productAdapter.GetProductById(productId, userId);
        }
        public bool Edit(ProductViewModel viewModel, int userId)
        {
            return productAdapter.Edit(viewModel, userId);
        }
        public bool Delete(int productId)
        {
            return productAdapter.Delete(productId);
        }


    }
}
