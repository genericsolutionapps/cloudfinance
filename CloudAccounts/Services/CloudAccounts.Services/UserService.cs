﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IUserService
    {
        List<UserViewModel> GetAllUsers(int userId);
        UserViewModel GetUserById(int userId);
        UserViewModel Create(UserViewModel model, int userId);
        bool Edit(UserViewModel userVM);
    }

    public class UserService : IUserService
    {
        private UserAdapter userAdapter;

        public UserService()
        {
            userAdapter = new UserAdapter();
        }

        public UserViewModel Create(UserViewModel model, int userId)
        {
            try
            {
                return userAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Edit(UserViewModel userVM)
        {
            try
            {
                return userAdapter.Edit(userVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public UserViewModel GetUserById(int userId)
        {
            return userAdapter.GetUserById(userId);
        }


        public List<UserViewModel> GetAllUsers(int userId)
        {
            return userAdapter.GetAllUsers(userId);
        }

    }
}
