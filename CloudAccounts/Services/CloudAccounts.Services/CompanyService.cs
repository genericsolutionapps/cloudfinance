﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface ICompanyService
    {
        CompanyViewModel GetCompanyByUserId(int userId);
        bool Edit(CompanyViewModel model);
    }


    public class CompanyService : ICompanyService
    {
        private CompanyAdapter companyAdapter;

        public CompanyService()
        {
            companyAdapter = new CompanyAdapter();
        }


        public CompanyViewModel GetCompanyByUserId(int userId)
        {
            try
            {
                return companyAdapter.GetCompanyByUserId(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Edit(CompanyViewModel model)
        {
            try
            {
                return companyAdapter.Edit(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

