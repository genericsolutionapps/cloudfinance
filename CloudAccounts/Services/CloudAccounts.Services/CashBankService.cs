﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface ICashBankService
    {
        ResponseObject Create(CashBankViewModel model, int userId);
        List<CashBankViewModel> GetAllAccounts(int userId);
        CashBankViewModel GetCashBankById(int cashBankId, int userId);
        bool Edit(CashBankViewModel viewModel, int userId);
        bool Delete(int cashBankId, int userId);
        ResponseObject AddGeneralVoucher(GLDetailViewModel viewModel, int userId);
    }

    public class CashBankService : ICashBankService
    {
        private CashBankAdapter cashBankAdapter;

        public CashBankService()
        {
            cashBankAdapter = new CashBankAdapter();
        }

        public ResponseObject Create(CashBankViewModel model, int userId)
        {
            try
            {
                return cashBankAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(CashBankViewModel viewModel, int userId)
        {
            return cashBankAdapter.Edit(viewModel, userId);
        }

        public bool Delete(int cashBankId, int userId)
        {
            return cashBankAdapter.Delete(cashBankId, userId);
        }


        public ResponseObject AddGeneralVoucher(GLDetailViewModel model, int userId)
        {
            try
            {
                return cashBankAdapter.AddGeneralVoucher(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CashBankViewModel> GetAllAccounts(int userId)
        {
            try
            {
                return cashBankAdapter.GetAllAccounts(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public CashBankViewModel GetCashBankById(int cashBankId, int userId)
        {
            try
            {
                return cashBankAdapter.GetCashBankById(cashBankId, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}








