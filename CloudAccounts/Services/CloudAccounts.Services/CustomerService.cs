﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface ICustomerService
    {
        List<CustomerViewModel> GetAllBusinessPartnersByType(int userId);
        ResponseObject Create(CustomerViewModel customerViewModel, int userId);
        List<AuthorizedPersonViewModel> GetAuthorizedPersonsByCustomerId(int customerId);
        bool Edit(CustomerViewModel viewModel);
        ResponseObject Delete(int customerId);

        //CompanyViewModel GetCompanyByUserId(int userId);
        //bool Edit(CompanyViewModel model);
    }


    public class CustomerService : ICustomerService
    {
        private CustomerAdapter customerAdapter;

        public CustomerService()
        {
            customerAdapter = new CustomerAdapter();
        }


        public List<CustomerViewModel> GetAllBusinessPartnersByType(int userId)
        {
            return customerAdapter.GetAllBusinessPartnersByType(userId);
        }

        public List<AuthorizedPersonViewModel> GetAuthorizedPersonsByCustomerId(int customerId)
        {
            return customerAdapter.GetAuthorizedPersonsByCustomerId(customerId);
        }

        public ResponseObject Create(CustomerViewModel model, int userId)
        {
            try
            {
                return customerAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(CustomerViewModel viewModel)
        {
            return customerAdapter.Edit(viewModel);
        }

        public ResponseObject Delete(int customerId)
        {
            return customerAdapter.Delete(customerId);
        }

    }
}
