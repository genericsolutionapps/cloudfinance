﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IEmployeeService
    {
        ResponseObject Create(EmployeeViewModel model, int userId);
        List<EmployeeViewModel> GetAllEmployees(int userId);
        EmployeeViewModel GetEmployeeById(int employeeId, int userId);
        ResponseObject AddAdvance(AllocationViewModel viewModel, int userId);
        bool Edit(EmployeeViewModel viewModel, int userId);
        bool Delete(int employeeId, int userId);
    }

    public class EmployeeService : IEmployeeService
    {
        private EmployeeAdapter employeeAdapter;

        public EmployeeService()
        {
            employeeAdapter = new EmployeeAdapter();
        }

        public ResponseObject Create(EmployeeViewModel model, int userId)
        {
            try
            {
                return employeeAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(EmployeeViewModel viewModel, int userId)
        {
            return employeeAdapter.Edit(viewModel, userId);
        }

        public bool Delete(int employeeId, int userId)
        {
            return employeeAdapter.Delete(employeeId, userId);
        }

        public List<EmployeeViewModel> GetAllEmployees(int userId)
        {
            try
            {
                return employeeAdapter.GetAllEmployees(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeViewModel GetEmployeeById(int employeeId, int userId)
        {
            try
            {
                return employeeAdapter.GetEmployeeById(employeeId, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseObject AddAdvance(AllocationViewModel viewModel, int userId)
        {
            try
            {
                return employeeAdapter.AddAdvance(viewModel, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}








