﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IInvoiceService
    {
        ResponseObject Create(InvoiceHeaderViewModel model, int userId);
        List<InvoiceHeaderViewModel> GetAllInvoices(int userId, DateRangeViewModel dateRangeViewModel);
        List<InvoiceHeaderViewModel> GetAllInvoicesByCustomerId(int customerId);
        InvoiceHeaderViewModel GetInvoiceById(int invoiceHeaderId);
        List<InvoiceHeaderViewModel> GetInvoicesByProductId(int productId, int userId);
        List<InvoiceDetailViewModel> GetAllInvoiceDetailsByInvoiceHeaderId(int invoiceHeaderId);
        List<AllocationViewModel> GetAllAllocationsByInvoiceHeaderId(int invoiceHeaderId);
        List<InvoiceDetailViewModel> GetAllInvoiceDetails(int userId);
        List<BalanceByMonthViewModel> GetBalanceByMonth(int userId);
        List<List<InvoiceHeaderViewModel>> GetAllDelayedInvoices(int userId);
        ResponseObject AddAllocation(AllocationViewModel viewModel, int userId);
        ResponseObject Delete(int invoiceHeaderId);
        bool Edit(InvoiceHeaderViewModel viewModel, int userId);

        //VAT Reprot
        List<List<InvoiceHeaderViewModel>> GetVATByMonth(int userId, int currentMonth, int year);

        //Cash Bank Report
        List<AllocationViewModel> GetAllAllocations(int userId, DateRangeViewModel dateRangeViewModel);
    }

    public class InvoiceService : IInvoiceService
    {
        private InvoiceAdapter invoiceAdapter;

        public InvoiceService()
        {
            invoiceAdapter = new InvoiceAdapter();
        }

        public List<InvoiceHeaderViewModel> GetAllInvoices(int userId, DateRangeViewModel dateRangeViewModel)
        {
            return invoiceAdapter.GetAllInvoices(userId, dateRangeViewModel);
        }

        public ResponseObject Create(InvoiceHeaderViewModel model, int userId)
        {
            try
            {
                return invoiceAdapter.Create(model, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InvoiceHeaderViewModel GetInvoiceById(int invoiceHeaderId)
        {
            try
            {
                return invoiceAdapter.GetInvoiceById(invoiceHeaderId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceDetailViewModel> GetAllInvoiceDetailsByInvoiceHeaderId(int invoiceHeaderId)
        {
            return invoiceAdapter.GetAllInvoiceDetailsByInvoiceHeaderId(invoiceHeaderId);
        }

        public List<InvoiceDetailViewModel> GetAllInvoiceDetails(int userId)
        {
            return invoiceAdapter.GetAllInvoiceDetails(userId);
        }

        public List<InvoiceHeaderViewModel> GetAllInvoicesByCustomerId(int customerId)
        {
            return invoiceAdapter.GetAllInvoicesByCustomerId(customerId);
        }

        public List<InvoiceHeaderViewModel> GetInvoicesByProductId(int productId, int userId)
        {
            return invoiceAdapter.GetInvoicesByProductId(productId, userId);
        }

        public List<AllocationViewModel> GetAllAllocationsByInvoiceHeaderId(int invoiceHeaderId)
        {
            return invoiceAdapter.GetAllAllocationsByInvoiceHeaderId(invoiceHeaderId);
        }

        public List<BalanceByMonthViewModel> GetBalanceByMonth(int userId)
        {
            return invoiceAdapter.GetBalanceByMonth(userId);
        }

        public ResponseObject AddAllocation(AllocationViewModel viewModel, int userId)
        {
            return invoiceAdapter.AddAllocation(viewModel, userId);
        }

        public ResponseObject Delete(int invoiceHeaderId)
        {
            return invoiceAdapter.Delete(invoiceHeaderId);
        }

        public bool Edit(InvoiceHeaderViewModel viewModel, int userId)
        {
            return invoiceAdapter.Edit(viewModel, userId);
        }

        public List<List<InvoiceHeaderViewModel>> GetAllDelayedInvoices(int userId)
        {
            return invoiceAdapter.GetAllDelayedInvoices(userId);
        }

        //VAT Report
        public List<List<InvoiceHeaderViewModel>> GetVATByMonth(int userId, int currentMonth, int year)
        {
            return invoiceAdapter.GetVATByMonth(userId, currentMonth, year);
        }

        //Cash Bank Report
        public List<AllocationViewModel> GetAllAllocations(int userId, DateRangeViewModel dateRangeViewModel)
        {
            return invoiceAdapter.GetAllAllocations(userId, dateRangeViewModel);
        }



    }
}
