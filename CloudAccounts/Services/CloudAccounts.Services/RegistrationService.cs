﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IRegistrationService
    {
        UserInfo Create(RegistrationViewModel model);
        bool ActivateAccount(string token);
        bool CheckEmailVerification(int userId);
    }

    public class RegistrationService : IRegistrationService
    {
        private RegistrationAdapter registrationAdapter;

        public RegistrationService()
        {
            registrationAdapter = new RegistrationAdapter();
        }



        public UserInfo Create(RegistrationViewModel model)
        {
            try
            {
                UserInfo info = registrationAdapter.Create(model);
                return info;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ActivateAccount(string token)
        {
            try
            {

                return registrationAdapter.ActivateAccount(token);

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool CheckEmailVerification(int userId)
        {
            try
            {

                return registrationAdapter.CheckEmailVerification(userId);

            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
