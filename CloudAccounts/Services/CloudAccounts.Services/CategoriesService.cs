﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface ICategoriesService
    {
        CategoryViewModel Create(CategoryViewModel categoryViewModel);
        List<CategoryViewModel> GetAllCategories(int userId);
        List<CategoryViewModel> GetAllSalesCategories(DateRangeViewModel viewModel);
        List<CategoryViewModel> GetAllCustomerCategories(DateRangeViewModel viewModel);
        List<CategoryViewModel> GetCategoriesByType(DateRangeViewModel viewModel, string CategoryType);
        bool Edit(CategoryViewModel viewModel);
        bool Delete(int categoryId);
    }

    public class CategoriesService : ICategoriesService
    {
        private CategoriesAdapter categoriesAdapter;

        public CategoriesService()
        {
            categoriesAdapter = new CategoriesAdapter();
        }


        public List<CategoryViewModel> GetAllCategories(int userId)
        {
            return categoriesAdapter.GetAllCategories(userId);
        }

        public List<CategoryViewModel> GetAllSalesCategories(DateRangeViewModel viewModel)
        {
            return categoriesAdapter.GetAllSalesCategories(viewModel);
        }

        public List<CategoryViewModel> GetAllCustomerCategories(DateRangeViewModel viewModel)
        {
            return categoriesAdapter.GetAllCustomerCategories(viewModel);
        }

        public List<CategoryViewModel> GetCategoriesByType(DateRangeViewModel viewModel, string categoryType)
        {
            return categoriesAdapter.GetCategoriesByType(viewModel, categoryType);
        }

        public bool Delete(int categoryId)
        {
            return categoriesAdapter.Delete(categoryId);
        }

        public bool Edit(CategoryViewModel viewModel)
        {
            return categoriesAdapter.Edit(viewModel);
        }



        public CategoryViewModel Create(CategoryViewModel categoryViewModel)
        {
            try
            {
                CategoryViewModel model = categoriesAdapter.Create(categoryViewModel);
                return categoryViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
