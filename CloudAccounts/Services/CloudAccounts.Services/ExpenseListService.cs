﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;

namespace CloudAccounts.Services
{
    public interface IExpenseListService
    {
        ExpenseListViewModel Create(ExpenseListViewModel expenseList);
    }

    public class ExpenseListService : IExpenseListService
    {
        private ExpenseListAdapter expenseListAdapter;

        public ExpenseListService()
        {
            expenseListAdapter = new ExpenseListAdapter();
        }

        public ExpenseListViewModel Create(ExpenseListViewModel expenseList)
        {
            try
            {
                ExpenseListViewModel model = expenseListAdapter.Create(expenseList);
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
