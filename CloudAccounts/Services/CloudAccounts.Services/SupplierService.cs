﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.SharedModel;
using CloudAccounts.Adapter;


namespace CloudAccounts.Services
{
    public interface ISupplierService
    {
        int Create(SupplierViewModel model);
        SupplierViewModel GetSupplierByID(int supplierID);
        List<CurrencyViewModel> GetAllCurrencies();
        List<SupplierViewModel> GetAllSupplierLists();
        //void SaveNotes(supplierHistoryViewModel model);
        //List<SupplierViewModel> GetAllSupplier();
        //List<CurrencyViewModel> GetAllCurrencies();
    }

    public class SupplierService : ISupplierService
    {
        public SupplierViewModel supplierViewModel { get; set; }
        private SupplierAdapter SupplierAdapter { get; set; }

        public SupplierService()
        {
            SupplierAdapter = new SupplierAdapter();
        }

        public int Create(SupplierViewModel model)
        {
            return SupplierAdapter.Create(model);
            //return supplierViewModel;
        }

        public SupplierViewModel GetSupplierByID(int supplierID)
        {
            return SupplierAdapter.GetSupplierByID(supplierID);
        }

        public List<SupplierViewModel> GetAllSupplierLists()
        {
            return SupplierAdapter.GetAllSupplierLists();
        }

        //public void SaveNotes(supplierHistoryViewModel model)
        //{
        //    SupplierAdapter.SaveNotes(model);
        //}

        public List<CurrencyViewModel> GetAllCurrencies()
        {
            return SupplierAdapter.GetAllCurrencies();
        }

    }
}
