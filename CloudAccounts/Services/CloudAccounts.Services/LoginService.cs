﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface ILoginService
    {
        UserInfo LoginSubmit(LoginViewModel model);
        bool ForgotPassword(LoginViewModel model);

    }


    public class LoginService : ILoginService
    {
        private LoginAdapter loginAdapter;

        public LoginService()
        {
            loginAdapter = new LoginAdapter();
        }



        public UserInfo LoginSubmit(LoginViewModel model)
        {
            try
            {
                UserInfo info = loginAdapter.LoginSubmit(model);
                return info;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool ForgotPassword(LoginViewModel model)
        {
            try
            {
                var result = loginAdapter.ForgotPassword(model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
