﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class UserAdapter
    {
        private IUserRepository userRepository;
        private IUnitofWork unitofWork;

        public UserAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            userRepository = new UserRepository(unitofWork.instance);
        }

        public List<UserViewModel> GetAllUsers(int userId)
        {
            var userList = userRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<UserViewModel> userViewModelList = new List<UserViewModel>();

            foreach (var user in userList)
            {
                UserViewModel userViewModel = new UserViewModel()
                {
                    //UserId = user.UserId,
                    Name = user.Name,
                    Email = user.Email,
                    UserStatusTypeCode = user.UserStatusTypeCode,
                    CashBankTransaction = user.CashBankTransaction,
                    SalesTransaction = user.SalesTransaction,
                    ExpenseTransaction = user.ExpenseTransaction,
                    EmployeesTransaction = user.EmployeesTransaction
                };

                userViewModelList.Add(userViewModel);
            }

            if (userViewModelList != null)
            {
                return userViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public UserViewModel Create(UserViewModel model, int userId)
        {
            User user = userRepository.Get(x => x.Email == model.Email);

            //UserViewModel addedUser = new UserViewModel();
            if (user == null)
            {
                //create new user

                User userModel = new User();
                userModel.Email = model.Email;
                userModel.Password = model.Password;
                userModel.Name = model.Name;
                userModel.CompanyId = userRepository.GetById(userId).CompanyId;
                userModel.UserStatusTypeCode = model.UserStatusTypeCode;
                userModel.CashBankTransaction = model.CashBankTransaction;
                userModel.EmployeesTransaction = model.EmployeesTransaction;
                userModel.ExpenseTransaction = model.ExpenseTransaction;
                userModel.SalesTransaction = model.SalesTransaction;

                userRepository.Add(userModel);

                unitofWork.Commit();

                MailMessage message = new MailMessage();
                message.From = new MailAddress("saad.hasnain@gsolutions.com.pk");

                message.To.Add(new MailAddress(userModel.Email));

                message.Subject = "Cloud Accounts | User Invite";
                message.Body = "Login using the url and authentication below. <br /> cloudaccounts.gsolutions.com.pk <br /> Username : " + userModel.Email + " Password : " + userModel.Password;

                SmtpClient client = new SmtpClient();
                client.Send(message);

                return model;

            }
            else
            {
                throw new CustomException("A user with the same name already exist, please choose a new user name.");
            }
        }



        public UserViewModel GetUserById(int userId)
        {
            var user = userRepository.GetById(userId);

            if(user != null)
            {
                UserViewModel userVM = new UserViewModel()
                {
                    //UserId = user.UserId,
                    UserStatusTypeCode = user.UserStatusTypeCode,
                    Name = user.Name,
                    Email = user.Email
                };
                return userVM;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }

        }


        public bool Edit(UserViewModel viewModel)
        {
            try
            {
                User model = userRepository.Get(x => x.Email == viewModel.Email);

                model.Name = viewModel.Name;
                model.Password = model.Password;
                model.Phone = model.Phone;
                model.SalesTransaction = viewModel.SalesTransaction;
                model.ExpenseTransaction = viewModel.ExpenseTransaction;
                model.EmployeesTransaction = viewModel.EmployeesTransaction;
                model.CashBankTransaction = viewModel.CashBankTransaction;
                model.CompanyId = model.CompanyId;
                model.Email = viewModel.Email;
                model.UserStatusTypeCode = viewModel.UserStatusTypeCode;


                userRepository.Update(model);

                unitofWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



    }
}



