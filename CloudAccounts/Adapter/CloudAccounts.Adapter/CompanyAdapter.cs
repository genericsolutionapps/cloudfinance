﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class CompanyAdapter
    {
        private ICompanyRepository companyRepository;
        private IUserRepository userRepository;
        private IUnitofWork unitofWork;

        public CompanyAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            companyRepository = new CompanyRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
        }


        public CompanyViewModel GetCompanyByUserId(int userId)
        {
            if (userId > 0)
            {
                Company company = companyRepository.GetById(userRepository.GetById(userId).CompanyId);

                CompanyViewModel companyViewModel = new CompanyViewModel()
                {
                    CompanyId = company.CompanyId,
                    CompanyName = company.Name,
                    CommercialTitle = company.CommercialTitle,
                    Email = company.Email,
                    District = company.District,
                    Fax = company.Fax,
                    Wire = company.Wire,
                    Province = company.Province,
                    VD = company.VD,
                    Phone = company.Phone,
                    //if else to set sector according to the companytypecode
                    //Sector = (company.CompanyTypeCode == "0001") ? "Software"
                    //            : (company.CompanyTypeCode == "0002") ? "Training"
                    //            : (company.CompanyTypeCode == "0003") ? "Production"
                    //            : (company.CompanyTypeCode == "0004") ? "Public Relations"
                    //            : (company.CompanyTypeCode == "0005") ? "Construction"
                    //            : "Retail",

                    Sector = company.CompanyTypeCode,

                    Address = company.OpenAddress,
                    TaxInformation = company.VNo
                };

                return companyViewModel;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }


        }


        public bool Edit(CompanyViewModel viewModel)
        {
            try
            {
                Company model = companyRepository.GetById(viewModel.CompanyId);

                model.CommercialTitle = viewModel.CommercialTitle;
                model.CompanyTypeCode = viewModel.Sector;
                model.Email = viewModel.Email;
                model.Name = viewModel.CompanyName;
                model.OpenAddress = viewModel.Address;
                model.Phone = model.Phone;
                model.VNo = viewModel.TaxInformation;
                model.VD = viewModel.VD;
                model.District = viewModel.District;
                model.Province = viewModel.Province;
                model.Wire = viewModel.Wire;
                model.Fax = viewModel.Fax;

                companyRepository.Update(model);

                unitofWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}



















