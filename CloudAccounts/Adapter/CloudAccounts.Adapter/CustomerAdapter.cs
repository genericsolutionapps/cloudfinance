﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class CustomerAdapter
    {
        private IUserRepository userRepository;
        private ICustomerRepository customerRepository;
        private IAuthorizedPersonRepository authorizedPersonRepository;
        private IInvoiceHeaderRepository invoiceHeaderRepository;
        private IAllocationRepository allocationRepository;
        private ICategoriesRepository categoriesRepository;
        private IAccountRepository accountRepository;
        private IGLHeaderRepository glHeaderRepository;
        private IGLDetailRepository glDetailRepository;


        private IUnitofWork unitofWork;

        public CustomerAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            customerRepository = new CustomerRepository(unitofWork.instance);
            authorizedPersonRepository = new AuthorizedPersonRepository(unitofWork.instance);
            invoiceHeaderRepository = new InvoiceHeaderRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
            allocationRepository = new AllocationRepository(unitofWork.instance);
            categoriesRepository = new CategoriesRepository(unitofWork.instance);
            accountRepository = new AccountRepository(unitofWork.instance);
            glHeaderRepository = new GLHeaderRepository(unitofWork.instance);
            glDetailRepository = new GLDetailRepository(unitofWork.instance);
        }

        public List<CustomerViewModel> GetAllBusinessPartnersByType(int userId)
        {
            var businessPartnerList = customerRepository
                .GetAll()
                .Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<CustomerViewModel> customerViewModelList = new List<CustomerViewModel>();

            foreach (var customer in businessPartnerList)
            {
                CustomerViewModel customerViewModel = new CustomerViewModel
                {
                    CustomerId = customer.CustomerId,
                    CompanyId = customer.CompanyId,
                    CompanyTitle = customer.CompanyTitle,
                    ShortName = customer.ShortName,
                    Email = customer.Email,
                    PhoneNumber = customer.PhoneNumber,
                    FaxNumber = customer.FaxNumber,
                    IBANNumber = customer.IBANNumber,
                    District = customer.District,
                    Province = customer.Province,
                    CustomerType = customer.CustomerType,
                    VKN = customer.VKN,
                    Tax = customer.Tax,
                    OpeningBalanceDate = customer.OpeningBalanceDate,
                    Address = customer.Address,
                    OpeningBalanceAmount = customer.OpeningBalanceAmount,
                    CategoryId = customer.CategoryId,
                    BusinessPartnerType = customer.BusinessPartnerType,
                    TotalInvoicedAmount = invoiceHeaderRepository
                        .GetAll()
                        .Where(x => x.CustomerId == customer.CustomerId && x.VoucherType == "0001")
                        .Sum(x => x.TotalAmount * x.CurrencyRate),
                    TotalPayable = invoiceHeaderRepository
                        .GetAll()
                        .Where(x => x.CustomerId == customer.CustomerId && x.VoucherType == "0002")
                        .Sum(x => x.TotalAmount * x.CurrencyRate)
                };
                customerViewModel.TotalRemainingAmount = customerViewModel.TotalInvoicedAmount - allocationRepository
                    .GetAll()
                    .Where(x => x.InvoiceHeader.CustomerId == customer.CustomerId && x.InvoiceHeader.Customer.BusinessPartnerType == "0001")
                    .Sum(x => x.Amount);

                customerViewModel.RemainingPayable = customerViewModel.TotalPayable - allocationRepository
                    .GetAll()
                    .Where(x => x.InvoiceHeader.CustomerId == customer.CustomerId && x.InvoiceHeader.Customer.BusinessPartnerType == "0002")
                    .Sum(x => x.Amount);

                //var allInvoices = invoiceHeaderRepository.GetAll().Where(x => x.CustomerId == customer.CustomerId);

                //foreach (var invoice in allInvoices)
                //{
                //    var allocatedAmount = allocationRepository.GetAll().Where(x => x.InvoiceHeaderId == invoice.InvoiceHeaderId).Sum(x => x.Amount);

                //    customerViewModel.TotalInvoicedAmount = customerViewModel.TotalInvoicedAmount + invoice.TotalAmount;

                //    customerViewModel.TotalRemainingAmount = customerViewModel.TotalRemainingAmount + invoice.TotalAmount - allocatedAmount;
                //}
                customerViewModelList.Add(customerViewModel);
            }

            if (customerViewModelList != null)
            {
                return customerViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public List<AuthorizedPersonViewModel> GetAuthorizedPersonsByCustomerId(int customerId)
        {
            var authorizedPersonList = authorizedPersonRepository.GetAll().Where(x => x.CustomerId == customerId);

            List<AuthorizedPersonViewModel> authorizedPersonsViewModelList = new List<AuthorizedPersonViewModel>();

            foreach (var authorizedPerson in authorizedPersonList)
            {
                AuthorizedPersonViewModel authorizedPersonViewModel = new AuthorizedPersonViewModel
                {
                    APId = authorizedPerson.APId,
                    Name = authorizedPerson.Name,
                    Email = authorizedPerson.Email,
                    Telephone = authorizedPerson.Telephone,
                    Notes = authorizedPerson.Notes,
                    CompanyId = authorizedPerson.CompanyId,
                    CustomerId = authorizedPerson.CustomerId,
                };

                authorizedPersonsViewModelList.Add(authorizedPersonViewModel);
            }

            if (authorizedPersonsViewModelList != null)
            {
                return authorizedPersonsViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public bool AddGLEntries(CustomerViewModel model, int companyId)
        {
            GLHeader glHeader = new GLHeader
            {
                TransactionType = "Opening Balance",
                TransactionNumber = null,
                Description = "Opening Balance of" + model.CompanyTitle,
                CompanyId = companyId,
                TransactionDate = model.OpeningBalanceDate ?? DateTime.Now
            };

            glHeaderRepository.Add(glHeader);
            unitofWork.Commit();

            try
            {
                GLDetail glDetailDebit = new GLDetail
                {
                    GLHeaderId = glHeader.GLHeaderId,
                    AccountCode = (model.BusinessPartnerType == "0001") ? accountRepository.GetAll().LastOrDefault().AccountCode :
                        accountRepository.Get(x => x.AccountName == "Opening Balance").AccountCode,
                    Debit = model.OpeningBalanceAmount ?? 0,
                    Credit = 0
                };

                glDetailRepository.Add(glDetailDebit);

                GLDetail glDetailCredit = new GLDetail
                {
                    GLHeaderId = glHeader.GLHeaderId,
                    AccountCode = (model.BusinessPartnerType == "0001") ? accountRepository.Get(x => x.AccountName == "Opening Balance").AccountCode :
                        accountRepository.GetAll().LastOrDefault().AccountCode,
                    Debit = 0,
                    Credit = model.OpeningBalanceAmount ?? 0,
                };

                glDetailRepository.Add(glDetailCredit);

                unitofWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                glHeaderRepository.Delete(glHeader);
                return false;
            }
        }


        public ResponseObject Create(CustomerViewModel model, int userId)
        {
            if (model != null && model.CompanyTitle != "")
            {
                ResponseObject response = new ResponseObject();

                int compId = userRepository.GetById(userId).CompanyId;

                Category categoryModel = new Category();

                bool categoryInsertCheck = false;

                if (model.CategoryId == null)
                {
                    var duplicateCategory = categoriesRepository.GetMany(x => x.CategoryName == categoryModel.CategoryName).FirstOrDefault();

                    if (duplicateCategory == null)
                    {
                        categoryModel.CategoryName = model.CategoryName;
                        categoryModel.CategoryType = "0006";
                        categoryModel.CategoryColor = "#ffffff";
                        categoryModel.CompanyId = compId;
                        categoriesRepository.Add(categoryModel);
                        unitofWork.Commit();
                        categoryInsertCheck = true;

                        categoryModel = categoriesRepository.GetMany(x => x.CategoryName == categoryModel.CategoryName).FirstOrDefault();

                        model.CategoryId = categoryModel.CategoryId;

                    }
                    else
                    {
                        model.CategoryId = duplicateCategory.CategoryId;
                    }

                }



                Account account = new Account
                {
                    AccountName = model.CompanyTitle,
                    AccountCode = ((accountRepository.GetAll().LastOrDefault() == null) ? "0001" :
                        (Convert.ToInt32(accountRepository.GetAll().LastOrDefault().AccountCode) + 1).ToString().PadLeft(4, '0')),
                    AccountType = (model.BusinessPartnerType == "0001") ? "Account Receivable" : "Account Payable",
                    CompanyId = compId,
                    Nature = (model.BusinessPartnerType == "0001") ? "ASSET" : "PAYABLE"
                };
                accountRepository.Add(account);
                unitofWork.Commit();



                try
                {
                    if (model.OpeningBalanceAmount > 0 && model.OpeningBalanceAmount != null)
                    {
                        AddGLEntries(model, compId);
                    }
                    Customer customer = new Customer();
                    customer.CompanyId = compId;
                    customer.CompanyTitle = model.CompanyTitle;
                    customer.ShortName = model.ShortName;
                    customer.Email = model.Email;
                    customer.PhoneNumber = model.PhoneNumber;
                    customer.FaxNumber = model.FaxNumber;
                    customer.IBANNumber = model.IBANNumber;
                    customer.District = model.District;
                    customer.Province = model.Province;
                    customer.CustomerType = model.CustomerType;
                    customer.VKN = model.VKN;
                    customer.Tax = model.Tax;
                    customer.OpeningBalanceDate = model.OpeningBalanceDate;
                    customer.OpeningBalanceAmount = model.OpeningBalanceAmount;
                    customer.CategoryId = model.CategoryId != null ? model.CategoryId : categoriesRepository.Get(x => x.CategoryName == "un-customer").CategoryId;
                    customer.Address = model.Address;
                    customer.AccountCode = account.AccountCode;
                    customer.BusinessPartnerType = model.BusinessPartnerType;

                    customerRepository.Add(customer);

                    unitofWork.Commit();

                    if (model.AuthorizedPersonsCollection != null)
                    {
                        foreach (var authorizedPersonDetail in model.AuthorizedPersonsCollection)
                        {
                            AuthorizedPerson authorizedPerson = new AuthorizedPerson();
                            authorizedPerson.Name = authorizedPersonDetail.Name;
                            authorizedPerson.Email = authorizedPersonDetail.Email;
                            authorizedPerson.Telephone = authorizedPersonDetail.Telephone;
                            authorizedPerson.Notes = authorizedPersonDetail.Notes;
                            authorizedPerson.CompanyId = userRepository.GetById(userId).CompanyId;
                            authorizedPerson.CustomerId = customer.CustomerId;

                            authorizedPersonRepository.Add(authorizedPerson);
                        }
                    }

                    unitofWork.Commit();
                    response.IsSuccess = true;
                    response.ResultData = customer.CustomerId;
                    return response;
                }
                catch (Exception e)
                {
                    accountRepository.Delete(account);

                    if(categoryInsertCheck == true)
                    {
                        categoriesRepository.Delete(categoryModel);
                    }

                    response.IsSuccess = false;
                    return response;
                }
            }

            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public bool Edit(CustomerViewModel viewModel)
        {
            try
            {
                Customer customer = customerRepository.GetById(viewModel.CustomerId);

                customer.CustomerId = viewModel.CustomerId;
                customer.CompanyTitle = viewModel.CompanyTitle;
                customer.ShortName = viewModel.ShortName;
                customer.Email = viewModel.Email;
                customer.PhoneNumber = viewModel.PhoneNumber;
                customer.FaxNumber = viewModel.FaxNumber;
                customer.IBANNumber = viewModel.IBANNumber;
                customer.District = viewModel.District;
                customer.Province = viewModel.Province;
                customer.CustomerType = viewModel.CustomerType;
                customer.VKN = viewModel.VKN;
                customer.Tax = viewModel.Tax;
                customer.OpeningBalanceDate = viewModel.OpeningBalanceDate;
                customer.OpeningBalanceAmount = viewModel.OpeningBalanceAmount;
                customer.CategoryId = viewModel.CategoryId;
                customer.Address = viewModel.Address;

                customerRepository.Update(customer);

                unitofWork.Commit();

                foreach (var authorizedPersonDetail in viewModel.AuthorizedPersonsCollection)
                {
                    AuthorizedPerson authorizedPerson = authorizedPersonRepository.GetById(authorizedPersonDetail.APId);
                    authorizedPerson.Name = authorizedPersonDetail.Name;
                    authorizedPerson.Email = authorizedPersonDetail.Email;
                    authorizedPerson.Telephone = authorizedPersonDetail.Telephone;
                    authorizedPerson.Notes = authorizedPersonDetail.Notes;
                    authorizedPerson.CompanyId = authorizedPersonDetail.CompanyId;
                    authorizedPerson.CustomerId = customer.CustomerId;

                    authorizedPersonRepository.Update(authorizedPerson);
                }
                unitofWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ResponseObject Delete(int customerId)
        {
            ResponseObject response = new ResponseObject();

            var allInvoicesForThisCustomer = invoiceHeaderRepository.GetAll().Where(x => x.CustomerId == customerId);

            if (allInvoicesForThisCustomer.Count() > 0)
            {
                response.IsSuccess = false;
                response.ErrorMessage = "Cannot delete! There are invoices against this customer";
                return response;
            }

            var authorizedPersons = authorizedPersonRepository.GetAll().Where(x => x.CustomerId == customerId);

            foreach (var authorizedPerson in authorizedPersons)
            {
                authorizedPersonRepository.Delete(authorizedPerson);
            }

            customerRepository.Delete(x => x.CustomerId == customerId);

            try
            {
                unitofWork.Commit();
                response.IsSuccess = true;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}





