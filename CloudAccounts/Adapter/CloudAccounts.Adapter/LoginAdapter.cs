﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class LoginAdapter
    {
        private IUserRepository userRepository;
        private IUnitofWork unitofWork;

        public LoginAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            userRepository = new UserRepository(unitofWork.instance);
        }

        public UserInfo LoginSubmit(LoginViewModel model)
        {
            User user = userRepository.Get(x => x.Email == model.User_Name);

            UserInfo info = new UserInfo();

            if (user != null)
            {
                if (user.Password == model.Password)
                {
                    unitofWork.Commit();

                    info.UserId = user.UserId;
                    info.Email = user.Email;
                }
                else
                {
                    throw new CustomException("Login username and password does not match. Please try again.");
                }
            }
            else
            {
                throw new CustomException("Login username does not exist. Please try entering correct one.");
            }
            return info;
        }

        public bool ForgotPassword(LoginViewModel model)
        {
            User user = userRepository.Get(x => x.Email == model.User_Name);

            UserInfo info = new UserInfo();

            if (user != null)
            {
                try
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("saad.hasnain@gsolutions.com.pk");

                    message.To.Add(new MailAddress(user.Email));

                    message.Subject = "Cloud Accounts | Password Recovery";
                    message.Body = "You have requested for password recovery, your current password is " + user.Password;

                    SmtpClient client = new SmtpClient();
                    client.Send(message);

                    return true;
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
            }
            else
            {
                throw new CustomException("Email does not exist. Please try entering correct one.");
            }
        }
    }
}
