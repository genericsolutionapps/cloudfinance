﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class InvoiceAdapter
    {
        private IUserRepository userRepository;
        private IInvoiceHeaderRepository invoiceHeaderRepository;
        private IInvoiceDetailRepository invoiceDetailRepository;
        private IAllocationRepository allocationRepository;
        private ICategoriesRepository categoriesRepository;
        private IGLHeaderRepository glHeaderRepository;
        private IGLDetailRepository glDetailRepository;
        private ICustomerRepository customerRepository;
        private IProductRepository productRepository;
        private IAccountRepository accountRepository;
        private IEmployeeRepository employeeRepository;
        private ICashBankRepository cashBankRepository;

        private IUnitofWork unitofWork;

        public InvoiceAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            userRepository = new UserRepository(unitofWork.instance);
            invoiceHeaderRepository = new InvoiceHeaderRepository(unitofWork.instance);
            invoiceDetailRepository = new InvoiceDetailRepository(unitofWork.instance);
            allocationRepository = new AllocationRepository(unitofWork.instance);
            categoriesRepository = new CategoriesRepository(unitofWork.instance);
            glHeaderRepository = new GLHeaderRepository(unitofWork.instance);
            glDetailRepository = new GLDetailRepository(unitofWork.instance);
            productRepository = new ProductRepository(unitofWork.instance);
            customerRepository = new CustomerRepository(unitofWork.instance);
            accountRepository = new AccountRepository(unitofWork.instance);
            employeeRepository = new EmployeeRepository(unitofWork.instance);
            cashBankRepository = new CashBankRepository(unitofWork.instance);
        }


        public bool AddGlDetailEntriesSalary(InvoiceHeaderViewModel viewModel, int gLHeaderId)
        {
            GLDetail glDetailDebit = new GLDetail
            {
                GLHeaderId = gLHeaderId,
                AccountCode = employeeRepository.GetById(viewModel.CustomerId ?? 0).AccountCode,
                Debit = viewModel.TotalAmount,
                Credit = 0,
                Explanation = viewModel.Description != null ? "Debit entry for " + viewModel.Description : null
            };

            glDetailRepository.Add(glDetailDebit);

            GLDetail glDetailCredit = new GLDetail
            {
                GLHeaderId = gLHeaderId,
                AccountCode = cashBankRepository.Get(x => x.AccountName == "Bank").AccountCode,
                Debit = 0,
                Credit = viewModel.TotalAmount,
                Explanation = viewModel.Description != null ? "Credit entry for " + viewModel.Description : null
            };
            glDetailRepository.Add(glDetailCredit);

            unitofWork.Commit();

            return true;
        }


        public bool AddGlDetailEntriesExpense(InvoiceHeaderViewModel model, int gLHeaderId)
        {
            foreach (var expenseDetail in model.InvoiceDetailCollection)
            {
                GLDetail glDetailDebit = new GLDetail
                {
                    GLHeaderId = gLHeaderId,
                    AccountCode = productRepository.GetById(expenseDetail.ProductId).AccountCode,
                    Debit = expenseDetail.Amount,
                    Credit = 0,
                    Explanation = model.Description != null ? "Debit entry for " + model.Description : null
                };
                glDetailRepository.Add(glDetailDebit);
            }

            GLDetail glDetailCredit = new GLDetail
            {
                GLHeaderId = gLHeaderId,
                AccountCode = customerRepository.GetById((long)model.CustomerId).AccountCode,
                Debit = 0,
                Credit = model.TotalAmount,
                Explanation = model.Description != null ? "Credit entry for " + model.Description : null
            };
            glDetailRepository.Add(glDetailCredit);

            unitofWork.Commit();

            return true;
        }


        public bool AddGlDetailEntriesInvoice(InvoiceHeaderViewModel model, int gLHeaderId)
        {
            GLDetail glDetailDebit = new GLDetail
            {
                GLHeaderId = gLHeaderId,
                AccountCode = customerRepository.GetById((long)model.CustomerId).AccountCode,
                Debit = model.TotalAmount,
                Credit = 0,
                Explanation = model.Description != null ? "Debit entry for " + model.Description : null
            };

            glDetailRepository.Add(glDetailDebit);

            foreach (var invoiceDetail in model.InvoiceDetailCollection)
            {
                GLDetail glDetailCredit = new GLDetail
                {
                    GLHeaderId = gLHeaderId,
                    AccountCode = productRepository.GetById(invoiceDetail.ProductId).AccountCode,
                    Debit = 0,
                    Credit = invoiceDetail.Amount,
                    Explanation = model.Description != null ? "Credit entry for " + model.Description : null
                };
                glDetailRepository.Add(glDetailCredit);
            }
            unitofWork.Commit();

            return true;
        }



        public bool AddGlEntries(InvoiceHeaderViewModel viewModel, int companyId)
        {
            GLHeader glHeader = new GLHeader
            {
                TransactionType = viewModel.VoucherType == "0001" ? "Invoice" : "Bill",
                TransactionNumber = viewModel.InvoiceNo,
                Description = viewModel.Description,
                CompanyId = companyId,
                TransactionDate = viewModel.Date
            };
            glHeaderRepository.Add(glHeader);
            unitofWork.Commit();

            try
            {
                if (viewModel.VoucherType == "0001")
                {
                    return AddGlDetailEntriesInvoice(viewModel, glHeader.GLHeaderId);
                }
                else if (viewModel.VoucherType == "0002" && viewModel.InvoiceDetailCollection != null)
                {
                    return AddGlDetailEntriesExpense(viewModel, glHeader.GLHeaderId);
                }
                else
                {
                    return AddGlDetailEntriesSalary(viewModel, glHeader.GLHeaderId);
                }
            }
            catch (Exception ex)
            {
                glHeaderRepository.Delete(glHeader);
                return false;
            }
        }


        public ResponseObject Create(InvoiceHeaderViewModel viewModel, int userId)
        {
            ResponseObject response = new ResponseObject();

            if (viewModel != null)
            {
                int companyId = userRepository.GetById(userId).CompanyId;

                if (AddGlEntries(viewModel, companyId))
                {
                    InvoiceHeader invoiceHeader = new InvoiceHeader
                    {
                        CompanyId = companyId,
                        TotalAmount = viewModel.TotalAmount,
                        CustomerId = viewModel.CustomerId,
                        InvoiceNo = viewModel.InvoiceNo,
                        Date = viewModel.Date,
                        DueDate = viewModel.DueDate,
                        Status = viewModel.Status,
                        Description = viewModel.Description,
                        CategoryId = viewModel.CategoryId != null ? viewModel.CategoryId :
                            viewModel.VoucherType == "0001" ? categoriesRepository.Get(x => x.CategoryName == "un-sales").CategoryId :
                            categoriesRepository.Get(x => x.CategoryName == "un-expense").CategoryId,
                        CurrencyRate = viewModel.CurrencyRate,
                        BaseRate = viewModel.BaseRate,
                        VoucherType = viewModel.VoucherType,
                        CurrencyId = viewModel.CurrencyId
                    };

                    invoiceHeaderRepository.Add(invoiceHeader);

                    unitofWork.Commit();

                    var listDetail = new List<InvoiceDetail>();

                    if (viewModel.InvoiceDetailCollection != null)
                    {
                        if (viewModel.InvoiceDetailCollection.Any(x => x.ProductId != 0))
                        {
                            foreach (var invoiceDetailModel in viewModel.InvoiceDetailCollection)
                            {
                                InvoiceDetail invoiceDetail = new InvoiceDetail
                                {
                                    InvoiceHeaderID = invoiceHeader.InvoiceHeaderId,
                                    ProductId = invoiceDetailModel.ProductId,
                                    Quantity = invoiceDetailModel.Quantity,
                                    UnitPrice = invoiceDetailModel.UnitPrice,
                                    DiscountPerc = (double)invoiceDetailModel.DiscountPerc,
                                    Amount = invoiceDetailModel.Amount,
                                    TaxPerc = (double)invoiceDetailModel.TaxPerc,
                                    Unit = invoiceDetailModel.Unit,
                                    Description = invoiceDetailModel.Description
                                };
                                invoiceDetailRepository.Add(invoiceDetail);

                                unitofWork.Commit();
                                listDetail.Add(invoiceDetail);
                            }
                        }
                        else
                        {
                            foreach (var item in listDetail)
                            {
                                invoiceDetailRepository.Delete(item);
                            }

                            invoiceHeaderRepository.Delete(invoiceHeader);
                            throw new CustomException("Please make sure you have selected valid Products.");
                        }
                        response.IsSuccess = true;
                        return response;
                    }
                    response.IsSuccess = true;
                    return response;
                }
                else
                {
                    throw new CustomException("Your request cannot be completed, there has been a technical error.");
                }
            }

            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public ResponseObject AddAllocation(AllocationViewModel viewModel, int userId)
        {
            ResponseObject response = new ResponseObject();
            int companyId = userRepository.GetById(userId).CompanyId;

            //if allocation is coming from voucher(either invoice or bill)
            if (viewModel != null && viewModel.InvoiceHeaderId != null)
            {
                var invoiceHeader = invoiceHeaderRepository.GetById((long)viewModel.InvoiceHeaderId);

                GLHeader glHeader = new GLHeader
                {
                    TransactionType = "Allocation",
                    TransactionNumber = invoiceHeader.InvoiceNo,
                    Description = invoiceHeader.Description,
                    CompanyId = companyId,
                    TransactionDate = viewModel.Date
                };

                glHeaderRepository.Add(glHeader);
                unitofWork.Commit();

                try
                {
                    //if clearing invoice            cash or bank debit, business partner credit
                    //if clearing expense            cash or bank account credit, business partner debit
                    GLDetail glDetailDebit = new GLDetail
                    {
                        GLHeaderId = glHeader.GLHeaderId,
                        AccountCode = viewModel.AllocationType == "0001" ?
                            (viewModel.AllocationMode == "0001" ? cashBankRepository.Get(x => x.AccountName == "Cash In Hand").AccountCode :
                            cashBankRepository.Get(x => x.AccountName == "Bank").AccountCode) :
                            customerRepository.GetById(invoiceHeader.CustomerId ?? 0).AccountCode,
                        Debit = viewModel.Amount,
                        Credit = 0
                    };

                    glDetailRepository.Add(glDetailDebit);

                    GLDetail glDetailCredit = new GLDetail
                    {
                        GLHeaderId = glHeader.GLHeaderId,
                        AccountCode = viewModel.AllocationType == "0001" ? customerRepository.GetById(invoiceHeader.CustomerId ?? 0).AccountCode :
                            (viewModel.AllocationMode == "0001" ? cashBankRepository.Get(x => x.AccountName == "Cash In Hand").AccountCode :
                            cashBankRepository.Get(x => x.AccountName == "Bank").AccountCode),
                        Debit = 0,
                        Credit = viewModel.Amount
                    };

                    glDetailRepository.Add(glDetailCredit);

                    unitofWork.Commit();
                }
                catch (Exception ex)
                {
                    glHeaderRepository.Delete(glHeader);
                }

                Allocation allocation = new Allocation
                {
                    Date = viewModel.Date,
                    InvoiceHeaderId = viewModel.InvoiceHeaderId,
                    ExpiryDate = viewModel.ExpiryDate,
                    Account = viewModel.Account,
                    Amount = viewModel.Amount,
                    Explanation = viewModel.Explanation,
                    AllocationType = viewModel.AllocationType,
                    AllocationMode = viewModel.AllocationMode
                };

                allocationRepository.Add(allocation);

                unitofWork.Commit();
                response.IsSuccess = true;
                return response;
            }
            //if allocation is coming from customer
            else if (viewModel != null && viewModel.CustomerId != null)
            {
                viewModel.InvoiceHeaderIdList.Sort();

                decimal allocationAmount = viewModel.Amount;

                foreach (int invoiceHeaderId in viewModel.InvoiceHeaderIdList)
                {
                    if (allocationAmount == 0)
                    {
                        break;
                    }
                    var invoiceHeader = invoiceHeaderRepository.GetById(invoiceHeaderId);

                    GLHeader glHeader = new GLHeader
                    {
                        TransactionType = "Allocation",
                        TransactionNumber = invoiceHeader.InvoiceNo,
                        Description = invoiceHeader.Description,
                        CompanyId = companyId,
                        TransactionDate = viewModel.Date
                    };

                    glHeaderRepository.Add(glHeader);
                    unitofWork.Commit();

                    decimal allocatedAmount = invoiceHeader.Allocations.Sum(x => x.Amount);
                    decimal remainingAmount = invoiceHeader.TotalAmount - allocatedAmount;

                    if (invoiceHeader.TotalAmount > allocatedAmount)
                    {
                        if (remainingAmount > allocationAmount || remainingAmount == allocationAmount)
                        {
                            try
                            {
                                GLDetail glDetailDebit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = "0000",
                                    Debit = remainingAmount,
                                    Credit = 0
                                };

                                glDetailRepository.Add(glDetailDebit);

                                GLDetail glDetailCredit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = customerRepository.GetById((long)viewModel.CustomerId).AccountCode,
                                    Debit = 0,
                                    Credit = remainingAmount
                                };

                                glDetailRepository.Add(glDetailCredit);

                                unitofWork.Commit();
                            }
                            catch (Exception e)
                            {
                                glHeaderRepository.Delete(glHeader);
                            }

                            Allocation allocation = new Allocation
                            {
                                Date = viewModel.Date,
                                InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                                ExpiryDate = viewModel.ExpiryDate,
                                Account = viewModel.Account,
                                Amount = remainingAmount,
                                Explanation = viewModel.Explanation,
                                AllocationType = viewModel.AllocationType,
                                AllocationMode = viewModel.AllocationMode
                            };

                            allocationRepository.Add(allocation);

                            unitofWork.Commit();
                            response.IsSuccess = true;

                            allocationAmount = allocationAmount - allocation.Amount;
                        }
                        else if (remainingAmount < allocationAmount)
                        {
                            try
                            {
                                GLDetail glDetailDebit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = "0000",
                                    Debit = remainingAmount,
                                    Credit = 0
                                };

                                glDetailRepository.Add(glDetailDebit);

                                GLDetail glDetailCredit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = customerRepository.GetById((long)viewModel.CustomerId).AccountCode,
                                    Debit = 0,
                                    Credit = remainingAmount
                                };

                                glDetailRepository.Add(glDetailCredit);

                                unitofWork.Commit();
                            }
                            catch (Exception e)
                            {
                                glHeaderRepository.Delete(glHeader);
                            }

                            Allocation allocation = new Allocation
                            {
                                Date = viewModel.Date,
                                InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                                ExpiryDate = viewModel.ExpiryDate,
                                Account = viewModel.Account,
                                Amount = remainingAmount,
                                Explanation = viewModel.Explanation,
                                AllocationType = viewModel.AllocationType,
                                AllocationMode = viewModel.AllocationMode
                            };

                            allocationRepository.Add(allocation);

                            unitofWork.Commit();
                            response.IsSuccess = true;

                            allocationAmount = allocationAmount - allocation.Amount;
                        }
                    }

                }
                return response;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }



        public List<InvoiceHeaderViewModel> GetAllInvoices(int userId, DateRangeViewModel dateRangeViewModel)
        {
            IEnumerable<InvoiceHeader> invoiceHeaderList;

            if (dateRangeViewModel.ReportName == null)
            {
                invoiceHeaderList = invoiceHeaderRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);
            }
            else
            {
                invoiceHeaderList = invoiceHeaderRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId && x.Date >= dateRangeViewModel.StartDate && x.Date <= dateRangeViewModel.EndDate);
            }

            List<InvoiceHeaderViewModel> invoiceHeaderViewModelList = new List<InvoiceHeaderViewModel>();

            foreach (var invoiceHeader in invoiceHeaderList)
            {
                decimal totalAllocatedAmount = invoiceHeader.Allocations.Count() > 0 ? invoiceHeader.Allocations.Sum(x => x.Amount) : 0;

                InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel();
                invoiceHeaderViewModel.InvoiceHeaderId = invoiceHeader.InvoiceHeaderId;
                invoiceHeaderViewModel.CompanyId = invoiceHeader.CompanyId;
                invoiceHeaderViewModel.CustomerId = invoiceHeader.CustomerId ?? 0;
                invoiceHeaderViewModel.CategoryId = invoiceHeader.CategoryId;
                invoiceHeaderViewModel.BaseRate = invoiceHeader.BaseRate;
                invoiceHeaderViewModel.CurrencyRate = invoiceHeader.CurrencyRate;
                invoiceHeaderViewModel.TotalAmount = invoiceHeader.TotalAmount;
                invoiceHeaderViewModel.TotalAllocatedAmount = totalAllocatedAmount;
                invoiceHeaderViewModel.InvoiceNo = invoiceHeader.InvoiceNo;
                invoiceHeaderViewModel.Date = invoiceHeader.Date;
                invoiceHeaderViewModel.DueDate = invoiceHeader.DueDate;
                invoiceHeaderViewModel.Status = invoiceHeader.Status;
                invoiceHeaderViewModel.Description = invoiceHeader.Description;
                invoiceHeaderViewModel.VoucherType = invoiceHeader.VoucherType;
                invoiceHeaderViewModel.CurrencyId = invoiceHeader.CurrencyId;
                invoiceHeaderViewModel.CustomerName = invoiceHeaderViewModel.CustomerId != 0 ? invoiceHeader.Customer.CompanyTitle : null;

                invoiceHeaderViewModelList.Add(invoiceHeaderViewModel);
            }

            if (invoiceHeaderViewModelList != null)
            {
                return invoiceHeaderViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public List<List<InvoiceHeaderViewModel>> GetVATByMonth(int userId, int currentMonth, int year)
        {
            //int currMonth = month - arrLength;

            List<List<InvoiceHeaderViewModel>> listOfMonths = new List<List<InvoiceHeaderViewModel>>();

            for (int i = 0; i <= 2; i++)
            {
                if (currentMonth - i >= 1)
                {
                    var invoiceHeadersByMonth = invoiceHeaderRepository.GetAll().Where(x => x.Date.Month == currentMonth - i && x.Date.Year == year);

                    List<InvoiceHeaderViewModel> invoiceHeaderViewModelsByMonth = new List<InvoiceHeaderViewModel>();

                    foreach (var invoiceHeader in invoiceHeadersByMonth)
                    {
                        InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel
                        {
                            CompanyId = invoiceHeader.CompanyId,
                            CustomerName = invoiceHeader.Customer.CompanyTitle,
                            Date = invoiceHeader.Date,
                            Description = invoiceHeader.Description,
                            TotalVAT = 0,
                            InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                            InvoiceNo = invoiceHeader.InvoiceNo,
                            VoucherType = invoiceHeader.VoucherType,
                        };
                        invoiceHeader
                            .InvoiceDetails
                            .ToList()
                            .ForEach(x =>
                            {
                                invoiceHeaderViewModel.TotalVAT = invoiceHeaderViewModel.TotalVAT + ((x.UnitPrice) * ((decimal)x.TaxPerc)) / 100;
                            });

                        invoiceHeaderViewModelsByMonth.Add(invoiceHeaderViewModel);
                    }
                    listOfMonths.Add(invoiceHeaderViewModelsByMonth);
                }
                else if (currentMonth - i < 1)
                {
                    var invoiceHeadersByMonth = invoiceHeaderRepository.GetAll().Where(x => x.Date.Month == (currentMonth + 12) - i && x.Date.Year == year - 1);

                    List<InvoiceHeaderViewModel> invoiceHeaderViewModelsByMonth = new List<InvoiceHeaderViewModel>();

                    foreach (var invoiceHeader in invoiceHeadersByMonth)
                    {
                        InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel
                        {
                            CompanyId = invoiceHeader.CompanyId,
                            CustomerName = invoiceHeader.Customer.CompanyTitle,
                            Date = invoiceHeader.Date,
                            Description = invoiceHeader.Description,
                            TotalVAT = 0,
                            InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                            InvoiceNo = invoiceHeader.InvoiceNo,
                            VoucherType = invoiceHeader.VoucherType
                        };
                        invoiceHeader
                            .InvoiceDetails
                            .ToList()
                            .ForEach(x =>
                            {
                                invoiceHeaderViewModel.TotalVAT = invoiceHeaderViewModel.TotalVAT + ((x.UnitPrice) * ((decimal)x.TaxPerc)) / 100;
                            });

                        invoiceHeaderViewModelsByMonth.Add(invoiceHeaderViewModel);
                    }
                    listOfMonths.Add(invoiceHeaderViewModelsByMonth);
                }
            }

            return listOfMonths;
        }

        public List<BalanceByMonthViewModel> GetBalanceByMonth(int userId)
        {
            List<BalanceByMonthViewModel> balanceByMonthViewModelList = new List<BalanceByMonthViewModel>();

            //ticks calculation
            DateTime firstMonth = new DateTime(2017, 12, 01);
            TimeSpan span = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time = firstMonth.Subtract(span);
            var result = time.Ticks / 10000;

            DateTime secondMonth = new DateTime(2018, 01, 01);
            TimeSpan span1 = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time1 = secondMonth.Subtract(span);
            var result1 = time1.Ticks / 10000;

            DateTime thirdMonth = new DateTime(2018, 02, 01);
            TimeSpan span2 = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time2 = thirdMonth.Subtract(span);
            var result2 = time2.Ticks / 10000;

            DateTime fourthMonth = new DateTime(2018, 03, 01);
            TimeSpan span3 = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time3 = fourthMonth.Subtract(span);
            var result3 = time3.Ticks / 10000;


            //var amountByMonth1 = invoiceHeaderRepository.GetAll().Where(x => x.Date > firstMonth && x.Date < secondMonth).Sum(x => x.TotalAmount);
            //var amountByMonth2 = invoiceHeaderRepository.GetAll().Where(x => x.Date > secondMonth && x.Date < thirdMonth).Sum(x => x.TotalAmount);
            //var amountByMonth3 = invoiceHeaderRepository.GetAll().Where(x => x.Date > thirdMonth && x.Date < fourthMonth).Sum(x => x.TotalAmount);
            //var amountByMonth4 = invoiceHeaderRepository.GetAll().Where(x => x.Date > new DateTime(2018, 03, 01) && x.Date < new DateTime(2018, 04, 01)).Sum(x => x.TotalAmount);

            var vm1 = new BalanceByMonthViewModel { Balance = invoiceHeaderRepository.GetAll().Where(x => x.Date > firstMonth && x.Date < secondMonth).Sum(x => x.TotalAmount), Ticks = result };
            balanceByMonthViewModelList.Add(vm1);

            var vm2 = new BalanceByMonthViewModel { Balance = invoiceHeaderRepository.GetAll().Where(x => x.Date > secondMonth && x.Date < thirdMonth).Sum(x => x.TotalAmount), Ticks = result1 };
            balanceByMonthViewModelList.Add(vm2);

            var vm3 = new BalanceByMonthViewModel { Balance = invoiceHeaderRepository.GetAll().Where(x => x.Date > thirdMonth && x.Date < fourthMonth).Sum(x => x.TotalAmount), Ticks = result2 };
            balanceByMonthViewModelList.Add(vm3);

            var vm4 = new BalanceByMonthViewModel { Balance = invoiceHeaderRepository.GetAll().Where(x => x.Date > new DateTime(2018, 03, 01) && x.Date < new DateTime(2018, 04, 01)).Sum(x => x.TotalAmount), Ticks = result3 };
            balanceByMonthViewModelList.Add(vm4);

            if (balanceByMonthViewModelList != null)
            {
                return balanceByMonthViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }

        }


        public List<List<InvoiceHeaderViewModel>> GetAllDelayedInvoices(int userId)
        {

            List<List<InvoiceHeaderViewModel>> getAllDelayedInvoices = new List<List<InvoiceHeaderViewModel>>();

            List<InvoiceHeaderViewModel> updated = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(updated);

            List<InvoiceHeaderViewModel> delayed30 = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(delayed30);

            List<InvoiceHeaderViewModel> delayed60 = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(delayed60);

            List<InvoiceHeaderViewModel> delayed90 = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(delayed90);

            List<InvoiceHeaderViewModel> delayed120 = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(delayed120);

            List<InvoiceHeaderViewModel> delayed120plus = new List<InvoiceHeaderViewModel>();
            getAllDelayedInvoices.Add(delayed120plus);


            var openInvoices = invoiceHeaderRepository
                .GetAll()
                .Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId && x.Allocations.Sum(y => y.Amount) < x.TotalAmount);


            foreach (var invoiceHeader in openInvoices)
            {
                decimal totalAllocatedAmount = invoiceHeader.Allocations.Count() > 0 ? invoiceHeader.Allocations.Sum(x => x.Amount) : 0;

                InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel();
                invoiceHeaderViewModel.InvoiceHeaderId = invoiceHeader.InvoiceHeaderId;
                invoiceHeaderViewModel.CompanyId = invoiceHeader.CompanyId;
                invoiceHeaderViewModel.CustomerId = invoiceHeader.CustomerId ?? 0;
                invoiceHeaderViewModel.CategoryId = invoiceHeader.CategoryId;
                invoiceHeaderViewModel.BaseRate = invoiceHeader.BaseRate;
                invoiceHeaderViewModel.CurrencyRate = invoiceHeader.CurrencyRate;
                invoiceHeaderViewModel.TotalAmount = invoiceHeader.TotalAmount;
                invoiceHeaderViewModel.TotalAllocatedAmount = totalAllocatedAmount;
                invoiceHeaderViewModel.InvoiceNo = invoiceHeader.InvoiceNo;
                invoiceHeaderViewModel.Date = invoiceHeader.Date;
                invoiceHeaderViewModel.DueDate = invoiceHeader.DueDate;
                invoiceHeaderViewModel.Status = invoiceHeader.Status;
                invoiceHeaderViewModel.Description = invoiceHeader.Description;
                invoiceHeaderViewModel.VoucherType = invoiceHeader.VoucherType;
                invoiceHeaderViewModel.CurrencyId = invoiceHeaderViewModel.CurrencyId;

                if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays < 1)
                {
                    updated.Add(invoiceHeaderViewModel);
                }
                else if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays > 0 && DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays < 31)
                {
                    delayed30.Add(invoiceHeaderViewModel);
                }
                else if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays > 30 && DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays < 61)
                {
                    delayed60.Add(invoiceHeaderViewModel);
                }
                else if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays > 60 && DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays < 91)
                {
                    delayed90.Add(invoiceHeaderViewModel);
                }
                else if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays > 90 && DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays < 121)
                {
                    delayed120.Add(invoiceHeaderViewModel);
                }
                else if (DateTime.Now.Subtract(invoiceHeader.DueDate).TotalDays > 121)
                {
                    delayed120plus.Add(invoiceHeaderViewModel);
                }
            }

            return getAllDelayedInvoices;
        }


        public List<InvoiceDetailViewModel> GetAllInvoiceDetailsByInvoiceHeaderId(int invoiceHeaderId)
        {
            var invoiceDetailList = invoiceDetailRepository.GetAll().Where(x => x.InvoiceHeaderID == invoiceHeaderId);

            List<InvoiceDetailViewModel> invoiceDetailViewModelList = new List<InvoiceDetailViewModel>();

            foreach (var invoiceDetail in invoiceDetailList)
            {
                InvoiceDetailViewModel invoiceDetailViewModel = new InvoiceDetailViewModel()
                {
                    InvoiceDetailID = invoiceDetail.InvoiceDetailID,
                    InvoiceHeaderID = invoiceDetail.InvoiceHeaderID,
                    ProductId = invoiceDetail.ProductId,
                    ProductName = invoiceDetail.Product.Name,
                    Quantity = invoiceDetail.Quantity,
                    UnitPrice = invoiceDetail.UnitPrice,
                    DiscountPerc = (Decimal)invoiceDetail.DiscountPerc,
                    Amount = (Decimal)invoiceDetail.Amount,
                    TaxPerc = (Decimal)invoiceDetail.TaxPerc,
                    Description = invoiceDetail.Description,
                    Unit = invoiceDetail.Unit
                };

                invoiceDetailViewModelList.Add(invoiceDetailViewModel);
            }

            if (invoiceDetailViewModelList != null)
            {
                return invoiceDetailViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public List<AllocationViewModel> GetAllAllocationsByInvoiceHeaderId(int invoiceHeaderId)
        {
            var allocationsList = allocationRepository.GetAll().Where(x => x.InvoiceHeaderId == invoiceHeaderId);

            List<AllocationViewModel> allocationViewModelList = new List<AllocationViewModel>();

            foreach (var allocation in allocationsList)
            {
                AllocationViewModel allocationViewModel = new AllocationViewModel()
                {
                    AllocationId = allocation.AllocationId,
                    Date = allocation.Date,
                    ExpiryDate = allocation.ExpiryDate,
                    Account = allocation.Account,
                    Amount = allocation.Amount,
                    Explanation = allocation.Explanation,
                    AllocationType = allocation.AllocationType,
                    AllocationMode = allocation.AllocationMode,
                    InvoiceHeaderId = allocation.InvoiceHeaderId
                };

                allocationViewModelList.Add(allocationViewModel);
            }

            if (allocationViewModelList != null)
            {
                return allocationViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public List<InvoiceHeaderViewModel> GetAllInvoicesByCustomerId(int customerId)
        {
            var invoiceHeaderList = invoiceHeaderRepository.GetAll().Where(x => x.CustomerId == customerId);

            List<InvoiceHeaderViewModel> invoiceHeaderViewModelList = new List<InvoiceHeaderViewModel>();

            foreach (var invoiceHeader in invoiceHeaderList)
            {
                var allocatedAmount = allocationRepository.GetAll().Where(x => x.InvoiceHeaderId == invoiceHeader.InvoiceHeaderId).Sum(x => x.Amount);

                InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel()
                {
                    InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                    Description = invoiceHeader.Description,
                    Date = invoiceHeader.Date,
                    DueDate = invoiceHeader.DueDate,
                    TotalAmount = invoiceHeader.TotalAmount,
                    TotalAllocatedAmount = allocatedAmount,
                    VoucherType = invoiceHeader.VoucherType,
                    CurrencyId = invoiceHeader.CurrencyId
                };

                if (invoiceHeaderViewModel.TotalAmount - invoiceHeaderViewModel.TotalAllocatedAmount > 0)
                {
                    invoiceHeaderViewModelList.Add(invoiceHeaderViewModel);
                }
            }

            if (invoiceHeaderViewModelList != null)
            {
                return invoiceHeaderViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }



        public InvoiceHeaderViewModel GetInvoiceById(int invoiceHeaderId)
        {
            try
            {
                var invoice = invoiceHeaderRepository.GetById(invoiceHeaderId);

                decimal totalAllocatedAmount = invoice.Allocations.Count() > 0 ? invoice.Allocations.Sum(x => x.Amount) : 0;

                InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel
                {
                    BaseRate = invoice.BaseRate,
                    CategoryId = invoice.CategoryId,
                    CompanyId = invoice.CompanyId,
                    CurrencyRate = invoice.CurrencyRate,
                    CustomerId = invoice.CustomerId,
                    Date = invoice.Date,
                    Description = invoice.Description,
                    DueDate = invoice.DueDate,
                    InvoiceHeaderId = invoice.InvoiceHeaderId,
                    InvoiceNo = invoice.InvoiceNo,
                    Status = invoice.Status,
                    TotalAmount = invoice.TotalAmount,
                    VoucherType = invoice.VoucherType,
                    CustomerName = (invoice.Customer != null) ? invoice.Customer.CompanyTitle : "",
                    TotalAllocatedAmount = totalAllocatedAmount,
                    CurrencyId = invoice.CurrencyId
                };

                if (invoice.InvoiceDetails.Count() > 0)
                {
                    List<InvoiceDetailViewModel> InvoiceDetailViewModelList = new List<InvoiceDetailViewModel>();

                    foreach (var invoiceDetail in invoice.InvoiceDetails)
                    {
                        InvoiceDetailViewModel invoiceDetailViewModel = new InvoiceDetailViewModel
                        {
                            Amount = invoiceDetail.Amount,
                            Description = invoiceDetail.Description,
                            DiscountPerc = (decimal)invoiceDetail.DiscountPerc,
                            InvoiceDetailID = invoiceDetail.InvoiceDetailID,
                            ProductId = invoiceDetail.ProductId,
                            Quantity = invoiceDetail.Quantity,
                            TaxPerc = (decimal)invoiceDetail.TaxPerc,
                            Unit = invoiceDetail.Unit,
                            UnitPrice = invoiceDetail.UnitPrice,
                            ProductName = invoiceDetail.Product.Name
                        };
                        InvoiceDetailViewModelList.Add(invoiceDetailViewModel);
                    }
                    invoiceHeaderViewModel.InvoiceDetailCollection = InvoiceDetailViewModelList;
                }
                return invoiceHeaderViewModel;
            }

            catch (Exception e)
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }



        public List<InvoiceHeaderViewModel> GetInvoicesByProductId(int prodcutId, int userId)
        {
            var invoiceDetailList = invoiceDetailRepository
                .GetAll()
                .Where(x => x.ProductId == prodcutId && x.InvoiceHeader.CompanyId == userRepository.GetById(userId).CompanyId);

            List<InvoiceHeaderViewModel> invoiceHeaderViewModelList = new List<InvoiceHeaderViewModel>();

            foreach (var invoiceDetail in invoiceDetailList)
            {
                var invoiceHeader = invoiceHeaderRepository.GetById(invoiceDetail.InvoiceHeaderID);

                InvoiceHeaderViewModel invoiceHeaderViewModel = new InvoiceHeaderViewModel
                {
                    InvoiceHeaderId = invoiceHeader.InvoiceHeaderId,
                    Description = invoiceHeader.Description,
                    Date = invoiceHeader.Date,
                    DueDate = invoiceHeader.DueDate,
                    TotalAmount = invoiceHeader.TotalAmount,
                    CustomerName = customerRepository.GetById((long)invoiceHeader.CustomerId).CompanyTitle,
                    VoucherType = invoiceHeader.VoucherType,
                    CurrencyId = invoiceHeader.CurrencyId,
                    ProductQuantity = invoiceDetail.Quantity
                    //TotalAllocatedAmount = allocatedAmount
                };
                invoiceHeaderViewModelList.Add(invoiceHeaderViewModel);
            }

            if (invoiceHeaderViewModelList != null)
            {
                return invoiceHeaderViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public ResponseObject Delete(int invoiceHeaderId)
        {
            ResponseObject response = new ResponseObject();

            var allAllocationsAgainstThisInvoice = allocationRepository.GetAll().Where(x => x.InvoiceHeaderId == invoiceHeaderId);

            if (allAllocationsAgainstThisInvoice.Count() > 0)
            {
                response.IsSuccess = false;
                response.ErrorMessage = "Cannot delete! There are invoices against this customer";
                return response;
            }

            var invoiceDetails = invoiceDetailRepository.GetAll().Where(x => x.InvoiceHeaderID == invoiceHeaderId);

            foreach (var invoiceDetail in invoiceDetails)
            {
                invoiceDetailRepository.Delete(invoiceDetail);
            }

            invoiceHeaderRepository.Delete(x => x.InvoiceHeaderId == invoiceHeaderId);

            try
            {
                unitofWork.Commit();
                response.IsSuccess = true;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Edit(InvoiceHeaderViewModel viewModel, int userId)
        {
            try
            {
                InvoiceHeader invoiceHeader = invoiceHeaderRepository.GetById(viewModel.InvoiceHeaderId);

                //invoiceHeader.InvoiceHeaderId = viewModel.InvoiceHeaderId;
                invoiceHeader.CompanyId = userRepository.GetById(userId).CompanyId;
                invoiceHeader.CustomerId = viewModel.CustomerId;
                invoiceHeader.TotalAmount = viewModel.TotalAmount;
                invoiceHeader.InvoiceNo = viewModel.InvoiceNo;
                invoiceHeader.Date = viewModel.Date;
                invoiceHeader.DueDate = viewModel.DueDate;
                invoiceHeader.Status = viewModel.Status;
                invoiceHeader.Description = viewModel.Description;
                invoiceHeader.CategoryId = viewModel.CategoryId;
                invoiceHeader.CurrencyRate = viewModel.CurrencyRate;
                invoiceHeader.BaseRate = viewModel.BaseRate;
                invoiceHeader.VoucherType = viewModel.VoucherType;
                invoiceHeader.CurrencyId = viewModel.CurrencyId;

                invoiceHeaderRepository.Update(invoiceHeader);

                unitofWork.Commit();

                foreach (var invoiceDetail in viewModel.InvoiceDetailCollection)
                {
                    InvoiceDetail detail = invoiceDetailRepository.GetById(invoiceDetail.InvoiceDetailID);

                    detail.InvoiceDetailID = invoiceDetail.InvoiceDetailID;
                    //detail.InvoiceHeaderID = invoiceDetail.InvoiceHeaderID;
                    detail.ProductId = invoiceDetail.ProductId;
                    detail.Quantity = invoiceDetail.Quantity;
                    detail.UnitPrice = invoiceDetail.UnitPrice;
                    detail.DiscountPerc = (double)invoiceDetail.DiscountPerc;
                    detail.Amount = invoiceDetail.Amount;
                    detail.Description = invoiceDetail.Description;
                    detail.Unit = invoiceDetail.Unit;

                    invoiceDetailRepository.Update(detail);

                    unitofWork.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public List<InvoiceDetailViewModel> GetAllInvoiceDetails(int userId)
        {
            var invoiceDetailList = invoiceDetailRepository.GetAll().Where(x => x.InvoiceHeader.CompanyId == userRepository.GetById(userId).CompanyId);

            List<InvoiceDetailViewModel> invoiceDetailViewModelList = new List<InvoiceDetailViewModel>();

            foreach (var invoiceDetail in invoiceDetailList)
            {
                InvoiceDetailViewModel invoiceDetailViewModel = new InvoiceDetailViewModel()
                {
                    InvoiceDetailID = invoiceDetail.InvoiceDetailID,
                    InvoiceHeaderID = invoiceDetail.InvoiceHeaderID,
                    ProductId = invoiceDetail.ProductId,
                    ProductName = invoiceDetail.Product.Name,
                    Quantity = invoiceDetail.Quantity,
                    UnitPrice = invoiceDetail.UnitPrice,
                    DiscountPerc = (Decimal)invoiceDetail.DiscountPerc,
                    Amount = (Decimal)invoiceDetail.Amount,
                    TaxPerc = (Decimal)invoiceDetail.TaxPerc,
                    BusinessPartnerName = invoiceDetail.InvoiceHeader.Customer.CompanyTitle,
                    Date = invoiceDetail.InvoiceHeader.Date,
                    VoucherType = invoiceDetail.InvoiceHeader.VoucherType
                };

                invoiceDetailViewModelList.Add(invoiceDetailViewModel);
            }

            if (invoiceDetailViewModelList != null)
            {
                return invoiceDetailViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public List<AllocationViewModel> GetAllAllocations(int userId, DateRangeViewModel dateRangeViewModel)
        {
            var allocationsList = allocationRepository.GetAll().Where(x => x.InvoiceHeader.CompanyId == userRepository.GetById(userId).CompanyId && x.Date >= dateRangeViewModel.StartDate && x.Date <= dateRangeViewModel.EndDate);

            List<AllocationViewModel> allocationViewModelList = new List<AllocationViewModel>();

            foreach (var allocation in allocationsList)
            {
                AllocationViewModel allocationViewModel = new AllocationViewModel
                {
                    AllocationId = allocation.AllocationId,
                    Date = allocation.Date,
                    ExpiryDate = allocation.ExpiryDate,
                    Account = allocation.Account,
                    Amount = allocation.Amount,
                    Explanation = allocation.Explanation,
                    AllocationType = allocation.AllocationType,
                    AllocationMode = allocation.AllocationMode,
                    InvoiceHeaderId = allocation.InvoiceHeaderId,
                    VoucherType = allocation.InvoiceHeader.VoucherType,
                    BusinessPartnerName = allocation.InvoiceHeader.Customer.CompanyTitle,
                };

                allocationViewModelList.Add(allocationViewModel);
            }

            if (allocationViewModelList != null)
            {
                return allocationViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }
    }
}





