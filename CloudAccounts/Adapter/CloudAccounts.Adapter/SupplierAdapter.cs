﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Data;
using CloudAccounts.SharedModel;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.Infrastructure.Common;

namespace CloudAccounts.Adapter
{
    public class SupplierAdapter
    {
        private ISupplierDetailRepository supplierDetailRepository;
        private ISupplierHeaderRepository supplierHeaderRepository;
        private ICurrencyRepository currencyRepository;
        private IUnitofWork unitofWork;

        public SupplierViewModel supplierModel { get; set; }

        public SupplierAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            supplierDetailRepository = new SupplierDetailRepository(unitofWork.instance);
            supplierHeaderRepository = new SupplierHeaderRepository(unitofWork.instance);
            currencyRepository = new CurrencyRepository(unitofWork.instance);
        }

        public int Create(SupplierViewModel supplierViewModel)
        {
            SupplierHeader header = new SupplierHeader();

            //header.companyID = Convert.ToInt32(supplierViewModel.companyID);
            header.shortName = supplierViewModel.shortName;
            header.category = supplierViewModel.category;
            header.emailAddress = supplierViewModel.emailAddress;
            header.phoneNum = supplierViewModel.phoneNum;
            header.faxNum = supplierViewModel.faxNum;
            header.IBAN_Num = supplierViewModel.IBAN_Num;
            header.openAddress = supplierViewModel.openAddress;
            header.district = supplierViewModel.district;
            header.province = supplierViewModel.province;

            header.TC_Identification = supplierViewModel.tc_identification;
            header.vkn_tckn = supplierViewModel.vkn_tckn;
            header.taxAdministration = supplierViewModel.taxAdministration;
            header.companyDate = Convert.ToDateTime(supplierViewModel.companyDate);
            header.price = supplierViewModel.price;
            header.currencySymbol = supplierViewModel.currencySymbol;
            header.msg = supplierViewModel.msg;
            //header.openingBal = supplierViewModel.openingBal;

            supplierHeaderRepository.Add(header);
            unitofWork.Commit();

            int id = supplierHeaderRepository.GetAll().LastOrDefault().supplierID;

            foreach (var item in supplierViewModel.supplierDetail)
            {
                supplierDetail detail = new supplierDetail();
                detail.supplierHeaderID = id;
                detail.supplierName = item.supplierName;
                detail.email = item.email;
                detail.telephone = item.telephone;
                detail.Notes = item.Notes;

                supplierDetailRepository.Add(detail);
                unitofWork.Commit();
            }

            return id;
        }

        public List<SupplierViewModel> GetAllSupplierLists()
        {
            List<SupplierViewModel> list = new List<SupplierViewModel>();
            var model = supplierHeaderRepository.GetAll().ToList();

            foreach (var item in model)
            {
                list.Add(new SupplierViewModel
                {
                    supplierID = item.supplierID,
                    CompanyName = item.companyName,
                    shortName = item.shortName,
                    category = item.category,
                    emailAddress = item.emailAddress,
                    phoneNum = item.phoneNum,
                    faxNum = item.faxNum,
                    IBAN_Num = item.IBAN_Num,
                    openAddress = item.openAddress,
                    district = item.district,
                    province = item.province,
                    tc_identification = item.TC_Identification,
                    vkn_tckn = item.vkn_tckn,
                    taxAdministration = item.taxAdministration,

                    currencySymbol = item.currencySymbol,
                    companyDate = (DateTime)item.companyDate,
                    //companyDate = Convert.ToString(item.companyDate),
                    price = Convert.ToInt32(item.price),
                    msg = item.msg
                });
            }

            return list;
        }


        public SupplierViewModel GetSupplierByID(int supplierID)
        {
            var supplierHeader = supplierHeaderRepository.GetById(supplierID);
            var supplierDetail = supplierDetailRepository.GetAll().Where(x => x.supplierHeaderID == supplierID);

            SupplierViewModel supplierModel = new SupplierViewModel();

            supplierModel.supplierID = supplierHeader.supplierID;
            supplierModel.CompanyName = supplierHeader.companyName;
            supplierModel.shortName = supplierHeader.shortName;
            supplierModel.category = supplierHeader.category;
            supplierModel.emailAddress = supplierHeader.emailAddress;
            supplierModel.phoneNum = supplierHeader.phoneNum;
            supplierModel.faxNum = supplierHeader.faxNum;
            supplierModel.IBAN_Num = supplierHeader.IBAN_Num;
            supplierModel.openAddress = supplierHeader.openAddress;
            supplierModel.district = supplierHeader.district;
            supplierModel.province = supplierHeader.province;
            if (supplierModel.tc_identification != null)
            {
                supplierModel.tour = "Real Person";
                supplierModel.tc_identification = supplierHeader.TC_Identification;
            }
            else
            {
                supplierModel.tour = "Legal Entity";
                supplierModel.vkn_tckn = supplierHeader.vkn_tckn;
                supplierModel.taxAdministration = supplierHeader.taxAdministration;

                foreach (var item in supplierDetail)
                {
                    supplierModel.supplierDetail = new List<supplierDetail>();
                    supplierModel.supplierDetail.Add(new supplierDetail()
                    {
                        supplierName = item.supplierName,
                        email = item.email,
                        telephone = item.telephone,
                        Notes = item.Notes
                    });
                }
            }

            return supplierModel;
        }

        //public List<SupplierViewModel> GetAllSupplier()
        //{
        //    return supplierModel;
        //}

        public List<CurrencyViewModel> GetAllCurrencies()
        {
            List<CurrencyViewModel> currencyList = new List<CurrencyViewModel>();
            var currencies = currencyRepository.GetAll();
            foreach (var item in currencies)
            {
                currencyList.Add(new CurrencyViewModel
                {
                    curr_ID = item.curr_ID,
                    code = item.code,
                    title = item.title,
                    symbol = item.symbol
                });
            }

            return currencyList;
        }

    }
}
