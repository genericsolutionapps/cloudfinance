﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Configuration;


namespace CloudAccounts.Adapter
{
    public class RegistrationAdapter
    {
        private IUserRepository userRepository;
        private CompanyRepository companyRepository;
        private IUnitofWork unitofWork;

        public RegistrationAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            userRepository = new UserRepository(unitofWork.instance);
            companyRepository = new CompanyRepository(unitofWork.instance);
        }

        public bool SendConfirmationEmail(string email, string guid)
        {
            try
            {
                string token = Convert.ToBase64String(Encoding.ASCII.GetBytes(guid));

                //var client = new SmtpClient("smtp.gmail.com", 587)
                //{
                //    Credentials = new NetworkCredential("genericsolutionpk@gmail.com", "hasnainsaad123321")
                //    ,
                //    EnableSsl = false
                //};

                //var client = new SmtpClient("relay-hosting.secure.net", 25)
                //{
                //    Credentials = new NetworkCredential("saad.hasnain@gsolutions.com.pk", "Abcd1234!@#$")
                //    ,
                //    UseDefaultCredentials = false,
                //    EnableSsl = false
                //};

                //client.Send("saad.hasnain@gsolutions.com.pk", email, "Cloud Accounts | Email Verification", "Click on the link to verify you account http://localhost:16363/Activation/Index/" + token);

                //int port = Convert.ToInt16(ConfigurationManager.AppSettings["Port"]);
                //string host = ConfigurationManager.AppSettings["Host"].ToString();
                //string emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                //string Password = ConfigurationManager.AppSettings["Password"].ToString();
                //bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["SSL"]);

                //MailMessage msg = new MailMessage();
                //msg.To.Add(new MailAddress(email));
                //msg.From = new MailAddress(emailFrom);
                //msg.Subject = "Cloud Accounts | Email Verification";
                //msg.Body = "Click on the link to verify you account http://localhost:16363/Activation/Index/" + token;
                //msg.IsBodyHtml = true;

                //SmtpClient client = new SmtpClient();
                //client.Credentials = new NetworkCredential(emailFrom, Password);
                //client.Port = port;
                //client.EnableSsl = ssl;
                //client.Host = host;
                ////client.Host = "localhost";

                //client.Send(msg);

                MailMessage message = new MailMessage();
                message.From = new MailAddress("saad.hasnain@gsolutions.com.pk");

                message.To.Add(new MailAddress(email));

                message.Subject = "Cloud Accounts | Email Verification";
                message.Body = "Click on the link to verify you account http://cloudaccounts.gsolutions.com.pk/Activation/Index/" + token;

                SmtpClient client = new SmtpClient();
                client.Send(message);

                //const string SERVER = "relay-hosting.secureserver.net";
                //S.MailMessage oMail = new System.Web.Mail.MailMessage();
                //oMail.From = "emailaddress@domainname";
                //oMail.To = "emailaddress@domainname";
                //oMail.Subject = "Test email subject";
                //oMail.BodyFormat = MailFormat.Html;	// enumeration
                //oMail.Priority = MailPriority.High;	// enumeration
                //oMail.Body = "Sent at: " + DateTime.Now;
                //SmtpMail.SmtpServer = SERVER;
                //SmtpMail.Send(oMail);
                //oMail = null;	// free up resources

                //MailMessage mailMessage = new MailMessage("hasnain.105@gmail.com", email);
                //mailMessage.Subject = "Test mail subject";
                //mailMessage.Body = "Test mail subject";

                //SmtpClient smtpClient = new SmtpClient();
                //smtpClient.Host = "smtp.gmail.com";
                //smtpClient.Port = 587;

                //smtpClient.Credentials = new System.Net.NetworkCredential
                //{
                //    UserName = "hasnain.105@gmail.com",
                //    Password = ""
                //};

                ////smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                ////smtpClient.UseDefaultCredentials = false;
                //smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public UserInfo Create(RegistrationViewModel model)
        {
            User user = userRepository.Get(x => x.Email == model.Email);

            UserInfo info = new UserInfo();

            var guid = Guid.NewGuid().ToString();

            if (user == null && SendConfirmationEmail(model.Email, guid) == true)
            {
                //create new user

                Company companyModel = new Company();
                companyModel.Name = model.CompanyName;
                companyModel.CommercialTitle = model.CompanyName;
                companyModel.Email = model.Email;
                companyModel.Phone = model.PhoneNumber;
                companyModel.CompanyTypeCode = model.IndustryType;
                companyModel.FeatureTypeCode = model.FeatureType;

                companyRepository.Add(companyModel);

                unitofWork.Commit();

                User userModel = new User();
                userModel.Email = model.Email;
                userModel.Password = model.Password;
                userModel.Name = model.Name;
                userModel.CompanyId = companyModel.CompanyId;
                userModel.UserStatusTypeCode = "0001";
                userModel.CashBankTransaction = model.CashBankTransaction;
                userModel.EmployeesTransaction = model.EmployeesTransaction;
                userModel.ExpenseTransaction = model.ExpenseTransaction;
                userModel.SalesTransaction = model.SalesTransaction;
                userModel.guid = guid;

                userRepository.Add(userModel);

                unitofWork.Commit();



                info.UserId = userModel.UserId;
                info.Email = userModel.Email;
            }
            else
            {
                throw new CustomException("A user with the same name already exist, please choose a new user name.");
            }

            return info;
        }


        public bool ActivateAccount(string token)
        {
            try
            {

                User user = userRepository.Get(x => x.guid == token);

                user.IsEmailConfirmed = true;

                userRepository.Update(user);
                unitofWork.Commit();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //CheckEmailVerification
        public bool CheckEmailVerification(int userId)
        {
            try
            {

                User user = userRepository.Get(x => x.UserId == userId);

                return user.IsEmailConfirmed ?? false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}













