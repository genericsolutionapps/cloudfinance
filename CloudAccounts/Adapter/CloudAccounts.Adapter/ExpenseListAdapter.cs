﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.SharedModel;
using CloudAccounts.Data;
using CloudAccounts.Adapter.Exceptions;

namespace CloudAccounts.Adapter
{
    public class ExpenseListAdapter
    {
        private IExpenseListRepository expenseRepository;
        private IUnitofWork unitOfWork;

        public ExpenseListAdapter()
        {
            unitOfWork = new UnitOfWork(new DatabaseFactory());
            expenseRepository = new ExpenseListRepository(unitOfWork.instance);
        }

        //public void Create(ExpenseListViewModel expenseList)

        public ExpenseListViewModel Create(ExpenseListViewModel expenseList)
        {
            try
            {
                ExpenseList expense = new ExpenseList();

                expense.explaination = expenseList.Explaination;
                expense.bill_Date = Convert.ToString(expenseList.bill_Date);
                expense.bill_Num = expenseList.bill_Num;
                expense.totalAmount = expenseList.TotalAmount;
                expense.total_VAT = expenseList.total_VAT;
                expense.paymentStatus = expenseList.paymentStatus;
                expense.payment_Date = Convert.ToString(expenseList.payment_Date);
                expense.supplierName = expenseList.supplierName;

                expenseRepository.Add(expense);
                unitOfWork.Commit();

                expenseList.ID = expenseList.ID;
                return expenseList;
            }
            catch (Exception ex)
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
                //return false;
            }
        }
    }
}
