﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class ProductAdapter
    {
        private IProductRepository productRepository;
        private IUserRepository userRepository;
        private IAccountRepository accountRepository;
        private ICategoriesRepository categoriesRepository;
        private IInvoiceDetailRepository invoiceDetailRepository;

        private IUnitofWork unitofWork;

        public ProductAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());

            productRepository = new ProductRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
            accountRepository = new AccountRepository(unitofWork.instance);
            categoriesRepository = new CategoriesRepository(unitofWork.instance);
            invoiceDetailRepository = new InvoiceDetailRepository(unitofWork.instance);
        }

        public List<ProductViewModel> GetAllProducts(int userId)
        {
            var productList = productRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<ProductViewModel> productViewModelList = new List<ProductViewModel>();


            foreach (var product in productList)
            {
                ProductViewModel productViewModel = new ProductViewModel
                {
                    ProductId = product.ProductId,
                    CategoryId = product.CategoryId,
                    CompanyId = product.CompanyId,
                    Name = product.Name,
                    Code = product.Code,
                    Barcode = product.Barcode,
                    StartingQuantity = product.StartingQuantity ?? 0,
                    Unit = product.Unit,
                    PurchasePrice = product.PurchasePrice ?? 0,
                    PurchaseCurrencyRate = product.PurchaseCurrencyRate ?? 0,
                    SalesPrice = product.SalesPrice ?? 0,
                    SalesCurrencyRate = product.SalesCurrencyRate ?? 0,
                    SalesCurrencyId = product.SalesCurrencyId ?? 0,
                    KDVPerc = product.KDVPerc ?? 0,
                    BaseRate = product.BaseRate ?? 0,
                    PurchaseCurrencyId = product.PurchaseCurrencyId ?? 0,
                    CurrentQuantity = (product.StartingQuantity + invoiceDetailRepository.GetAll().Where(x => x.ProductId == product.ProductId && x.InvoiceHeader.VoucherType == "0002").Sum(x => x.Quantity) ?? 0) - invoiceDetailRepository.GetAll().Where(x => x.ProductId == product.ProductId && x.InvoiceHeader.VoucherType == "0001").Sum(x => x.Quantity),
                    AccountCode = product.AccountCode,
                    TotalStockIn = product.InvoiceDetails.Where(x => x.InvoiceHeader.VoucherType == "0002").Sum(x => x.Quantity),
                    TotalPurchasingAmount = product.InvoiceDetails.Where(x => x.InvoiceHeader.VoucherType == "0002").Sum(x => x.Amount * product.PurchaseCurrencyRate ?? 0 * x.InvoiceHeader.CurrencyRate)
                };

                productViewModelList.Add(productViewModel);
            }

            if (productViewModelList != null)
            {
                return productViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public ResponseObject Create(ProductViewModel model, int userId)
        {
            ResponseObject response = new ResponseObject();

            if (model != null)
            {
                int compId = userRepository.GetById(userId).CompanyId;

                try
                {
                    //open account
                    Account account = new Account
                    {
                        AccountName = model.Name,
                        AccountCode = ((accountRepository.GetAll().LastOrDefault() == null) ? "0001" :
                            (Convert.ToInt32(accountRepository.GetAll().LastOrDefault().AccountCode) + 1).ToString().PadLeft(4, '0')),
                        AccountType = "Inventory",
                        CompanyId = compId,
                        Nature = "ASSET"
                    };
                    accountRepository.Add(account);

                    //create product
                    Product product = new Product
                    {
                        CategoryId = model.CategoryId != 0 ? model.CategoryId : categoriesRepository.Get(x => x.CategoryName == "un-sales").CategoryId,
                        CompanyId = userRepository.GetById(userId).CompanyId,
                        AccountCode = account.AccountCode,
                        Name = model.Name,
                        Code = model.Code,
                        Barcode = model.Barcode,
                        StartingQuantity = model.StartingQuantity,
                        Unit = model.Unit,
                        PurchasePrice = model.PurchasePrice,
                        PurchaseCurrencyRate = model.PurchaseCurrencyRate,
                        SalesPrice = model.SalesPrice,
                        SalesCurrencyRate = model.SalesCurrencyRate,
                        SalesCurrencyId = model.SalesCurrencyId,
                        KDVPerc = model.KDVPerc,
                        BaseRate = model.BaseRate,
                        PurchaseCurrencyId = model.PurchaseCurrencyId
                    };
                    productRepository.Add(product);
                    unitofWork.Commit();
                }
                catch (Exception e)
                {
                    throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                }
            }

            return response;
        }

        public ProductViewModel GetProductById(int productId, int userId)
        {
            int companyId = userRepository.GetById(userId).CompanyId;

            var product = productRepository.Get(x => x.CompanyId == companyId && x.ProductId == productId);

            ProductViewModel productViewModel = new ProductViewModel
            {
                ProductId = product.ProductId,
                Name = product.Name,
                Barcode = product.Barcode,
                Code = product.Code,
                BaseRate = product.BaseRate??0,
                CategoryId = product.CategoryId,
                CompanyId = product.CompanyId,
                KDVPerc = product.KDVPerc ?? 0,
                PurchaseCurrencyRate = product.PurchaseCurrencyRate ?? 0,
                PurchasePrice = product.PurchasePrice * product.PurchaseCurrencyRate ?? 0,
                SalesCurrencyRate = product.SalesCurrencyRate ?? 0,
                SalesPrice = product.SalesPrice * product.SalesCurrencyRate ?? 0,
                SalesCurrencyId = product.SalesCurrencyId ?? 0,
                StartingQuantity = product.StartingQuantity ?? 0,
                Unit = product.Unit,
                PurchaseCurrencyId = product.PurchaseCurrencyId ?? 0,
                CurrentQuantity = (product.StartingQuantity + invoiceDetailRepository.GetAll().Where(x => x.ProductId == product.ProductId && x.InvoiceHeader.VoucherType == "0002").Sum(x => x.Quantity) ?? 0) - invoiceDetailRepository.GetAll().Where(x => x.ProductId == product.ProductId && x.InvoiceHeader.VoucherType == "0001").Sum(x => x.Quantity)
            };

            return productViewModel;
        }

        public bool Edit(ProductViewModel viewModel, int userId)
        {
            try
            {
                Product product = productRepository.GetById(viewModel.ProductId);
                product.CategoryId = viewModel.CategoryId != 0 ? viewModel.CategoryId : categoriesRepository.Get(x => x.CategoryName == "un-product").CategoryId;
                product.CompanyId = userRepository.GetById(userId).CompanyId;
                product.Name = viewModel.Name;
                product.Code = viewModel.Code;
                product.Barcode = viewModel.Barcode;
                product.StartingQuantity = viewModel.StartingQuantity;
                product.Unit = viewModel.Unit;
                product.PurchasePrice = viewModel.PurchasePrice;
                product.PurchaseCurrencyRate = viewModel.PurchaseCurrencyRate;
                product.SalesPrice = viewModel.SalesPrice;
                product.SalesCurrencyId = viewModel.SalesCurrencyId;
                product.KDVPerc = viewModel.KDVPerc;
                product.BaseRate = viewModel.BaseRate;
                product.PurchaseCurrencyId = product.PurchaseCurrencyId;

                productRepository.Update(product);

                unitofWork.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public bool Delete(int productId)
        {
            if (productId > 0)
            {
                var invoiceDetails = productRepository.Get(x => x.ProductId == productId).InvoiceDetails;

                if (invoiceDetails.Count == 0)
                {
                    Product product = productRepository.GetById(productId);

                    accountRepository.Delete(x => x.AccountCode == product.AccountCode);
                    productRepository.Delete(x => x.ProductId == productId);

                    unitofWork.Commit();
                    return true;
                }
            }

            else
            {
                throw new CustomException("This prodcut cannot be deleted, it has invoices against it.");
            }
            return false;
        }

    }
}
