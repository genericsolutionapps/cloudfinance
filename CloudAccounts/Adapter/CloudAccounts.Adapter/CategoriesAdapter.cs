﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class CategoriesAdapter
    {
        private ICompanyRepository companyRepository;
        private IUserRepository userRepository;
        private IInvoiceHeaderRepository invoiceHeaderRepository;
        private ICategoriesRepository categoriesRepository;
        private string[] lisOfColorss;

        private IUnitofWork unitofWork;

        public CategoriesAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());
            categoriesRepository = new CategoriesRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
            companyRepository = new CompanyRepository(unitofWork.instance);
            invoiceHeaderRepository = new InvoiceHeaderRepository(unitofWork.instance);

            lisOfColorss = new string[] { "#C0C0C0", "#808080", "#000000", "#FF0000", "#800000", "#FFFF00", "#808000", "#00FF00", "#008000", "#00FFFF", "#008080", "#0000FF", "#FF00FF", "#800080" };
        }


        public List<CategoryViewModel> GetAllCategories(int userId)
        {
            var categoryList = categoriesRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<CategoryViewModel> categoryViewModelList = new List<CategoryViewModel>();

            foreach (var category in categoryList)
            {
                CategoryViewModel categoryViewModel = new CategoryViewModel()
                {
                    CategoryId = category.CategoryId,
                    CategoryName = category.CategoryName,
                    CategoryColor = category.CategoryColor,
                    CategoryType = category.CategoryType
                };

                categoryViewModelList.Add(categoryViewModel);
            }

            if (categoryViewModelList != null)
            {
                return categoryViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public List<CategoryViewModel> GetCategoriesByType(DateRangeViewModel viewModel, string categoryType)
        {
            var voucherType = viewModel.ReportName == "Sales" ? "0001" : "0002";

            var categories = categoriesRepository
                .GetAll()
                .Where(x => x.CompanyId == userRepository.GetById(viewModel.UserId).CompanyId && x.CategoryType == categoryType);

            List<CategoryViewModel> categoryViewModelList = new List<CategoryViewModel>();

            foreach (var category in categories)
            {
                CategoryViewModel categoryViewModel = new CategoryViewModel();

                categoryViewModel.CategoryId = category.CategoryId;
                categoryViewModel.CategoryName = category.CategoryName;
                categoryViewModel.CategoryColor = category.CategoryColor;
                categoryViewModel.CategoryType = category.CategoryType;

                if (categoryType == "0001" || categoryType == "0002")
                {
                    categoryViewModel.BalanceAgainstThisCategory = category
                        .InvoiceHeaders
                        .Where(x => x.Date >= viewModel.StartDate && x.Date <= viewModel.EndDate && x.VoucherType == voucherType)
                        .Sum(x => x.TotalAmount * x.CurrencyRate);
                }
                else if (categoryType == "0004")
                {
                    category
                        .Products
                        .ToList()
                        .ForEach(product =>
                        {
                            categoryViewModel.BalanceAgainstThisCategory = categoryViewModel.BalanceAgainstThisCategory + product.InvoiceDetails
                                .Where(detail => detail.InvoiceHeader.Date >= viewModel.StartDate
                                    && detail.InvoiceHeader.Date <= viewModel.EndDate
                                    && detail.InvoiceHeader.VoucherType == voucherType)
                                .Sum(x => x.Amount * x.InvoiceHeader.CurrencyRate);
                        });
                }
                else if (categoryType == "0006")
                {
                    category
                        .Customers
                        .ToList()
                        .ForEach(cust =>
                        {
                            categoryViewModel.BalanceAgainstThisCategory = categoryViewModel.BalanceAgainstThisCategory + cust
                                .InvoiceHeaders
                                .Where(x => x.Date >= viewModel.StartDate && x.Date <= viewModel.EndDate && x.VoucherType == voucherType)
                                .Sum(x => x.TotalAmount * x.CurrencyRate);
                        });
                }
                categoryViewModelList.Add(categoryViewModel);
            }

            if (categoryViewModelList != null)
            {
                return categoryViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public List<CategoryViewModel> GetAllSalesCategories(DateRangeViewModel viewModel)
        {
            var salesCategories = categoriesRepository
                .GetAll()
                .Where(x => x.CompanyId == userRepository.GetById(viewModel.UserId).CompanyId && x.CategoryType == "0001");


            List<CategoryViewModel> categoryViewModelList = new List<CategoryViewModel>();

            foreach (var category in salesCategories)
            {
                CategoryViewModel categoryViewModel = new CategoryViewModel();

                categoryViewModel.CategoryId = category.CategoryId;
                categoryViewModel.CategoryName = category.CategoryName;
                categoryViewModel.CategoryColor = category.CategoryColor;
                categoryViewModel.CategoryType = category.CategoryType;
                categoryViewModel.BalanceAgainstThisCategory = category
                    .InvoiceHeaders
                    .Where(x => x.DueDate >= viewModel.StartDate && x.DueDate <= viewModel.EndDate)
                    .Sum(x => x.TotalAmount);

                categoryViewModelList.Add(categoryViewModel);
            }

            if (categoryViewModelList != null)
            {
                return categoryViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public List<CategoryViewModel> GetAllCustomerCategories(DateRangeViewModel viewModel)
        {
            var customerCategories = categoriesRepository
                .GetAll().Where(x => x.CompanyId == userRepository.GetById(viewModel.UserId).CompanyId && x.CategoryType == "0006");

            List<CategoryViewModel> categoryViewModelList = new List<CategoryViewModel>();

            foreach (var category in customerCategories)
            {
                var customers = category.Customers;

                decimal totalAmountAgainstThisCustomer = 0;

                foreach (var customer in customers)
                {
                    totalAmountAgainstThisCustomer = totalAmountAgainstThisCustomer + invoiceHeaderRepository
                        .GetAll()
                        .Where(x => x.CustomerId == customer.CustomerId && x.DueDate >= viewModel.StartDate && x.DueDate <= viewModel.EndDate)
                        .Sum(x => x.TotalAmount);
                }

                CategoryViewModel categoryViewModel = new CategoryViewModel()
                {
                    CategoryId = category.CategoryId,
                    CategoryName = category.CategoryName,
                    CategoryColor = category.CategoryColor,
                    CategoryType = category.CategoryType,
                    //BalanceAgainstThisCategory = category.InvoiceHeaders.Sum(x => x.TotalAmount)
                    BalanceAgainstThisCategory = totalAmountAgainstThisCustomer
                    //invoiceHeaderRepository.GetAll().Where(x => x.Category.CategoryType == "0001").Sum(x => x.TotalAmount)
                };
                categoryViewModelList.Add(categoryViewModel);
            }

            if (categoryViewModelList != null)
            {
                return categoryViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public bool Edit(CategoryViewModel viewModel)
        {
            try
            {
                Category model = categoriesRepository.GetById(viewModel.CategoryId);

                model.CategoryId = model.CategoryId;
                model.CategoryName = viewModel.CategoryName;
                model.CategoryType = viewModel.CategoryType;
                model.CategoryColor = viewModel.CategoryColor;
                model.CompanyId = model.CompanyId;

                categoriesRepository.Update(model);

                unitofWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public CategoryViewModel Create(CategoryViewModel categoryViewModel)
        {
            if (categoryViewModel != null)
            {
                Category category = new Category();

                category.CategoryName = categoryViewModel.CategoryName;

                //if this category is generating on fly then setting the category color below
                if (categoryViewModel.CategoryColor == null)
                {
                    var salesCategories = categoriesRepository.GetAll().Where(x => x.CategoryType == categoryViewModel.CategoryType);

                    for (int i = 0; i < lisOfColorss.Length; i++)
                    {
                        var status = salesCategories.Any(x => x.CategoryColor == lisOfColorss[i]);

                        if (!status)
                        {
                            category.CategoryColor = lisOfColorss[i];
                            categoryViewModel.CategoryColor = lisOfColorss[i];
                            break;
                        }
                    }
                    if (categoryViewModel.CategoryColor == null)
                    {
                        throw new CustomException("No colors left to create a new category, please select a color manually.");
                    }
                }
                else
                {
                    category.CategoryColor = categoryViewModel.CategoryColor;
                }

                category.CategoryType = categoryViewModel.CategoryType;
                category.CompanyId = userRepository.Get(x => x.UserId == categoryViewModel.UserId).CompanyId;

                categoriesRepository.Add(category);
                unitofWork.Commit();

                categoryViewModel.CategoryId = category.CategoryId;

                return categoryViewModel;
            }

            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public bool Delete(int categoryId)
        {
            if (categoryId > 0)
            {
                try
                {
                    categoriesRepository.Delete(categoriesRepository.GetById(categoryId));

                    unitofWork.Commit();
                }
                catch (Exception e)
                {
                    throw new CustomException("Cannot delete this category because it has open transactions against it.");
                }
                return true;
            }

            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }
    }
}
















