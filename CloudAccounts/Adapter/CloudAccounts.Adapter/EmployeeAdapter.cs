﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class EmployeeAdapter
    {
        private IEmployeeRepository employeeRepository;
        private IUserRepository userRepository;
        private IAccountRepository accountRepository;
        private IGLHeaderRepository glHeaderRepository;
        private IGLDetailRepository glDetailRepository;
        private ICashBankRepository cashBankRepository;
        private ICategoriesRepository categoriesRepository;
        private IUnitofWork unitofWork;

        public EmployeeAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());

            employeeRepository = new EmployeeRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
            accountRepository = new AccountRepository(unitofWork.instance);
            glHeaderRepository = new GLHeaderRepository(unitofWork.instance);
            glDetailRepository = new GLDetailRepository(unitofWork.instance);
            cashBankRepository = new CashBankRepository(unitofWork.instance);
            categoriesRepository = new CategoriesRepository(unitofWork.instance);
        }

        public List<EmployeeViewModel> GetAllEmployees(int userId)
        {
            var employees = employeeRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<EmployeeViewModel> employeeViewModelList = new List<EmployeeViewModel>();

            foreach (var employee in employees)
            {
                EmployeeViewModel employeeViewModel = new EmployeeViewModel
                {
                    EmployeeId = employee.EmployeeId,
                    CategoryId = employee.CategoryId,
                    CompanyId = employee.CompanyId,
                    IBAN = employee.IBAN,
                    IdentificationNumber = employee.IdentificationNumber,
                    Name = employee.Name,
                    Email = employee.Email,
                    EmployeeBalance = GetEmployeeBalance(employee.AccountCode)
                };
                employeeViewModelList.Add(employeeViewModel);
            }

            if (employeeViewModelList != null)
            {
                return employeeViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }

        public ResponseObject Create(EmployeeViewModel viewModel, int userId)
        {
            ResponseObject response = new ResponseObject();

            if (viewModel != null)
            {
                int compId = userRepository.GetById(userId).CompanyId;

                try
                {
                    //open account
                    Account account = new Account
                    {
                        AccountName = viewModel.Name,
                        AccountCode = ((accountRepository.GetAll().LastOrDefault() == null) ? "0001" :
                            (Convert.ToInt32(accountRepository.GetAll().LastOrDefault().AccountCode) + 1).ToString().PadLeft(4, '0')),
                        AccountType = "Employee",
                        CompanyId = compId,
                        Nature = "EXPENSE"
                    };
                    accountRepository.Add(account);

                    Employee employee = new Employee
                    {
                        CategoryId = viewModel.CategoryId != 0 ? viewModel.CategoryId : categoriesRepository.Get(x => x.CategoryName == "un-customer").CategoryId,
                        CompanyId = compId,
                        IBAN = viewModel.IBAN,
                        IdentificationNumber = viewModel.IdentificationNumber,
                        Name = viewModel.Name,
                        Email = viewModel.Email,
                        AccountCode = account.AccountCode
                    };
                    employeeRepository.Add(employee);
                    unitofWork.Commit();

                    response.IsSuccess = true;
                    return response;
                }
                catch(Exception e)
                {
                    throw new CustomException("A technical error occured, could not create employee");
                }
            }
            response.IsSuccess = false;
            return response;
        }

        public bool Edit(EmployeeViewModel viewModel, int userId)
        {
            try
            {
                Employee employee = employeeRepository.GetById(viewModel.EmployeeId);

                employee.CategoryId = viewModel.CategoryId;
                employee.Name = viewModel.Name;
                employee.IBAN = viewModel.IBAN;
                employee.IdentificationNumber = viewModel.IdentificationNumber;
                employee.Email = viewModel.Email;

                employeeRepository.Update(employee);

                unitofWork.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public bool Delete(int employeeId, int userId)
        {
            if (employeeId > 0)
            {
                var employee = employeeRepository.Get(x => x.EmployeeId == employeeId);

                employeeRepository.Delete(employee);

                unitofWork.Commit();

                return true;
            }
            else
            {
                throw new CustomException("Some technical error occured, this employee cannot be deleted.");
            }
        }

        public EmployeeViewModel GetEmployeeById(int employeeId, int userId)
        {
            int companyId = userRepository.GetById(userId).CompanyId;

            var employee = employeeRepository.Get(x => x.EmployeeId == employeeId && x.CompanyId == companyId);

            EmployeeViewModel employeeViewModel = new EmployeeViewModel
            {
                EmployeeId = employee.EmployeeId,
                CategoryId = employee.CategoryId,
                IBAN = employee.IBAN,
                Name = employee.Name,
                CompanyId = employee.CompanyId,
                IdentificationNumber = employee.IdentificationNumber,
                Email = employee.Email,
                EmployeeBalance = GetEmployeeBalance(employee.AccountCode)
            };

            return employeeViewModel;
        }

        public ResponseObject AddAdvance(AllocationViewModel viewModel, int userId)
        {
            //cash or bank account credit, salaries expense account debit

            ResponseObject response = new ResponseObject();

            var employee = employeeRepository.GetById((long)viewModel.CustomerId);

            GLHeader glHeader = new GLHeader
            {
                CompanyId = userRepository.GetById(userId).CompanyId,
                Description = "GIVING ADVANCE SALARY TO " + employee.Name,
                TransactionDate = viewModel.Date,
                TransactionType = "ADVANCE SALARY"
            };
            glHeaderRepository.Add(glHeader);

            unitofWork.Commit();
            try
            {
                GLDetail gLDetailDebit = new GLDetail
                {
                    GLHeaderId = glHeader.GLHeaderId,
                    AccountCode = employee.AccountCode,
                    Debit = viewModel.Amount,
                    Credit = 0,
                };
                glDetailRepository.Add(gLDetailDebit);

                GLDetail gLDetailCredit = new GLDetail
                {
                    GLHeaderId = glHeader.GLHeaderId,
                    AccountCode = cashBankRepository.GetById((long)viewModel.InvoiceHeaderId).AccountCode,   //note: InvoiceHeaderId is basically CashBankId here
                    Debit = 0,
                    Credit = viewModel.Amount,
                };
                glDetailRepository.Add(gLDetailCredit);

                unitofWork.Commit();
                response.IsSuccess = true;
                return response;
            }
            catch(Exception e)
            {
                glHeaderRepository.Delete(glHeader);
                unitofWork.Commit();
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public decimal GetEmployeeBalance(string accountCode)
        {
            var glDetails = glDetailRepository.GetAll().Where(x => x.AccountCode == accountCode);

            decimal debit = 0;
            decimal credit = 0;

            foreach (var glDetail in glDetails)
            {
                if (glDetail.Debit > 0)
                {
                    debit = debit + glDetail.Debit;
                }
                else if (glDetail.Credit > 0)
                {
                    credit = credit + glDetail.Credit;
                }
            }
            return debit - credit;
        }
    }
}















