﻿using AutoMapper;
using CloudAccounts.Adapter.Exceptions;
using CloudAccounts.Data;
using CloudAccounts.Infrastructure.Common;
using CloudAccounts.Infrastructure.Repository;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Adapter
{
    public class CashBankAdapter
    {
        private ICashBankRepository cashBankRepository;
        private IUserRepository userRepository;
        private IAccountRepository accountRepository;
        private IGLHeaderRepository glHeaderRepository;
        private IGLDetailRepository glDetailRepository;
        private IUnitofWork unitofWork;

        public CashBankAdapter()
        {
            unitofWork = new UnitOfWork(new DatabaseFactory());

            cashBankRepository = new CashBankRepository(unitofWork.instance);
            userRepository = new UserRepository(unitofWork.instance);
            accountRepository = new AccountRepository(unitofWork.instance);
            glHeaderRepository = new GLHeaderRepository(unitofWork.instance);
            glDetailRepository = new GLDetailRepository(unitofWork.instance);
        }

        public List<CashBankViewModel> GetAllAccounts(int userId)
        {
            var cashBankAccounts = cashBankRepository.GetAll().Where(x => x.CompanyId == userRepository.GetById(userId).CompanyId);

            List<CashBankViewModel> cashBankAccountViewModelList = new List<CashBankViewModel>();

            foreach (var cashBankAccount in cashBankAccounts)
            {
                CashBankViewModel cashViewModel = new CashBankViewModel
                {
                    AccountName = cashBankAccount.AccountName,
                    AccountType = cashBankAccount.AccountType,
                    AccountNumber = cashBankAccount.AccountNumber,
                    BankName = cashBankAccount.BankName,
                    Branch = cashBankAccount.Branch,
                    CashBankId = cashBankAccount.CashBankId,
                    CurrencyName = cashBankAccount.CurrencyName,
                    CurrencyRate = cashBankAccount.CurrencyRate,
                    IBAN = cashBankAccount.IBAN,
                    OpeningBalance = cashBankAccount.OpeningBalance,
                    OpeningBalanceDate = cashBankAccount.OpeningBalanceDate
                };

                cashBankAccountViewModelList.Add(cashViewModel);
            }

            if (cashBankAccountViewModelList != null)
            {
                return cashBankAccountViewModelList;
            }
            else
            {
                throw new CustomException("Your request cannot be completed, there has been a technical error.");
            }
        }


        public ResponseObject Create(CashBankViewModel model, int userId)
        {
            ResponseObject response = new ResponseObject();

            if (model != null)
            {
                int compId = userRepository.GetById(userId).CompanyId;

                try
                {
                    //open account
                    Account account = new Account
                    {
                        AccountName = model.AccountName,
                        AccountCode = ((accountRepository.GetAll().LastOrDefault() == null) ? "0001" :
                            (Convert.ToInt32(accountRepository.GetAll().LastOrDefault().AccountCode) + 1).ToString().PadLeft(4, '0')),
                        AccountType = (model.AccountType == "0001") ? "CASH" : "BANK",
                        CompanyId = compId,
                        Nature = "ASSET"
                    };
                    accountRepository.Add(account);

                    try
                    {
                        if (model.OpeningBalance > 0)
                        {
                            GLHeader glHeader = new GLHeader
                            {
                                CompanyId = compId,
                                Description = "Opening Balance for " + model.AccountName,
                                TransactionDate = model.OpeningBalanceDate ?? DateTime.Now,
                                TransactionType = "Opening Balance",
                            };
                            glHeaderRepository.Add(glHeader);
                            unitofWork.Commit();

                            try
                            {
                                GLDetail glDetailDebit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = account.AccountCode,
                                    Debit = model.OpeningBalance ?? 0,
                                    Credit = 0,
                                    Explanation = null
                                };
                                glDetailRepository.Add(glDetailDebit);

                                GLDetail glDetailCredit = new GLDetail
                                {
                                    GLHeaderId = glHeader.GLHeaderId,
                                    AccountCode = accountRepository.Get(x => x.AccountName == "Opening Balance").AccountCode,
                                    Debit = 0,
                                    Credit = model.OpeningBalance ?? 0,
                                    Explanation = null
                                };
                                glDetailRepository.Add(glDetailCredit);
                                unitofWork.Commit();
                            }
                            catch (Exception e)
                            {
                                accountRepository.Delete(account);
                                glHeaderRepository.Delete(glHeader);
                                throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                            }
                        }

                        //create cashBankAccount
                        CashBank cashBankAccount = new CashBank
                        {
                            AccountName = model.AccountName,
                            BankName = model.BankName,
                            CompanyId = compId,
                            Branch = model.Branch,
                            CurrencyName = model.CurrencyName,
                            IBAN = model.IBAN,
                            AccountNumber = model.AccountNumber,
                            CurrencyRate = model.CurrencyRate,
                            OpeningBalance = model.OpeningBalance,
                            OpeningBalanceDate = model.OpeningBalanceDate,
                            AccountType = model.AccountType,
                            AccountCode = account.AccountCode
                        };
                        cashBankRepository.Add(cashBankAccount);
                        unitofWork.Commit();
                    }
                    catch (Exception e)
                    {
                        accountRepository.Delete(account);
                        throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                    }
                }
                catch (Exception e)
                {
                    throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                }
            }

            return response;
        }

        public bool Edit(CashBankViewModel viewModel, int userId)
        {
            try
            {
                CashBank cashBank = cashBankRepository.GetById(viewModel.CashBankId);

                cashBank.AccountName = viewModel.AccountName;
                cashBank.BankName = viewModel.BankName;
                cashBank.Branch = viewModel.Branch;
                cashBank.AccountNumber = viewModel.AccountNumber;
                cashBank.IBAN = viewModel.IBAN;

                cashBankRepository.Update(cashBank);

                unitofWork.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public bool Delete(int cashBankId, int userId)
        {
            if (cashBankId > 0)
            {
                var cashBankAccount = cashBankRepository.Get(x => x.CashBankId == cashBankId);

                var tranactionsAgainstThisAccount = glDetailRepository.GetAll().Where(x => x.AccountCode == cashBankAccount.AccountCode);

                if (tranactionsAgainstThisAccount.Count() > 0)
                {
                    throw new CustomException("This account cannot be deleted, it has transactions against it.");
                }
                else if (tranactionsAgainstThisAccount.Count() == 0)
                {
                    cashBankRepository.Delete(cashBankAccount);

                    unitofWork.Commit();
                    return true;
                }
            }
            else
            {
                throw new CustomException("This prodcut cannot be deleted, it has invoices against it.");
            }
            return false;
        }

        public CashBankViewModel GetCashBankById(int cashBankId, int userId)
        {
            int companyId = userRepository.GetById(userId).CompanyId;

            var cashBankAccount = cashBankRepository.Get(x => x.CashBankId == cashBankId && x.CompanyId == companyId);

            var glDetails = glDetailRepository.GetAll().Where(x => x.AccountCode == cashBankAccount.AccountCode).ToList();

            List<GLDetailViewModel> glDetailList = new List<GLDetailViewModel>();

            if (glDetails.Count() > 0)
            {
                decimal balance = 0;

                foreach (var detail in glDetails)
                {
                    balance = balance + (detail.Debit - detail.Credit);

                    GLDetailViewModel gLDetailViewModel = new GLDetailViewModel
                    {
                        GLDetailId = detail.GLDetailId,
                        GLHeaderId = detail.GLHeaderId,
                        Debit = detail.Debit,
                        Credit = detail.Credit,
                        Explanation = detail.Explanation,
                        AccountCode = detail.AccountCode,
                        Date = detail.GLHeader.TransactionDate,
                        TransactionType = detail.GLHeader.TransactionType,
                        Balance = balance
                    };

                    glDetailList.Add(gLDetailViewModel);
                }
            }

            CashBankViewModel cashBankViewModel = new CashBankViewModel
            {
                AccountName = cashBankAccount.AccountName,
                AccountType = cashBankAccount.AccountType,
                AccountNumber = cashBankAccount.AccountNumber,
                BankName = cashBankAccount.BankName,
                Branch = cashBankAccount.Branch,
                CashBankId = cashBankAccount.CashBankId,
                CurrencyName = cashBankAccount.CurrencyName,
                CurrencyRate = cashBankAccount.CurrencyRate,
                IBAN = cashBankAccount.IBAN,
                OpeningBalance = cashBankAccount.OpeningBalance,
                OpeningBalanceDate = cashBankAccount.OpeningBalanceDate,
                AccountCode = cashBankAccount.AccountCode,
                GLDetails = glDetailList
            };

            return cashBankViewModel;
        }


        public ResponseObject AddGeneralVoucher(GLDetailViewModel model, int userId)
        {
            ResponseObject response = new ResponseObject();

            if (model != null)
            {
                int compId = userRepository.GetById(userId).CompanyId;

                try
                {
                    GLHeader glHeader = new GLHeader
                    {
                        CompanyId = compId,
                        Description = "General Voucher Entry",
                        TransactionDate = model.Date,
                        TransactionType = model.TransactionType,
                    };
                    glHeaderRepository.Add(glHeader);
                    unitofWork.Commit();

                    try
                    {
                        GLDetail glDetailDebit = new GLDetail
                        {
                            GLHeaderId = glHeader.GLHeaderId,
                            AccountCode = (model.Debit > 0) ? model.AccountCode : accountRepository.Get(x => x.AccountName == "General Expense").AccountCode,
                            Debit = (model.Debit > 0) ? model.Debit : model.Credit,
                            Credit = 0,
                            Explanation = model.Explanation
                        };
                        glDetailRepository.Add(glDetailDebit);

                        GLDetail glDetailCredit = new GLDetail
                        {
                            GLHeaderId = glHeader.GLHeaderId,
                            AccountCode = (model.Credit > 0) ? model.AccountCode : accountRepository.Get(x => x.AccountName == "General Expense").AccountCode,
                            Debit = 0,
                            Credit = (model.Debit > 0) ? model.Debit : model.Credit,
                            Explanation = model.Explanation
                        };
                        glDetailRepository.Add(glDetailCredit);
                        unitofWork.Commit();
                    }
                    catch (Exception e)
                    {
                        glHeaderRepository.Delete(glHeader);
                        throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                    }
                }
                catch (Exception e)
                {
                    throw new CustomException("Sorry! Your request could not be completed due to some technical error.");
                }
            }

            return response;
        }



    }
}
