﻿using CloudAccounts.Adapter;
using CloudAccounts.SharedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Services
{
    public interface IRegistrationService
    {
        UserInfo LoginSubmit(LoginViewModel model);

    }


    public class LoginService : ILoginService
    {
        private LoginAdapter loginAdapter;

        public LoginService()
        {
            loginAdapter = new LoginAdapter();
        }



        public UserInfo LoginSubmit(LoginViewModel model)
        {
            try
            {

                UserInfo info = loginAdapter.LoginSubmit(model);
                return info;

            }
            catch (Exception ex)
            {

                throw ex;

            }
        }

    }
}
