﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class ProductController : Controller
    {
        private IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var allProducts = productService.GetAllProducts(userId);

                    if (allProducts != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allProducts;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetProductById(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int productId = JsonConvert.DeserializeObject<int>(request.data);

                if (productId > 0)
                {
                    var product = productService.GetProductById(productId, SessionInfo.UserId);

                    if (product != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = product;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                ProductViewModel model = JsonConvert.DeserializeObject<ProductViewModel>(request.data);

                if (model != null)
                {
                    response = productService.Create(model, SessionInfo.UserId);
                    response.IsSuccess = true;
                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                ProductViewModel model = JsonConvert.DeserializeObject<ProductViewModel>(request.data);

                if (model != null)
                {
                    bool success = productService.Edit(model, SessionInfo.UserId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int productId = JsonConvert.DeserializeObject<int>(request.data);


                if (productId > 0)
                {
                    bool success = productService.Delete(productId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Index()
        {
            return View();
        }
    }
}
