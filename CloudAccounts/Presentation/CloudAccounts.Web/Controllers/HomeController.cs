﻿using CloudAccounts.SharedModel;
using CloudAccounts.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudAccounts.Web.Controllers
{
    public class HomeController : Controller
    {
        [AuthenticationFilter(Disabled = true)]
        public ActionResult Index()
        {
            return View();
        }

        [AuthenticationFilter(Disabled = true)]
        public JsonResult NullUserException()
        {

            ResponseObject response = new ResponseObject();

            response.IsSuccess = false;
            response.ResponseCode = "0002";
            response.ErrorMessage = "User session ended on server";

            return Json(response, JsonRequestBehavior.AllowGet);

        }

    }
}
