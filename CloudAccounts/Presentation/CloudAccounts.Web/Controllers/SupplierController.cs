﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CloudAccounts.Web.Filters;
using CloudAccounts.SharedModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CloudAccounts.Services;
using CloudAccounts.Data;


namespace CloudAccounts.Web.Controllers
{
    public class SupplierController : Controller
    {
        private ISupplierService supplierService;

        public SupplierController(ISupplierService supplierService)
        {
            this.supplierService = supplierService;
        }

        //
        // GET: /Supplier/

        public ActionResult Index()
        {
            //ResponseObject response = new ResponseObject();

            //try
            //{
            //    response.ResultData = GetAllCurrency();
            //}
            //catch (Exception ex)
            //{

            //}

            return View();
        }

        //[HttpPost]
        //[AuthenticationFilter(Disabled = true)]
        //public JsonResult Create(String data)
        //{
        //    ResponseObject response = new ResponseObject();

        //    try
        //    {
        //        RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
        //        SupplierViewModel model = JsonConvert.DeserializeObject<SupplierViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

        //        if (model != null)
        //        {
        //            int supplierID = supplierService.Create(model);
        //            if (supplierID != 0)
        //            {
        //                response.IsSuccess = true;
        //                response.ResultData = supplierID;
        //                //return GetSupplier(Convert.ToString(supplierID));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(false, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetSupplier(String data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                int supplierID = JsonConvert.DeserializeObject<int>(request.data);

                SupplierViewModel supplier = supplierService.GetSupplierByID(supplierID);

                if (supplier.supplierID != 0)
                {
                    response.IsSuccess = true;
                    response.ResultData = supplier;
                }

                //var supplier = supplierService.GetSupplierByID(supplierID);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllSuppliers()
        {
            ResponseObject response = new ResponseObject();
            try
            {
                response.ResultData = supplierService.GetAllSupplierLists();
                response.IsSuccess = true;

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //[HttpPost]
        //[AuthenticationFilter(Disabled = true)]
        //public JsonResult SaveCustomerSupplierNotes(string data)
        //{
        //    ResponseObject response = new ResponseObject();

        //    try
        //    {
        //        RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
        //        supplierHistoryViewModel model = JsonConvert.DeserializeObject<supplierHistoryViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

        //        if (model != null)
        //        {
        //            //supplierService.SaveNotes(model);
        //            //supplierService.SaveNotes(model);
        //            response.IsSuccess = true;
        //            response.ResultData = model;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response.IsSuccess = false;
        //        response.ErrorMessage = ex.Message;
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

    }
}
