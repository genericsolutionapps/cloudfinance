﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CloudAccounts.Web.Controllers
{
    public class ActivationController : Controller
    {
        private IRegistrationService registrationService;

        public ActivationController(IRegistrationService registrationService)
        {
            this.registrationService = registrationService;
        }

        [AuthenticationFilter(Disabled = true)]
        public ActionResult Index(string data)
        {

            byte[] tokenArray = Convert.FromBase64String(data);
            string tokenString = Encoding.ASCII.GetString(tokenArray);

            bool isActivated = registrationService.ActivateAccount(tokenString);

            Session["isActivated"] = isActivated;

            return View();
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult CheckEmailVerification()
        {
            ResponseObject response = new ResponseObject();
            try
            {
                var activated = registrationService.CheckEmailVerification(SessionInfo.UserId);

                if (activated != null && activated == true)
                {
                    response.IsSuccess = true;
                    response.ResultData = true;
                }
                else
                {
                    response.IsSuccess = true;
                    response.ResultData = false;
                }


            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }


}
