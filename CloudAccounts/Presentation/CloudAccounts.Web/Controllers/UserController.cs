﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class UserController : Controller
    {
        private IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var allUsers = userService.GetAllUsers(userId);

                    if (allUsers != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allUsers;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                UserViewModel model = JsonConvert.DeserializeObject<UserViewModel>(request.data);

                if (model != null)
                {
                    UserViewModel addedUser = userService.Create(model, Convert.ToInt32(SessionInfo.UserId));

                    //SessionInfo.SessionId = Session.SessionID;
                    //SessionInfo.UserId = info.UserId;
                    //SessionInfo.Email = info.Email;


                    if (addedUser != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = addedUser;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }




        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Details(string data)
        {

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var user = userService.GetUserById(userId);

                    if (user != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = user;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {

            //1.    data is a userVM
            //2.    against the id in this userVM, get the user from the repo and make the user = userVM and update database
            //3.    add user privliges in table.



            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                UserViewModel userVM = JsonConvert.DeserializeObject<UserViewModel>(request.data);

                if (userVM != null)
                {
                    bool user = userService.Edit(userVM);

                    if (user)
                    {
                        response.IsSuccess = true;
                        response.ResultData = user;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //public bool Edit(UserViewModel viewModel)
        //{
        //    try
        //    {
        //        User model = userRepository.GetById(viewModel.CompanyId);

        //        model.Name = viewModel.Name;
        //        model.Email = viewModel.Email;
        //        model.UserStatusTypeCode = viewModel.UserStatusTypeCode;
        //        model.Name = viewModel.CompanyName;
        //        model.OpenAddress = viewModel.Address;
        //        model.Phone = model.Phone;
        //        model.VNo = viewModel.TaxInformation;

        //        userRepository.Update(model);

        //        unitofWork.Commit();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}



    }
}











