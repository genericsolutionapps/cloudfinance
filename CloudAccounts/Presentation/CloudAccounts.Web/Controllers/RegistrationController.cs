﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class RegistrationController : Controller
    {
        private IRegistrationService registrationService;

        public RegistrationController(IRegistrationService registrationService)
        {
            this.registrationService = registrationService;
        }

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult RegisterUser(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                RegistrationViewModel model = JsonConvert.DeserializeObject<RegistrationViewModel>(request.data);

                if (model != null)
                {
                    UserInfo info = registrationService.Create(model);

                    SessionInfo.SessionId = Session.SessionID;
                    SessionInfo.UserId = info.UserId;
                    SessionInfo.Email = info.Email;


                    if (info != null && info.UserId > 0)
                    {
                        response.IsSuccess = true;
                        response.ResultData = info;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
