﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class InvoiceController : Controller
    {
        private IInvoiceService invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
        }

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                DateRangeViewModel dateRangeViewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                var allInvoices = invoiceService.GetAllInvoices(SessionInfo.UserId, dateRangeViewModel);

                if (allInvoices != null)
                {
                    response.IsSuccess = true;
                    response.ResultData = allInvoices;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllInvoiceDetails(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int usedId = JsonConvert.DeserializeObject<int>(request.data);

                if (usedId > 0)
                {
                    var invoiceDetails = invoiceService.GetAllInvoiceDetails(usedId);

                    if (invoiceDetails != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = invoiceDetails;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                InvoiceHeaderViewModel model = JsonConvert.DeserializeObject<InvoiceHeaderViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    response = invoiceService.Create(model, SessionInfo.UserId);

                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllInvoicesByCustomerId(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int customerId = JsonConvert.DeserializeObject<int>(request.data);

                if (customerId > 0)
                {
                    var allInvoicesByCustomerId = invoiceService.GetAllInvoicesByCustomerId(customerId);

                    if (allInvoicesByCustomerId != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allInvoicesByCustomerId;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetInvoiceById(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int invoiceHeaderId = JsonConvert.DeserializeObject<int>(request.data);

                if (invoiceHeaderId > 0)
                {
                    var invoice = invoiceService.GetInvoiceById(invoiceHeaderId);

                    if (invoice != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = invoice;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetInvoicesByProductId(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int productId = JsonConvert.DeserializeObject<int>(request.data);

                if (productId > 0)
                {
                    var invoicesByProductId = invoiceService.GetInvoicesByProductId(productId, SessionInfo.UserId);

                    if (invoicesByProductId != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = invoicesByProductId;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllInvoiceDetailsByInvoiceHeaderId(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int invoiceHeaderId = JsonConvert.DeserializeObject<int>(request.data);

                if (invoiceHeaderId > 0)
                {
                    var allInvoiceDetailsByInvoiceHeaderId = invoiceService.GetAllInvoiceDetailsByInvoiceHeaderId(invoiceHeaderId);

                    if (allInvoiceDetailsByInvoiceHeaderId != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allInvoiceDetailsByInvoiceHeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllAllocationsByInvoiceHeaderId(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int invoiceHeaderId = JsonConvert.DeserializeObject<int>(request.data);

                if (invoiceHeaderId > 0)
                {
                    var allAllocationsByInvoiceHeaderId = invoiceService.GetAllAllocationsByInvoiceHeaderId(invoiceHeaderId);

                    if (allAllocationsByInvoiceHeaderId != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allAllocationsByInvoiceHeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetBalanceByMonth(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var balanceByMonth = invoiceService.GetBalanceByMonth(userId);

                    if (balanceByMonth != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = balanceByMonth;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllDelayedInvoices(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var delayedInvoicesByMonth = invoiceService.GetAllDelayedInvoices(userId);

                    if (delayedInvoicesByMonth != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = delayedInvoicesByMonth;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult AddAllocation(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                AllocationViewModel model = JsonConvert.DeserializeObject<AllocationViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    response = invoiceService.AddAllocation(model, SessionInfo.UserId);

                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int invocieHeaderId = JsonConvert.DeserializeObject<int>(request.data);

                if (invocieHeaderId > 0)
                {
                    response = invoiceService.Delete(invocieHeaderId);

                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                InvoiceHeaderViewModel model = JsonConvert.DeserializeObject<InvoiceHeaderViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    bool success = invoiceService.Edit(model, SessionInfo.UserId);
                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //VAT Report
        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetVATByMonth(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int[] parameters = JsonConvert.DeserializeObject<int[]>(request.data);

                if (parameters.Length > 0)
                {
                    var VATByMonth = invoiceService.GetVATByMonth(SessionInfo.UserId, parameters[0], parameters[1]);

                    if (VATByMonth != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = VATByMonth;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //Cash Bank Report
        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllAllocations(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                var dateRangeViewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (dateRangeViewModel != null)
                {
                    var allocations = invoiceService.GetAllAllocations(SessionInfo.UserId, dateRangeViewModel);

                    if (allocations != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allocations;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}


