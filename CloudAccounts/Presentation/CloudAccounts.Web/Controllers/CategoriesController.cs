﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class CategoriesController : Controller
    {
        private ICategoriesService categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var allCategories = categoriesService.GetAllCategories(userId);

                    if(allCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CategoryViewModel model = JsonConvert.DeserializeObject<CategoryViewModel>(request.data);

                if (model != null)
                {
                    bool success = categoriesService.Edit(model);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CategoryViewModel model = JsonConvert.DeserializeObject<CategoryViewModel>(request.data);

                if (model != null)
                {
                    CategoryViewModel category = categoriesService.Create(model);

                    if (category.CategoryId != 0) 
                    {
                        response.IsSuccess = true;
                        response.ResultData = category;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int categoryId = JsonConvert.DeserializeObject<int>(request.data);


                if (categoryId > 0)
                {
                    bool success = categoriesService.Delete(categoryId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllSalesCategories(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                DateRangeViewModel viewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (viewModel.UserId > 0)
                {
                    var salesCategories = categoriesService.GetAllSalesCategories(viewModel);

                    if (salesCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = salesCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllProductCategories(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                DateRangeViewModel viewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (viewModel.UserId > 0)
                {
                    var productCategories = categoriesService.GetCategoriesByType(viewModel, "0004");

                    if (productCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = productCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllExpenseCategories(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                DateRangeViewModel viewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (viewModel.UserId > 0)
                {
                    var expenseCategories = categoriesService.GetCategoriesByType(viewModel, "0002");

                    if (expenseCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = expenseCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllBusinessPartnerCategories(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                DateRangeViewModel viewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (viewModel.UserId > 0)
                {
                    var businessPartnerCategories = categoriesService.GetCategoriesByType(viewModel, "0006");

                    if (businessPartnerCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = businessPartnerCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAllCustomerCategories(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                DateRangeViewModel viewModel = JsonConvert.DeserializeObject<DateRangeViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (viewModel.UserId > 0)
                {
                    var customerCategories = categoriesService.GetAllCustomerCategories(viewModel);

                    if (customerCategories != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = customerCategories;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
