﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using Newtonsoft.Json.Converters;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private IEmployeeService employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var employees = employeeService.GetAllEmployees(userId);

                    if (employees != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = employees;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                EmployeeViewModel model = JsonConvert.DeserializeObject<EmployeeViewModel>(request.data);

                if (model != null)
                {
                    response = employeeService.Create(model, SessionInfo.UserId);
                    response.IsSuccess = true;
                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                EmployeeViewModel model = JsonConvert.DeserializeObject<EmployeeViewModel>(request.data);

                if (model != null)
                {
                    bool success = employeeService.Edit(model, SessionInfo.UserId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int employeeId = JsonConvert.DeserializeObject<int>(request.data);


                if (employeeId > 0)
                {
                    bool success = employeeService.Delete(employeeId, SessionInfo.UserId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetEmployeeById(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int employeeId = JsonConvert.DeserializeObject<int>(request.data);

                if (employeeId > 0)
                {
                    var employee = employeeService.GetEmployeeById(employeeId, SessionInfo.UserId);

                    if (employee != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = employee;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult AddAdvance(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                AllocationViewModel model = JsonConvert.DeserializeObject<AllocationViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    response = employeeService.AddAdvance(model, SessionInfo.UserId);

                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}






