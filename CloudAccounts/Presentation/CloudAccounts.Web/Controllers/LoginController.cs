﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class LoginController : Controller
    {
        private ILoginService loginService;

        public LoginController(ILoginService loginService)
        {
            this.loginService = loginService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult LoginSubmit(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                LoginViewModel model = JsonConvert.DeserializeObject<LoginViewModel>(request.data);

                if (model != null)
                {
                    UserInfo info = loginService.LoginSubmit(model);

                    SessionInfo.SessionId = Session.SessionID;
                    SessionInfo.UserId = info.UserId;
                    SessionInfo.Email = info.Email;

                    if (info != null && info.UserId > 0)
                    {
                        response.IsSuccess = true;
                        response.ResultData = info;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult ForgotPassword(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                LoginViewModel model = JsonConvert.DeserializeObject<LoginViewModel>(request.data);

                if (model != null)
                {
                    var result = loginService.ForgotPassword(model);


                    if (result == true)
                    {
                        response.IsSuccess = true;
                        response.ResultData = true;
                    }
                    else
                    {
                        response.IsSuccess = true;
                        response.ResultData = false;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [AuthenticationFilter(Disabled = true)]
        public JsonResult LogoutUser(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                SessionInfo.KillSession();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
