﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CloudAccounts.Services;
using CloudAccounts.Web.Filters;
using CloudAccounts.SharedModel;
using Newtonsoft.Json;


namespace CloudAccounts.Web.Controllers
{
    public class ExpenseListController : Controller
    {
        private IExpenseListService expenseListService;

        public ExpenseListController(IExpenseListService expenseList)
        {
            this.expenseListService = expenseList;
        }

        //
        // GET: /ExpenseList/

        public ActionResult Index()
        {
            return View();
        }

        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(String data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                ExpenseListViewModel model = JsonConvert.DeserializeObject<ExpenseListViewModel>(request.data);

                if (model != null)
                {
                    ExpenseListViewModel expenseList = expenseListService.Create(model);

                    if (expenseList.ID != 0)
                    {
                        response.IsSuccess = true;
                        response.ResultData = expenseList;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
