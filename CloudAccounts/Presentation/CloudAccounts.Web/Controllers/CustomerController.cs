﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomerService customerService;

        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                string businessPartnerType = JsonConvert.DeserializeObject<string>(request.data);

                if (businessPartnerType != null && businessPartnerType != "")
                {
                    var businessPartners = customerService.GetAllBusinessPartnersByType(SessionInfo.UserId);

                    if (businessPartners != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = businessPartners;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetAuthorizedPersonsByCustomerId(string data)
        {

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int customerId = JsonConvert.DeserializeObject<int>(request.data);

                if (customerId > 0)
                {
                    var allAuthotizedPersons = customerService.GetAuthorizedPersonsByCustomerId(customerId);

                    if (allAuthotizedPersons != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = allAuthotizedPersons;
                    }

                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CustomerViewModel model = JsonConvert.DeserializeObject<CustomerViewModel>(request.data);

                if (model != null)
                {
                    response = customerService.Create(model, SessionInfo.UserId);

                    //response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;

            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);
                CustomerViewModel model = JsonConvert.DeserializeObject<CustomerViewModel>(request.data);
                if (model != null)
                {
                    bool success = customerService.Edit(model);
                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int customerId = JsonConvert.DeserializeObject<int>(request.data);

                if (customerId > 0)
                {
                    response = customerService.Delete(customerId);

                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }





        //public ActionResult Index()
        //{
        //    return View();
        //}

    }
}
