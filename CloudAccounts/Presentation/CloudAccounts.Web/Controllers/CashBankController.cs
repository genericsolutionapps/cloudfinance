﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class CashBankController : Controller
    {
        private ICashBankService cashBankService;

        public CashBankController(ICashBankService cashBankService)
        {
            this.cashBankService = cashBankService;
        }

        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    var cashBankAccounts = cashBankService.GetAllAccounts(userId);

                    if (cashBankAccounts != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = cashBankAccounts;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Create(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CashBankViewModel model = JsonConvert.DeserializeObject<CashBankViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    response = cashBankService.Create(model, SessionInfo.UserId);
                    response.IsSuccess = true;
                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CashBankViewModel model = JsonConvert.DeserializeObject<CashBankViewModel>(request.data);

                if (model != null)
                {
                    bool success = cashBankService.Edit(model, SessionInfo.UserId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Delete(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int cashBankId = JsonConvert.DeserializeObject<int>(request.data);


                if (cashBankId > 0)
                {
                    bool success = cashBankService.Delete(cashBankId, SessionInfo.UserId);

                    if (success)
                    {
                        response.IsSuccess = true;
                        response.ResultData = success;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult AddGeneralVoucher(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                GLDetailViewModel model = JsonConvert.DeserializeObject<GLDetailViewModel>(request.data, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                if (model != null)
                {
                    response = cashBankService.AddGeneralVoucher(model, SessionInfo.UserId);
                    response.IsSuccess = true;
                    response.ResultData = response.IsSuccess;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult GetCashBankById(string data)
        {
            ResponseObject response = new ResponseObject();
            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int cashBankId = JsonConvert.DeserializeObject<int>(request.data);

                if (cashBankId > 0)
                {
                    var cashBankAccount = cashBankService.GetCashBankById(cashBankId, SessionInfo.UserId);

                    if (cashBankAccount != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = cashBankAccount;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}






