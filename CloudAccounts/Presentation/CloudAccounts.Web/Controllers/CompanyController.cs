﻿using CloudAccounts.Services;
using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using CloudAccounts.Web.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CloudAccounts.Web.Controllers
{
    public class CompanyController : Controller
    {

        private ICompanyService companyService;

        public CompanyController(ICompanyService companyService)
        {
            this.companyService = companyService;
        }



        [HttpGet]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Index(string data)
        {


            //From the company repo, get the company against the argument of this method
            //Bind the company object to the response object then send the responce object to the front end and render the properties in the
                //response object on the frontend

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                int userId = JsonConvert.DeserializeObject<int>(request.data);

                if (userId > 0)
                {
                    CompanyViewModel model = companyService.GetCompanyByUserId(userId);

                    if (model != null)
                    {
                        response.IsSuccess = true;
                        response.ResultData = model;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AuthenticationFilter(Disabled = true)]
        public JsonResult Edit(string data)
        {

            ResponseObject response = new ResponseObject();

            try
            {
                RequestObject request = JsonConvert.DeserializeObject<RequestObject>(data);

                CompanyViewModel model = JsonConvert.DeserializeObject<CompanyViewModel>(request.data);

                if (model != null)
                {
                    bool success = companyService.Edit(model);

                    if (success)
                    {
                        response.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }


            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Index()
        {
            return View();
        }

    }
}
