using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using CloudAccounts.Services;
namespace CloudAccounts.Web
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers


            container.RegisterType<ILoginService, LoginService>();
            container.RegisterType<IRegistrationService, RegistrationService>();
            container.RegisterType<ICompanyService, CompanyService>();
            container.RegisterType<ICategoriesService, CategoriesService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IInvoiceService, InvoiceService>();
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<IExpenseListService, ExpenseListService>();
            container.RegisterType<ISupplierService, SupplierService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<ICashBankService, CashBankService>();
            container.RegisterType<IEmployeeService, EmployeeService>();


            // e.g. container.RegisterType<ITestService, TestService>();    
            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }
    }
}