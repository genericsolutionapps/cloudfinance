﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudAccounts.Web.Common
{
    public static class SessionInfo
    {
        public static int UserId
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Session["User_ID"]);
            }
            set
            {
                HttpContext.Current.Session["User_ID"] = value;
            }
        }

        public static string SessionId
        {
            get
            {
                return HttpContext.Current.Session["SessionId"].ToString();
            }
            set
            {
                HttpContext.Current.Session["SessionId"] = value;
            }
        }

        public static string Email
        {
            get
            {
                return HttpContext.Current.Session["Email"].ToString();
            }
            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }


        public static void KillSession() 
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}