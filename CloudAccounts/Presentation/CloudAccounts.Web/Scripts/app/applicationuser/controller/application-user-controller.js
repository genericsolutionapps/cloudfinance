﻿FSTApp.controller("ApplicationUserController", ['$scope', '$rootScope', '$timeout', 'ApplicationUserService', '$sessionStorage', 'ngDialog', 'EmployeeService', 'RoleService',
    function ($scope, $rootScope, $timeout, ApplicationUserService, $sessionStorage, ngDialog, EmployeeService, RoleService) {

        $scope.ApplicationUserViewModel = {

            User_ID: 0,
            Employee_ID: 0,
            User_Name: null,
            Password: null,
            Role_ID: 0,
            Is_Active: 'Active'

        }

        $scope.EmployeeViewModel = {

            Employee_ID: 0,
            Employee_Name: null,
            Is_Active: 'Active'
        }

        $scope.roleViewModel = {
            Role_ID: 0,
            Role_Name: null,
            Is_Active: 'Active'
        }

       

        $scope.editcheck = false;
        $scope.listapplicationuser = [];

        $scope.employeelist = [];
        $scope.selectedEmployee = null;
        $scope.rolelist = [];
        $scope.selectedRole = null;


        var loademployee = function () {

            EmployeeService.getallemployee(function (response) {

                var employeelist = response;

                angular.forEach(employeelist, function (value, key) {

                    $scope.employeelist.push(value);
                });
            });
        };

        var loadrole = function () {

            RoleService.getallrole(function (response) {

                var rolelist = response;

                angular.forEach(rolelist, function (value, key) {

                    $scope.rolelist.push(value);
                });
            });
        };

        var loadapplicationuser = function () {

            $scope.listapplicationuser = [];

            ApplicationUserService.getallapplicationuser(function (response) {

                var applicationuserlist = response;

                angular.forEach(applicationuserlist, function (value, key) {

                    $scope.listapplicationuser.push(value);

                });

            });

        };


        $scope.cancel = function () {

            $scope.ApplicationUserViewModel = {

                User_ID: 0,
                Employee_ID: 0,
                User_Name: null,
                Password: null,
                Role_ID: 0,
                Is_Active: true

            }

            $scope.selectedEmployee = null;
            $scope.selectedRole = null;
            $scope.editcheck = false;

        };


        $scope.save = function () {

            if ($scope.ApplicationUserViewModel.Is_Active == 'Active')
                $scope.ApplicationUserViewModel.Is_Active = true;
            else
                $scope.ApplicationUserViewModel.Is_Active = false;

            if ($scope.selectedEmployee != null && $scope.selectedEmployee != undefined && $scope.selectedRole != null && $scope.selectedRole != undefined) {

                $scope.ApplicationUserViewModel.Employee_ID = $scope.selectedEmployee;
                $scope.ApplicationUserViewModel.Role_ID = $scope.selectedRole;
            }

            //$scope.StatusViewModel.Code = '';

            if ($scope.ApplicationUserViewModel.User_Name == null || $scope.ApplicationUserViewModel.User_Name == undefined) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {
                    ApplicationUserService.createapplicationuser($scope.ApplicationUserViewModel, function (response) {

                        if (response == true)
                            alert('User has been added');

                        $scope.cancel();
                        $scope.selectedEmployee = null;
                        $scope.selectedRole = null;
                        loadapplicationuser();
                    });
                }
                else {

                    ApplicationUserService.editaplicationuser($scope.ApplicationUserViewModel, function (response) {

                        if (response == true)
                            alert('Status has been edit');

                        $scope.cancel();
                        $scope.selectedEmployee = null;
                        $scope.selectedRole = null;
                        $scope.editcheck = false;
                        loadapplicationuser();
                    });
                }
            }
        };


        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.ApplicationUserViewModel = item;
            $scope.selectedEmployee = '' + item.Employee_ID + '';
            $scope.selectedRole = '' + item.Role_ID + '';

        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loademployee();
            loadrole();
            loadapplicationuser();

            //$('#statuslist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $('#applicationuserlist').DataTable();
        });


}]);