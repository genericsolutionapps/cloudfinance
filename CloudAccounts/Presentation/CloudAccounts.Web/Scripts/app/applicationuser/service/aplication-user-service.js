﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("ApplicationUserService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {

        this.getallemployee = function (callback) {

            var request = commonobjects.requestobject;
            ServiceManager.execute(Resource.getallemployee(), function (response) {

                callback(response);
            });
        }

        this.getallrole = function (callback) {

            var request = commonobjects.requestobject;
            ServiceManager.execute(Resource.getallrole(), function (response) {

                callback(response);
            });
        }

        this.getallapplicationuser = function (callback) {

            var request = commonobjects.requestobject;

            ServiceManager.execute(Resource.getallapplicationuser(), function (response) {

                callback(response);

            });

        };

        this.createapplicationuser = function (model, file, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.createapplicationuser({ data: request }), function (response) {

                callback(response);

            });

        };

        this.editapplicationuser = function (model, file, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.editapplicationuser({ data: request }), function (response) {

                callback(response);

            });



        };

    }]);