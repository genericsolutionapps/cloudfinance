﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorProductService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', 'Upload', '$http', '$rootScope',
    function ($http, ServiceManager, Resource, commonobjects, Upload, $http, $rootScope) {


        this.getallfirstcategorymaster = function (callback) {


            ServiceManager.execute(Resource.getallfirstcategorymaster(), function (response) {

                callback(response);

            });

        };

        this.getallcolors = function (id, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(id);

            ServiceManager.execute(Resource.getallcolor({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.getallcategorymasterbyId = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.getallcategorymasterbyId({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.getallcategoryvendor = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.getallcategoryvendorwithoutheirarchy({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.getallsizes = function (id, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(id);

            ServiceManager.execute(Resource.getallsize({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.getallpacks = function (id, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(id);

            ServiceManager.execute(Resource.getallpack({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.createproduct = function (model, files, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            $rootScope.startSpin();

            //file.upload = Upload.upload({
            //    url: 'Product/CreateProduct',
            //    data: { data: JSON.stringify(request), files: files },
            //});

            //file.upload.then(function (response) {

            //    callback(response.data.ResultData);

            //}, function (response) {


            //}, function (evt) {

            //});



            $http({
                url: "/Product/CreateProduct",
                method: "POST",
                headers: { "Content-Type": undefined },
                transformRequest: function (data) {

                    var formData = new FormData();

                    formData.append("data", JSON.stringify(request));

                    for (var i = 0; i < files.length; i++) {

                        formData.append("files[" + i + "]", data.files[i]);
                    }
                    return formData;

                },
                data: { data: JSON.stringify(request), files: files }
            }).
            then(function successCallback(response) {

                $rootScope.stopSpin();
                callback(response.data.ResultData);

            }, function errorCallback(response) {

                $rootScope.stopSpin();
            });


        };

        //this.editcolor = function (model, file, callback) {

        //    var request = commonobjects.requestobject;

        //    request.data = JSON.stringify(model);

        //    file.upload = Upload.upload({

        //        url: 'Color/EditColor',

        //        data: { data: JSON.stringify(request), file: file },
        //    });

        //    file.upload.then(function (response) {

        //        callback(response.data.ResultData);

        //    }, function (response) {


        //    }, function (evt) {

        //    });

        //};


    }]);