﻿FSTApp.controller("VendorProductController", ['$scope', '$rootScope', 'VendorProductService', '$timeout', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, VendorProductService, $timeout, $sessionStorage, ngDialog) {

        $scope.ProductViewModel = {

            ProductId: 0,
            VendorId: 0,
            Status: false,
            InventoryStatus: null,
            StyleNo: null,
            ProductType: null,
            Size: 0,
            Pack: 0,
            MinimumQty: 0,
            CategoryVendorId: 0,
            FirstCategoryMasterId: 0,
            SecondCategoryMasterId: 0,
            ThirdCategoryMasterId: 0,
            FourthCategoryMasterId: 0,
            Title: null,
            Description: null,
            Terms: null,
            Price: 0,
            DiscountPerc: 0,
            DiscountAmount: 0,
            DiscountDateFrom: null,
            DiscountTimeFrom: null,
            DiscountDateTo: null,
            DiscountTimeto: null,
            AvailableDateFrom: null,
            AvailableTimeFrom: null,
            AvailableDateTo: null,
            AvailableTimeto: null,
            SortingNo: null,
            Shop: null,
            Material: null,
            ModelFit: null,
            Style: null,
            MidIn: null,
            Others: null,
            Length: null,
            ColorSwatch: false,
            productcolorlist: [],
            productimagelist: []

        };


        $scope.editcheck = false;

        $scope.listproducts = [];
        $scope.categorylist1 = [];
        $scope.categorylist2 = [];
        $scope.categorylist3 = [];
        $scope.categorylist4 = [];
        $scope.listcategoryvendor = [];
        $scope.listcolors = [];
        $scope.sizecolorlist = [];
        $scope.sizepacklist = [];
        $scope.packlist = [];

        $scope.selectedcategory1 = null;
        $scope.selectedcategory2 = null;
        $scope.selectedcategory3 = null;
        $scope.selectedcategory4 = null;
        $scope.selectedsizecolorlist = null;
        $scope.selectedsizepacklist = null;
        $scope.selectedpacklist = null;
        $scope.selectedstatus = null;
        $scope.selectedcategoryvendor = null;
        $scope.selectedcolor = [];
        $scope.selectedinventorystatus = null;

        $scope.files = [];
        $scope.filepreview = [];

        $scope.$watch('files', function () {

            var check = $scope.files;

            angular.forEach($scope.files, function (value, key) {

                var reader = new FileReader();

                reader.onload = function (loadEvent) {

                    $scope.$apply(function () {

                        $scope.filepreview.push({ url: loadEvent.target.result, file: value });

                    });
                }

                reader.readAsDataURL(value);

            });


        });

        $scope.deleteImage = function (option) {

            var indexfile = $scope.files.indexOf(option.file);
            var indexfilepreview = $scope.filepreview.indexOf(option);

            $scope.files.splice(indexfile, 1);
            $scope.filepreview.splice(indexfilepreview, 1);

        }

        var loadcolors = function () {

            $scope.listcolors = [];

            VendorProductService.getallcolors($sessionStorage.VendorId, function (response) {

                var colorlist = response;

                angular.forEach(colorlist, function (value, key) {

                    $scope.listcolors.push({ ColorId: value.ColorId, Name: value.Name, pic: "<img src='/" + value.Image + "'  />", selected: false });
                    // $scope.listcolors.push({ id: value.ColorId, label: value.Name });
                });

            });

        };

        var loadmastercategories = function () {

            $scope.categorylist1 = [];

            VendorProductService.getallfirstcategorymaster(function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist1.push(value);

                    });

                }

            });

        };

        var loadvendorcategory = function () {

            $scope.listcategoryvendor = [];

            VendorProductService.getallcategoryvendor($sessionStorage.VendorId, function (response) {

                var listcategoryvendor = response;

                angular.forEach(listcategoryvendor, function (value, key) {

                    $scope.listcategoryvendor.push(value);

                });

            });

        };

        var loadsize = function () {

            $scope.sizecolorlist = [];
            $scope.sizepacklist = [];

            VendorProductService.getallsizes($sessionStorage.VendorId, function (response) {

                var listsize = response;

                angular.forEach(listsize, function (value, key) {

                    $scope.sizecolorlist.push(value);
                    $scope.sizepacklist.push(value);

                });

            });

        };

        var loadpacks = function () {

            $scope.packlist = [];

            VendorProductService.getallpacks($sessionStorage.VendorId, function (response) {

                var listsize = response;

                angular.forEach(listsize, function (value, key) {

                    $scope.packlist.push(value);

                });

            });

        };

        $scope.categorychange1 = function () {

            $scope.categorylist2 = [];

            VendorProductService.getallcategorymasterbyId($scope.selectedcategory1, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist2.push(value);

                    });

                }

            });


        };

        $scope.categorychange2 = function () {

            $scope.categorylist3 = [];

            VendorProductService.getallcategorymasterbyId($scope.selectedcategory2, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist3.push(value);

                    });

                }

            });


        };

        $scope.categorychange3 = function () {

            $scope.categorylist4 = [];

            VendorProductService.getallcategorymasterbyId($scope.selectedcategory3, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist4.push(value);

                    });

                }

            });


        };

        $scope.toggletabs = function (value) {

            if (value == 'size') {

                $scope.ProductViewModel.ProductType = 'Size';
            }
            else {

                $scope.ProductViewModel.ProductType = 'Pack';
            }

        };

        $scope.toggleswatch = function (value) {

            if (value == 'yes') {

                $scope.ProductViewModel.ColorSwatch = true;
            }
            else {

                $scope.ProductViewModel.ColorSwatch = false;
            }

        };

        $scope.cancel = function () {

            $scope.ProductViewModel = {

                ProductId: 0,
                VendorId: 0,
                Status: false,
                InventoryStatus: null,
                StyleNo: null,
                ProductType: null,
                Size: 0,
                Pack: 0,
                MinimumQty: 0,
                CategoryVendorId: 0,
                FirstCategoryMasterId: 0,
                SecondCategoryMasterId: 0,
                ThirdCategoryMasterId: 0,
                FourthCategoryMasterId: 0,
                Title: null,
                Description: null,
                Terms: null,
                Price: 0,
                DiscountPerc: 0,
                DiscountAmount: 0,
                DiscountDateFrom: null,
                DiscountTimeFrom: null,
                DiscountDateTo: null,
                DiscountTimeto: null,
                AvailableDateFrom: null,
                AvailableTimeFrom: null,
                AvailableDateTo: null,
                AvailableTimeto: null,
                SortingNo: null,
                Shop: null,
                Material: null,
                ModelFit: null,
                Style: null,
                MidIn: null,
                Others: null,
                ColorSwatch: false,
                productcolorlist: [],
                productimagelist: []


            };

            $scope.selectedcategory1 = null;
            $scope.selectedcategory2 = null;
            $scope.selectedcategory3 = null;
            $scope.selectedcategory4 = null;

            $scope.selectedcategoryvendor = null;

            $scope.selectedcolor = [];

            $scope.editcheck = false;

        };

        $scope.create = function () {

            $scope.ProductViewModel.VendorId = $sessionStorage.VendorId;

            if ($scope.selectedcategory1 != null && $scope.selectedcategory1 != undefined) {
                $scope.ProductViewModel.FirstCategoryMasterId = $scope.selectedcategory1;

            }

            if ($scope.selectedcategory2 != null && $scope.selectedcategory2 != undefined) {
                $scope.ProductViewModel.SecondCategoryMasterId = $scope.selectedcategory2;

            }

            if ($scope.selectedcategory3 != null && $scope.selectedcategory3 != undefined) {
                $scope.ProductViewModel.ThirdCategoryMasterId = $scope.selectedcategory3;

            }

            if ($scope.selectedcategory4 != null && $scope.selectedcategory4 != undefined) {
                $scope.ProductViewModel.FourthCategoryMasterId = $scope.selectedcategory4;

            }

            if ($scope.selectedstatus == true) {

                $scope.ProductViewModel.Status = true;

            }
            else {

                $scope.ProductViewModel.Status = false;
            }



            if ($scope.selectedinventorystatus == 'In Stock') {

                $scope.ProductViewModel.InventoryStatus = 'In';

            }
            else {
                $scope.ProductViewModel.InventoryStatus = 'Out';

            }

            if ($scope.selectedcolor.Length != 0) {

                angular.forEach($scope.selectedcolor, function (value, key) {

                    $scope.ProductViewModel.productcolorlist.push({ ProductColorId: 0, ProductId: 0, ColorId: value.ColorId, ColorName: value.Name });

                });
            }

            if ($scope.selectedpacklist != null && $scope.selectedpacklist != undefined) {

                $scope.ProductViewModel.Pack = $scope.selectedpacklist;
            }

            if ($scope.selectedsizepacklist != null && $scope.selectedsizepacklist != undefined) {

                $scope.ProductViewModel.ProductType = "PackType";
                $scope.ProductViewModel.Size = $scope.selectedsizepacklist;

            }

            if ($scope.selectedsizecolorlist != null && $scope.selectedsizecolorlist != undefined) {

                $scope.ProductViewModel.ProductType = "ColorType";
                $scope.ProductViewModel.Size = $scope.selectedsizecolorlist;

            }

            if ($scope.selectedcategoryvendor != null && $scope.selectedcategoryvendor != undefined) {

                $scope.ProductViewModel.CategoryVendorId = $scope.selectedcategoryvendor;
            }


            if ($scope.form1.$invalid == false && $scope.form2.$invalid == false) {

                if ($scope.editcheck == false) {

                    VendorProductService.createproduct($scope.ProductViewModel, $scope.files, function (response) {

                        if (response == true) {

                            ngDialog.open({
                                template: '<p>Product "<strong>' + $scope.ProductViewModel.Title + '</strong>" has been added to your collection</p>',
                                plain: true,
                                className: 'ngdialog-theme-default'
                            });

                        }

                        $scope.cancel();

                        loadcolors();
                        loadmastercategories();
                        loadvendorcategory();
                        loadsize();
                        loadpacks();
                    });
                }
            }
            else {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }

        };


        $timeout(function () {

            loadcolors();
            loadmastercategories();
            loadvendorcategory();
            loadsize();
            loadpacks();
        });

    }]);