﻿/// <reference path="../../common/models/common-objects.js" />

PanelApp.service("CloneFolderService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {



    this.submitcall = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.submitcall({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.getcall = function (callback) {

        ServiceManager.execute(Resource.getcall(), function (response) {

            callback(response);

        });

    };

   
}]);