﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("DesignationService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {



        this.getalldesignation = function (callback) {

            var request = commonobjects.requestobject;

            ServiceManager.execute(Resource.getalldesignation(), function (response) {

                callback(response);

            });

        };


    }]);