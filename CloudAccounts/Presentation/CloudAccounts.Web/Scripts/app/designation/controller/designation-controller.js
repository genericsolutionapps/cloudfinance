﻿FSTApp.controller("DesignationController", ['$scope', '$rootScope', '$timeout', 'DesignationService', '$sessionStorage',
    function ($scope, $rootScope, $timeout, DesignationService, $sessionStorage) {

        $scope.DesignationViewModel = {
            Designation_ID: 0,
            Designation_Name: null,
            Is_Active: 'Active'


        };

        $scope.editcheck = false;

        $scope.listdesignation = [];

        var loaddesignation = function () {

            $scope.listdesignation = [];

            DesignationService.getalldesignation(function (response) {

                var designationlist = response;

                angular.forEach(designationlist, function (value, key) {

                    $scope.listdesignation.push(value);
                });

            });

        };


        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.DesignationViewModel = item;

        };


        $timeout(function () {

            loaddesignation();

            //$('#statuslist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $("#designationlist").DataTable();

        });

}]);