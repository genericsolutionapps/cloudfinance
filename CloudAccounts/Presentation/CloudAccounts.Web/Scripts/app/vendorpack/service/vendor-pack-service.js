﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorPackService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', 'Upload', function ($http, ServiceManager, Resource, commonobjects, Upload) {


    this.getallpacks = function (id, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(id);

        ServiceManager.execute(Resource.getallpack({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.createpack = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createpack({ data: request }), function (response) {

            callback(response);

        });

        
    };

    this.editpack = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editpack({ data: request }), function (response) {

            callback(response);

        });
    };


}]);