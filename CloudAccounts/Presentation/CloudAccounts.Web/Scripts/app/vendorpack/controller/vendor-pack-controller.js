﻿FSTApp.controller("VendorPackController", ['$scope', '$rootScope', 'VendorPackService', '$timeout', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, VendorPackService, $timeout, $sessionStorage, ngDialog) {

        $scope.PackViewModel = {

            PackId: 0,
            VendorId: 0,
            Name: null,
            Pack1: null,
            Pack2: null,
            Pack3: null,
            Pack4: null,
            Pack5: null,
            Pack6: null,
            Pack7: null,
            Pack8: null,
            Pack9: null,
            Pack10: null,
            Active: false,
            IsDefault: false

        };

        $scope.editcheck = false;

        $scope.listpacks = [];

        var loadpacks = function () {

            $scope.listpacks = [];

            VendorPackService.getallpacks($sessionStorage.VendorId, function (response) {

                var packlist = response;

                angular.forEach(packlist, function (value, key) {

                    $scope.listpacks.push(value);

                });

            });

        };

        $scope.cancel = function () {

            $scope.PackViewModel = {

                PackId: 0,
                VendorId: 0,
                Name: null,
                Pack1: null,
                Pack2: null,
                Pack3: null,
                Pack4: null,
                Pack5: null,
                Pack6: null,
                Pack7: null,
                Pack8: null,
                Pack9: null,
                Pack10: null,
                Active: false,
                IsDefault: false

            };

            $scope.editcheck = false;

        };

        $scope.save = function (file) {

            if ($scope.PackViewModel.Name == null) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {
                if ($scope.editcheck == false) {

                    $scope.PackViewModel.VendorId = $sessionStorage.VendorId;

                    VendorPackService.createpack($scope.PackViewModel, function (response) {

                        if (response == true)
                            alert('Pack has been added');

                        $scope.cancel();
                        loadpacks();

                    });
                }
                else {

                    VendorPackService.editpack($scope.PackViewModel, function (response) {

                        if (response == true)
                            alert('Pack has been edit');

                        $scope.cancel();
                        loadpacks();

                    });
                }
            }

        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.PackViewModel = item;
        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadpacks();
        });

    }]);