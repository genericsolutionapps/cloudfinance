﻿/// <reference path="../libraries/angular/angular-route.min.js" />
/// <reference path="../libraries/angular/angular.min.js" />

var FSTApp = angular.module('FSTApp', ['ngRoute', 'ngResource', 'ngStorage', 'underscore', 'ngFileUpload', 'ngDialog', 'ngMask', 'isteven-multi-select',
    'angularjs-datetime-picker', 'angularSpinner', 'angularUtils.directives.dirPagination']);

FSTApp.config(['$routeProvider', '$locationProvider', '$qProvider', function ($routeProvider, $locationProvider, $qProvider) {

    $locationProvider.hashPrefix('');

    $routeProvider.
        when('/', {
            templateUrl: 'Scripts/app/login/template/index.html'
        }).
        when('/Login', {
            templateUrl: 'Scripts/app/login/template/index.html'
        }).
        when('/Register', {
            templateUrl: 'Scripts/app/register/template/index.html'
        }).
        when('/Status', {
            templateUrl: 'Scripts/app/status/template/index.html'
        }).
        when('/Dashboard', {
            templateUrl: 'Scripts/app/dashboard/template/index.html'
        }).
        when('/VendorColor', {
            templateUrl: 'Scripts/app/vendorcolor/template/index.html'
        }).
        when('/VendorPack', {
            templateUrl: 'Scripts/app/vendorpack/template/index.html'
        }).
        when('/VendorSize', {
            templateUrl: 'Scripts/app/vendorsize/template/index.html'
        }).
        when('/VendorProduct', {
            templateUrl: 'Scripts/app/vendorproduct/template/index.html'
        }).
        when('/Subcategory', {
            templateUrl: 'Scripts/app/subcategory/template/index.html'
        }).
        when('/Categories', {
            templateUrl: 'Scripts/app/categories/template/index.html'
        }).
        when('/PaymentReport', {
            templateUrl: 'Scripts/app/payment-report/template/index.html'
        }).
        when('/Company', {
            templateUrl: 'Scripts/app/company/template/index.html'
        }).
        when('/CompanyEdit', {
            templateUrl: 'Scripts/app/company/template/edit.html'
        }).
        when('/ApplicationUser', {
            templateUrl: 'Scripts/app/applicationuser/template/index.html'
        }).
        when('/User', {
            templateUrl: 'Scripts/app/user/template/index.html'
        }).
        when('/employee', {
            templateUrl: 'Scripts/app/employee/template/index.html'
        }).
        when('/CreateEmployee', {
            templateUrl: 'Scripts/app/employee/template/CreateEmployee.html'
        }).
        when('/EmployeeInformation', {
            templateUrl: 'Scripts/app/employee/template/EmployeeInformation.html'
        }).
        when('/Salary', {
            templateUrl: 'Scripts/app/employee/template/Salary.html'
        }).
        when('/Visit', {
            templateUrl: 'Scripts/app/visit/template/index.html'
        }).
        when('/Role', {
            templateUrl: 'Scripts/app/role/template/index.html'
        }).
        when('/Invoice', {
            templateUrl: 'Scripts/app/invoice/template/index.html'
        }).
        when('/Invoice/:customerId', {
            templateUrl: 'Scripts/app/invoice/template/index.html'
        }).
        when('/Customer', {
            templateUrl: 'Scripts/app/customer/template/index.html'
        }).
        when('/SalesReport', {
            templateUrl: 'Scripts/app/sales-report/template/index.html'
        }).
        when('/IncomeExpenseReport', {
            templateUrl: 'Scripts/app/income-expense-report/template/index.html'
        }).
        when('/CollectionsReport', {
            templateUrl: 'Scripts/app/collections-report/template/index.html'
        }).
        when('/ExpenseList', {
            templateUrl: 'Scripts/app/expense/template/index.html'
        }).
        when('/CreateExpense', {
            templateUrl: 'Scripts/app/expense/template/CreateExpense.html'
        }).
        when('/ExpenseInformation', {
            templateUrl: 'Scripts/app/expense/template/ExpenseInformation.html'
        }).
        when('/ExpenseReport', {
            templateUrl: 'Scripts/app/expense-report/template/index.html'
        }).
        when('/VATReport', {
            templateUrl: 'Scripts/app/VAT-report/template/index.html'
        }).
        when('/Supplier', {
            templateUrl: 'Scripts/app/Supplier/template/index.html'
        }).
        when('/ProductList', {
            templateUrl: 'Scripts/app/product/template/index.html'
        }).
        when('/CreateProduct', {
            templateUrl: 'Scripts/app/product/template/CreateProduct.html'
        }).
        when('/ProductInformation', {
            templateUrl: 'Scripts/app/product/template/ProductInformation.html'
        }).
        when('/StockHistory', {
            templateUrl: 'Scripts/app/stock-history/template/index.html'
        }).
        when('/ProductsInStockReport', {
            templateUrl: 'Scripts/app/products-in-stock-report/template/index.html'
        }).
        when('/CashBankAccountList', {
            templateUrl: 'Scripts/app/cash-bank/template/Index.html'
        }).
        when('/CreateAccount', {
            templateUrl: 'Scripts/app/cash-bank/template/CreateAccount.html'
        }).
        when('/ForgetPassword', {
            templateUrl: 'Scripts/app/login/template/forgot-password.html'
        }).
        when('/CashBankReport', {
            templateUrl: 'Scripts/app/cash-bank-report/template/index.html'
        }).
        when('/CashBankAccountInformation', {
            templateUrl: 'Scripts/app/cash-bank/template/CashBankAccountInformation.html'
        });

    $qProvider.errorOnUnhandledRejections(false);

}]).
run(['$rootScope', '$location', '$sessionStorage', function ($rootScope, $location, $sessionStorage) {

    $rootScope.$on("$routeChangeStart", function (event, next, current) {

        setTimeout(function () {

            window.scrollTo(0, 0);
            initPage();

        });

        if ($sessionStorage.UserId != null && $sessionStorage.UserId != undefined) {

            $rootScope.loggedIn = true;

        }
        else {
            $rootScope.loggedIn = false;
        }



    });

}]);

