﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("ExpenseListService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.create = function (expenseListViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(expenseListViewModel);

        ServiceManager.execute(Resource.createExpenseList({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

}]);