﻿FSTApp.controller("ExpenseListController", ['$scope', '$rootScope', '$timeout', 'ExpenseListService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, ExpenseListService, $sessionStorage, ngDialog) {


        var InitializeExpenseList = function () {

            $scope.invoicenumber = $scope.exchange = $scope.expenseListGenerals.amountConversionDiv = false;
            showExpenseForm();

        }

        var showExpenseForm = function () {
            $scope.expenseList = false,
            $scope.expenseForm = true
        }

        var showExpenseList = function () {
            $scope.expenseList = true,
            $scope.expenseForm = false
        }

        $scope.invoiceNumber = function () {
            $scope.invoicenumber = true;
        }

        $scope.hideInvoiceNumber = function () {
            $scope.invoicenumber = false;
        }
        /******/
        $scope.expenseListGenerals = {
            totalAmount: '',
            conversionRate: '',
            //totalAmountCoverted: totalAmount * conversionRate,
            totalAmountCoverted: '',
            //totalAmountCurrency: '',
            totalVAT_Perc: '',
            amountConversionDiv: false
        }

        $scope.showAmountConversionDiv = function () {

            if ($scope.expenseListGenerals.amountConversionDiv)
                $scope.expenseListGenerals.amountConversionDiv = false;
            else
                $scope.expenseListGenerals.amountConversionDiv = true;
        }

        $scope.showExchangeInput = function () {

            if ($scope.expenseListGenerals.totalAmountCurrency.code != 'TRL') {
                $scope.exchange = true;
            }
            else {
                $scope.exchange = false;
            }
        }

        $scope.currencies = [{ code: 'TRL', name: 'Turkish Lira' }, { code: 'USD', name: 'US Dollars' }, { code: 'EUR', name: 'Euro' }, { code: 'GBO', name: 'British Sterlini' }]

        $scope.billViewModel = {
            explaination: '',
            bill_Date: '',
            bill_Num: '',
            totalAmount: '',
            total_VAT: '',
            paymentStatus: '',
            payment_Date: '',
            supplierName: '',

            paid_Again: '',
            receivableDate: '',
            paidAccount: '',
            expenseCategory: '',
            revenueExpLabel: '',
            bill_image: ''
        }

        paymentStatus = ['Payable', 'Paid', 'Employee Paid']

        /* $scope.Save() = function(){
 
             $scope.temp = {
                 explaination: '',
                 bill_Date: '',
                 bill_Num: '',
                 totalAmount: '',
                 total_VAT: '',
                 paymentStatus: '',
                 payment_Date: '',
                 supplierName: '',
 
                 paid_Again: '',
                 receivableDate: '',
                 paidAccount: '',
                 expenseCategory: '',
                 revenueExpLabel: '',
                 bill_image: ''
         }
 
             $scope.temp.explaination = $scope.billViewModel.explaination;
             $scope.temp.bill_Date = $scope.billViewModel.bill_Date;
             $scope.temp.bill_Num = $scope.billViewModel.bill_Num;
             $scope.temp.totalAmount = $scope.billViewModel.totalAmount;
             $scope.temp.total_VAT = $scope.billViewModel.total_VAT;
             $scope.temp.paymentStatus = $scope.billViewModel.paymentStatus;
             $scope.temp.payment_Date = $scope.billViewModel.payment_Date;
             $scope.temp.supplierName = $scope.billViewModel.supplierName;
 
             $scope.temp.paid_Again = $scope.billViewModel.paid_Again;
             $scope.temp.receivableDate = $scope.billViewModel.receivableDate;
             $scope.temp.paidAccount = $scope.billViewModel.paidAccount;
             $scope.temp.expenseCategory = $scope.billViewModel.expenseCategory;
             $scope.temp.revenueExpLabel = $scope.billViewModel.revenueExpLabel;
             $scope.temp.bill_image = $scope.billViewModel.bill_image;
 
             $scope.ExpenseListService.create($scope.billViewModel, function (response) {
 
                 if (response.ID != 0) {
                     InitializeExpenseList();
                 }
 
             });
 
         }*/

        InitializeExpenseList();


    }]);

