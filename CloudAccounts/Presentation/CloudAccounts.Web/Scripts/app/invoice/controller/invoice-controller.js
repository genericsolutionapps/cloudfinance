﻿FSTApp.controller("InvoiceController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'InvoiceService', 'CategoriesService', 'CustomerService', 'ProductService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, InvoiceService, CategoriesService, CustomerService, ProductService, $sessionStorage, ngDialog) {


        var customerId = $routeParams.customerId;

        $scope.rows = [$scope.invoiceDetail];
        $scope.invoiceHeaderList = [];
        $scope.allocations = [];

        $scope.visibility = {
            invoiceList: true,
            invoiceForm: false,
            invoiceInformation: false,
            totalSection: false,
            invoiceDetailDescription: false,
            invoiceDetailUnit: false,
            allocations: false,
            allocationValidation: false,
            addCustomerInformation: false
        };

        $scope.invoiceHeader = {
            InvoiceHeaderId: 0,
            invoiceDetailCollection: '',
            TotalAllocatedAmount: 0,
            CustomerId: null,
            TotalAmount: 0,
            InvoiceNo: null,
            Date: '',
            DueDate: '',
            Status: 'Unpaid',
            Description: '',
            CategoryId: null,
            CustomerAddress: null,
            CustomerPhone: null,
            CurrencyRate: 1,
            BaseRate: 1,
            VoucherType: '0001',     //0001 for invoice          //0002 for bill
            CurrencyId: 0
        };

        $scope.invoiceDetail = {
            //rowId: 1,
            ProductId: null,
            ProductName: '',
            Quantity: 1,
            UnitPrice: 0,
            DiscountPerc: 0,
            TaxPerc: 0,
            Amount: 0,
            Description: null,
            Unit: null,
            descriptioncheck: false,
            unitIndex: false
        };

        $scope.allocation = {
            AllocationId: 0,
            Date: '',
            ExpiryDate: '',
            Account: 'Safe Account',
            Amount: 0,
            Explanation: '',
            AllocationType: '0001',              //0001 for collection       //0002 for payment
            AllocationMode: '',                  //0001 for cash             //0002 for bank
            CustomerId: null,
            InvoiceHeaderId: null,
            //Source: 'invoice'
        };

        $scope.CustomerViewModel = {
            CustomerId: 0,
            CompanyTitle: $scope.selectedCustomer,
            ShortName: null,
            PhoneNumber: null,
            District: null,
            Province: null,
            CustomerType: '0001',
            VKN: null,
            Tax: null,
            CategoryId: null,
            Address: null,
            BusinessPartnerType: '0001'
        };

        $scope.selectedCustomerCategory = '';
        $scope.customerCategories = [];
        $scope.customerCategorySearchList = [];

        $scope.summary = {
            SubTotal: 0,
            TotalTax: 0,
            GrandTotal: 0,
            TotalDiscount: 0
        };

        $scope.selectedCategory = '';
        $scope.salesCategories = [];
        $scope.salesCategorySearchList = [];

        $scope.selectedCustomer = '';
        $scope.allCustomers = [];
        $scope.customersSearchList = [];

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];
        $scope.selectedCurrency = 'LIRA';
        $scope.currencySymbol = 'la la-turkish-lira';

        $scope.allProducts = [];
        $scope.productSearchList = [];



        var initialize = function () {

            $scope.invoiceHeaderList = [];

            InvoiceService.getAllInvoices({ ReportName: null }, function (response) {

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);

                    value.DueDate = parseDate(value.DueDate);

                    if (value.VoucherType == '0001') {
                        $scope.invoiceHeaderList.push(value);
                    }
                });
            });

            if ($routeParams.customerId == null) {
                $scope.visibility.invoiceList = true;
                $scope.visibility.invoiceForm = false;
                $scope.visibility.invoiceInformation = false;
            }
            else if ($routeParams.customerId > 0) {
                $scope.visibility.invoiceList = false;
                $scope.visibility.invoiceForm = true;
            }
        };
        initialize();



        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };


        var getAllCategories = function (callback) {

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0001') {
                        $scope.salesCategories.push(value);
                        $scope.salesCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                    else if (value.CategoryType === '0006') {
                        $scope.customerCategories.push(value);
                        $scope.customerCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                });

                loadAutoComplete('ddlCategory', $scope.salesCategorySearchList);
                loadAutoComplete('ddlCustomerCategory', $scope.customerCategorySearchList);

                callback();
            });
        };

        var getAllProducts = function (callback) {

            ProductService.getAllProducts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.allProducts.push(value);
                    $scope.productSearchList.push({ id: value.ProductId, name: value.Name });
                });

                loadAutoComplete('ddlProduct0', $scope.productSearchList);

                callback();
            });
        };

        var getAllCustomers = function (callback) {

            CustomerService.getAllBusinessPartnersByType($sessionStorage.UserId, function (response) {

                $scope.customersSearchList = [];

                angular.forEach(response, function (value, key) {
                    $scope.allCustomers.push(value);
                    $scope.customersSearchList.push({ id: value.CustomerId, name: value.CompanyTitle });
                });

                loadAutoComplete('ddlcustomer', $scope.customersSearchList);

                callback();
            });
        };


        var getCurrencyExchangeRate = function () {

            InvoiceService.getUSDRate(function (response) {
                $scope.currencies[1].rate = response.USD_TRY.val;
            });
            InvoiceService.getEURRate(function (response) {
                $scope.currencies[2].rate = response.EUR_TRY.val;
            });
            InvoiceService.getGBPRate(function (response) {
                $scope.currencies[3].rate = response.GBP_TRY.val;
            });
        };
        getCurrencyExchangeRate();

        $scope.calculate = function (index) {
            var amount = $scope.rows[index].UnitPrice * $scope.rows[index].Quantity;
            var amountAfterTax = amount + (amount * $scope.rows[index].TaxPerc / 100);
            var amountAfterDiscount = amountAfterTax - (amountAfterTax * $scope.rows[index].DiscountPerc / 100);

            $scope.rows[index].Amount = amountAfterDiscount.toFixed(2);

            var subTotal = 0;
            var totalTaxAmount = 0;
            var grandTaxAmount = 0;
            var totalDiscountAmount = 0;

            angular.forEach($scope.rows, function (value, key) {
                subTotal = subTotal + (value.UnitPrice * value.Quantity);
                totalTaxAmount = totalTaxAmount + ((value.UnitPrice * value.Quantity - ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100)) * value.TaxPerc / 100);
                totalDiscountAmount = totalDiscountAmount + ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100);
            });

            $scope.summary.SubTotal = subTotal;
            $scope.summary.TotalTax = totalTaxAmount;
            $scope.summary.TotalDiscount = totalDiscountAmount;
            $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;

            $scope.invoiceHeader.TotalAmount = $scope.summary.GrandTotal;

            //$scope.summary.SubTotal = $scope.summary.SubTotal + amountAfterDiscount;
        };


        var resolveCalls = function (callback) {
            getAllCategories(function () {
                getAllCustomers(function () {
                    getAllProducts(function () {
                        callback();
                    });
                });
            });
        }


        $scope.invoiceForm = function (source) {
            $scope.visibility.invoiceList = false;
            $scope.visibility.invoiceForm = true;
            $scope.visibility.totalSection = true;
            $scope.visibility.invoiceInformation = false;

            resolveCalls(function () {
                if (source == 'create') {
                    $scope.invoiceHeader.InvoiceHeaderId = 0;
                    $scope.invoiceHeader.invoiceDetailCollection = '';
                    $scope.invoiceHeader.TotalAllocatedAmount = 0;
                    $scope.invoiceHeader.CustomerId = null;
                    $scope.invoiceHeader.TotalAmount = 0;
                    $scope.invoiceHeader.InvoiceNo = null;
                    $scope.invoiceHeader.Status = 'Unpaid';
                    $scope.invoiceHeader.Description = '';
                    $scope.invoiceHeader.CategoryId = null;
                    $scope.invoiceHeader.CustomerAddress = null;
                    $scope.invoiceHeader.CustomerPhone = null;
                    $scope.invoiceHeader.CurrencyRate = 1;
                    $scope.invoiceHeader.BaseRate = 1;
                    $scope.invoiceHeader.CurrencyId = 0;

                    $scope.invoiceDetail.ProductId = null;
                    $scope.invoiceDetail.ProductName = '';
                    $scope.invoiceDetail.Quantity = 1;
                    $scope.invoiceDetail.UnitPrice = 0;
                    $scope.invoiceDetail.DiscountPerc = 0,
                        $scope.invoiceDetail.TaxPerc = 0;
                    $scope.invoiceDetail.Amount = 0;
                    $scope.invoiceDetail.Description = null;
                    $scope.invoiceDetail.Unit = null;

                    $scope.rows = [$scope.invoiceDetail];

                    $scope.selectedCategory = '';
                    $scope.selectedCustomer = '';

                    $scope.calculate(0);

                    //$scope.SubTotal: 0,
                    //$scope.TotalTax: 0,
                    //$scope.GrandTotal: 0,
                    //$scope.TotalDiscount: 0
                }
                else if (source == 'edit') {

                    $scope.setCustomer($scope.invoiceHeader.CustomerId);
                    $scope.setCategory($scope.invoiceHeader.CategoryId, 'invoice');
                    angular.forEach($scope.invoiceHeader.invoiceDetailCollection, function (value, key) {
                        $scope.setProduct(key);
                    });
                }
            });
        };


        $scope.invoiceInformation = function (invoiceHeaderId) {
            $scope.visibility.invoiceList = false;
            $scope.visibility.invoiceInformation = true;
            $scope.visibility.totalSection = true;
            $scope.visibility.invoiceForm = false;

            //setting the invoice header object
            angular.forEach($scope.invoiceHeaderList, function (value, key) {
                if (value.InvoiceHeaderId === invoiceHeaderId) {
                    $scope.invoiceHeader.InvoiceHeaderId = value.InvoiceHeaderId;
                    $scope.invoiceHeader.invoiceDetailCollection = value.InvoiceDetailCollection;
                    $scope.invoiceHeader.TotalAllocatedAmount = value.TotalAllocatedAmount;
                    $scope.invoiceHeader.CustomerId = value.CustomerId;
                    $scope.invoiceHeader.TotalAmount = value.TotalAmount;
                    $scope.invoiceHeader.InvoiceNo = value.InvoiceNo;
                    $scope.invoiceHeader.Date = value.Date;
                    $scope.invoiceHeader.DueDate = value.DueDate;
                    $scope.invoiceHeader.Status = value.Status;
                    $scope.invoiceHeader.Description = value.Description;
                    $scope.invoiceHeader.CategoryId = value.CategoryId;
                    $scope.invoiceHeader.CurrencyId = value.CurrencyId;
                }
            });

            $scope.allocation.Amount = ($scope.invoiceHeader.TotalAmount - $scope.invoiceHeader.TotalAllocatedAmount).toFixed(2);
            $scope.allocation.InvoiceHeaderId = $scope.invoiceHeader.InvoiceHeaderId;

            //setting all invoice detail objects
            InvoiceService.getAllInvoiceDetailsByInvoiceHeaderId(invoiceHeaderId, function (response) {

                $scope.rows = [];

                var subTotal = 0;
                var totalTaxAmount = 0;
                var grandTaxAmount = 0;
                var totalDiscountAmount = 0;

                angular.forEach(response, function (value, key) {
                    if (value.Description != null)
                        value.descriptioncheck = true;

                    if (value.Unit != null)
                        value.unitcheck = true;

                    $scope.rows.push(value);
                    subTotal = subTotal + (value.UnitPrice * value.Quantity);
                    totalTaxAmount = totalTaxAmount + ((value.UnitPrice * value.Quantity - ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100)) * value.TaxPerc / 100);
                    totalDiscountAmount = totalDiscountAmount + ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100);
                });

                $scope.invoiceHeader.invoiceDetailCollection = $scope.rows;

                $scope.summary.SubTotal = subTotal;
                $scope.summary.TotalTax = totalTaxAmount;
                $scope.summary.TotalDiscount = totalDiscountAmount;
                $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;
            });


            //setting all allocation objects
            InvoiceService.getAllAllocationsByInvoiceHeaderId(invoiceHeaderId, function (response) {

                $scope.allocations = [];

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);
                    value.ExpiryDate = parseDate(value.ExpiryDate);

                    $scope.allocations.push(value);
                });

                if ($scope.allocations.length > 0) {
                    $scope.visibility.allocations = true;
                }
            });

            resolveCalls(function () {
                $scope.setCustomer($scope.invoiceHeader.CustomerId);
            });
        };


        $scope.addNewRow = function () {

            $scope.rows.push(angular.copy($scope.invoiceDetail));

            var id = 'ddlProduct' + ($scope.rows.length - 1);

            $scope.rows[$scope.rows.length - 1].ProductName = '';
            $scope.rows[$scope.rows.length - 1].Quantity = 1;
            $scope.rows[$scope.rows.length - 1].UnitPrice = 0;
            $scope.rows[$scope.rows.length - 1].DiscountPerc = 0;
            $scope.rows[$scope.rows.length - 1].Amount = 0;
            $scope.rows[$scope.rows.length - 1].TaxPerc = 0;

            setTimeout(function () {

                loadAutoComplete(id, $scope.productSearchList)

            }, 500);
        }

        $scope.setCategory = function (categoryId, source) {

            if (source == 'invoice') {

                var input = $('#ddlCategory');
                input.trigger('input');
                input.trigger('change');

                //create
                if (categoryId == undefined) {

                    angular.forEach($scope.salesCategories, function (value, key) {

                        if (value.CategoryName === $scope.selectedCategory) {
                            $scope.invoiceHeader.CategoryId = value.CategoryId;
                        }
                    });
                }
                //edit
                else if (categoryId > 0) {

                    angular.forEach($scope.salesCategories, function (value, key) {

                        if (value.CategoryId === categoryId) {
                            $scope.selectedCategory = value.CategoryName;
                        }
                    });
                }
            }
            else if (source == 'customer') {

                var input = $('#ddlCustomerCategory');
                input.trigger('input');
                input.trigger('change');

                angular.forEach($scope.customerCategories, function (value, key) {

                    if (value.CategoryName === $scope.selectedCustomerCategory) {
                        $scope.CustomerViewModel.CategoryId = value.CategoryId;
                    }
                });
            }
        };

        $scope.setCustomer = function (customerId) {

            var input = $('#ddlcustomer');
            input.trigger('input');
            input.trigger('change');

            $scope.invoiceHeader.CustomerId = null;
            $scope.invoiceHeader.CustomerAddress = null;
            $scope.invoiceHeader.CustomerPhone = null;

            $scope.visibility.addCustomerInformation = false;

            if (customerId == undefined) {
                angular.forEach($scope.allCustomers, function (value, key) {

                    if (value.CompanyTitle === $scope.selectedCustomer) {
                        $scope.invoiceHeader.CustomerId = value.CustomerId;
                        $scope.invoiceHeader.CustomerAddress = value.Address;
                        $scope.invoiceHeader.CustomerPhone = value.PhoneNumber;

                        $scope.visibility.addCustomerInformation = false;
                    }
                });
                if ($scope.invoiceHeader.CustomerId == null && $scope.selectedCustomer != "") {
                    $scope.invoiceHeader.CustomerId = null;
                    $scope.invoiceHeader.CustomerAddress = null;
                    $scope.invoiceHeader.CustomerPhone = null;

                    $scope.visibility.addCustomerInformation = true;
                }
                if ($scope.selectedCustomer == "") {
                    $scope.invoiceHeader.CustomerId = null;
                }
            }
            else if (customerId > 0) {

                angular.forEach($scope.allCustomers, function (value, key) {
                    if (value.CustomerId === customerId) {
                        $scope.selectedCustomer = value.CompanyTitle;
                    }
                });
            }
        };

        $scope.setProduct = function (index) {

            var input = $('#ddlProduct' + index);
            input.trigger('input');
            input.trigger('change');

            angular.forEach($scope.allProducts, function (value, key) {

                if (value.Name === $scope.rows[index].ProductName) {
                    $scope.rows[index].ProductId = value.ProductId;
                    $scope.rows[index].ProductName = value.Name;
                    $scope.rows[index].UnitPrice = (value.SalesPrice * value.SalesCurrencyRate).toFixed(2);
                    $scope.rows[index].TaxPerc = value.KDVPerc;
                    $scope.calculate(index);
                }
            });
            if ($scope.rows[index].ProductName == '') {
                $scope.rows[index].ProductId = null;
                $scope.rows[index].ProductName = '';
                $scope.rows[index].UnitPrice = 0;
                $scope.calculate(index);
            }
        }

        $scope.setCurrency = function (index) {
            $scope.invoiceHeader.CurrencyRate = $scope.currencies[index].rate.toFixed(4);
            $scope.selectedCurrency = $scope.currencies[index].name;
            $scope.currencySymbol = $scope.currencies[index].symbol;
            $scope.invoiceHeader.CurrencyId = index;
        };


        $scope.removeRow = function (index) {
            $scope.rows.splice(index, 1);

            var subTotal = 0;
            var totalTaxAmount = 0;
            var grandTaxAmount = 0;
            var totalDiscountAmount = 0;

            angular.forEach($scope.rows, function (value, key) {
                subTotal = subTotal + (value.UnitPrice * value.Quantity);
                totalTaxAmount = totalTaxAmount + (value.UnitPrice * value.TaxPerc / 100);
                totalDiscountAmount = totalDiscountAmount + (value.UnitPrice * value.DiscountPerc / 100);
            });

            $scope.summary.SubTotal = subTotal;
            $scope.summary.TotalTax = totalTaxAmount;
            $scope.summary.TotalDiscount = totalDiscountAmount;
            $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;

        }



        var createInvoiceDependencies = function (callback) {

            var success = true;

            //to create both category and customer
            if ($scope.selectedCategory != '' && !$scope.salesCategories.some(function (element) { return element.CategoryName == $scope.selectedCategory; }) && $scope.invoiceHeader.CustomerId == 0) {

                CategoriesService.create({ CategoryName: $scope.selectedCategory, CategoryType: '0001', UserId: $sessionStorage.UserId }, function (response) {

                    if (typeof response.CategoryId == 'number') {

                        $scope.invoiceHeader.CategoryId = response.CategoryId;

                        CustomerService.createCustomer($scope.CustomerViewModel, function (response) {

                            if (typeof response == 'number') {

                                $scope.invoiceHeader.CustomerId = response;
                                callback(success);
                            }
                            else {
                                alertify.error("customer could not be created because of some technical error.");
                                success = false;
                                callback(success);
                            }
                        })
                    }
                    else {
                        alertify.error("category could not be created because of some technical error.");
                        success = false;
                        callback(success);
                    }
                })
            }
            //to create category
            else if ($scope.selectedCategory != '' && !$scope.salesCategories.some(function (element) { return element.CategoryName == $scope.selectedCategory; }) && $scope.invoiceHeader.CustomerId > 0) {

                CategoriesService.create({ CategoryName: $scope.selectedCategory, CategoryType: '0001', UserId: $sessionStorage.UserId }, function (response) {

                    if (typeof response.CategoryId == 'number') {

                        $scope.invoiceHeader.CategoryId = response.CategoryId;
                        callback(success);
                    }
                    else {
                        alertify.error("Category could not be created because of some technical error.");
                        success = false;
                        callback(success);
                    }
                })
            }
            //to create customer
            else if ($scope.invoiceHeader.CategoryId > 0 && $scope.invoiceHeader.CustomerId == null) {

                CustomerService.createCustomer($scope.CustomerViewModel, function (response) {

                    if (typeof response == 'number') {

                        $scope.invoiceHeader.CustomerId = response;
                        callback(success);
                    }
                    else {
                        alertify.error("Customer could not be created because of some technical error.");
                        success = false;
                        callback(success);
                    }
                })
            }
            else {
                callback(success);
            }
        };


        $scope.createInvoice = function () {

            $scope.invoiceHeader.invoiceDetailCollection = $scope.rows;

            createInvoiceDependencies(function (success) {

                if (success) {
                    //create
                    if ($scope.invoiceHeader.InvoiceHeaderId == 0) {

                        //for (var i = 0; i < $scope.invoiceHeader.invoiceDetailCollection; i++) {

                        //    if ($scope.invoiceHeader.InvoiceDetailCollection[i].ProductId == null)
                        //        $scope.invoiceHeader.InvoiceDetailCollection[i].ProductId = 0;
                        //}

                        angular.forEach($scope.invoiceHeader.invoiceDetailCollection, function (value, index) {

                            if (value.ProductId == null)
                                value.ProductId = 0;

                        });

                        //&& !$scope.invoiceHeader.invoiceDetailCollection.some(function (element) { return element.ProductId == null; })) {
                        InvoiceService.createInvoice($scope.invoiceHeader, function (responseInvoice) {

                            if (responseInvoice == true) {

                                alertify.success("Invoice Created Successfully");
                                initialize();
                            }
                        });
                    }
                    //edit
                    else if ($scope.invoiceHeader.InvoiceHeaderId > 0 && !$scope.invoiceHeader.invoiceDetailCollection.some(function (element) { return element.ProductId == null; })) {
                        InvoiceService.edit($scope.invoiceHeader, function (response) {

                            if (response == true) {

                                alertify.success("Invoice Edited Successfully");
                                initialize();
                            }
                        });
                    }
                    else {
                        alertify.error("Please fill all mandatory fields.");
                    }
                }
                else {
                    alertify.error("Invoice Dependencies could not be created because of some technical error.");
                }
            });
        }


        $scope.addDescription = function (index, row) {

            row.descriptioncheck = true;
            $scope.rows[index].descriptioncheck = true;

            $scope.visibility.invoiceDetailDescription = true;
            $scope.descriptionIndex = index;

        };

        $scope.addUnit = function (index, row) {

            row.unitcheck = true;
            $scope.rows[index].unitcheck = true;

            $scope.visibility.invoiceDetailUnit = true;
            $scope.descriptionIndex = index;
        };

        $scope.removeDescription = function (index, row) {

            row.descriptioncheck = false;
            $scope.rows[index].descriptioncheck = false;

            $scope.visibility.invoiceDetailDescription = false;
            $scope.descriptionIndex = index;

        };

        $scope.removeUnit = function (index, row) {

            row.unitcheck = false;
            $scope.rows[index].unitcheck = false;

            $scope.visibility.invoiceDetailUnit = false;
            $scope.descriptionIndex = index;
        };


        $scope.addAllocation = function () {

            if ($scope.allocation.Amount > 0 && $scope.allocation.Amount <= $scope.invoiceHeader.TotalAmount - $scope.invoiceHeader.TotalAllocatedAmount) {

                if ($scope.allocation.ExpiryDate == '') {
                    $scope.allocation.AllocationMode = '0001';
                }
                else {
                    $scope.allocation.AllocationMode = '0002';
                }

                InvoiceService.addAllocation($scope.allocation, function (response) {

                    if (response == true) {
                        $scope.visibility.allocationValidation = false;

                        initialize();
                    }
                });
            }
            else {
                $scope.visibility.allocationValidation = true;
            }
        };


        $scope.deleteInvoiceById = function (invoiceHeaderId) {
            InvoiceService.deleteInvoiceById(invoiceHeaderId, function (response) {

                if (response == true) {

                    initialize();
                }
                else {
                    window.alert(response.ErrorMessage);
                }
            });
        };

        $scope.Cancel = function () {

            $scope.visibility.invoiceList = true;
            $scope.visibility.invoiceForm = false;
            $scope.visibility.invoiceInformation = false;
        };



    }]);






