﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("InvoiceService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.createInvoice = function (viewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(viewModel);

        ServiceManager.execute(Resource.createinvoice({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };


    this.edit = function (invoiceHeaderViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(invoiceHeaderViewModel);

        ServiceManager.execute(Resource.editinvoice({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.deleteInvoiceById = function (invoiceHeaderId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(invoiceHeaderId);

        ServiceManager.execute(Resource.deleteinvoicebyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getInvoiceById = function (invoiceHeaderId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(invoiceHeaderId);

        ServiceManager.execute(Resource.getinvoicebyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };


    this.addAllocation = function (allocationViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(allocationViewModel);

        ServiceManager.execute(Resource.addallocation({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllAllocations = function (dateRangeViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(dateRangeViewModel);

        ServiceManager.execute(Resource.getallallocations({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllAllocationsByInvoiceHeaderId = function (invoiceHeaderId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(invoiceHeaderId);

        ServiceManager.execute(Resource.getallallocationsbyinvoiceheaderid({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllDelayedInvoices = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getalldelayedinvoices({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllInvoiceDetails = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallinvoicedetails({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getInvoicesByProductId = function (productId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(productId);

        ServiceManager.execute(Resource.getinvoicesbyproductid({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getUSDRate = function (callback) {

        ServiceManager.execute(Resource.getusdrate(), function (response) {

            callback(response);
        });
    };

    this.getEURRate = function (callback) {

        ServiceManager.execute(Resource.geteurrate(), function (response) {

            callback(response);
        });
    };

    this.getGBPRate = function (callback) {

        ServiceManager.execute(Resource.getgbprate(), function (response) {

            callback(response);
        });
    };


    this.delete = function (categoryId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(categoryId);

        ServiceManager.execute(Resource.deletecategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };


    this.getAllInvoices = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallinvoices({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllInvoiceDetailsByInvoiceHeaderId = function (invoiceHeaderId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(invoiceHeaderId);

        ServiceManager.execute(Resource.getallinvoicedetailsbyinvoiceheaderid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.getAllInvoicesByCustomerId = function (customerId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(customerId);

        ServiceManager.execute(Resource.getallinvoicesbycustomerid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.getBalanceByMonth = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getbalancebymonth({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };



}]);





