﻿FSTApp.controller("CategoryController", ['$scope', '$rootScope', '$timeout', 'CategoryService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, CategoryService, $sessionStorage, ngDialog) {

    $scope.CategoryViewModel = {
        CategoryID: 0,
        Category_Name: null,
        Is_Active: 'Active'


    };

    $scope.editcheck = false;

    $scope.listcategory = [];

    var loadcategory = function () {

        $scope.listcategory = [];

        CategoryService.getallcategory(function (response) {
          
            var categorylist = response;
            
            angular.forEach(categorylist, function (value, key) {
            
                $scope.listcategory.push(value);
            });

            });
        
        };


    $scope.cancel = function () {

        $scope.CategoryViewModel = {
            CategoryID: 0,
            Category_Name: null,
            Is_Active: 'Active'

        };

        $scope.editcheck = false;

    };

    $scope.save = function () {

        if ($scope.CategoryViewModel.Is_Active == 'Active')
            $scope.CategoryViewModel.Is_Active = true;
        else
            $scope.CategoryViewModel.Is_Active = false;

        if ($scope.CategoryViewModel.Category_Name == null || $scope.CategoryViewModel.Category_Name == undefined) {

            ngDialog.open({
                template: '<p>Please fill mandetory fields.</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });

        }
        else {
            if ($scope.editcheck == false) {
                CategoryService.createcategory($scope.CategoryViewModel, function (response) {

                    if (response == true)
                        alert('Category has not been added');
                    $scope.cancel();
                    load.category();
                });
            }
            else {
             
                CategoryService.editcategory($scope.CategoryViewModel, function (response){
                
                    if(response == true)
                        alert('Category has not been edited');

                    $scope.cancel();
                    $scope.editcheck = false;
                    loadcategory();
                });
            }
        }
    };
    $scope.edit = function (item) {

        $scope.editcheck = true;

        $scope.CategoryViewModel = item;

    };

    $scope.delete = function (item) {
    
    };


    $timeout(function () {
    
        loadcategory();
        
        //$('#statuslist').DataTable({
        //    'paging': true,
        //    'lengthChange': false,
        //    'searching': false,
        //    'ordering': true,
        //    'info': true,
        //    'autoWidth': false
        //});

        $("#categorylist").DataTable();

    });

}]);