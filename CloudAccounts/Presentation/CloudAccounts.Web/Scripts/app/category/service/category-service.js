﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("CategoryService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {



        this.getallcategory = function (callback) {

            var request = commonobjects.requestobject;

            ServiceManager.execute(Resource.getallcategory(), function (response) {

                callback(response);

            });

        };

        this.createcategory = function (model, file, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.createcategory({ data: request }), function (response) {

                callback(response);

            });

        };

        this.editcategory = function (model, file, callback) {

            var request = commonobject.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.editcategory({ data: request }), function (response) {

                callback(response);

            });

        };

    }]);