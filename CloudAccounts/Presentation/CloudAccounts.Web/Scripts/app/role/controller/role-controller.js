﻿FSTApp.controller("RoleController", ['$scope', '$rootScope', '$timeout', 'RoleService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, RoleService, $sessionStorage, ngDialog) {

        $scope.roleViewModel = {
            Role_ID: 0,
            Role_Name: null,
            Is_Active: 'Active'
        };

        $scope.editcheck = false;

        $scope.listrole = [];

        var loadrole = function () {

            $scope.listrole = [];

            RoleService.getallrole(function (response) {

                var rolelist = response;

                angular.forEach(rolelist, function (value, key) {

                    $scope.listrole.push(value);

                });

            });

        };

        $scope.cancel = function () {

            $scope.RoleViewModel = {
                Role_ID: 0,
                Role_Name: null,
                Is_Active: true
            };

            $scope.editcheck = false;

        };

        $scope.save = function () {

            if ($scope.RoleViewModel.Is_Active == 'Active')
                $scope.RoleViewModel.Is_Active = true;
            else
                $scope.RoleViewModel.Is_Active = false;

            $scope.RoleViewModel.Code = '';

            if ($scope.RoleViewModel.Role_Name == null || $scope.RoleViewModel.Role_Name == undefined) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {
                    RoleService.createrole($scope.RoleViewModel, function (response) {

                        if (response == true)
                            alert('role has been added');

                        $scope.cancel();
                        loadrole();
                    });
                }
                else {

                    RoleService.editrole($scope.RoleViewModel, function (response) {

                        if (response == true)
                            alert('Role has been edit');

                        $scope.cancel();
                        $scope.editcheck = false;
                        loadrole();
                    });
                }
            }
        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.RoleViewModel = item;

        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadrole();

            //$('#rolelist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $('#rolelist').DataTable();
        });

    }]);