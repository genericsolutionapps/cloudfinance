﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("RoleService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {



    this.getallrole = function (callback) {

        var request = commonobjects.requestobject;

        ServiceManager.execute(Resource.getallrole(), function (response) {

            callback(response);

        });

    };

    this.createrole = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createrole({ data: request }), function (response) {

            callback(response);

        });

    };

    this.editrole = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editrole({ data: request }), function (response) {

            callback(response);

        });



    };

}]);