﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorColorService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', 'Upload', function ($http, ServiceManager, Resource, commonobjects, Upload) {


    //this.login = function (model, callback) {

    //    //var request = commonobjects.requestobject;

    //    //request.data = JSON.stringify(model);

    //    //ServiceManager.execute(Resource.submitvendorregistration({ data: request }), function (response) {

    //    //    callback(response);

    //    //});

    //};

    this.getallmastercolor = function (callback) {


        ServiceManager.execute(Resource.getallmastercolor(), function (response) {

            callback(response);

        });

    };

    this.getallcolor = function (id, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(id);

        ServiceManager.execute(Resource.getallcolor({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.createcolor = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        //ServiceManager.execute(Resource.createcolor({ data: request }), function (response) {

        //    callback(response);

        //});

        file.upload = Upload.upload({
            url: 'Color/CreateColor',
            data: { data: JSON.stringify(request), file: file },
        });

        file.upload.then(function (response) {

            callback(response.data.ResultData);

        }, function (response) {


        }, function (evt) {

        });

    };

    this.editcolor = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        //ServiceManager.execute(Resource.editcolor({ data: request }), function (response) {

        //    callback(response);

        //});

        file.upload = Upload.upload({

            url: 'Color/EditColor',

            data: { data: JSON.stringify(request), file: file },
        });

        file.upload.then(function (response) {

            callback(response.data.ResultData);

        }, function (response) {


        }, function (evt) {

        });

    };


}]);