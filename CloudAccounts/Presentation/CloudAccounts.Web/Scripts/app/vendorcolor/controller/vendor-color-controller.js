﻿FSTApp.controller("VendorColorController", ['$scope', '$rootScope', 'VendorColorService', '$timeout', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, VendorColorService, $timeout, $sessionStorage, ngDialog) {

        $scope.ColorViewModel = {

            ColorId: 0,
            VendorId: 0,
            MasterColorId: 0,
            Name: null,
            Code: null,
            Image: null,
            Active: true

        };

        $scope.MasterColorViewModel = {

            MasterColorId: 0,
            MasterColorName: null,
            MasterColorCode: null,
            MasterColorImage: null,
            Active: true

        };

        $scope.editcheck = false;

        $scope.listcolors = [];

        $scope.mastercolorlist = [];
        $scope.selectedMasterColor = null;

        var loadmastercolor = function () {


            VendorColorService.getallmastercolor(function (response) {

                var mastercolorlist = response;

                angular.forEach(mastercolorlist, function (value, key) {

                    $scope.mastercolorlist.push(value);

                });

            });

        };

        var loadcolor = function () {

            $scope.listcolors = [];

            VendorColorService.getallcolor($sessionStorage.VendorId, function (response) {

                var colorlist = response;

                angular.forEach(colorlist, function (value, key) {

                    $scope.listcolors.push(value);

                });

            });

        };

        $scope.cancel = function () {

            $scope.ColorViewModel = {

                ColorId: 0,
                VendorId: 0,
                MasterColorId: 0,
                Name: null,
                Code: null,
                Image: null,
                Active: true

            };
            $scope.selectedMasterColor = null;
            $scope.editcheck = false;

        };

        $scope.save = function (file) {

            if ($scope.ColorViewModel.Active == 'Active')
                $scope.ColorViewModel.Active = true;
            else
                $scope.ColorViewModel.Active = false;

            if ($scope.selectedMasterColor != null && $scope.selectedMasterColor != undefined) {
                $scope.ColorViewModel.MasterColorId = $scope.selectedMasterColor;

            }

            $scope.ColorViewModel.VendorId = $sessionStorage.VendorId;
            $scope.ColorViewModel.Image = 'test';
            $scope.ColorViewModel.Code = 'test';

            if ($scope.ColorViewModel.Name == null || $scope.ColorViewModel.Name == undefined || $scope.ColorViewModel.MasterColorId == 0
                || $scope.ColorViewModel.Image == null || $scope.ColorViewModel.Image == undefined) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {
                    VendorColorService.createcolor($scope.ColorViewModel, file, function (response) {

                        if (response == true)
                            alert('Color has been added');

                        $scope.cancel();
                        $scope.selectedMasterColor = null;
                        loadcolor();
                    });
                }
                else {

                    VendorColorService.editcolor($scope.ColorViewModel, file, function (response) {

                        if (response == true)
                            alert('Color has been edit');

                        $scope.cancel();
                        $scope.selectedMasterColor = null;
                        $scope.editcheck = false;
                        loadcolor();
                    });
                }
            }
        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.ColorViewModel = item;
            $scope.selectedMasterColor = '' + item.MasterColorId + '';

        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadmastercolor();
            loadcolor();
        });

    }]);