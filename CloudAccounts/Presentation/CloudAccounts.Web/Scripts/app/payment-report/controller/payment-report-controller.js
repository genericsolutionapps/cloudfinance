﻿FSTApp.controller("PaymentReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'PaymentReportService', 'InvoiceService', 'CategoriesService', 'CustomerService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, PaymentReportService, InvoiceService, CategoriesService, CustomerService, $sessionStorage, ngDialog) {


        $scope.visibility1 = {
            delayed30: false,
            delayed60: false,
            delayed90: false,
            delayed120: false,
            delayed120plus: false
        };

        $scope.visibility = [false, false, false, false, false, false];


        $scope.delayedInvoices = [];

        $scope.delayedInvoicesAmount = [0, 0, 0, 0, 0, 0];

        $scope.totalDelayedAmount = 0;

        var initialize = function () {

            InvoiceService.getAllDelayedInvoices($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    angular.forEach(value, function (voucher, key) {

                        if (voucher.VoucherType == '0002') {
                            voucher.Date = parseDate(voucher.Date);

                            voucher.DueDate = parseDate(voucher.DueDate);

                            $scope.totalDelayedAmount = $scope.totalDelayedAmount + (voucher.TotalAmount - voucher.TotalAllocatedAmount);
                        }
                    });

                    $scope.delayedInvoices.push(value);
                });

                angular.forEach($scope.delayedInvoices, function (value, i) {

                    $scope.delayedInvoicesAmount[i] = 0;

                    angular.forEach(value, function (voucher, key) {

                        if (voucher.VoucherType == '0002') {
                            $scope.delayedInvoicesAmount[i] = $scope.delayedInvoicesAmount[i] + (voucher.TotalAmount - voucher.TotalAllocatedAmount);
                        }
                    });
                });
            });
        };

        initialize();

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };


        $scope.setVisibility = function (index) {
            angular.forEach($scope.visibility, function (value, key) {
                $scope.visibility[key] = false;
            });
            $scope.visibility[index] = true;
        }


        //Flot Bar Chart
        //$(function () {
        //    var barOptions = {
        //        series: {
        //            bars: {
        //                show: true,
        //                barWidth: 0.6,
        //                fill: true,
        //                align: 'center',
        //                fillColor: {
        //                    colors: [{
        //                        opacity: 0.8
        //                    }, {
        //                        opacity: 0.8
        //                    }]
        //                }
        //            }
        //        },
        //        xaxis: {
        //            show: false,
        //            mode: 'categories',
        //            tickDecimals: 0
        //        },
        //        yaxis: {
        //            show: false,
        //        },
        //        colors: ["#18C5A9"],
        //        grid: {
        //            color: "#999999",
        //            hoverable: true,
        //            clickable: true,
        //            tickColor: '#DADDE0',// "#D4D4D4",
        //            borderWidth: 0
        //        },
        //        legend: {
        //            show: false
        //        },
        //        tooltip: true,
        //        tooltipOpts: {
        //            content: "x: %x, y: %y"
        //        }
        //    };
        //    var barData = {
        //        label: "bar",
        //        data: [
        //            [1, 44],
        //            [2, 44],
        //            [3, 44],
        //            [4, 44],
        //            [5, 44],
        //            [6, 44]
        //        ]
        //    };
        //    //$.plot($("#flot_bar_chart"), [barData], barOptions);
        //});





    }]);






