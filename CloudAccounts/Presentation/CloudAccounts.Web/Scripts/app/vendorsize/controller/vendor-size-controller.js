﻿FSTApp.controller("VendorSizeController", ['$scope', '$rootScope', 'VendorSizeService', '$timeout', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, VendorSizeService, $timeout, $sessionStorage, ngDialog) {

        $scope.SizeViewModel = {

            SizeId: 0,
            VendorId: 0,
            SizeTitle: null,
            Size1: null,
            Size2: null,
            Size3: null,
            Size4: null,
            Size5: null,
            Size6: null,
            Size7: null,
            Size8: null,
            Size9: null,
            Size10: null,
            Active: false,
            IsDefault: false

        };

        $scope.editcheck = false;

        $scope.listsizes = [];

        var loadsizes = function () {

            $scope.listsizes = [];

            VendorSizeService.getallsizes($sessionStorage.VendorId, function (response) {

                var sizelist = response;

                angular.forEach(sizelist, function (value, key) {

                    $scope.listsizes.push(value);

                });

            });

        };

        $scope.cancel = function () {

            $scope.SizeViewModel = {

                SizeId: 0,
                VendorId: 0,
                SizeTitle: null,
                Size1: null,
                Size2: null,
                Size3: null,
                Size4: null,
                Size5: null,
                Size6: null,
                Size7: null,
                Size8: null,
                Size9: null,
                Size10: null,
                Active: false,
                IsDefault: false

            };

            $scope.editcheck = false;

        };

        $scope.save = function (file) {

            if ($scope.SizeViewModel.SizeTitle == null) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {

                    $scope.SizeViewModel.VendorId = $sessionStorage.VendorId;

                    VendorSizeService.createsize($scope.SizeViewModel, function (response) {

                        if (response == true)
                            alert('Sizes has been added');

                        $scope.cancel();
                        loadsizes();

                    });
                }
                else {

                    VendorSizeService.editsize($scope.SizeViewModel, function (response) {

                        if (response == true)
                            alert('Sizes has been edit');

                        $scope.cancel();
                        loadsizes();

                    });
                }
            }

        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.SizeViewModel = item;
        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadsizes();
        });

    }]);