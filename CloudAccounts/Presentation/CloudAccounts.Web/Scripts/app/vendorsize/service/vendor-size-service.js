﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorSizeService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', 'Upload', function ($http, ServiceManager, Resource, commonobjects, Upload) {


    this.getallsizes = function (id, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(id);

        ServiceManager.execute(Resource.getallsize({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.createsize = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createsize({ data: request }), function (response) {

            callback(response);

        });


    };

    this.editsize = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editsize({ data: request }), function (response) {

            callback(response);

        });
    };


}]);