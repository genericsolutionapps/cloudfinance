﻿FSTApp.controller("StatusController", ['$scope', '$rootScope', '$timeout', 'StatusService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, StatusService, $sessionStorage, ngDialog) {

    $scope.StatusViewModel = {
        StatusId: 0,
        Code: 0,
        Name: null,
        Is_Active: 'Active'
    };

    $scope.editcheck = false;

    $scope.liststatus = [];

    var loadstatus = function () {

        $scope.liststatus = [];

        StatusService.getallstatus(function (response) {

            var statuslist = response;

            angular.forEach(statuslist, function (value, key) {

                $scope.liststatus.push(value);

            });

        });

    };

    $scope.cancel = function () {

        $scope.StatusViewModel = {
            StatusId: 0,
            Code: 0,
            Name: null,
            Is_Active: true
        };

        $scope.editcheck = false;

    };

    $scope.save = function () {

        if ($scope.StatusViewModel.Is_Active == 'Active')
            $scope.StatusViewModel.Is_Active = true;
        else
            $scope.StatusViewModel.Is_Active = false;

        $scope.StatusViewModel.Code = '';

        if ($scope.StatusViewModel.Name == null || $scope.StatusViewModel.Name == undefined ) {

            ngDialog.open({
                template: '<p>Please fill mandetory fields.</p>',
                plain: true,
                className: 'ngdialog-theme-default'
            });

        }
        else {

            if ($scope.editcheck == false) {
                StatusService.createstatus($scope.StatusViewModel, function (response) {

                    if (response == true)
                        alert('Status has been added');

                    $scope.cancel();
                    loadstatus();
                });
            }
            else {

                StatusService.editstatus($scope.StatusViewModel, function (response) {

                    if (response == true)
                        alert('Status has been edit');

                    $scope.cancel();
                    $scope.editcheck = false;
                    loadstatus();
                });
            }
        }
    };

    $scope.edit = function (item) {

        $scope.editcheck = true;

        $scope.StatusViewModel = item;

    };

    $scope.delete = function (item) {



    };

    $timeout(function () {

        loadstatus();

        //$('#statuslist').DataTable({
        //    'paging': true,
        //    'lengthChange': false,
        //    'searching': false,
        //    'ordering': true,
        //    'info': true,
        //    'autoWidth': false
        //});

        $('#statuslist').DataTable();
    });

}]);