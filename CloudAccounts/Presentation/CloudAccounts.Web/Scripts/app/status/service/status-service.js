﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("StatusService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {


   
    this.getallstatus = function (callback) {

        var request = commonobjects.requestobject;

        ServiceManager.execute(Resource.getallstatus(), function (response) {

            callback(response);

        });

    };

    this.createstatus = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createstatus({ data: request }), function (response) {

            callback(response);

        });

    };

    this.editstatus = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editstatus({ data: request }), function (response) {

            callback(response);

        });

       

    };
   
}]);