﻿FSTApp.controller("CustomerController", ['$scope', '$rootScope', '$location', '$timeout', 'CustomerService', 'CategoriesService', 'InvoiceService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $location, $timeout, CustomerService, CategoriesService, InvoiceService, $sessionStorage, ngDialog) {

        $scope.visibility = {
            customerList: true,
            customerForm: false,
            customerInformation: false,
            addressAbroad: false,
            legalEntity: true,
            realPerson: true,
            openingBalance: false,
            noInvoices: false,
            allocationValidation: false
        };

        $scope.customerList = [];

        $scope.invoicesAgainstThisCustomer = [];

        $scope.CustomerViewModel = {
            CustomerId: 0,
            CompanyTitle: '',
            ShortName: null,
            Email: null,
            PhoneNumber: null,
            FaxNumber: null,
            IBANNumber: null,
            District: null,
            Province: null,
            CustomerType: '0001',
            VKN: null,
            Tax: null,
            OpeningBalanceDate: null,
            OpeningBalanceAmount: null,
            CategoryId: null,
            Address: null,
            AuthorizedPersonsCollection: '',
            TotalInvoicedAmount: 0,
            TotalRemainingAmount: 0,
            BusinessPartnerType: ''
        };


        $scope.selectedCategory = '';
        $scope.customerCategories = [];
        $scope.customerCategorySearchList = [];

        $scope.AuthorizedPerson = {
            Name: '',
            Email: null,
            Telephone: null,
            Notes: null
        };

        $scope.allocations = [];

        $scope.allocation = {
            AllocationId: 0,
            Date: '',
            ExpiryDate: '',
            Account: 'Safe Account',
            Amount: 0,
            Explanation: '',
            AllocationType: '0001',
            AllocationMode: '',
            CustomerId: null,
            InvoiceHeaderId: null,
            InvoiceHeaderIdList: ''
        };

        $scope.rows = [$scope.AuthorizedPerson];

        $scope.addNewRow = function () {

            $scope.rows.push(angular.copy($scope.AuthorizedPerson));

            $scope.rows[$scope.rows.length - 1].Name = '';
            $scope.rows[$scope.rows.length - 1].Email = null;
            $scope.rows[$scope.rows.length - 1].Telephone = null;
            $scope.rows[$scope.rows.length - 1].Notes = null;
        }

        $scope.deleteCustomerById = function (customerId) {
            CustomerService.deleteCustomerById(customerId, function (response) {

                if (response == true) {

                    loadBusinessPartnerTable();
                }
            });
        };

        //called when the edit icon next to a customer row is clicked
        $scope.customerInformation = function (customerId) {
            $scope.visibility.customerForm = false;
            $scope.visibility.customerList = false;
            $scope.visibility.customerInformation = true;

            angular.forEach($scope.customerList, function (value, key) {
                if (value.CustomerId === customerId) {
                    $scope.CustomerViewModel.CustomerId = value.CustomerId;
                    $scope.CustomerViewModel.CompanyTitle = value.CompanyTitle;
                    $scope.CustomerViewModel.ShortName = value.ShortName;
                    $scope.CustomerViewModel.Email = value.Email;
                    $scope.CustomerViewModel.PhoneNumber = value.PhoneNumber;
                    $scope.CustomerViewModel.FaxNumber = value.FaxNumber;
                    $scope.CustomerViewModel.IBANNumber = value.IBANNumber;
                    $scope.CustomerViewModel.District = value.District;
                    $scope.CustomerViewModel.Province = value.Province;
                    $scope.CustomerViewModel.CustomerType = value.CustomerType;
                    $scope.CustomerViewModel.VKN = value.VKN;
                    $scope.CustomerViewModel.Tax = value.Tax;
                    $scope.CustomerViewModel.OpeningBalanceDate = value.OpeningBalanceDate;
                    $scope.CustomerViewModel.OpeningBalanceAmount = value.OpeningBalanceAmount;
                    $scope.CustomerViewModel.CategoryId = value.CategoryId;
                    $scope.CustomerViewModel.Address = value.Address;
                    $scope.CustomerViewModel.AuthorizedPersonsCollection = value.AuthorizedPersonsCollection;
                    $scope.CustomerViewModel.TotalInvoicedAmount = value.TotalInvoicedAmount;
                    $scope.CustomerViewModel.TotalRemainingAmount = value.TotalRemainingAmount;
                    $scope.CustomerViewModel.BusinessPartnerType = value.BusinessPartnerType;
                }
            });
            $scope.allocation.Amount = $scope.CustomerViewModel.TotalRemainingAmount;
            $scope.allocation.CustomerId = $scope.CustomerViewModel.CustomerId;


            if ($scope.CustomerViewModel.OpeningBalanceAmount > 0) {
                $scope.visibility.openingBalance = true;
            }

            InvoiceService.getAllInvoicesByCustomerId(customerId, function (response) {

                $scope.invoicesAgainstThisCustomer = [];
                if (response.length > 0) {
                    angular.forEach(response, function (value, key) {
                        var serverDate = new Date(parseInt(value.DueDate.replace(/(^.*\()|([+-].*$)/g, '')));
                        value.DueDate = serverDate.getDate() + "/" + (serverDate.getMonth() + 1) + "/" + serverDate.getFullYear();
                        $scope.invoicesAgainstThisCustomer.push(value);
                    });
                }
                else if (response.length == 0) {
                    $scope.visibility.noInvoices = true;
                }
            });

            CustomerService.getAuthorizedPersonsByCustomerId(customerId, function (response) {

                $scope.rows = [];

                angular.forEach(response, function (value, key) {
                    $scope.rows.push(value);
                });

            });
        };

        $scope.createCustomer = function () {

            var businessPartnerType = $location.search().businessPartnerType;

            if (businessPartnerType != undefined) {
                if (businessPartnerType == 'customer') {
                    $scope.CustomerViewModel.BusinessPartnerType = '0001';
                }
                else if (businessPartnerType == 'supplier') {
                    $scope.CustomerViewModel.BusinessPartnerType = '0002';
                }
            }

            $scope.CustomerViewModel.CategoryName = $scope.selectedCategory;

            //create
            if ($scope.CustomerViewModel.CustomerId == 0) {
                $scope.CustomerViewModel.AuthorizedPersonsCollection = $scope.rows;

                CustomerService.createCustomer($scope.CustomerViewModel, function (response) {

                    if (typeof response == 'number') {

                        loadBusinessPartnerTable();
                    }
                });
            }
                //edit
            else if ($scope.CustomerViewModel.CustomerId > 0) {
                $scope.CustomerViewModel.AuthorizedPersonsCollection = $scope.rows;

                CustomerService.edit($scope.CustomerViewModel, function (response) {

                    if (response == true) {

                        loadBusinessPartnerTable();
                    }
                });
            }
        };


        $scope.createInvoice = function (customerId) {

            $location.path('/Invoice/').search({ customerId: customerId });
        };


        var loadBusinessPartnerTable = function () {

            var businessPartnerType = $location.search().businessPartnerType;

            CustomerService.getAllBusinessPartnersByType(businessPartnerType, function (response) {

                $scope.customerList = [];

                angular.forEach(response, function (value, key) {
                    $scope.customerList.push(value);
                });

                //if (businessPartnerType == 'customer') {
                //    angular.forEach(response, function (value, key) {
                //        $scope.customerList.push(value);
                //    });
                //}
                //else if (businessPartnerType == 'supplier') {
                //    angular.forEach(response, function (value, key) {
                //        if (value.BusinessPartnerType == '0002') {
                //            $scope.customerList.push(value);
                //        }
                //    });
                //}

                $scope.visibility.customerList = true;
                $scope.visibility.customerForm = false;
                $scope.visibility.customerInformation = false;
            });
        };

        loadBusinessPartnerTable();

        var getAllCustomerCategory = function () {

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0006') {
                        $scope.customerCategories.push(value);
                        $scope.customerCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                });
                $scope.setCustomerCategory();
                loadAutoComplete('ddlCategory', $scope.customerCategorySearchList);
            });
        };

        $scope.setCustomerCategory = function () {

            angular.forEach($scope.customerCategories, function (value, key) {

                if ($scope.CustomerViewModel.CategoryId == null) {
                    if (value.CategoryName === $scope.selectedCategory) {
                        $scope.CustomerViewModel.CategoryId = value.CategoryId;
                    }
                }
                else if ($scope.CustomerViewModel.CategoryId > 0) {
                    if (value.CategoryId === $scope.CustomerViewModel.CategoryId) {
                        $scope.selectedCategory = value.CategoryName;
                    }
                }
            });
        };


        $scope.removeRow = function (index) {
            $scope.rows.splice(index, 1);
        }


        //Called when create new customer or edit button is clicked
        $scope.customerForm = function () {
            $scope.visibility.customerList = false;
            $scope.visibility.customerForm = true;
            $scope.visibility.customerInformation = false;

            getAllCustomerCategory();
        }

        $scope.customerType = function () {

            if ($scope.visibility.legalEntity == true) {
                $scope.visibility.legalEntity = false;
                $scope.visibility.realPerson = true;
                $scope.CustomerViewModel.CustomerType = '0002';
            }
            else {
                $scope.visibility.legalEntity = true;
                $scope.visibility.realPerson = false;
                $scope.CustomerViewModel.CustomerType = '0001';
            }
        }

        $timeout(function () {

            loadAutoComplete('ddlcustomer', $scope.source);

        });


        $scope.addAllocation = function () {

            if ($scope.allocation.Amount > 0 && $scope.allocation.Amount <= $scope.CustomerViewModel.TotalRemainingAmount) {

                $scope.allocation.InvoiceHeaderIdList = [];

                angular.forEach($scope.invoicesAgainstThisCustomer, function (value, key) {
                    $scope.allocation.InvoiceHeaderIdList.push(value.InvoiceHeaderId);
                });

                if ($scope.allocation.ExpiryDate == '') {
                    $scope.allocation.AllocationMode = '0001';
                }
                else {
                    $scope.allocation.AllocationMode = '0002';
                }

                InvoiceService.addAllocation($scope.allocation, function (response) {

                    if (response == true) {
                        loadBusinessPartnerTable();
                    }
                });
            }
            else {
                $scope.visibility.allocationValidation = true;
            }
        };

        $scope.Cancel = function () {

            $scope.visibility.customerList = true;
            $scope.visibility.customerForm = false;
            $scope.visibility.customerInformation = false;
        };


    }]);






