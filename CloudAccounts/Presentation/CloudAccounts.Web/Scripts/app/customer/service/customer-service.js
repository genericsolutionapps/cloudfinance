﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("CustomerService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.createCustomer = function (customerViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(customerViewModel);

        ServiceManager.execute(Resource.createcustomer({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.edit = function (customerViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(customerViewModel);

        ServiceManager.execute(Resource.editcustomer({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.deleteCustomerById = function (customerId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(customerId);

        ServiceManager.execute(Resource.deletecustomerbyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.getAllBusinessPartnersByType = function (businessPartnerType, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(businessPartnerType);

        ServiceManager.execute(Resource.getallbusinesspartnersbytype({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.getAuthorizedPersonsByCustomerId = function (customerId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(customerId);

        ServiceManager.execute(Resource.getauthorizedpersonsbycustomerid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


}]);





