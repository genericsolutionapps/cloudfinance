﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("SubcategoryService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {

        this.getallcategory = function (callback) {

            var request = commonobjects.requestobject;
            ServiceManager.execute(Resource.getallcategory(), function (response) {

                callback(response);

            });

        };

        this.getallsubcategory = function (callback) {

            var request = commonobjects.requestobject;

            ServiceManager.execute(Resource.getallsubcategory(), function (response) {

                callback(response);

            });

        };

        this.createsubcategory = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.createsubcategory({ data: request }), function (response) {

                callback(response);

            });

        };

        this.editsubcategory = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.editsubcategory({ data: request }), function (response) {

                callback(response);

            });



        };

    }]);