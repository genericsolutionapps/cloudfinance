﻿FSTApp.controller("SubcategoryController", ['$scope', '$rootScope', '$timeout', 'SubcategoryService', '$sessionStorage', 'ngDialog', 'CategoryService',
    function ($scope, $rootScope, $timeout, SubcategoryService, $sessionStorage, ngDialog, CategoryService) {

        $scope.SubcategoryViewModel = {
            SubCategoryID: 0,
            CategoryID: 0,
            SubCategory_Name: null,
            Is_Active: 'Active'
        };


        $scope.CategoryViewModel = {

            CategoryID: 0,
            Category_Name: null,
            Is_Active: 'Active'
        }

        

        $scope.editcheck = false;

        $scope.listsubcategory = [];

        $scope.categorylist = [];
        $scope.selectedCategory = null;


        var loadcategory = function () {


            CategoryService.getallcategory(function (response) {

                var categorylist = response;

                angular.forEach(categorylist, function (value, key) {

                    $scope.categorylist.push(value);

                });

            });

        };


        var loadsubcategory = function () {

            $scope.listsubcategory = [];

            SubcategoryService.getallsubcategory(function (response) {

                var subcategorylist = response;

                angular.forEach(subcategorylist, function (value, key) {

                    $scope.listsubcategory.push(value);

                });

            });

        };


        $scope.cancel = function () {

            $scope.SubcategoryViewModel = {
                SubCategoryID: 0,
                CategoryID: 0,
                SubCategory_Name: null,
                Is_Active: true
            };

            $scope.selectedCategory = null;
            $scope.editcheck = false;

        };

        $scope.save = function () {

            if ($scope.SubcategoryViewModel.Is_Active == 'Active')
                $scope.SubcategoryViewModel.Is_Active = true;
            else
                $scope.SubcategoryViewModel.Is_Active = false;

            if ($scope.selectedCategory != null && $scope.selectedCategory != undefined) {

                $scope.SubcategoryViewModel.CategoryID = $scope.selectedCategory;
            }


            if ($scope.SubcategoryViewModel.SubCategory_Name == null || $scope.SubcategoryViewModel.SubCategory_Name == undefined) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {
                    SubcategoryService.createsubcategory($scope.SubcategoryViewModel, function (response) {

                        if (response == true)
                            alert('subcategory has been added');

                        $scope.cancel();
                        $scope.selectedCategory = null;
                        loadsubcategory();
                    });
                }
                else {

                    SubcategoryService.editsubcategory($scope.SubcategoryViewModel, function (response) {

                        if (response == true)
                            alert('subcategory has been edit');

                        $scope.cancel();
                        $scope.selectedCategory = null;
                        $scope.editcheck = false;
                        loadsubcategory();
                    });
                }
            }
        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.SubcategoryViewModel = item;
            $scope.selectedCategory = '' + item.CategoryID + '';

        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadcategory();
            loadsubcategory();

            //$('#subcategorylist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $('#subcategorylist').DataTable();
        });

    }]);