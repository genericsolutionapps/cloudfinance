﻿FSTApp.factory("CommonObject", function () {

    var commonobject = {};

    commonobject.requestobject = {

        data: ''

    };

    commonobject.responseobject = {

        ErrorMessage: '',
        ResponseCode: '',
        IsSuccess: '',
        ResultData: ''

    }

    return commonobject;

});