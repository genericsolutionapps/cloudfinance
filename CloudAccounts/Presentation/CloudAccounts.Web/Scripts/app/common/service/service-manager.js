﻿FSTApp.factory("ServiceManager", ['CommonObject', 'ngDialog', '$rootScope', '$location', 'Resource', '$sessionStorage',
    function (commonobject, ngDialog, $rootScope, $location, Resource, $sessionStorage) {

    var serviceManager = {};

    serviceManager.execute = function (service, callback) {

        service.$promise.then(function (response) {

            if (response.IsSuccess) {

                callback(response.ResultData);

            }
            else if(response.IsSuccess == undefined){
                callback(response);
            }
            else if (!response.IsSuccess) {

                if (response.ResponseCode == "0002") {

                    //ngDialog.open({
                    //    template: '<p>' + response.ErrorMessage + '</p>',
                    //    plain: true,
                    //    className: 'ngdialog-theme-default'
                    //});

                    alertify.error(response.ErrorMessage);

                    _redirecttologin();

                    $rootScope.stopSpin();
                }
                else {

                    if (response.ErrorMessage != null && response.ErrorMessage != undefined) {

                        //ngDialog.open({
                        //    template: '<p>' + response.ErrorMessage + '.</p>',
                        //    plain: true,
                        //    className: 'ngdialog-theme-default'
                        //});

                        alertify.error(response.ErrorMessage);

                        $rootScope.stopSpin();
                    }
                }
                callback(response.IsSuccess);
            }
        });
    };

    var _redirecttologin = function () {

        Resource.logoutuser({ data: JSON.stringify($sessionStorage.UserId) }).$promise.then(function (response) {

            if (response.IsSuccess == true) {

                delete $sessionStorage.UserId;
                delete $sessionStorage.Username;
                delete $sessionStorage.VendorId;
                delete $sessionStorage.BuyerId;

                $location.path('/VendorLogin');

            }

        });

    };


    return serviceManager;

}]);