﻿FSTApp.factory("Resource", ['$resource', function ($resource) {

    return $resource('', {}, {

        'submitvendorregistration': { url: 'VendorRegister/Submit', method: "POST", isArray: false },
        'editvendor': { url: 'VendorRegister/EditVendor', method: "POST", isArray: false },
        'getallvendorlist': { url: 'VendorRegister/ReturnAllVendor', method: "GET", isArray: false },
        'loginsubmit': { url: 'Login/LoginSubmit', method: "POST", isArray: false },
        'forgotpassword': { url: 'Login/ForgotPassword', method: "POST", isArray: false },
        'checkemailverification': { url: 'Activation/CheckEmailVerification', method: "GET", isArray: false },
        'registration': { url: 'Registration/RegisterUser', method: "POST", isArray: false },
        'logincontrolpanel': { url: 'PanelLogin/LoginControlPanel', method: "POST", isArray: false },

        'getcompany': { url: 'Company/Index', method: "GET", isArray: false },

        //Categories
        'createcategory': { url: 'Categories/Create', method: "POST", isArray: false },
        'getallcategories': { url: 'Categories/Index', method: "GET", isArray: false },
        'editcategories': { url: 'Categories/Edit', method: "POST", isArray: false },
        'deletecategories': { url: 'Categories/Delete', method: "POST", isArray: false },
        'getallsalescategories': { url: 'Categories/GetAllSalesCategories', method: "GET", isArray: false },
        'getallcustomercategories': { url: 'Categories/GetAllCustomerCategories', method: "GET", isArray: false },
        'getallproductcategories': { url: 'Categories/GetAllProductCategories', method: "GET", isArray: false },


        //Customers
        'getallbusinesspartnersbytype': { url: 'Customer/Index', method: "GET", isArray: false },
        'createcustomer': { url: 'Customer/Create', method: "POST", isArray: false },
        'getauthorizedpersonsbycustomerid': { url: 'Customer/GetAuthorizedPersonsByCustomerId', method: "GET", isArray: false },
        'deletecustomerbyid': { url: 'Customer/Delete', method: "POST", isArray: false },
        'editcustomer': { url: 'Customer/Edit', method: "POST", isArray: false },



        'createmastercategory': { url: 'PanelCategory/CreateMasterCategory', method: "POST", isArray: false },
        'getallmastercategory': { url: 'PanelCategory/GetAllMasterCategory', method: "POST", isArray: false },
        'getallmastercategorybyparentid': { url: 'PanelCategory/GetAllMasterCategoryByParentId', method: "GET", isArray: false },
        'createmarketingcategory': { url: 'PanelCategory/CreateMarketingCategory', method: "POST", isArray: false },
        'createeventcategory': { url: 'PanelCategory/CreateEventCategory', method: "POST", isArray: false },
        'getalleventcategory': { url: 'PanelCategory/GetAllEventCategory', method: "GET", isArray: false },
        'getalleventcategorybyparentid': { url: 'PanelCategory/GetAllEventCategoryByParentId', method: "GET", isArray: false },
        'getallmastercolor': { url: 'Color/ReturnAllMasterColorColor', method: "GET", isArray: false },
        'getallcolor': { url: 'Color/ReturnAllColor', method: "GET", isArray: false },
        'createcolor': { url: 'Color/CreateColor', method: "GET", isArray: false },
        'editcolor': { url: 'Color/EditColor', method: "GET", isArray: false },
        'getallpack': { url: 'Pack/ReturnAllPack', method: "GET", isArray: false },
        'createpack': { url: 'Pack/CreatePack', method: "GET", isArray: false },
        'editpack': { url: 'Pack/EditPack', method: "GET", isArray: false },
        'getallsize': { url: 'Size/ReturnAllSize', method: "GET", isArray: false },
        'createsize': { url: 'Size/CreateSize', method: "GET", isArray: false },
        'editsize': { url: 'Size/EditSize', method: "GET", isArray: false },
        'getallfirstcategorymaster': { url: 'CategoryVendor/ReturnAllFirstCategoryMaster', method: "GET", isArray: false },
        'getallcategorymasterbyId': { url: 'CategoryVendor/ReturnAllCategoryMasterById', method: "GET", isArray: false },
        'getallcategoryvendor': { url: 'CategoryVendor/ReturnAllCategoryVendor', method: "GET", isArray: false },
        'getallcategoryvendorwithoutheirarchy': { url: 'CategoryVendor/ReturnAllCategoryVendorWithOutHeirarchy', method: "GET", isArray: false },
        'createcategoryvendor': { url: 'CategoryVendor/CreateCategoryVendor', method: "GET", isArray: false },
        'editcategoryvendor': { url: 'CategoryVendor/EditCategoryVendor', method: "GET", isArray: false },
        'logoutuser': { url: 'Login/LogoutUser', method: "POST", isArray: false },
        'createvendorgroup': { url: 'VendorGroup/CreateVendorGroup', method: "POST", isArray: false },
        'editvendorgroup': { url: 'VendorGroup/EditVendorGroup', method: "POST", isArray: false },
        'getallvendorgroup': { url: 'VendorGroup/ReturnAllVendorGroup', method: "GET", isArray: false },
        'getallmembershiptype': { url: 'VendorRegister/ReturnAllMembershipType', method: "GET", isArray: false },
        'getallraces': { url: 'Race/ReturnAllRaces', method: "GET", isArray: false },
        'createrace': { url: 'Race/CreateRace', method: "GET", isArray: false },
        'editrace': { url: 'Race/EditRace', method: "GET", isArray: false },
        'getallpriceranges': { url: 'PriceRange/ReturnAllPriceRange', method: "GET", isArray: false },
        'createpricerange': { url: 'PriceRange/CreatePriceRange', method: "GET", isArray: false },
        'editpricerange': { url: 'PriceRange/EditPriceRange', method: "GET", isArray: false },
        'getallstatus': { url: 'Status/ReturnAllStatus', method: "GET", isArray: false },
        'createstatus': { url: 'Status/CreateStatus', method: "GET", isArray: false },
        'editstatus': { url: 'Status/EditStatus', method: "GET", isArray: false },



        'getallsubcategory': { url: 'Subcategory/ReturnAllSubcategory', method: "GET", isArray: false },
        'createsubcategory': { url: 'Subcategory/CreateSubcategory', method: "GET", isArray: false },
        'editsubcategory': { url: 'Subcategory/EditSubcategory', method: "GET", isArray: false },
        'getallcategory': { url: 'Subcategory/ReturnAllCategory', method: "GET", isArray: false },


        'getalluser': { url: 'User/Index', method: "GET", isArray: false },
        'getuserbyid': { url: 'User/Details', method: "GET", isArray: false },
        'edituser': { url: 'User/Edit', method: "POST", isArray: false },
        'inviteuser': { url: 'User/Create', method: "POST", isArray: false },


        'getallapplicationuser': { url: 'ApplicationUser/ReturnAllApplicationUser', method: "GET", isArray: false },
        'createapplicationuser': { url: 'ApplicationUser/CreateApplicationUser', method: "GET", isArray: false },
        'editapplicationuser': { url: 'ApplicationUser/EditApplicationUser', method: "GET", isArray: false },
        'getallemployee': { url: 'ApplicationUser/ReturnAllEmployee', method: "GET", isArray: false },
        'getallrole': { url: 'ApplicationUser/ReturnAllRole', method: "GET", isArray: false },


        //invoices
        'createinvoice': { url: 'Invoice/Create', method: "POST", isArray: false },
        'getallinvoices': { url: 'Invoice/Index', method: "GET", isArray: false },
        'getallinvoicesbycustomerid': { url: 'Invoice/GetAllInvoicesByCustomerId', method: "GET", isArray: false },
        'getallinvoicedetailsbyinvoiceheaderid': { url: 'Invoice/GetAllInvoiceDetailsByInvoiceHeaderId', method: "GET", isArray: false },
        //'getcurrencyexchangerate': { url: 'https://data.fixer.io/api/latest?access_key=441e7973e3d8a07a40d32afd4c128996&base=TRY&symbols=USD,EUR,GBP', method: "GET", isArray: false },
        //'getusdrate': { url: 'https://data.fixer.io/api/latest?access_key=441e7973e3d8a07a40d32afd4c128996&base=TRY&base=USD&symbols=TRY', method: "GET", isArray: false },
        //'geteurrate': { url: 'https://data.fixer.io/api/latest?access_key=441e7973e3d8a07a40d32afd4c128996&base=TRY&base=EUR&symbols=TRY', method: "GET", isArray: false },
        //'getgbprate': { url: 'https://data.fixer.io/api/latest?access_key=441e7973e3d8a07a40d32afd4c128996&base=TRY&base=GBP&symbols=TRY', method: "GET", isArray: false },

        'getusdrate': { url: 'http://free.currencyconverterapi.com/api/v5/convert?q=USD_TRY&compact=y', method: "GET", isArray: false },
        'geteurrate': { url: 'http://free.currencyconverterapi.com/api/v5/convert?q=EUR_TRY&compact=y', method: "GET", isArray: false },
        'getgbprate': { url: 'http://free.currencyconverterapi.com/api/v5/convert?q=GBP_TRY&compact=y', method: "GET", isArray: false },

        'addallocation': { url: 'Invoice/AddAllocation', method: "POST", isArray: false },
        'getallallocationsbyinvoiceheaderid': { url: 'Invoice/GetAllAllocationsByInvoiceHeaderId', method: "GET", isArray: false },
        'deleteinvoicebyid': { url: 'Invoice/Delete', method: "POST", isArray: false },
        'editinvoice': { url: 'Invoice/Edit', method: "POST", isArray: false },
        'getbalancebymonth': { url: 'Invoice/GetBalanceByMonth', method: "GET", isArray: false },
        'getalldelayedinvoices': { url: 'Invoice/GetAllDelayedInvoices', method: "GET", isArray: false },
        'getinvoicesbyproductid': { url: 'Invoice/GetInvoicesByProductId', method: "GET", isArray: false },
        'getinvoicebyid': { url: 'Invoice/GetInvoiceById', method: "GET", isArray: false },
        'getallinvoicedetails': { url: 'Invoice/GetAllInvoiceDetails', method: "GET", isArray: false },
        'getallallocations': { url: 'Invoice/GetAllAllocations', method: "GET", isArray: false },



        'getallcategory': { url: 'Category/ReturnAllCategory', method: "GET", isArray: false },
        //'createcategory': { url: 'Category/CreateCategory', method: "GET", isArray: false },
        //'editcategory': { url: 'Category/EditCategory', method: "GET", isArray: false },

        'getallrole': { url: 'Role/ReturnAllrole', method: "GET", isArray: false },
        'createrole': { url: 'Role/CreateRole', method: "GET", isArray: false },
        'editrole': { url: 'Role/EditRole', method: "GET", isArray: false },




        //'getallcompany': { url: 'Company/ReturnAllCompany', method: "GET", isArray: false },
        'createcompany': { url: 'Company/CreateCompany', method: "GET", isArray: false },
        'editcompany': { url: 'Company/Edit', method: "POST", isArray: false },

        'getallvisit': { url: 'Visit/ReturnAllVisit', method: "GET", isArray: false },
        'createvisit': { url: 'Visit/CreateVisit', method: "GET", isArray: false },
        'editvisit': { url: 'Visit/EditVisit', method: "GET", isArray: false },



        'getalldesignation': { url: 'Designation/ReturnAllDesignation', method: "GET", isArray: false },
        'getalldepartment': { url: 'Department/ReturnAllDepartment', method: "GET", isArray: false },
        'getallbranch': { url: 'Branch/ReturnAllBranch', method: "GET", isArray: false },

        //product
        'addproduct': { url: 'Product/Create', method: "POST", isArray: false },
        'getallproducts': { url: 'Product/Index', method: "GET", isArray: false },
        'getproductbyid': { url: 'Product/GetProductById', method: "GET", isArray: false },
        'editproduct': { url: 'Product/Edit', method: "POST", isArray: false },
        'deleteprodcut': { url: 'Product/Delete', method: "POST", isArray: false },


        //cash-bank
        'createaccount': { url: 'CashBank/Create', method: "POST", isArray: false },
        'getallaccounts': { url: 'CashBank/Index', method: "GET", isArray: false },
        'getcashbankbyid': { url: 'CashBank/GetCashBankById', method: "GET", isArray: false },
        'addgeneralvoucher': { url: 'CashBank/AddGeneralVoucher', method: "POST", isArray: false },
        'editcashbankaccount': { url: 'CashBank/Edit', method: "POST", isArray: false },
        'deletecashbankaccount': { url: 'CashBank/Delete', method: "POST", isArray: false },


        //Noor
        'createExpenseList': { url: '', method: "POST", isArray: false },
        'createSupplier': { url: 'Supplier/Create', method: "POST", isArray: false },
        'getSupplier': { url: 'Supplier/Getsupplier', method: "POST", isArray: false },
        'getAllSupplierList': { url: 'Supplier/GetAllSuppliers', method: "POST", isArray: false },
        'getcurrency': { url: 'Supplier/GetAllCurrency', method: "GET", isArray: false },

        'getAllCompanies': { url: 'Company/GetAllCompanies', method: "POST", isArray: false },
        'saveNotes': { url: 'Supplier/SaveCustomerSupplierNotes', method: "POST", isArray: false },


        //Employees
        'getemployeebyid': { url: 'Employee/GetEmployeeById', method: "GET", isArray: false },
        'editemployee': { url: 'Employee/Edit', method: "POST", isArray: false },
        'getallemployees': { url: 'Employee/Index', method: "GET", isArray: false },
        'createemployee': { url: 'Employee/Create', method: "POST", isArray: false },
        'deleteemployee': { url: 'Employee/Delete', method: "POST", isArray: false },
        'addadvance': { url: 'Employee/AddAdvance', method: "POST", isArray: false },
        'addsalary': { url: 'Employee/AddSalary', method: "POST", isArray: false },


        //VAT Report
        'getVATbymonth': { url: 'Invoice/GetVATByMonth', method: "GET", isArray: false },

        //Expense Report
        'getallexpensecategories': { url: 'Categories/GetAllExpenseCategories', method: "GET", isArray: false },
        'getallbusinesspartnercategories': { url: 'Categories/GetAllBusinessPartnerCategories', method: "GET", isArray: false },


    });

}]);




