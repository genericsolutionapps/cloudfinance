﻿FSTApp.controller("SidebarController", ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'ServiceManager', 'ngDialog', '$sessionStorage', 'Resource',
    function ($scope, $rootScope, $location, $routeParams, $timeout, ServiceManager, ngDialog, $sessionStorage, Resource) {

        //settings
        $scope.company = function () {

            $location.search({});
            $location.path('/Company');
        };

        $scope.categories = function () {

            $location.search({});
            $location.path('/Categories');
        };

        $scope.user = function () {

            $location.search({});
            $location.path('/User');
        };

        $scope.paymentReport = function () {

            $location.search({});
            $location.path('/PaymentReport');
        };

        //sales
        $scope.invoice = function () {

            $location.search({});
            $location.path('/Invoice');
        };

        $scope.customer = function () {

            $location.search({});
            $location.path('/Customer').search({ businessPartnerType: 'customer' });
        };

        $scope.salesReport = function () {

            $location.search({});
            $location.path('/SalesReport');
        };

        $scope.incomeExpenseReport = function () {

            $location.search({});
            $location.path('/IncomeExpenseReport');
        };

        $scope.collectionsReport = function () {

            $location.search({});
            $location.path('/CollectionsReport');
        };

        //expense
        $scope.expenseList = function () {

            $location.search({});
            $location.path('/ExpenseList');
        };

        $scope.supplier = function () {

            $location.search({});
            $location.path('/Customer').search({ businessPartnerType: 'supplier' });
        };

        $scope.employee = function () {

            $location.search({});
            $location.path('/employee');
        };

        $scope.expenseReport = function () {

            $location.search({});
            $location.path('/ExpenseReport');
        };

        $scope.VATReport = function () {

            $location.search({});
            $location.path('/VATReport');
        };

        //cash / bank
        $scope.cashBank = function () {

            $location.search({});
            $location.path('/CashBankAccountList');
        };

        $scope.cashBankReport = function () {

            $location.search({});
            $location.path('/CashBankReport');
        };

        //stock
        $scope.product = function () {

            $location.search({});
            $location.path('/ProductList');
        };

        $scope.stockHistory = function () {

            $location.search({});
            $location.path('/StockHistory');
        };

        $scope.productsInStockReport = function () {

            $location.search({});
            $location.path('/ProductsInStockReport');
        };

        $scope.logOut = function () {

            $location.search({});

            ServiceManager.execute(Resource.logoutuser({ data: JSON.stringify($sessionStorage.UserId) }), function (response) {

                delete $sessionStorage.UserId;
                delete $sessionStorage.Username;
                delete $sessionStorage.VendorId;
                delete $sessionStorage.BuyerId;

                $location.path('/');
            });
        };
    }]);



