﻿FSTApp.controller("HeaderController", ['$scope', '$rootScope', '$location', '$timeout', 'ServiceManager', 'ngDialog', '$sessionStorage', 'Resource',
    function ($scope, $rootScope, $location, $timeout, ServiceManager, ngDialog, $sessionStorage, Resource) {

        var intialize = function () {
            $scope.Email = $sessionStorage.Email;
        }


        intialize();


        $scope.dashboard = function () {

            $location.path('/Dashboard');
        };

        $scope.logOut = function () {

            $location.search({});

            ServiceManager.execute(Resource.logoutuser({ data: JSON.stringify($sessionStorage.UserId) }), function (response) {

                delete $sessionStorage.UserId;
                delete $sessionStorage.Username;
                delete $sessionStorage.VendorId;
                delete $sessionStorage.BuyerId;

                $location.path('/');
            });
        };

    }]);


