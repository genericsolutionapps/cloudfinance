﻿FSTApp.controller("MasterController", ['$scope', 'usSpinnerService', '$rootScope', '$location', 'CommonObject', '$sessionStorage', 'ServiceManager', 'Resource',

function ($scope, usSpinnerService, $rootScope, $location, commonobjects, $sessionStorage, ServiceManager, Resource) {


    //var initialize = function () {

    //    if (!$rootScope.loggedIn) {

    //        $location.path('/');
    //    }
    //};

    //initialize();



    $rootScope.startSpin = function () {

        var result = document.getElementsByClassName("us-spinner-wrapper");
        var element = angular.element(result);
        element.addClass('loading');
        usSpinnerService.spin('spinner-1');

    };

    $rootScope.stopSpin = function () {

        var result = document.getElementsByClassName("us-spinner-wrapper");
        var element = angular.element(result);
        element.removeClass('loading');
        usSpinnerService.stop('spinner-1');
    };

    $scope.logout = function () {

        ServiceManager.execute(Resource.logoutuser({ data: JSON.stringify($sessionStorage.UserId) }), function (response) {

            delete $sessionStorage.UserId;
            delete $sessionStorage.Username;
            delete $sessionStorage.VendorId;
            delete $sessionStorage.BuyerId;

            $location.path('/VendorLogin');
        });
    };

}]);