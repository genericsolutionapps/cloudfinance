﻿FSTApp.controller("CashBankListController", ['$scope', '$rootScope', '$timeout', 'CashBankService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, CashBankService, $sessionStorage, ngDialog, $location) {

        var initialize = function () {

            var userId = $sessionStorage.UserId;

            $scope.accounts = [];

            CashBankService.getAllAccounts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.accounts.push(value);
                });
            });
        };


        initialize();

        $scope.createAccount = function (accountType) {

            $location.path('/CreateAccount').search({ accountType: accountType });
        };

        $scope.accountInformation = function (cashBankId) {

            $location.path('/CashBankAccountInformation/').search({ cashBankId: cashBankId });
        };


    }]);
