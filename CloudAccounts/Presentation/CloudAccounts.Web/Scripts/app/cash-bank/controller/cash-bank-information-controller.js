﻿FSTApp.controller("CashBankAccountInformationController", ['$scope', '$rootScope', '$timeout', 'CashBankService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, CashBankService, $sessionStorage, ngDialog, $location) {

        $scope.generalVoucherViewModel = {
            GLDetailId: 0,
            GLHeaderId: 0,
            Date: null,
            Amount: 0,
            Explanation: null,
            Debit: 0,
            Credit: 0,
            AccountCode: null,
            TransactionType: null
            //GeneralVoucherType: null
        };

        $scope.cashBankAccount = [];

        $scope.totalDebitAmount = 0;
        $scope.totalCreditAmount = 0;

        //$scope.product = [];

        //$scope.allInvoicesByProductId = [];


        var initialize = function () {

            var cashBankId = $location.search().cashBankId;

            CashBankService.getCashBankById(cashBankId, function (response) {

                $scope.totalDebitAmount = 0;
                $scope.totalCreditAmount = 0;

                angular.forEach(response.GLDetails, function (value, key) {

                    value.Date = parseDate(value.Date);

                    $scope.totalDebitAmount = $scope.totalDebitAmount + value.Debit;
                    $scope.totalCreditAmount = $scope.totalCreditAmount + value.Credit;
                });

                response.OpeningBalanceDate = parseDate(response.OpeningBalanceDate);

                $scope.cashBankAccount.push(response);
            });
        };


        initialize();


        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

        $scope.addGeneralVoucher = function (type) {

            $scope.generalVoucherViewModel.AccountCode = $scope.cashBankAccount[0].AccountCode;

            if (type == 'add') {
                $scope.generalVoucherViewModel.Debit = $scope.generalVoucherViewModel.Amount;
                $scope.generalVoucherViewModel.Credit = 0;
                $scope.generalVoucherViewModel.TransactionType = 'Cash Addition';
            }
            else if (type == 'remove') {
                $scope.generalVoucherViewModel.Debit = 0;
                $scope.generalVoucherViewModel.Credit = $scope.generalVoucherViewModel.Amount;
                $scope.generalVoucherViewModel.TransactionType = 'Cash Removal';
            }

            CashBankService.addGeneralVoucher($scope.generalVoucherViewModel, function (response) {

                initialize();
            });
        };

        $scope.delete = function (cashBankId) {

            CashBankService.delete(cashBankId, function (response) {

                if (response == true) {
                    $location.path('/CashBankAccountList');
                }
            });
        };

        $scope.edit = function () {

            var cashBankId = $scope.cashBankAccount[0].CashBankId;

            $location.path('/CreateAccount/').search({ cashBankId: cashBankId });
        };

        //$scope.delete = function () {

        //    var productId = $scope.product[0].ProductId;

        //    ProductService.delete(productId, function (response) {

        //        if (response == true) {

        //            $location.path('/ProductList');
        //        }
        //    });
        //};



    }]);
