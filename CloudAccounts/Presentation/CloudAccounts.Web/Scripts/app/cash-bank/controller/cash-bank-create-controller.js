﻿FSTApp.controller("CashBankCreateController", ['$scope', '$rootScope', '$timeout', 'CashBankService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, CashBankService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.visibility = {
            Bank: true,
            Edit: false
        };

        $scope.accountViewModel = {
            CashBankId: 0,
            AccountName: null,
            CurrencyName: 'TRL – Turkish Lira',
            CurrencyRate: 1,
            OpeningBalance: 0,
            OpeningBalanceDate: null,
            BankName: null,
            Branch: null,
            AccountNumber: null,
            IBAN: null,
            AccountType: null
        };

        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: new Date(),
            EndDate: new Date(),
            ReportName: ''
        };

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1 },
            { index: 1, name: 'USD', rate: 2 },
            { index: 2, name: 'EUR', rate: 3 },
            { index: 3, name: 'GBP', rate: 4 }
        ];

        $scope.selectedCurrency = 'LIRA';


        var initialize = function () {

            var accountType = $location.search().accountType;

            if (accountType == undefined) {

                $scope.visibility.Edit = true;

                var cashBankId = $location.search().cashBankId;

                CashBankService.getCashBankById(cashBankId, function (response) {

                    if (response) {
                        $scope.accountViewModel.CashBankId = response.CashBankId;
                        $scope.accountViewModel.AccountName = response.AccountName;
                        $scope.accountViewModel.CurrencyName = response.CurrencyName;
                        $scope.accountViewModel.CurrencyRate = response.CurrencyRate;
                        $scope.accountViewModel.OpeningBalance = response.OpeningBalance;
                        $scope.accountViewModel.OpeningBalanceDate = response.OpeningBalanceDate;
                        $scope.accountViewModel.BankName = response.BankName;
                        $scope.accountViewModel.Branch = response.Branch;
                        $scope.accountViewModel.AccountNumber = response.AccountNumber;
                        $scope.accountViewModel.IBAN = response.IBAN;
                        $scope.accountViewModel.AccountType = response.AccountType;

                        if ($scope.accountViewModel.AccountType == '0001') {
                            $scope.visibility.Bank = false;
                        }
                        else if ($scope.accountViewModel.AccountType == '0002') {
                            $scope.visibility.Bank = true;
                        }
                    }
                });
            }
            else {
                $scope.visibility.Edit = false;

                if (accountType == 'cash') {
                    $scope.visibility.Bank = false;
                }
                else if (accountType == 'bank') {
                    $scope.visibility.Bank = true;
                }
            }
        };

        initialize();

        $scope.setCurrency = function (currencyName) {

            if ($scope.accountViewModel.CurrencyName == 'TRL – Turkish Lira') {

                $scope.accountViewModel.CurrencyRate = $scope.currencies[0].rate.toFixed(4);
                $scope.selectedCurrency = $scope.currencies[0].name;
            }
            else if ($scope.accountViewModel.CurrencyName == 'USD – US Dollar') {

                $scope.accountViewModel.CurrencyRate = $scope.currencies[1].rate.toFixed(4);
                $scope.selectedCurrency = $scope.currencies[1].name;
            }
            else if ($scope.accountViewModel.CurrencyName == 'EUR – Euro') {

                $scope.accountViewModel.CurrencyRate = $scope.currencies[2].rate.toFixed(4);
                $scope.selectedCurrency = $scope.currencies[2].name;
            }
            else if ($scope.accountViewModel.CurrencyName == 'GBP – British Pond') {

                $scope.accountViewModel.CurrencyRate = $scope.currencies[3].rate.toFixed(4);
                $scope.selectedCurrency = $scope.currencies[3].name;
            }
        };

        $scope.createAccount = function () {

            var accountType = $location.search().accountType;

            if (accountType != undefined) {

                if (accountType == 'cash') {
                    $scope.accountViewModel.AccountType = '0001';
                }
                else if (accountType == 'bank') {
                    $scope.accountViewModel.AccountType = '0002';
                }

                CashBankService.createAccount($scope.accountViewModel, function (response) {

                    if (response == true) {

                        $location.path('/CashBankAccountList');
                    }
                });
            }
            else if (accountType == undefined) {
                CashBankService.edit($scope.accountViewModel, function (response) {

                    if (response == true) {

                        $location.path('/CashBankAccountList');
                    }
                });
            }

        }

        var getCurrencyExchangeRate = function () {

            InvoiceService.getUSDRate(function (response) {
                $scope.currencies[1].rate = response.USD_TRY.val;
            });
            InvoiceService.getEURRate(function (response) {
                $scope.currencies[2].rate = response.EUR_TRY.val;
            });
            InvoiceService.getGBPRate(function (response) {
                $scope.currencies[3].rate = response.GBP_TRY.val;
            });
        };
        getCurrencyExchangeRate();


    }]);













