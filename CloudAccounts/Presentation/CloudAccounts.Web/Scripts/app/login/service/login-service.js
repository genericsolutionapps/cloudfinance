﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("LoginService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {


    this.loginsubmit = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.loginsubmit({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.forgotpassword = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.forgotpassword({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };
   
}]);