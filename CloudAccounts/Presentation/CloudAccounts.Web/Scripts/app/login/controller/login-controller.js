﻿FSTApp.controller("LoginController", ['$scope', '$rootScope', '$location', '$timeout', 'LoginService', 'ngDialog', '$sessionStorage',
    function ($scope, $rootScope, $location, $timeout, LoginService, ngDialog, $sessionStorage) {


        $('body').addClass('login-page');

        $scope.Login = {

            User_Name: '',
            Password: ''

        };

        $scope.alert = function () {

            alertify.alert("Please check your Email address for a recovery email.");
        };


        $scope.submit = function () {

            if ($scope.Login.User_Name != '' && $scope.Login.Password != '') {

                $rootScope.startSpin();
                LoginService.loginsubmit($scope.Login, function (response) {

                    delete $sessionStorage.UserId;
                    delete $sessionStorage.Email;

                    $sessionStorage.UserId = response.UserId;
                    $sessionStorage.Email = response.Email;

                    $rootScope.Email = response.Email;

                    $rootScope.stopSpin();

                    $location.path('/Dashboard');
                });

            }
            else {

                ngDialog.open({
                    template: '<p>Please enter username and password.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
            }

        };


        $scope.register = function () {

            $rootScope.startSpin();

            $location.path('/Register');

        };

        $scope.forgot = function () {

            //$rootScope.startSpin();

            $location.path('/ForgetPassword');

        };


    }]);