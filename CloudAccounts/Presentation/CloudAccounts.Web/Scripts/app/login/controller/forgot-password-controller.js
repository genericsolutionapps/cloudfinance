﻿FSTApp.controller("ForgetController", ['$scope', '$rootScope', '$location', '$timeout', 'LoginService', 'ngDialog', '$sessionStorage',
    function ($scope, $rootScope, $location, $timeout, LoginService, ngDialog, $sessionStorage) {


        $('body').addClass('login-page');

        $scope.Login = {

            User_Name: '',
            Password: ''

        };

        $scope.submit = function () {

            if ($scope.Login.User_Name != '') {

                $rootScope.startSpin();

                LoginService.forgotpassword($scope.Login, function (response) {

                    $rootScope.stopSpin();

                    if (response == true) {

                        alertify.alert("Your password has been sent to your email. Please check your email address.", function () {


                            

                        });

                        $location.path('/Login');
                    }

                });

            }
            else {
                alertify.alert("Enter email address first.");
            }
        };


        //$scope.submit = function () {

        //    if ($scope.Login.User_Name != '' && $scope.Login.Password != '') {

        //        $rootScope.startSpin();
        //        LoginService.loginsubmit($scope.Login, function (response) {

        //            delete $sessionStorage.UserId;
        //            delete $sessionStorage.Email;

        //            $sessionStorage.UserId = response.UserId;
        //            $sessionStorage.Email = response.Email;

        //            $rootScope.Email = response.Email;

        //            $rootScope.stopSpin();

        //            $location.path('/Dashboard');
        //        });

        //    }
        //    else {

        //        ngDialog.open({
        //            template: '<p>Please enter username and password.</p>',
        //            plain: true,
        //            className: 'ngdialog-theme-default'
        //        });
        //    }

        //};


        //$scope.register = function () {

        //    $rootScope.startSpin();

        //    $location.path('/Register');

        //};


    }]);