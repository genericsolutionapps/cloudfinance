﻿FSTApp.controller("CashBankReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'InvoiceService', 'ProductService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, InvoiceService, ProductService, $sessionStorage, ngDialog) {

        $scope.allocations = [];

        $scope.TotalBuyingAmount = 0;
        $scope.TotalSellingAmount = 0;

        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: new Date(),
            EndDate: new Date(),
            ReportName: 'CashBank'
        };

        var barData = {
            label: "bar",
            data: []
        };//2674800000 one month in ticks


        $scope.setDate = function (dateType) {
            if (dateType == undefined) {
                $('#startDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                var date = new Date();

                var startDate = new Date(date.getTime() - 30 * 24 * 60 * 60 * 1000);

                $('#startDate input').datepicker('setDate', startDate);


                $('#endDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                $('#endDate input').datepicker('setDate', new Date());
            }

            $scope.DateRangeViewModel.StartDate = $('#startDate input').val();
            $scope.DateRangeViewModel.EndDate = $('#endDate input').val();

            $scope.DateFilterValue = $scope.DateRangeViewModel.StartDate + " - " + $scope.DateRangeViewModel.EndDate;
        };


        var initialize = function () {

            $scope.setDate();

            InvoiceService.getAllAllocations($scope.DateRangeViewModel, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.allocations.push(value);
                });
                $scope.calculateSummary('0001');
                $scope.calculateSummary('0002');

                initializeBarChart();
            });
        };

        initialize();



        $scope.calculateSummary = function (voucherType) {

            if (voucherType == "0001") {
                $scope.TotalSellingAmount = $scope.allocations.reduce(function (accumulator, currentValue) {
                    if (currentValue.VoucherType == "0001") {
                        return accumulator + currentValue.Amount;
                    }
                    else if (currentValue.VoucherType == "0002") {
                        return accumulator + 0;
                    }
                }, 0);
            }
            else if (voucherType == "0002") {
                $scope.TotalBuyingAmount = $scope.allocations.reduce(function (accumulator, currentValue) {
                    if (currentValue.VoucherType == "0002") {
                        return accumulator + currentValue.Amount;
                    }
                    else if (currentValue.VoucherType == "0001") {
                        return accumulator + 0;
                    }
                }, 0);
            }
        };


        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };


        var initializeBarChart = function () {

            angular.forEach($scope.allocations, function (value, key) {
                barData.data.push(['1451599200000', value.Amount]);
            });
            var barOptions = {
                series: {
                    bars: {
                        show: true,
                        //barWidth: 15 * 24 * 60 * 60 * 1000,
                        barWidth: 0.6,
                        align: 'center',
                        fillColor: {
                            colors: [{
                                opacity: 0.8
                            }, {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                xaxis: {
                    //mode: "time",
                    mode: "categories",
                    timeformat: "%m-%Y",
                    tickSize: [1, "month"]
                },
                colors: ["#18C5A9"],
                grid: {
                    color: "#999999",
                    hoverable: true,
                    clickable: true,
                    tickColor: '#DADDE0',// "#D4D4D4",
                    borderWidth: 0
                },
                legend: {
                    show: false
                },
                tooltip: true,
                tooltipOpts: {
                    //content: "x: %x, y: %y"
                    content: "collected %y in %x"
                }
            };

            var data1 = [["04-2016", -60]];

            angular.forEach($scope.allocations, function (value, key) {
                data1.push(["01-2016", value.Amount]);
            });

            //var data1 = [["01-2016", 0], ["02-2016", 0], ["03-2016", 104], ["04-2016", 67]];
            var data2 = [["01-2016", 48], ["02-2016", 48], ["03-2016", 53], ["04-2016", 37]];
            $.plot($("#flot_bar_chart"), [{ data: data1, label: "Data1" }], barOptions);

            //$.plot($("#flot_bar_chart"), [barData], barOptions);

            //var data1 = [["01-2016", 0], ["02-2016", 0], ["03-2016", 104], ["04-2016", 67]];
            //var data2 = [["01-2016", 48], ["02-2016", 48], ["03-2016", 53], ["04-2016", 37]];
            //var plotObj = $.plot($("#placeholder"), [{ data: data1, label: "Data1"}, { data: data2, label: "Data2"}],
            //options);

        };



    }]);






