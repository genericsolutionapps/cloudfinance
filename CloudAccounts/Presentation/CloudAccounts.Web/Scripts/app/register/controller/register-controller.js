﻿FSTApp.controller("RegisterController", ['$scope', '$rootScope', '$location', '$timeout', 'RegisterService', 'ngDialog', '$sessionStorage',
    function ($scope, $rootScope, $location, $timeout, RegisterService, ngDialog, $sessionStorage) {


        $('body').addClass('login-page');

        $rootScope.stopSpin();


        $scope.Registration_Forms = {
            Form1: true,
            Form2: false,
            Form3: false,
            Form4: false,
        };

        $scope.Registration_model = {

            Email: '',
            Password: '',
            Name: '',
            PhoneNumber: '',
            CompanyName: '',
            IndustryType: '',
            FeatureType: '',
            UserStatusTypeCode: '',
            CashBankTransaction: '',
            SalesTransaction: '',
            ExpenseTransaction: '',
            EmployeesTransaction: ''

        };



        $scope.form1Submit = function () {

            $rootScope.startSpin();

            if ($scope.Registration_model.UserName != '' && $scope.Registration_model.Password != '') {

                $scope.Registration_Forms.Form1 = false;
                $scope.Registration_Forms.Form2 = true;
                $scope.Registration_Forms.Form3 = false;
                $scope.Registration_Forms.Form4 = false;

            }

            $rootScope.stopSpin();
        };


        $scope.form2Submit = function () {

            $rootScope.startSpin();

            if ($scope.Registration_model.Name != '' && $scope.Registration_model.PhoneNumber != '' && $scope.Registration_model.CompanyName != '') {

                $scope.Registration_Forms.Form1 = false;
                $scope.Registration_Forms.Form2 = false;
                $scope.Registration_Forms.Form3 = true;
                $scope.Registration_Forms.Form4 = false;

            }

            $rootScope.stopSpin();
        };


        $scope.form3Submit = function (industryType) {

            $rootScope.startSpin();

            $scope.Registration_model.IndustryType = industryType;

            $scope.Registration_Forms.Form1 = false;
            $scope.Registration_Forms.Form2 = false;
            $scope.Registration_Forms.Form3 = false;
            $scope.Registration_Forms.Form4 = true;

            $rootScope.stopSpin();

        };


        $scope.form4Submit = function (featureType) {

            $rootScope.startSpin();

            $scope.Registration_model.FeatureType = featureType;
            $scope.Registration_model.UserStatusTypeCode = '0001';
            $scope.Registration_model.CashBankTransaction = '0001';
            $scope.Registration_model.SalesTransaction = '0001';
            $scope.Registration_model.ExpenseTransaction = '0001';
            $scope.Registration_model.EmployeesTransaction = '0001';

            RegisterService.registersubmit($scope.Registration_model, function (response) {

                delete $sessionStorage.UserId;
                delete $sessionStorage.Email;

                $sessionStorage.UserId = response.UserId;
                $sessionStorage.Email = response.Email;

                $rootScope.Email = response.Email;


                $rootScope.stopSpin();

                $location.path('/Dashboard');

            })
        };



    }]);
