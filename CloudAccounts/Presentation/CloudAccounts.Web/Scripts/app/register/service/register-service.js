﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("RegisterService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.registersubmit = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.registration({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    //debug AuthenticationFilterAttribute

   
}]);