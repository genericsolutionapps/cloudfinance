﻿FSTApp.controller("ExpenseInformationController", ['$scope', '$rootScope', '$timeout', 'InvoiceService', 'ExpenseService', 'CashBankService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, InvoiceService, ExpenseService, CashBankService, $sessionStorage, ngDialog, $location) {

        $scope.summary = {
            SubTotal: 0,
            TotalTax: 0,
            GrandTotal: 0,
            TotalDiscount: 0
        };

        $scope.expense = [];

        $scope.allocations = [];

        $scope.cashBankAccounts = [];

        $scope.allocation = {
            AllocationId: 0,
            Date: '',
            ExpiryDate: '',
            Account: 'Safe Account',
            Amount: 0,
            Explanation: '',
            AllocationType: '0002',         //0001 for collection       //0002 for payment
            AllocationMode: '',             //0001 for cash             //0002 for bank
            CustomerId: null,
            InvoiceHeaderId: null,
            //Source: 'invoice'
        };


        var initialize = function () {

            var invoiceHeaderId = $location.search().invoiceHeaderId;

            InvoiceService.getInvoiceById(invoiceHeaderId, function (response) {

                response.TotalAmount = response.TotalAmount.toFixed(2);
                response.TotalAllocatedAmount = response.TotalAllocatedAmount.toFixed(2);

                response.Date = parseDate(response.Date);

                response.DueDate = parseDate(response.DueDate);

                $scope.expense.push(response);

                calculate();

                $scope.allocation.Amount = $scope.expense[0].TotalAmount - $scope.expense[0].TotalAllocatedAmount;
                $scope.allocation.InvoiceHeaderId = $scope.expense[0].InvoiceHeaderId;
            });

            CashBankService.getAllAccounts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.cashBankAccounts.push(value);
                });
            });

            //setting all allocation objects
            InvoiceService.getAllAllocationsByInvoiceHeaderId(invoiceHeaderId, function (response) {

                $scope.allocations = [];

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);
                    value.ExpiryDate = parseDate(value.ExpiryDate);

                    $scope.allocations.push(value);
                });

                if ($scope.allocations.length > 0) {
                    $scope.visibility.allocations = true;
                }
            });
        };

        initialize();

        $scope.cancel = function () {

            $scope.CompanyViewModel = {
                Company_ID: 0,
                Description: null,
                Company_Name: null,
                Business: null,
                Address: null,
                Is_Active: true
            };

            $scope.editcheck = false;

        };

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

        var calculate = function (index) {
            //var amount = $scope.rows[index].UnitPrice * $scope.rows[index].Quantity;
            //var amountAfterTax = amount + (amount * $scope.rows[index].TaxPerc / 100);
            //var amountAfterDiscount = amountAfterTax - (amountAfterTax * $scope.rows[index].DiscountPerc / 100);

            //$scope.rows[index].Amount = amountAfterDiscount.toFixed(2);

            var subTotal = 0;
            var totalTaxAmount = 0;
            var grandTaxAmount = 0;
            var totalDiscountAmount = 0;

            angular.forEach($scope.expense[0].InvoiceDetailCollection, function (value, key) {
                subTotal = subTotal + (value.UnitPrice * value.Quantity);
                totalTaxAmount = totalTaxAmount + ((value.UnitPrice * value.Quantity - ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100)) * value.TaxPerc / 100);
                totalDiscountAmount = totalDiscountAmount + ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100);
            });

            $scope.summary.SubTotal = subTotal;
            $scope.summary.TotalTax = totalTaxAmount;
            $scope.summary.TotalDiscount = totalDiscountAmount;
            $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;

            $scope.expense.TotalAmount = $scope.summary.GrandTotal;

            //$scope.summary.SubTotal = $scope.summary.SubTotal + amountAfterDiscount;
        };

        $scope.deleteInvoiceById = function (invoiceHeaderId) {
            InvoiceService.deleteInvoiceById(invoiceHeaderId, function (response) {

                if (response == true) {

                    $location.path('/ExpenseList');
                }
                else {
                    window.alert(response.ErrorMessage);
                }
            });
        };

        $scope.addAllocation = function () {

            if ($scope.allocation.Amount > 0 && $scope.allocation.Amount <= $scope.expense[0].TotalAmount - $scope.expense[0].TotalAllocatedAmount) {

                if ($scope.allocation.ExpiryDate == '') {
                    $scope.allocation.AllocationMode = '0001';
                }
                else {
                    $scope.allocation.AllocationMode = '0002';
                }

                InvoiceService.addAllocation($scope.allocation, function (response) {

                    if (response == true) {

                        $location.path('/ExpenseList');
                    }
                });
            }
            else {
                $scope.allocationValidation = true;
            }
        };


        $scope.expenseForm = function () {

            $location.path('/CreateExpense');
        };




    }]);





