﻿FSTApp.controller("ExpenseListController", ['$scope', '$rootScope', '$timeout', 'ExpenseService', 'InvoiceService','$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, ExpenseService, InvoiceService, $sessionStorage, ngDialog, $location) {


        var initialize = function () {

            $scope.expenseHeaderList = [];

            InvoiceService.getAllInvoices({ ReportName: null }, function (response) {

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);

                    value.DueDate = parseDate(value.DueDate);

                    if (value.VoucherType == '0002') {
                        $scope.expenseHeaderList.push(value);
                    }
                });
            });
        };

        initialize();

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        }

        $scope.CompanyViewModel = {
            CompanyId: 0,
            CompanyName: null,
            CommercialTitle: null,
            Email: null,
            Phone: null,
            Sector: null,
            Address: 'Active',
            TaxInformation: null
        };

        $scope.createBill = function () {

            $location.path('/CreateExpense');
        };

        $scope.expenseInformation = function (invoiceHeaderId) {

            $location.path('/ExpenseInformation').search({ invoiceHeaderId: invoiceHeaderId });;
        };


    }]);
