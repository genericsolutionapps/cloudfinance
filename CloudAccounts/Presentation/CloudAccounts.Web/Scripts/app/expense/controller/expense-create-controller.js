﻿FSTApp.controller("ExpenseCreateController", ['$scope', '$rootScope', '$timeout', 'ExpenseService', 'CategoriesService', 'InvoiceService', 'CustomerService', 'ProductService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, ExpenseService, CategoriesService, InvoiceService, CustomerService, ProductService, $sessionStorage, ngDialog, $location) {

        $scope.visibility = {
            //totalSection: false,
            expenseDetailDescription: false,
            expenseDetailUnit: false,
        };

        $scope.summary = {
            SubTotal: 0,
            TotalTax: 0,
            GrandTotal: 0,
            TotalDiscount: 0
        };

        $scope.expenseHeader = {
            InvoiceHeaderId: 0,
            //Explanation: '',
            Description: '',
            Date: null,
            InvoiceNo: null,
            CurrencyRate: 1,
            BaseRate: 1,
            Status: 'PAYABLE',
            DueDate: null,
            //BusinessPartnerId: 0,
            CustomerId: 0,
            CategoryId: null,
            InvoiceDetailCollection: '',
            TotalAmount: 0,
            VoucherType: '0002'         //0002 because this is bill

            //TotalAllocatedAmount: 0,
            //TotalAmount: 0,
            //Date: '',
            //DueDate: '',
            //CustomerAddress: null,
            //CustomerPhone: null,
        };

        $scope.expenseDetail = {
            ProductName: '',
            Quantity: 1,
            UnitPrice: 0,
            DiscountPerc: 0,
            TaxPerc: 0,
            Amount: 0,
            Description: null,
            Unit: null
        };

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];
        $scope.selectedCurrency = 'LIRA';
        $scope.currencySymbol = 'la la-turkish-lira';

        $scope.selectedCustomer = '';
        $scope.allCustomers = [];
        $scope.customersSearchList = [];

        $scope.allProducts = [];
        $scope.productSearchList = [];

        $scope.summary = {
            SubTotal: 0,
            TotalTax: 0,
            GrandTotal: 0,
            TotalDiscount: 0
        };

        $scope.rows = [$scope.expenseDetail];

        $scope.expenseCategories = [];
        $scope.expenseCategorySearchList = [];


        var initialize = function () {

            var invoiceHeaderId = $location.search().invoiceHeaderId;

            getAllCustomers();
            getAllExpenseCategory();
            getCurrencyExchangeRate();
            getAllProducts();

            if (invoiceHeaderId > 0) {

                InvoiceService.getInvoiceById(invoiceHeaderId, function (response) {

                    response.Date = parseDate(response.Date);
                    response.DueDate = parseDate(response.DueDate);

                    $scope.expenseHeader.InvoiceHeaderId = response.InvoiceHeaderId;
                    $scope.expenseHeader.Description = response.Description;
                    $scope.expenseHeader.Date = response.Date;
                    $scope.expenseHeader.InvoiceNo = response.InvoiceNo;
                    $scope.expenseHeader.CurrencyRate = response.CurrencyRate;
                    $scope.expenseHeader.BaseRate = response.BaseRate;
                    $scope.expenseHeader.Status = response.Status;
                    $scope.expenseHeader.DueDate = response.DueDate;
                    $scope.expenseHeader.CustomerId = response.CustomerId;
                    $scope.expenseHeader.CategoryId = response.CategoryId;
                    $scope.expenseHeader.InvoiceDetailCollection = response.InvoiceDetailCollection;
                    $scope.expenseHeader.TotalAmount = response.TotalAmount;
                    $scope.expenseHeader.VoucherType = response.VoucherType;         //0001 because this is bill

                    $scope.setCategory($scope.expenseHeader.CategoryId);
                    $scope.setCustomer($scope.expenseHeader.CustomerId);
                    //$scope.setCurrency($scope.expenseHeader.);

                    $scope.rows = [];

                    angular.forEach($scope.expenseHeader.InvoiceDetailCollection, function (value, key) {

                        $scope.rows.push(value);
                    });
                    $scope.calculate();
                });
            }
        };

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

        var getAllCustomers = function () {

            CustomerService.getAllBusinessPartnersByType($sessionStorage.UserId, function (response) {

                $scope.customersSearchList = [];

                angular.forEach(response, function (value, key) {
                    $scope.allCustomers.push(value);
                    $scope.customersSearchList.push({ id: value.CustomerId, name: value.CompanyTitle });
                });

                loadAutoComplete('ddlcustomer', $scope.customersSearchList);
            });
        };

        var getAllExpenseCategory = function () {

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0002') {
                        $scope.expenseCategories.push(value);
                        $scope.expenseCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                });

                loadAutoComplete('ddlCategory', $scope.expenseCategorySearchList);
            });
        };

        var getCurrencyExchangeRate = function () {

            InvoiceService.getUSDRate(function (response) {
                $scope.currencies[1].rate = response.USD_TRY.val;
            });
            InvoiceService.getEURRate(function (response) {
                $scope.currencies[2].rate = response.EUR_TRY.val;
            });
            InvoiceService.getGBPRate(function (response) {
                $scope.currencies[3].rate = response.GBP_TRY.val;
            });
        };

        var getAllProducts = function () {

            ProductService.getAllProducts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.allProducts.push(value);
                    $scope.productSearchList.push({ id: value.ProductId, name: value.Name });
                });

                loadAutoComplete('ddlProduct0', $scope.productSearchList);
            });
        };

        $scope.calculate = function (index) {

            if (index != undefined) {

                var amount = $scope.rows[index].UnitPrice * $scope.rows[index].Quantity;
                var amountAfterTax = amount + (amount * $scope.rows[index].TaxPerc / 100);
                var amountAfterDiscount = amountAfterTax - (amountAfterTax * $scope.rows[index].DiscountPerc / 100);

                $scope.rows[index].Amount = amountAfterDiscount.toFixed(2);
            }

            var subTotal = 0;
            var totalTaxAmount = 0;
            var grandTaxAmount = 0;
            var totalDiscountAmount = 0;

            angular.forEach($scope.rows, function (value, key) {
                subTotal = subTotal + (value.UnitPrice * value.Quantity);
                totalTaxAmount = totalTaxAmount + ((value.UnitPrice * value.Quantity - ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100)) * value.TaxPerc / 100);
                totalDiscountAmount = totalDiscountAmount + ((value.UnitPrice * value.Quantity) * value.DiscountPerc / 100);
            });

            $scope.summary.SubTotal = subTotal;
            $scope.summary.TotalTax = totalTaxAmount;
            $scope.summary.TotalDiscount = totalDiscountAmount;
            $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;


            $scope.expenseHeader.TotalAmount = $scope.summary.GrandTotal;
        };

        $scope.removeRow = function (index) {
            $scope.rows.splice(index, 1);

            var subTotal = 0;
            var totalTaxAmount = 0;
            var grandTaxAmount = 0;
            var totalDiscountAmount = 0;

            angular.forEach($scope.rows, function (value, key) {
                subTotal = subTotal + (value.UnitPrice * value.Quantity);
                totalTaxAmount = totalTaxAmount + (value.UnitPrice * value.TaxPerc / 100);
                totalDiscountAmount = totalDiscountAmount + (value.UnitPrice * value.DiscountPerc / 100);
            });

            $scope.summary.SubTotal = subTotal;
            $scope.summary.TotalTax = totalTaxAmount;
            $scope.summary.TotalDiscount = totalDiscountAmount;
            $scope.summary.GrandTotal = ($scope.summary.SubTotal - $scope.summary.TotalDiscount) + $scope.summary.TotalTax;

        }

        $scope.addNewRow = function () {

            $scope.rows.push(angular.copy($scope.expenseDetail));

            var id = 'ddlProduct' + ($scope.rows.length - 1);

            $scope.rows[$scope.rows.length - 1].ProductName = '';
            $scope.rows[$scope.rows.length - 1].Quantity = 1;
            $scope.rows[$scope.rows.length - 1].UnitPrice = 0;
            $scope.rows[$scope.rows.length - 1].DiscountPerc = 0;
            $scope.rows[$scope.rows.length - 1].Amount = 0;
            $scope.rows[$scope.rows.length - 1].TaxPerc = 0;

            setTimeout(function () {

                loadAutoComplete(id, $scope.productSearchList)

            }, 500);
        };

        $scope.setCurrency = function (index) {
            $scope.expenseHeader.CurrencyRate = $scope.currencies[index].rate.toFixed(4);
            $scope.selectedCurrency = $scope.currencies[index].name;
            $scope.currencySymbol = $scope.currencies[index].symbol;
        };

        $scope.setCategory = function (categoryId) {

            var input = $('#ddlCategory');
            input.trigger('input');
            input.trigger('change');

            //create
            if (categoryId == undefined) {

                angular.forEach($scope.expenseCategories, function (value, key) {

                    if (value.CategoryName === $scope.selectedCategory) {
                        $scope.expenseHeader.CategoryId = value.CategoryId;
                    }
                });
            }
            //edit
            else if (categoryId > 0) {

                angular.forEach($scope.expenseCategories, function (value, key) {

                    if (value.CategoryId === categoryId) {
                        $scope.selectedCategory = value.CategoryName;
                    }
                });
            }
        };

        $scope.setPaymentStatus = function (status) {
            $scope.expenseHeader.Status = status;
        };

        $scope.setCustomer = function (customerId) {

            var input = $('#ddlcustomer');
            input.trigger('input');
            input.trigger('change');

            if (customerId == undefined) {
                angular.forEach($scope.allCustomers, function (value, key) {

                    if (value.CompanyTitle === $scope.selectedCustomer) {
                        $scope.expenseHeader.CustomerId = value.CustomerId;
                        //$scope.expenseHeader.CustomerAddress = value.Address;
                        //$scope.expenseHeader.CustomerPhone = value.PhoneNumber;
                    }
                });
                if ($scope.expenseHeader.CustomerId == 0) {
                    $scope.expenseHeader.CustomerId = 0;
                    //$scope.expenseHeader.CustomerAddress = null;
                    //$scope.expenseHeader.CustomerPhone = null;
                }
                if ($scope.selectedCustomer == "") {
                    $scope.expenseHeader.CustomerId = 0;
                }
            }
            else if (customerId != undefined) {

                angular.forEach($scope.allCustomers, function (value, key) {
                    if (value.CustomerId === customerId) {
                        $scope.selectedCustomer = value.CompanyTitle;
                    }
                });
            }
        };

        $scope.setProduct = function (index) {

            var input = $('#ddlProduct' + index);
            input.trigger('input');
            input.trigger('change');

            angular.forEach($scope.allProducts, function (value, key) {

                if (value.Name === $scope.rows[index].ProductName) {
                    $scope.rows[index].ProductId = value.ProductId;
                    $scope.rows[index].ProductName = value.Name;
                    $scope.rows[index].UnitPrice = (value.PurchasePrice * value.PurchaseCurrencyRate).toFixed(2);
                    $scope.rows[index].TaxPerc = value.KDVPerc;
                    $scope.calculate(index);
                }
            });
            if ($scope.rows[index].ProductName == '') {
                $scope.rows[index].ProductId = 0;
                $scope.rows[index].ProductName = '';
                $scope.rows[index].UnitPrice = 0;
                $scope.calculate(index);
            }
        }

        $scope.addDescription = function (index) {
            $scope.visibility.expenseDetailDescription = true;
            $scope.descriptionIndex = index;
        };
        $scope.addUnit = function (index) {
            $scope.visibility.expenseDetailUnit = true;
            $scope.descriptionIndex = index;
        };


        $scope.createBill = function () {

            $scope.expenseHeader.InvoiceDetailCollection = $scope.rows;

            //create
            if ($scope.expenseHeader.InvoiceHeaderId == 0) {

                if ($scope.expenseHeader.CustomerId == 0) {

                    CustomerService.createCustomer({ CompanyTitle: $scope.selectedCustomer, CustomerType: '0002', BusinessPartnerType: '0002' }, function (response) {

                        if (typeof response == 'number') {
                            $scope.expenseHeader.CustomerId = response;

                            InvoiceService.createInvoice($scope.expenseHeader, function (responseInvoice) {

                                if (responseInvoice == true) {

                                    $location.path('/ExpenseList');
                                }
                            });
                        }
                    });
                }
                else {
                    InvoiceService.createInvoice($scope.expenseHeader, function (responseInvoice) {

                        if (responseInvoice == true) {

                            $location.path('/ExpenseList');
                        }
                    });
                }
            }
            //edit
            else if ($scope.expenseHeader.InvoiceHeaderId > 0) {

                if ($scope.expenseHeader.CustomerId == 0) {

                    CustomerService.createCustomer({ CompanyTitle: $scope.selectedCustomer, CustomerType: '0002', BusinessPartnerType: '0002' }, function (response) {

                        $scope.expenseHeader.CustomerId = response;
                    });
                    InvoiceService.edit($scope.expenseHeader, function (response) {

                        if (response == true) {

                            $location.path('/ExpenseList');
                        }
                    });
                }
                else {
                    InvoiceService.edit($scope.expenseHeader, function (response) {

                        if (response == true) {

                            $location.path('/ExpenseList');
                        }
                    });
                }
            }
        }


        $scope.Cancel = function () {

            $location.path('/ExpenseList');
        };

        initialize();

    }]);







