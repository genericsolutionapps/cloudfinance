﻿FSTApp.controller("VendorRegisterController", ['$scope', 'VendorRegisterService', 'Upload', 'ngDialog', 'CommonObject', '$sessionStorage', '$location', 'VendorLoginService', '$rootScope',

function ($scope, VendorRegisterService, Upload, ngDialog, commonobjects, $sessionStorage, $location, VendorLoginService, $rootScope) {

    $scope.VendorRegisterModel = {
        VendorId: 0,
        CompanyName: '',
        BusinessLiscense: '',
        FirstName: '',
        LastName: '',
        Email: '',
        Phone: '',
        Website: '',
        FacebookLink: '',
        PInterestLink: '',
        YouTubeLink: '',
        LinkedIn: '',
        InstagramLink: '',
        ShippingStreetAddress: '',
        ShippingSuite: '',
        ShippingCity: '',
        ShippingState: '',
        ShippingZipCode: '',
        ShippingCountry: '',
        BillingStreetAddress: '',
        BillingSuite: '',
        BillingCity: '',
        BillingState: '',
        BillingZipCode: '',
        BillingCountry: '',
        SameAsShipping: false,
        LiscenseDocument: '',
        Username: '',
        Password: '',
        PrimaryLocation: '',
        Timezone: '',
        Created_On: '',
        MembershipCode: '',
        Group_Id: 0,
        ReferedByNoBuyers: 0,
        Since: '',
        Sales: 0,
        Feedback: ''
    };

    $scope.VendorLogin = {

        Username: '',
        Password: ''

    };

    $scope.sameasshipping = false;


    $scope.submit = function (file) {



        if ($scope.form1.$valid == true && $scope.form2.$valid == true && $scope.form3.$valid == true && $scope.form4.$valid == true) {

            $rootScope.startSpin();

            VendorRegisterService.submit($scope.VendorRegisterModel, file, function (response) {

                $scope.VendorLogin.Username = $scope.VendorRegisterModel.Username;
                $scope.VendorLogin.Password = $scope.VendorRegisterModel.Password;

                VendorLoginService.loginsubmit($scope.VendorLogin, function (response) {



                    delete $sessionStorage.UserId;
                    delete $sessionStorage.Username;
                    delete $sessionStorage.VendorId;
                    delete $sessionStorage.BuyerId;

                    $sessionStorage.UserId = response.UserId;
                    $sessionStorage.Username = response.Username;
                    $sessionStorage.VendorId = response.VendorId;
                    $sessionStorage.BuyerId = response.BuyerId;

                    $rootScope.stopSpin();
                    $location.path('/VendorDashboard');


                });

            });
        }
        else {

            ngDialog.open({
                template: '<p>Please complete all the mandetory fields. Fields with "*" in red are mandatory.</p>',
                plain: true,
                className: 'ngdialog-theme-default'
            });
        }

    };

    $scope.sameasshippingchanged = function () {

        if ($scope.sameasshipping == true) {


            var vendorModel = $scope.VendorRegisterModel;

            vendorModel.BillingCity = vendorModel.ShippingCity;
            vendorModel.BillingCountry = vendorModel.ShippingCountry;
            vendorModel.BillingState = vendorModel.ShippingState;
            vendorModel.BillingStreetAddress = vendorModel.ShippingStreetAddress;
            vendorModel.BillingSuite = vendorModel.ShippingSuite;
            vendorModel.BillingZipCode = vendorModel.ShippingZipCode;
            vendorModel.SameAsShipping = true;

        }
        else {
            var vendorModel = $scope.VendorRegisterModel;

            vendorModel.BillingCity = '';
            vendorModel.BillingCountry = '';
            vendorModel.BillingState = '';
            vendorModel.BillingStreetAddress = '';
            vendorModel.BillingSuite = '';
            vendorModel.BillingZipCode = '';
            vendorModel.SameAsShipping = false;
        }

    };



}]);