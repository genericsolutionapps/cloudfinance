﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorRegisterService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', 'Upload', function ($http, ServiceManager, Resource, commonobjects, Upload) {


    this.submit = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        //ServiceManager.execute(Resource.submitvendorregistration({ data: JSON.stringify(request) }), function (response) {

        //    callback(response);

        //});

        file.upload = Upload.upload({
            url: 'VendorRegister/Submit',
            data: { data: JSON.stringify(request), file: file },
        });

        file.upload.then(function (response) {
            
            callback(response.data.ResultData);

        }, function (response) {


        }, function (evt) {

        });

    };


}]);