﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("CompanyService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {


    this.getCompanyByUserId = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getcompany({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.editCompany = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editcompany({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.getallcompany = function (callback) {

        var request = commonobjects.requestobject;

        ServiceManager.execute(Resource.getallcompany(), function (response) {

            callback(response);

        });

    };

    this.createcompany = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createcompany({ data: request }), function (response) {

            callback(response);

        });

    };

    this.editcompany = function (model, file, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editcompany({ data: request }), function (response) {

            callback(response);

        });



    };

}]);