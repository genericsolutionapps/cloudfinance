﻿FSTApp.controller("CompanyEditController", ['$scope', '$rootScope', '$timeout', 'CompanyService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, CompanyService, $sessionStorage, ngDialog, $location) {


        var initialize = function () {

            var userId = $sessionStorage.UserId;

            CompanyService.getCompanyByUserId(userId, function (response) {

                $scope.CompanyViewModel.CompanyId = response.CompanyId;
                $scope.CompanyViewModel.CompanyName = response.CompanyName;
                $scope.CompanyViewModel.CommercialTitle = response.CommercialTitle;
                $scope.CompanyViewModel.Email = response.Email;
                $scope.CompanyViewModel.Phone = response.Phone;
                $scope.CompanyViewModel.Sector = response.Sector;
                $scope.CompanyViewModel.Address = response.Address;
                $scope.CompanyViewModel.TaxInformation = response.TaxInformation;
                $scope.CompanyViewModel.VD = response.VD;
                $scope.CompanyViewModel.District = response.District;
                $scope.CompanyViewModel.Province = response.Province;
                $scope.CompanyViewModel.Wire = response.Wire;
                $scope.CompanyViewModel.Fax = response.Fax;


            })
        };


        initialize();


        $scope.CompanyViewModel = {
            CompanyId: 0,
            CompanyName: null,
            CommercialTitle: null,
            Email: null,
            Phone: null,
            Sector: null,
            Address: 'Active',
            TaxInformation: null,
            VD: null,
            District: null,
            Province: null

        };


        $scope.Cancel = function () {

            //$scope.CompanyViewModel = {
            //    Company_ID: 0,
            //    Description: null,
            //    Company_Name: null,
            //    Business: null,
            //    Address: null,
            //    Is_Active: true
            //};
            //$scope.editcheck = false;
            $location.path('/Company');
        };

        //$scope.FileUpload = function () {

        //    Dropzone.options.fileupload = {
        //        paramName: "file", // The name that will be used to transfer the file
        //        maxFilesize: 5, // MB
        //        dictDefaultMessage: '<div class="text-center mb-3"><i class="la la-cloud-upload text-primary" style="font-size:50px"></i></div> <strong>Drop files here or click to upload. </strong></br> (This is just a demo dropzone. Selected files are not actually uploaded.)',
        //        init: function () {
        //            this.on("addedfile", function (file) {
        //                var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn btn-danger btn-sm btn-block'>Remove</a>");
        //                var _this = this;
        //                removeButton.addEventListener("click", function (e) {
        //                    e.preventDefault();
        //                    e.stopPropagation();
        //                    _this.removeFile(file);
        //                });
        //                file.previewElement.appendChild(removeButton);
        //            });
        //        }
        //    }
        //};

        $(document).ready(function () {
            $("#fileuploader").uploadFile({
                url: "YOUR_FILE_UPLOAD_URL",
                fileName: "myfile",
                showPreview: true,
                previewHeight: "100px",
                previewWidth: "140px",
                multiple: false,
                dragDrop: false,
                maxFileCount: 1,
            });
        });

        //setTimeout(function () {
        //    $("#fileupload").fileinput();
        //}, 500);


        //$(document).on('ready', function () {
        //    $("#fileupload").fileinput({ showCaption: false, dropZoneEnabled: false });
        //});

        $scope.SaveChanges = function () {

            CompanyService.editCompany($scope.CompanyViewModel, function (response) {
                $location.path('/Company');
            })
        };



    }]);
