﻿FSTApp.controller("CompanyController", ['$scope', '$rootScope', '$timeout', 'CompanyService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, CompanyService, $sessionStorage, ngDialog, $location) {


        var initialize = function () {

            var userId = $sessionStorage.UserId;

            CompanyService.getCompanyByUserId(userId, function (response) {

                $scope.CompanyViewModel.CompanyId = response.CompanyId;
                $scope.CompanyViewModel.CompanyName = response.CompanyName;
                $scope.CompanyViewModel.CommercialTitle = response.CommercialTitle;
                $scope.CompanyViewModel.Email = response.Email;
                $scope.CompanyViewModel.Phone = response.Phone;
                $scope.CompanyViewModel.Sector = response.Sector;
                $scope.CompanyViewModel.Address = response.Address;
                $scope.CompanyViewModel.TaxInformation = response.TaxInformation;


            })
        };


        initialize();

        $scope.CompanyViewModel = {
            CompanyId: 0,
            CompanyName: null,
            CommercialTitle: null,
            Email: null,
            Phone: null,
            Sector: null,
            Address: 'Active',
            TaxInformation: null
        };

        $scope.RedirectToEdit = function () {

            $location.path('/CompanyEdit');
        };


    }]);
