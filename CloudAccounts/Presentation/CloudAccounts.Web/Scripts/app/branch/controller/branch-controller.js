﻿FSTApp.controller("BranchController", ['$scope', '$rootScope', '$timeout', 'BranchService', '$sessionStorage',
    function ($scope, $rootScope, $timeout, BranchService, $sessionStorage) {

        $scope.BranchViewModel = {
            Branch_ID: 0,
            Branch_Name: null,
            Is_Active: 'Active'


        };

        $scope.editcheck = false;

        $scope.listbranch = [];

        var loadbranch = function () {

            $scope.listbranch = [];

            BranchService.getallbranch(function (response) {

                var branchlist = response;

                angular.forEach(branchlist, function (value, key) {

                    $scope.listbranch.push(value);
                });

            });

        };


        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.BranchViewModel = item;

        };


        $timeout(function () {

            loadbranch();

            //$('#statuslist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $("#branchlist").DataTable();

        });

}]);