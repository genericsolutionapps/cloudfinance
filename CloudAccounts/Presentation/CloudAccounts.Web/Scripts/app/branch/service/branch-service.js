﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("BranchService", ['$http', 'ServiceManager', 'Resource', 'CommonObject',
    function ($http, ServiceManager, Resource, commonobjects) {



        this.getalldepartment = function (callback) {

            var request = commonobjects.requestobject;

            ServiceManager.execute(Resource.getalldepartment(), function (response) {

                callback(response);

            });

        };


    }]);