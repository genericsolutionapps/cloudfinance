﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("CategoriesService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.create = function (categoryViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(categoryViewModel);

        ServiceManager.execute(Resource.createcategory({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.edit = function (categoryViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(categoryViewModel);

        ServiceManager.execute(Resource.editcategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };

    this.getAllSalesCategories = function (dateRangeViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(dateRangeViewModel);

        ServiceManager.execute(Resource.getallsalescategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };

    this.getAllCustomerCategories = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallcustomercategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllExpenseCategories = function (dateRangeViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(dateRangeViewModel);

        ServiceManager.execute(Resource.getallexpensecategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllSupplierCategories = function (dateRangeViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(dateRangeViewModel);

        ServiceManager.execute(Resource.getallbusinesspartnercategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.getAllProductCategories = function (dateRangeViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(dateRangeViewModel);

        ServiceManager.execute(Resource.getallproductcategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };



    this.delete = function (categoryId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(categoryId);

        ServiceManager.execute(Resource.deletecategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };


    this.getAllCategories = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallcategories({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


}]);





