﻿FSTApp.controller("CategoriesController", ['$scope', '$rootScope', '$timeout', 'CategoriesService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, CategoriesService, $sessionStorage, ngDialog) {

        $('[name="demo"]').paletteColorPicker();
        $('[name="demo1"]').paletteColorPicker();


        //$scope.colorArray = [
        //    { "primary": "#E91E63" },
        //    { "primary_dark": "#C2185B" },
        //    { "primary_light": "#F8BBD0" },
        //    { "accent": "#CDDC39" },
        //    { "primary_text": "#212121" },
        //    { "secondary_text": "#727272" },
        //    { "divider": "#B6B6B6" }
        //];

        var InitializeCategories = function () {

            $scope.SalesCategories = [];
            $scope.ExpenseCategories = [];
            $scope.RevenueCategories = [];
            $scope.ServicesCategories = [];
            $scope.WorkingCategories = [];
            $scope.CustomerCategories = [];

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0001') {
                        $scope.SalesCategories.push(value);
                    }
                    else if (value.CategoryType === '0002') {
                        $scope.ExpenseCategories.push(value);
                    }
                    else if (value.CategoryType === '0003') {
                        $scope.RevenueCategories.push(value);
                    }
                    else if (value.CategoryType === '0004') {
                        $scope.ServicesCategories.push(value);
                    }
                    else if (value.CategoryType === '0005') {
                        $scope.WorkingCategories.push(value);
                    }
                    else {
                        $scope.CustomerCategories.push(value);
                    }


                });

            });
        };

        InitializeCategories();


        $scope.CategoryViewModel = {
            CategoryId: 0,
            CategoryName: '',
            CategoryColor: '',
            CategoryType: '',
            UserId: $sessionStorage.UserId
        };



        $scope.setCategoryType = function (categoryType) {

            $scope.CategoryViewModel.CategoryType = categoryType;

            $scope.CategoryViewModel.CategoryName = '';
            $scope.CategoryViewModel.CategoryColor = '';

        };


        $scope.EditCategory = function (category) {

            $('[name="demo1"]').val(category.CategoryColor);

            $scope.CategoryViewModel.CategoryType = category.CategoryType;
            $scope.CategoryViewModel.CategoryId = category.CategoryId;

            $scope.CategoryViewModel.CategoryName = category.CategoryName;
            //$scope.CategoryViewModel.CategoryColor = category.CategoryColor;


        };


        $scope.Save = function () {

            $scope.temp = {
                CategoryName: '',
                CategoryColor: '',
                CategoryType: '',
                UserId: $sessionStorage.UserId
            };

            $scope.temp.CategoryId = $scope.CategoryViewModel.CategoryId;
            $scope.temp.CategoryName = $scope.CategoryViewModel.CategoryName;
            $scope.temp.CategoryColor = $scope.CategoryViewModel.CategoryColor;
            $scope.temp.CategoryType = $scope.CategoryViewModel.CategoryType;


            if ($scope.temp.CategoryType === '0001') {
                $scope.SalesCategories.push($scope.temp);
            }
            else if ($scope.temp.CategoryType === '0002') {
                $scope.ExpenseCategories.push($scope.temp);
            }
            else if ($scope.temp.CategoryType === '0003') {
                $scope.RevenueCategories.push($scope.temp);
            }
            else if ($scope.temp.CategoryType === '0004') {
                $scope.ServicesCategories.push($scope.temp);
            }
            else if ($scope.temp.CategoryType === '0005') {
                $scope.WorkingCategories.push($scope.temp);
            }
            else {
                $scope.CustomerCategories.push($scope.temp);
            }

            //setting the category color with jquery because ng-model is updating
            $scope.CategoryViewModel.CategoryColor = $('[name="demo"]').val();

            CategoriesService.create($scope.CategoryViewModel, function (response) {

                if (response.CategoryId != 0) {
                    InitializeCategories();
                }

            });

        };


        $scope.SaveChanges = function () {

            CategoriesService.edit($scope.CategoryViewModel, function (response) {

                if (response == true) {

                    InitializeCategories();
                }
            });

        }


        $scope.Delete = function (categoryId) {

            CategoriesService.delete(categoryId, function (response) {

                if (response == true) {

                    InitializeCategories();
                }
            });

        }



    }]);