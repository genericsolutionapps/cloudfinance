﻿FSTApp.controller("ProductsInStockReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'ProductsInStockReportService', 'ProductService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, ProductsInStockReportService, ProductService, $sessionStorage, ngDialog) {

        $scope.products = [];

        var initialize = function () {

            ProductService.getAllProducts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.products.push(value);
                });
            });
        };

        initialize();
        //(product.CurrentQuantity * product.SalesPrice).toFixed(2)
        $scope.calculateEstimatedValues = function (source) {

            if(source == 'sales'){

                return $scope.products.reduce((accumulator, currentValue) => accumulator + currentValue.CurrentQuantity * currentValue.SalesPrice * currentValue.SalesCurrencyRate, 0);
            }
            else if(source == 'purchase'){

                return $scope.products.reduce((accumulator, currentValue) => accumulator + currentValue.CurrentQuantity * currentValue.PurchasePrice * currentValue.PurchaseCurrencyRate, 0);
            }
        };

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

    }]);






