﻿FSTApp.controller("DashboardController", ['$scope', '$rootScope', '$timeout', 'DashboardService', function ($scope, $rootScope, $timeout, DashboardService) {

    $scope.visibility = false;

    $timeout(function () {

        //InitDashboard();
        //InitDemo();
        //TreeInit();

        InitDashboard();
        initPage();
        initLayout();

        DashboardService.checkEmailVerification(function (response) {

            if (response == true) {
                $scope.visibility = true;
            }
            else {

                $scope.visibility = false;
            }

        });


    });

}]);