﻿FSTApp.controller("DepartmentController", ['$scope', '$rootScope', '$timeout', 'DepartmentService', '$sessionStorage',
    function ($scope, $rootScope, $timeout, DepartmentService, $sessionStorage) {

        $scope.DepartmentViewModel = {
            Department_ID: 0,
            Department_Name: null,
            Is_Active: 'Active'


        };

        $scope.editcheck = false;

        $scope.listdepartment = [];

        var loaddepartment = function () {

            $scope.listdepartment = [];

            DepartmentService.getalldepartment(function (response) {

                var departmentlist = response;

                angular.forEach(departmentlist, function (value, key) {

                    $scope.listdepartment.push(value);
                });

            });

        };


        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.DepartmentViewModel = item;

        };


        $timeout(function () {

            loaddepartment();

            //$('#statuslist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $("#departmentlist").DataTable();

        });

}]);