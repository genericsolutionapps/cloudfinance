﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("EmployeeService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.getEmployeeById = function (employeeId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(employeeId);

        ServiceManager.execute(Resource.getemployeebyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };


    this.edit = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editemployee({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };


    this.addAdvance = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.addadvance({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    //this.addSalary = function (model, callback) {

    //    var request = commonobjects.requestobject;

    //    request.data = JSON.stringify(model);

    //    ServiceManager.execute(Resource.addsalary({ data: JSON.stringify(request) }), function (response) {

    //        callback(response);
    //    });
    //};

    this.getAllEmployees = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallemployees({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.create = function (viewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(viewModel);

        ServiceManager.execute(Resource.createemployee({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

    this.delete = function (employeeId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(employeeId);

        ServiceManager.execute(Resource.deleteemployee({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };


}]);








