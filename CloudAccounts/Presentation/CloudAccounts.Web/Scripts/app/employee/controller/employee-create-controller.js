﻿FSTApp.controller("EmployeeCreateController", ['$scope', '$rootScope', '$timeout', 'EmployeeService', 'CategoriesService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, EmployeeService, CategoriesService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.EmployeeViewModel = {
            EmployeeId: 0,
            Name: null,
            IdentificationNumber: null,
            IBAN: null,
            Email: null,
            CategoryId: 0
        };

        $scope.employeeCategories = [];
        $scope.employeeCategorySearchList = [];
        $scope.selectedCategory = '';


        var getAllEmployeeCategory = function () {

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0005') {
                        $scope.employeeCategories.push(value);
                        $scope.employeeCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                });

                loadAutoComplete('ddlCategory', $scope.employeeCategorySearchList);
            });
        };

        $scope.setCategory = function (categoryId) {

            var input = $('#ddlCategory');
            input.trigger('input');
            input.trigger('change');

            //create
            if (categoryId == undefined) {

                angular.forEach($scope.employeeCategories, function (value, key) {

                    if (value.CategoryName === $scope.selectedCategory) {
                        $scope.EmployeeViewModel.CategoryId = value.CategoryId;
                    }
                });
            }
            //edit
            else if (categoryId > 0) {

                angular.forEach($scope.employeeCategories, function (value, key) {

                    if (value.CategoryId === categoryId) {
                        $scope.selectedCategory = value.CategoryName;
                    }
                });
            }
        };

        $scope.Create = function () {

            var employeeId = $location.search().employeeId;

            //create
            if (employeeId == undefined) {

                EmployeeService.create($scope.EmployeeViewModel, function (response) {

                    if (response == true) {

                        $location.path('/employee');
                    }
                });
            }
            //edit
            else if (employeeId > 0) {

                $scope.EmployeeViewModel.EmployeeId = employeeId;

                EmployeeService.edit($scope.EmployeeViewModel, function (response) {

                    if (response == true) {

                        $location.path('/employee');
                    }
                });
            }
        }

        $scope.Cancel = function () {

            $location.path('/employee');
        };


        var initialize = function () {

            var employeeId = $location.search().employeeId;

            getAllEmployeeCategory();

            if (employeeId > 0) {

                EmployeeService.getEmployeeById(employeeId, function (response) {

                    $scope.EmployeeViewModel.EmployeeId = response.EmployeeId;
                    $scope.EmployeeViewModel.Name = response.Name;
                    $scope.EmployeeViewModel.IdentificationNumber = response.IdentificationNumber;
                    $scope.EmployeeViewModel.IBAN = response.IBAN;
                    $scope.EmployeeViewModel.Email = response.Email;
                    $scope.EmployeeViewModel.CategoryId = response.CategoryId;

                    $scope.setCategory($scope.EmployeeViewModel.CategoryId);
                });
            }
        };

        initialize();



    }]);









