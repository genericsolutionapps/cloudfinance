﻿FSTApp.controller("EmployeeInformationController", ['$scope', '$rootScope', '$timeout', 'EmployeeService', 'CashBankService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, EmployeeService, CashBankService, $sessionStorage, ngDialog, $location) {

        $scope.Employee = [];

        $scope.accountsSearchList = [];
        $scope.allAccounts = [];
        $scope.selectedAccount = '';

        $scope.allocations = [];

        $scope.allocation = {
            AllocationId: 0,
            Date: '',
            ExpiryDate: '',
            Account: '',
            Amount: 0,
            Explanation: '',
            AllocationType: '0002',         //0001 for collection       //0002 for payment
            AllocationMode: '',             //0001 for cash             //0002 for bank
            CustomerId: null,
            InvoiceHeaderId: null,
            //Source: 'invoice'
        };

        var getAllAccounts = function () {

            CashBankService.getAllAccounts($sessionStorage.UserId, function (response) {

                $scope.accountsSearchList = [];

                angular.forEach(response, function (value, key) {
                    $scope.allAccounts.push(value);
                    $scope.accountsSearchList.push({ id: value.CustomerId, name: value.CompanyTitle });
                });

                //$scope.allocation.Account = $scope.allAccounts[0];

                loadAutoComplete('ddlcustomer', $scope.accountsSearchList);
            });
        };

        var initialize = function () {

            var employeeId = $location.search().employeeId;

            EmployeeService.getEmployeeById(employeeId, function (response) {

                $scope.Employee.push(response);
            });
            getAllAccounts();
        };

        initialize();

        $scope.addAdvance = function () {

            var employeeId = $location.search().employeeId;

            $scope.allocation.CustomerId = employeeId;

            angular.forEach($scope.allAccounts, function (value, key) {

                if (value.AccountName == $scope.selectedAccount.AccountName) {
                    $scope.allocation.InvoiceHeaderId = value.CashBankId;
                }
            });

            if ($scope.allocation.InvoiceHeaderId != null && $scope.allocation.CustomerId != null) {

                EmployeeService.addAdvance($scope.allocation, function (response) {

                    if (response == true) {

                        $location.path('/employee');
                    }
                });
            }
        };

        $scope.cancel = function () {

            $scope.CompanyViewModel = {
                Company_ID: 0,
                Description: null,
                Company_Name: null,
                Business: null,
                Address: null,
                Is_Active: true
            };

            $scope.editcheck = false;

        };

        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

        $scope.delete = function (employeeId) {

            EmployeeService.delete(employeeId, function (response) {

                if (response == true) {

                    $location.path('/employee');
                }
                else {
                    window.alert(response.ErrorMessage);
                }
            });
        };

        $scope.employeeForm = function () {

            $location.path('/CreateEmployee');
        };

        $scope.createSalary = function (employeeId) {

            $location.search({});
            $location.path('/Salary');
        };

    }]);












