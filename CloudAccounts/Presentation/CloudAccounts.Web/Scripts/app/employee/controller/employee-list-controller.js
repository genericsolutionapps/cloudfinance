﻿FSTApp.controller("EmployeeListController", ['$scope', '$rootScope', '$timeout', 'EmployeeService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, EmployeeService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.EmployeeViewModel = {
            EmployeeId: 0,
            Name: null,
            IdentificationNumber: null,
            IBAN: null,
            Email: null,
            CategoryId: 0
        };

        $scope.employees = [];

        var initialize = function () {

            $scope.employees = [];

            EmployeeService.getAllEmployees($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.employees.push(value);
                });
            });
        };

        initialize();

        $scope.createEmployee = function () {

            $location.path('/CreateEmployee');
        };

        $scope.employeeInformation = function (employeeId) {

            $location.path('/EmployeeInformation').search({ employeeId: employeeId });;
        };

    }]);









