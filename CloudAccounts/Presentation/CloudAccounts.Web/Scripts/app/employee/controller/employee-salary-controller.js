﻿FSTApp.controller("EmployeeSalaryController", ['$scope', '$rootScope', '$timeout', 'EmployeeService', 'CategoriesService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, EmployeeService, CategoriesService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.SalaryViewModel = {
            Explanation: null,
            Name: null,
            Date: null,
            Amount: null,
            Status: null,
            CategoryId: 0,
            PaymentDate: null,
            CustomerId: 0,
            CurrencyRate: 1,
            BaseRate: 1
        };

        $scope.expenseHeader = {
            InvoiceHeaderId: 0,
            //Explanation: '',
            Description: '',
            Date: null,
            InvoiceNo: null,
            CurrencyRate: 1,
            BaseRate: 1,
            Status: 'PAYABLE',
            DueDate: null,
            //BusinessPartnerId: 0,
            CustomerId: null,              //this is being used as EmployeeId
            CategoryId: null,
            InvoiceDetailCollection: '',
            TotalAmount: 0,
            VoucherType: '0002'            //0002 for salary expense            //basically this is just an expense
        };

        $scope.EmployeeViewModel = {
            EmployeeId: 0,
            Name: null,
            IdentificationNumber: null,
            IBAN: null,
            Email: null,
            CategoryId: 0
        };

        $scope.employees = [];
        $scope.employeeSearchList = [];
        $scope.selectedEmployee = '';


        $scope.employeeCategories = [];
        $scope.employeeCategorySearchList = [];
        $scope.selectedCategory = '';

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];

        $scope.selectedCurrency = 'LIRA';


        var getAllExpenseCategory = function () {

            CategoriesService.getAllCategories($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    if (value.CategoryType === '0002') {
                        $scope.employeeCategories.push(value);
                        $scope.employeeCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                    }
                });

                loadAutoComplete('ddlCategory', $scope.employeeCategorySearchList);
            });
        };

        var getCurrencyExchangeRate = function () {

            InvoiceService.getUSDRate(function (response) {
                $scope.currencies[1].rate = response.USD_TRY.val;
            });
            InvoiceService.getEURRate(function (response) {
                $scope.currencies[2].rate = response.EUR_TRY.val;
            });
            InvoiceService.getGBPRate(function (response) {
                $scope.currencies[3].rate = response.GBP_TRY.val;
            });
        };
        getCurrencyExchangeRate();

        $scope.setCurrency = function (index) {

            $scope.expenseHeader.CurrencyRate = $scope.currencies[index].rate.toFixed(4);
            $scope.selectedCurrency = $scope.currencies[index].name;
        };

        $scope.setPaymentStatus = function (status) {
            $scope.expenseHeader.Status = status;
        };

        $scope.setCategory = function (categoryId) {

            var input = $('#ddlCategory');
            input.trigger('input');
            input.trigger('change');

            //create
            if (categoryId == undefined) {

                angular.forEach($scope.employeeCategories, function (value, key) {

                    if (value.CategoryName === $scope.selectedCategory) {
                        $scope.expenseHeader.CategoryId = value.CategoryId;
                    }
                });
            }
                //edit
            else if (categoryId > 0) {

                angular.forEach($scope.employeeCategories, function (value, key) {

                    if (value.CategoryId === categoryId) {
                        $scope.selectedCategory = value.CategoryName;
                    }
                });
            }
        };

        $scope.Create = function () {

            var employeeId = $location.search().employeeId;

            //create
            if (employeeId == undefined) {

                InvoiceService.createInvoice($scope.expenseHeader, function (responseInvoice) {

                    if (responseInvoice == true) {

                        $location.path('/ExpenseList');
                    }
                });
            }
            //edit
            else if (employeeId > 0) {

                //$scope.EmployeeViewModel.EmployeeId = employeeId;

                //EmployeeService.edit($scope.EmployeeViewModel, function (response) {

                //    if (response == true) {

                //        $location.path('/employee');
                //    }
                //});
            }
        }

        $scope.setEmployee = function (employeeId) {

            var input = $('#ddlEmployee');
            input.trigger('input');
            input.trigger('change');

            //create
            if (employeeId == undefined) {

                angular.forEach($scope.employees, function (value, key) {

                    if (value.Name === $scope.selectedEmployee) {
                        $scope.expenseHeader.CustomerId = value.EmployeeId;
                        $scope.expenseHeader.Name = value.Name;
                    }
                });
            }
                //edit
            else if (employeeId > 0) {

                angular.forEach($scope.employeeCategories, function (value, key) {

                    if (value.employeeId === employeeId) {
                        $scope.expenseHeader.CustomerId = value.EmployeeId;
                        $scope.expenseHeader.Name = value.Name;
                    }
                });
            }
        };


        $scope.Cancel = function () {

            $location.path('/employee');
        };


        var initialize = function () {

            getAllExpenseCategory();

            EmployeeService.getAllEmployees($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.employees.push(value);
                    $scope.employeeSearchList.push({ id: value.EmployeeId, name: value.Name });
                });
                loadAutoComplete('ddlEmployee', $scope.employeeSearchList);
            });
        };

        initialize();



    }]);









