﻿FSTApp.controller("UserController", ['$scope', '$rootScope', '$location', '$timeout', 'UserService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $location, $timeout, UserService, $sessionStorage, ngDialog) {


        $scope.Visibilty = {
            List: true,
            Details: false,
            Edit: false,
            Invite: false
        };


        $scope.UserViewModel = {

            Name: '',
            Email: '',
            Password: '',
            UserStatusTypeCode: '',
            CashBankTransaction: '',
            SalesTransaction: '',
            ExpenseTransaction: '',
            EmployeesTransaction: ''

        };


        $scope.Users = [];


        var initialize = function () {

            var userId = $sessionStorage.UserId;

            UserService.getAllUser(userId, function (response) {

                angular.forEach(response, function (value, key) {
                    $scope.Users.push(value);
                });

            })
        };


        initialize();



        $scope.Invite = function () {

            $scope.Visibilty.List = false;
            $scope.Visibilty.Details = false;
            $scope.Visibilty.Edit = false;
            $scope.Visibilty.Invite = true;

        };


        $scope.InviteUser = function () {

            //Take the userVM and save that userVM in the database
            //take the email from userVM and to that email, send an invitation email with credentials
            //set the UserStatusTypeCode appropiratly
            //agaisnt this user, show a row in the user list

            $scope.UserViewModel.UserStatusTypeCode = '0003';

            UserService.inviteUser($scope.UserViewModel, function (response) {


                if(response != null && response != undefined){

                    $scope.UserViewModel.Name = response.Name;
                    $scope.UserViewModel.Email = response.Email;
                    $scope.UserViewModel.Password = response.Password;
                    $scope.UserViewModel.UserStatusTypeCode = response.UserStatusTypeCode;
                    $scope.UserViewModel.CashBankTransaction = response.CashBankTransaction;
                    $scope.UserViewModel.SalesTransaction = response.SalesTransaction;
                    $scope.UserViewModel.ExpenseTransaction = response.ExpenseTransaction;
                    $scope.UserViewModel.EmployeesTransaction = response.EmployeesTransaction;

                    $scope.Users.push($scope.UserViewModel);

                    $scope.Visibilty.List = true;
                    $scope.Visibilty.Details = false;
                    $scope.Visibilty.Edit = false;
                    $scope.Visibilty.Invite = false;

                }

                $scope.UserViewModel

            })

        };


        $scope.Details = function (email) {

            $rootScope.startSpin();

            $scope.Visibilty.List = false;
            $scope.Visibilty.Details = true;
            $scope.Visibilty.Edit = false;
            $scope.Visibilty.Invite = false;


            angular.forEach($scope.Users, function (value, key) {
                if(value.Email === email){
                    //$scope.UserViewModel.UserId = value.UserId;
                    $scope.UserViewModel.Name = value.Name;
                    $scope.UserViewModel.Email = value.Email;
                    $scope.UserViewModel.UserStatusTypeCode = value.UserStatusTypeCode;


                    //Password: '',
                    //CashBankTransaction: '',
                    //SalesTransaction: '',
                    //ExpenseTransaction: '',
                    //EmployeesTransaction: ''


                    $scope.UserViewModel.CashBankTransaction = value.CashBankTransaction;
                    $scope.UserViewModel.SalesTransaction = value.SalesTransaction;
                    $scope.UserViewModel.ExpenseTransaction = value.ExpenseTransaction;
                    $scope.UserViewModel.EmployeesTransaction = value.EmployeesTransaction;

                }
            });


            $rootScope.stopSpin();
        };

        $scope.Edit = function (userId) {

            $rootScope.startSpin();

            $scope.Visibilty.List = false;
            $scope.Visibilty.Details = false;
            $scope.Visibilty.Edit = true;
            $scope.Visibilty.Invite = false;


            //UserService.edit($scope.CompanyViewModel, function (response) {

            //    $scope.Visibilty.Details = true;
            //    $scope.Visibilty.Edit = false;

            //})

            $rootScope.stopSpin();
        };







        $scope.SaveEdit = function () {

            $rootScope.startSpin();

            $scope.UserViewModel.Password = null;

            UserService.edit($scope.UserViewModel, function (response) {

                $scope.Visibilty.Details = true;
                $scope.Visibilty.Edit = false;

            })

            $rootScope.stopSpin();
        };



    }]);





