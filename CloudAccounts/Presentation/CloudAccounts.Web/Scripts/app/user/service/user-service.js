﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("UserService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {


    this.getUserById = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getuserbyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.edit = function (userViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userViewModel);

        ServiceManager.execute(Resource.edituser({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.inviteUser = function (userViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userViewModel);

        ServiceManager.execute(Resource.inviteuser({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


    this.getAllUser = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getalluser({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };




    this.submitcall = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.submitcall({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };


   
}]);