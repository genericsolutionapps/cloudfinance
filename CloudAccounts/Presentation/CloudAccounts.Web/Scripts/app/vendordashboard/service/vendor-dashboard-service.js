﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VendorDashboardService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', '$rootScope',
    function ($http, ServiceManager, Resource, commonobjects, $rootScope) {


        this.getallfirstcategorymaster = function (callback) {

            ServiceManager.execute(Resource.getallfirstcategorymaster(), function (response) {

                callback(response);

            });

        };

        this.getallcategorymasterbyId = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.getallcategorymasterbyId({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.getallcategoryvendor = function (model, callback) {

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.getallcategoryvendor({ data: JSON.stringify(request) }), function (response) {

                callback(response);

            });

        };

        this.createcategoryvendor = function (model, callback) {

            $rootScope.startSpin();

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.createcategoryvendor({ data: JSON.stringify(request) }), function (response) {

                $rootScope.stopSpin();

                callback(response);

            });


        };

        this.editcategoryvendor = function (model, callback) {

            $rootScope.startSpin();

            var request = commonobjects.requestobject;

            request.data = JSON.stringify(model);

            ServiceManager.execute(Resource.editcategoryvendor({ data: JSON.stringify(request) }), function (response) {

                $rootScope.stopSpin();

                callback(response);

            });


        };


    }]);