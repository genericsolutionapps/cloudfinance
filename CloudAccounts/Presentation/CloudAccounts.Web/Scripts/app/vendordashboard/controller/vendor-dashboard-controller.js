﻿FSTApp.controller("VendorDashboardController", ['$scope', '$rootScope', 'VendorDashboardService', '$timeout', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, VendorDashboardService, $timeout, $sessionStorage, ngDialog) {

        $scope.categorylist1 = [];
        $scope.categorylist2 = [];
        $scope.categorylist3 = [];
        $scope.categorylist4 = [];

        $scope.selectedcategory1 = null;
        $scope.selectedcategory2 = null;
        $scope.selectedcategory3 = null;
        $scope.selectedcategory4 = null;

        $scope.listvendorcategory = [];

        $scope.CategoryVendorViewModel = {

            CategoryVendorId: 0,
            VendorId: 0,
            Name: null,
            HeirarchyName: null,
            FirstCategoryMasterId: 0,
            SecondCategoryMasterId: 0,
            ThirdCategoryMasterId: 0,
            FourthCategoryMasterId: 0,
            Active: false

        };

        $scope.editcheck = false;

        var loadmastercategories = function () {

            $scope.categorylist1 = [];

            VendorDashboardService.getallfirstcategorymaster(function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist1.push(value);

                    });

                }

            });

        };

        var loadvendorcategories = function () {

            $scope.listvendorcategory = [];

            VendorDashboardService.getallcategoryvendor($sessionStorage.VendorId, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.listvendorcategory.push(value);

                    });

                }

            });

        };

        $scope.categorychange1 = function (callback) {

            $scope.categorylist2 = [];

            VendorDashboardService.getallcategorymasterbyId($scope.selectedcategory1, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist2.push(value);

                    });

                }

                if (callback != null && callback != undefined) {

                    callback();
                }

            });


        };

        $scope.categorychange2 = function (callback) {

            $scope.categorylist3 = [];

            VendorDashboardService.getallcategorymasterbyId($scope.selectedcategory2, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist3.push(value);

                    });

                }

                if (callback != null && callback != undefined) {

                    callback();
                }


            });


        };

        $scope.categorychange3 = function (callback) {

            $scope.categorylist4 = [];

            VendorDashboardService.getallcategorymasterbyId($scope.selectedcategory3, function (response) {

                if (response != null && response != undefined) {

                    angular.forEach(response, function (value, key) {

                        $scope.categorylist4.push(value);

                    });

                }

                if (callback != null && callback != undefined) {

                    callback();
                }


            });


        };

        $scope.cancel = function () {

            $scope.editcheck = false;

            $scope.selectedcategory1 = null;
            $scope.selectedcategory2 = null;
            $scope.selectedcategory3 = null;
            $scope.selectedcategory4 = null;

            $scope.CategoryVendorViewModel = {

                CategoryVendorId: 0,
                VendorId: 0,
                Name: null,
                HeirarchyName: null,
                FirstCategoryMasterId: 0,
                SecondCategoryMasterId: 0,
                ThirdCategoryMasterId: 0,
                FourthCategoryMasterId: 0,
                Active: false

            };
        };

        $scope.save = function (file) {

            $scope.CategoryVendorViewModel.VendorId = $sessionStorage.VendorId;

            if ($scope.selectedcategory1 != null && $scope.selectedcategory1 != undefined) {
                $scope.CategoryVendorViewModel.FirstCategoryMasterId = $scope.selectedcategory1;

            }

            if ($scope.selectedcategory2 != null && $scope.selectedcategory2 != undefined) {
                $scope.CategoryVendorViewModel.SecondCategoryMasterId = $scope.selectedcategory2;

            }

            if ($scope.selectedcategory3 != null && $scope.selectedcategory3 != undefined) {
                $scope.CategoryVendorViewModel.ThirdCategoryMasterId = $scope.selectedcategory3;

            }

            if ($scope.selectedcategory4 != null && $scope.selectedcategory4 != undefined) {
                $scope.CategoryVendorViewModel.FourthCategoryMasterId = $scope.selectedcategory4;

            }

            if ($scope.CategoryVendorViewModel.Name == null || $scope.CategoryVendorViewModel.Name == '') {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else if ($scope.CategoryVendorViewModel.FirstCategoryMasterId == null || $scope.CategoryVendorViewModel.FirstCategoryMasterId == 0) {

                ngDialog.open({
                    template: '<p>First category should be selected.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {
                if ($scope.editcheck == false) {
                    VendorDashboardService.createcategoryvendor($scope.CategoryVendorViewModel, function (response) {

                        if (response == true)
                            alert('Category has been added');

                        $scope.cancel();
                        $scope.selectedcategory1 = null;
                        $scope.selectedcategory2 = null;
                        $scope.selectedcategory3 = null;
                        $scope.selectedcategory4 = null;

                        loadvendorcategories();
                    });
                }
                else {

                    VendorDashboardService.editcategoryvendor($scope.CategoryVendorViewModel, function (response) {

                        if (response == true)
                            alert('Category has been edit');

                        $scope.cancel();
                        $scope.selectedcategory1 = null;
                        $scope.selectedcategory2 = null;
                        $scope.selectedcategory3 = null;
                        $scope.selectedcategory4 = null;
                        $scope.editcheck = false;

                        loadvendorcategories();
                    });
                }
            }

        };

        $scope.edit = function (item) {

            $rootScope.startSpin();

            $scope.editcheck = true;

            $scope.CategoryVendorViewModel = item;
            $scope.selectedcategory1 = '' + item.FirstCategoryMasterId + '';
            $scope.selectedcategory2 = '' + item.SecondCategoryMasterId + '';
            $scope.selectedcategory3 = '' + item.ThirdCategoryMasterId + '';
            $scope.selectedcategory4 = '' + item.FourthCategoryMasterId + '';

            $scope.categorychange1(function () {

                $scope.selectedcategory2 = '' + item.SecondCategoryMasterId + '';

                $scope.categorychange2(function () {

                    $scope.selectedcategory3 = '' + item.ThirdCategoryMasterId + '';

                    $scope.categorychange3(function () {

                        $scope.selectedcategory4 = '' + item.FourthCategoryMasterId + '';

                        $rootScope.stopSpin();

                    });

                });

            });
            


        };

        $scope.delete = function (item) {



        };


        $timeout(function () {

            loadvendorcategories();
            loadmastercategories();

        });





    }]);