﻿FSTApp.controller("VendorLoginController", ['$scope', 'VendorLoginService', '$sessionStorage', '$location', 'ngDialog', '$rootScope',
    function ($scope, VendorLoginService, $sessionStorage, $location, ngDialog, $rootScope) {

        $scope.VendorLogin = {

            Username: '',
            Password: ''

        };


        $scope.submit = function () {

            if ($scope.VendorLogin.Username != '' && $scope.VendorLogin.Password != '') {

                $rootScope.startSpin();
                VendorLoginService.loginsubmit($scope.VendorLogin, function (response) {

                    delete $sessionStorage.UserId;
                    delete $sessionStorage.Username;
                    delete $sessionStorage.VendorId;
                    delete $sessionStorage.BuyerId;

                    $sessionStorage.UserId = response.UserId;
                    $sessionStorage.Username = response.Username;
                    $sessionStorage.VendorId = response.VendorId;
                    $sessionStorage.BuyerId = response.BuyerId;

                    $rootScope.Username = response.Username;

                    $rootScope.stopSpin();

                    $location.path('/VendorDashboard');


                });

            }
            else {

                ngDialog.open({
                    template: '<p>Please enter username and password.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
            }

        };



    }]);