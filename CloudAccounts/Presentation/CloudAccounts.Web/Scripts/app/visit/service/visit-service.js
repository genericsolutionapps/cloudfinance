﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VisitService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {

    this.getallcompany = function (callback) {

        ServiceManager.execute(Resource.getallcompany(), function (response) {
            callback(response);
        });
    };

    this.getallstatus = function (callback) {

        ServiceManager.execute(Resource.getallstatus(), function (response) {
            callback(response);
        });
    };

    this.getallemployee = function (callback) {

        ServiceManager.execute(Resource.getallemployee(), function (response) {
            callback(response);
        });
    };

    this.getallvisit = function (callback) {

        var request = commonobjects.requestobject;

        ServiceManager.execute(Resource.getallvisit(), function (response) {

            callback(response);

        });

    };

    this.createvisit = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.createvisit({ data: request }), function (response) {

            callback(response);

        });

    };

    this.editvisit = function (model, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(model);

        ServiceManager.execute(Resource.editvisit({ data: request }), function (response) {

            callback(response);

        });



    };

}]);