﻿FSTApp.controller("VisitController", ['$scope', '$rootScope', '$timeout', 'VisitService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $timeout, VisitService, $sessionStorage, ngDialog) {

        $scope.VisitViewModel = {
            VisitId: 0,
            Date: null,
            Employee_ID: 0,
            StatusId: 0,
            Company_ID: 0,
            Name: null,
            LastVisit: null,
            Address: null,
            ProductIntro: null,
            NewInquiry: null,
            NewOrder: null,
            Payment: null,
            Advertising: null,
            StockLevel: null,
            Remarks: null

        };

        $scope.CompanyViewModel = {
            Company_ID: 0,
            Code: null,
            Company_Name: null,
            Is_Active: true
        };

        $scope.EmployeeViewModel = {
            Employee_ID: 0,
            Employee_Name: null,
            Email: null,
            Contact_No: null,
            Secondary_Contact_No: null,
            Address: null,
            Permanent_Address: null,
            Is_Active: true
        };

        $scope.StatusViewModel = {
            StatusId: 0,
            Description: null,
            Name: null,
            Title: null,
            Mobile_No: null,
            Address: null,
            Is_Active: true
        };


        $scope.editcheck = false;

        $scope.listvisit = [];
        $scope.companylist = [];
        $scope.statuslist = [];
        $scope.employeelist = [];

        $scope.selectedcompany = null;
        $scope.selectedstatus = null;
        $scope.selectedemployee = null;

        var loadcompany = function () {

            VisitService.getallcompany(function (response) {
                var companylist = response;
                angular.forEach(companylist, function (value, key) {
                    $scope.companylist.push(value);
                });
            });
        };
        var loadstatus = function () {
            VisitService.getallstatus(function (response) {
                var statuslist = response;
                angular.forEach(statuslist, function (value, key) {
                    $scope.statuslist.push(value);
                });
            });
        };
        var loademployee = function () {
            VisitService.getallemployee(function (response) {
                var employeelist = response;
                angular.forEach(employeelist, function (value, key) {
                    $scope.employeelist.push(value);
                });
            });
        };

        var loadvisit = function () {

            $scope.listvisit = [];

            VisitService.getallvisit(function (response) {

                var visitlist = response;

                angular.forEach(visitlist, function (value, key) {

                    $scope.listvisit.push(value);

                });

            });

        };

        $scope.cancel = function () {

            $scope.VisitViewModel = {
                VisitId: 0,
                Date: null,
                Employee_ID: 0,
                StatusId: 0,
                Company_ID: 0,
                Name: null,
                Time: null,
                LastVisit: null,
                Address: null,
                ProductIntro: null,
                NewInquiry: null,
                NewOrder: null,
                Payment: null,
                Advertising: null,
                StockLevel: null,
                Remarks: null
            };
            $scope.selectedcompany = null;
            $scope.selectedstatus = null;
            $scope.selectedemployee = null;
            $scope.editcheck = false;

        };

        $scope.save = function () {

            if ($scope.selectedcompany != null && $scope.selectedcompany != undefined && $scope.selectedstatus != null && $scope.selectedstatus != undefined && $scope.selectedemployee != null && $scope.selectedemployee != undefined) {
                $scope.VisitViewModel.Company_ID = $scope.selectedcompany;
                $scope.VisitViewModel.StatusId = $scope.selectedstatus;
                $scope.VisitViewModel.Employee_ID = $scope.selectedemployee;
            }

            if ($scope.VisitViewModel.Name == null || $scope.VisitViewModel.Name == undefined || $scope.VisitViewModel.Company_ID == 0 || $scope.VisitViewModel.StatusId == 0 || $scope.VisitViewModel.Employee_ID == 0) {

                ngDialog.open({
                    template: '<p>Please fill mandetory fields.</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

            }
            else {

                if ($scope.editcheck == false) {
                    VisitService.createvisit($scope.VisitViewModel, function (response) {

                        if (response == true)
                            alert('Visit Information has been added');

                        $scope.cancel();
                        $scope.selectedcompany = null;
                        $scope.selectedstatus = null;
                        $scope.selectedemployee = null;
                        loadvisit();
                    });
                }
                else {

                    VisitService.editvisit($scope.VisitViewModel, function (response) {

                        if (response == true)
                            alert('Visit Information has been edit');

                        $scope.cancel();
                        $scope.selectedcompany = null;
                        $scope.selectedstatus = null;
                        $scope.selectedemployee = null;
                        $scope.editcheck = false;
                        loadvisit();
                    });
                }
            }
        };

        $scope.edit = function (item) {

            $scope.editcheck = true;

            $scope.VisitViewModel = item;
            $scope.selectedcompany = '' + item.Company_ID + '';
            $scope.selectedstatus = '' + item.StatusId + '';
            $scope.selectedemployee = '' + item.Employee_ID + '';

        };

        $scope.delete = function (item) {



        };

        $timeout(function () {

            loadcompany();
            loademployee();
            loadstatus();
            loadvisit();

            //$('#statuslist').DataTable({
            //    'paging': true,
            //    'lengthChange': false,
            //    'searching': false,
            //    'ordering': true,
            //    'info': true,
            //    'autoWidth': false
            //});

            $('#visitlist').DataTable();
        });

    }]);