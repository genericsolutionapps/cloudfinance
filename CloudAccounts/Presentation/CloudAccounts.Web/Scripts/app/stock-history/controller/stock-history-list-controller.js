﻿FSTApp.controller("StockHistoryListController", ['$scope', '$rootScope', '$timeout', 'InvoiceService', 'ProductService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, InvoiceService, ProductService, $sessionStorage, ngDialog, $location) {

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];

        $scope.invoiceDetails = [];

        var initialize = function () {

            var userId = $sessionStorage.UserId;

            InvoiceService.getAllInvoiceDetails($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.invoiceDetails.push(value);
                });
            });
        };


        initialize();

        $scope.parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

        $scope.productInformation = function (productId) {

            $location.path('/ProductInformation/').search({ productId: productId });
        };


    }]);
