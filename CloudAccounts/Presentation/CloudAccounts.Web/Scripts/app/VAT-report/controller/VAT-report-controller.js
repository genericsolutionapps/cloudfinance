﻿FSTApp.controller("VATReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'VATReportService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, VATReportService, $sessionStorage, ngDialog) {

        $scope.monthList = [];

        $scope.thisMonth;

        $scope.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $scope.currentMonth = new Date().getMonth() - $scope.monthList.length;

        $scope.calculateMonth = function () {

            if ($scope.currentMonth >= 0) {
                $scope.currentMonth = $scope.currentMonth;
            }
            else if ($scope.currentMonth < 0) {
                $scope.currentMonth + 12;
            }
        };

        $scope.initialize = function () {

            VATReportService.getVATByMonth([new Date().getMonth() + 1 - $scope.monthList.length, new Date().getFullYear()], function (response) {

                angular.forEach(response, function (value, key) {

                    if (value[key] != null) {

                        angular.forEach(value, function (value1, key) {

                            value1.Date = parseDate(value1.Date);
                        });
                    }
                    $scope.monthList.push(value);
                });
                $scope.thisMonth = $scope.monthList[0];

                $scope.calculateMonth();
            });
        };

        $scope.initialize();


        $scope.calculateVAT = function (month) {

            return month.reduce((accumulator, currentValue) => accumulator + currentValue.TotalVAT, 0);
        };


        $scope.loadInvoicesByMonth = function (month) {

            $scope.thisMonth = month;
        };


        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };


    }]);






