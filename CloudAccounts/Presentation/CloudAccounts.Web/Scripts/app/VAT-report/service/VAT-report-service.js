﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("VATReportService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {


    this.getVATByMonth = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getVATbymonth({ data: JSON.stringify(request) }), function (response) {

            callback(response);
        });
    };

}]);





