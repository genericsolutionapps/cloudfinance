﻿FSTApp.controller("IncomeExpenseReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'IncomeExpenseReportService', 'InvoiceService', 'CategoriesService', 'CustomerService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, IncomeExpenseReportService, InvoiceService, CategoriesService, CustomerService, $sessionStorage, ngDialog) {


        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: '',
            EndDate: '',
            ReportName: 'IncomeExpense'
        };


        $scope.PieChartDataSales = [];
        $scope.PieChartDataCustomers = [];
        $scope.TotalAmountInSalesCategories = 0;


        var initializePieCharts = function () {

            $scope.PieChartDataSales = [];

            CategoriesService.getAllSalesCategories($scope.DateRangeViewModel, function (response) {

                $scope.TotalAmountInSalesCategories = 0;

                angular.forEach(response, function (value, key) {

                    $scope.TotalAmountInSalesCategories = $scope.TotalAmountInSalesCategories + value.BalanceAgainstThisCategory;
                });

                angular.forEach(response, function (value, key) {

                    $scope.PieChartDataSales.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / $scope.TotalAmountInSalesCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });

                var plotObj = $.plot($("#flot_pie_chart"), $scope.PieChartDataSales, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    legend: {
                        show: false
                    },
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    }
                });
            });
        };




        $scope.setDate = function (dateType) {
            if (dateType == undefined) {
                $('#startDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                var date = new Date();

                var startDate = new Date(date.getTime() - 30 * 24 * 60 * 60 * 1000);

                $('#startDate input').datepicker('setDate', startDate);


                $('#endDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                $('#endDate input').datepicker('setDate', new Date());
            }

            $scope.DateRangeViewModel.StartDate = $('#startDate input').val();
            $scope.DateRangeViewModel.EndDate = $('#endDate input').val();

            initializePieCharts();
        };

        $scope.setDate();


    }]);






