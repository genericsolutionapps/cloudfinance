﻿FSTApp.controller("ProductCreateController", ['$scope', '$rootScope', '$timeout', 'ProductService', 'CategoriesService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, ProductService, CategoriesService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.visibility = {
            StartQuantity: "true",
        };

        $scope.productViewModel = {
            CompanyId: 0,
            Name: null,
            Code: null,
            Barcode: null,
            CategoryId: 0,
            StartingQuantity: 0,
            Unit: null,
            PurchasePrice: null,
            PurchaseCurrencyRate: 1,
            SalesPrice: null,
            SalesCurrencyRate: 1,
            SalesCurrencyId: 0,
            KDVPerc: "18",
            BaseRate: 1,
            PurchaseCurrencyId: 0
        };

        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: new Date(),
            EndDate: new Date(),
            ReportName: ''
        };

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];

        $scope.selectedPurchaseCurrency = 'LIRA';
        $scope.selectedSalesCurrency = 'LIRA';

        $scope.selectedCategory = '';

        $scope.productCategories = [];
        $scope.productCategorySearchList = [];

        var initialize = function () {

            var productId = $location.search().productId;

            CategoriesService.getAllProductCategories($scope.DateRangeViewModel, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.productCategories.push(value);
                    $scope.productCategorySearchList.push({ id: value.CategoryId, name: value.CategoryName });
                });
                loadAutoComplete('ddlCategory', $scope.productCategorySearchList);
            })
            if (productId > 0) {
                ProductService.getProductById(productId, function (response) {
                    $scope.productViewModel.CompanyId = response.CompanyId;
                    $scope.productViewModel.Name = response.Name;
                    $scope.productViewModel.Code = response.Code;
                    $scope.productViewModel.Barcode = response.Barcode;
                    $scope.productViewModel.CategoryId = response.CategoryId;
                    $scope.productViewModel.StartingQuantity = response.StartingQuantity;
                    $scope.productViewModel.Unit = response.Unit;
                    $scope.productViewModel.PurchasePrice = response.PurchasePrice;
                    $scope.productViewModel.PurchaseCurrencyRate = response.PurchaseCurrencyRate;
                    $scope.productViewModel.SalesPrice = response.SalesPrice;
                    $scope.productViewModel.SalesCurrencyRate = response.SalesCurrencyRate;
                    $scope.productViewModel.KDVPerc = response.KDVPerc;
                    $scope.productViewModel.BaseRate = response.BaseRate;

                    angular.forEach($scope.productCategorySearchList, function (value, key) {

                        if (value.id == response.CategoryId)
                            $scope.selectedCategory = value.name;
                    });
                });
            }
        };

        initialize();

        $scope.setCategory = function () {

            var input = $('#ddlCategory');
            input.trigger('input');
            input.trigger('change');

            angular.forEach($scope.productCategories, function (value, key) {

                if (value.CategoryName === $scope.selectedCategory) {
                    $scope.productViewModel.CategoryId = value.CategoryId;
                }
            });
        };

        $scope.setCurrency = function (index, type) {
            if (type == 'purchase') {
                $scope.productViewModel.PurchaseCurrencyRate = $scope.currencies[index].rate.toFixed(4);
                $scope.selectedPurchaseCurrency = $scope.currencies[index].name;

                $scope.productViewModel.PurchaseCurrencyId = index;
            }
            else if (type == 'sales') {
                $scope.productViewModel.SalesCurrencyRate = $scope.currencies[index].rate.toFixed(4);
                $scope.selectedSalesCurrency = $scope.currencies[index].name;

                $scope.productViewModel.SalesCurrencyId = index;
            }
        };

        $scope.create = function () {

            var productId = $location.search().productId;

            if (productId == undefined) {
                ProductService.create($scope.productViewModel, function (response) {

                    if (response == true) {

                        $location.path('/ProductList');
                    }
                });
            }
            else if (productId > 0) {

                $scope.productViewModel.productId = productId;

                ProductService.edit($scope.productViewModel, function (response) {

                    if (response == true) {

                        $location.path('/ProductList');
                    }
                });
            }
        }

        var getCurrencyExchangeRate = function () {

            InvoiceService.getUSDRate(function (response) {
                $scope.currencies[1].rate = response.USD_TRY.val;
            });
            InvoiceService.getEURRate(function (response) {
                $scope.currencies[2].rate = response.EUR_TRY.val;
            });
            InvoiceService.getGBPRate(function (response) {
                $scope.currencies[3].rate = response.GBP_TRY.val;
            });
        };
        getCurrencyExchangeRate();

        $scope.createBill = function () {

            $location.path('/CreateBill');
        };

        $scope.Cancel = function () {

            $location.path('/ProductList');
        };


    }]);













