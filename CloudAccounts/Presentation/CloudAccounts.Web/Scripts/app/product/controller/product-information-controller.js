﻿FSTApp.controller("ProductInformationController", ['$scope', '$rootScope', '$timeout', 'ProductService', 'InvoiceService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, ProductService, InvoiceService, $sessionStorage, ngDialog, $location) {

        $scope.product = [];

        $scope.allInvoicesByProductId = [];

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];


        var initialize = function () {

            var productId = $location.search().productId;

            ProductService.getProductById(productId, function (response) {

                $scope.product.push(response);
            });

            InvoiceService.getInvoicesByProductId(productId, function (response) {

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);

                    value.DueDate = parseDate(value.DueDate);

                    $scope.allInvoicesByProductId.push(value);
                });
            });
        };


        initialize();


        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };


        $scope.createProduct = function () {

            $location.path('/CreateProduct');
        };

        $scope.edit = function () {

            var productId = $scope.product[0].ProductId;

            $location.path('/CreateProduct/').search({ productId: productId });
        };

        $scope.delete = function () {

            var productId = $scope.product[0].ProductId;

            ProductService.delete(productId, function (response) {

                if (response == true) {

                    $location.path('/ProductList');
                }
            });
        };



    }]);
