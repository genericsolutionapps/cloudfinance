﻿FSTApp.controller("ProductListController", ['$scope', '$rootScope', '$timeout', 'ProductService', '$sessionStorage', 'ngDialog', '$location',
    function ($scope, $rootScope, $timeout, ProductService, $sessionStorage, ngDialog, $location) {

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];

        var initialize = function () {

            var userId = $sessionStorage.UserId;

            $scope.productList = [];

            ProductService.getAllProducts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.productList.push(value);
                });
            });
        };


        initialize();

        $scope.CompanyViewModel = {
            CompanyId: 0,
            CompanyName: null,
            CommercialTitle: null,
            Email: null,
            Phone: null,
            Sector: null,
            Address: 'Active',
            TaxInformation: null
        };

        $scope.createProduct = function () {

            $location.path('/CreateProduct');
        };

        $scope.productInformation = function (productId) {

            $location.path('/ProductInformation/').search({ productId: productId });
        };


    }]);
