﻿/// <reference path="../../common/models/common-objects.js" />

FSTApp.service("ProductService", ['$http', 'ServiceManager', 'Resource', 'CommonObject', function ($http, ServiceManager, Resource, commonobjects) {



    this.edit = function (productViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(productViewModel);

        ServiceManager.execute(Resource.editproduct({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.getProductById = function (productId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(productId);

        ServiceManager.execute(Resource.getproductbyid({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.create = function (productViewModel, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(productViewModel);

        ServiceManager.execute(Resource.addproduct({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });
    };

    this.getAllProducts = function (userId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(userId);

        ServiceManager.execute(Resource.getallproducts({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    this.delete = function (productId, callback) {

        var request = commonobjects.requestobject;

        request.data = JSON.stringify(productId);

        ServiceManager.execute(Resource.deleteprodcut({ data: JSON.stringify(request) }), function (response) {

            callback(response);

        });

    };

    //this.getBalanceByMonth = function (userId, callback) {

    //    var request = commonobjects.requestobject;

    //    request.data = JSON.stringify(userId);

    //    ServiceManager.execute(Resource.getbalancebymonth({ data: JSON.stringify(request) }), function (response) {

    //        callback(response);
    //    });
    //};



}]);





