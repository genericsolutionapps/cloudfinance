﻿FSTApp.controller("SalesReportController", ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'SalesReportService', 'InvoiceService', 'CategoriesService', 'CustomerService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $location, $routeParams, $timeout, SalesReportService, InvoiceService, CategoriesService, CustomerService, $sessionStorage, ngDialog) {

        $scope.invoiceHeaderList = [];
        $scope.customerList = [];


        $scope.visibility = {
            noData: false
        };

        var date1 = new Date();
        date1.setMonth(11);
        date1.setYear(2017);

        var date2 = new Date();
        date2.setMonth(0);
        date2.setYear(2018);

        var date3 = new Date();
        date3.setMonth(1);
        date3.setYear(2018);

        var date4 = new Date();
        date4.setMonth(2);
        date4.setYear(2018);

        //$scope.DateFilterValue = '';

        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: new Date(),
            EndDate: new Date(),
            ReportName: 'Sales'
        };


        var barData = {
            label: "bar",
            data: []
        };//2674800000 one month in ticks

        $scope.PieChartDataSales = [];
        $scope.PieChartDataCustomers = [];



        var initializePieCharts = function () {

            $scope.PieChartDataSales = [];
            $scope.PieChartDataCustomers = [];

            CategoriesService.getAllSalesCategories($scope.DateRangeViewModel, function (response) {

                var totalAmountInSalesCategories = 0;

                angular.forEach(response, function (value, key) {

                    totalAmountInSalesCategories = totalAmountInSalesCategories + value.BalanceAgainstThisCategory;
                });

                angular.forEach(response, function (value, key) {

                    $scope.PieChartDataSales.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / totalAmountInSalesCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });
                var plotObj = $.plot($("#flot_pie_chart"), $scope.PieChartDataSales, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    }
                });

            });
            CategoriesService.getAllCustomerCategories($scope.DateRangeViewModel, function (response) {

                var totalAmountInCustomerCategories = 0;

                angular.forEach(response, function (value, key) {

                    totalAmountInCustomerCategories = totalAmountInCustomerCategories + value.BalanceAgainstThisCategory;
                });

                angular.forEach(response, function (value, key) {

                    $scope.PieChartDataCustomers.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / totalAmountInCustomerCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });

                var plotObj = $.plot($("#flot_pie_chart1"), $scope.PieChartDataCustomers, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    }
                });
            });
        };

        var initializeBarChart = function () {

            InvoiceService.getBalanceByMonth($sessionStorage.UserId, function (response) {
                angular.forEach(response, function (value, key) {
                    barData.data.push([value.Ticks, value.Balance]);
                });

                var barOptions = {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 15 * 24 * 60 * 60 * 1000,
                            align: 'center',
                            fillColor: {
                                colors: [{
                                    opacity: 0.8
                                }, {
                                    opacity: 0.8
                                }]
                            }
                        }
                    },
                    xaxis: {
                        mode: "time",
                        timeformat: "%m-%Y",
                        tickSize: [1, "month"]
                    },
                    colors: ["#18C5A9"],
                    grid: {
                        color: "#999999",
                        hoverable: true,
                        clickable: true,
                        tickColor: '#DADDE0',// "#D4D4D4",
                        borderWidth: 0
                    },
                    legend: {
                        show: false
                    },
                    tooltip: true,
                    tooltipOpts: {
                        //content: "x: %x, y: %y"
                        content: "collected %y in %x"
                    }
                };
                $.plot($("#flot_bar_chart"), [barData], barOptions);

            });

        };

        var getAllInvoices = function () {

            $scope.invoiceHeaderList = [];

            InvoiceService.getAllInvoices($scope.DateRangeViewModel, function (response) {

                angular.forEach(response, function (value, key) {

                    var dateFormat = new Date(parseInt(value.Date.replace(/(^.*\()|([+-].*$)/g, '')));

                    var day = dateFormat.getDate() + "/";
                    var month = (dateFormat.getMonth() + 1);
                    if (month < 10) {
                        month = "0" + month + "/";
                    }
                    else {
                        month = month + "/";
                    }
                    var year = dateFormat.getFullYear();

                    value.Date = day + month + year;

                    var expiryDateFormat = new Date(parseInt(value.DueDate.replace(/(^.*\()|([+-].*$)/g, '')));

                    var expiryDay = expiryDateFormat.getDate() + "/";
                    var expiryMonth = (expiryDateFormat.getMonth() + 1);
                    if (expiryMonth < 10) {
                        expiryMonth = "0" + expiryMonth + "/";
                    }
                    else {
                        expiryMonth = expiryMonth + "/";
                    }
                    var expiryYear = expiryDateFormat.getFullYear();

                    value.DueDate = expiryDay + expiryMonth + expiryYear;

                    $scope.invoiceHeaderList.push(value);
                });
            });
        };

        var getAllCustomers = function () {

            CustomerService.getAllBusinessPartnersByType($sessionStorage.UserId, function (response) {

                $scope.customerList = [];

                angular.forEach(response, function (value, key) {
                    $scope.customerList.push(value);
                });
            });

        };

        $scope.invoiceInformation = function (invoiceHeaderId) {

            $location.path('/Invoice/').search({ invoiceHeaderId: invoiceHeaderId });
        }


        $scope.setDate = function (dateType) {
            if (dateType == undefined) {
                $('#startDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                var date = new Date();

                var startDate = new Date(date.getTime() - 30 * 24 * 60 * 60 * 1000);

                $('#startDate input').datepicker('setDate', startDate);


                $('#endDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                $('#endDate input').datepicker('setDate', new Date());
            }

            $scope.DateRangeViewModel.StartDate = $('#startDate input').val();
            $scope.DateRangeViewModel.EndDate = $('#endDate input').val();

            $scope.DateFilterValue = $scope.DateRangeViewModel.StartDate + " - " + $scope.DateRangeViewModel.EndDate;

            initializePieCharts();
            initializeBarChart();
            getAllInvoices();
            getAllCustomers();
        };

        $scope.setDate();


    }]);






