﻿FSTApp.controller("ExpenseReportController", ['$scope', '$rootScope', '$routeParams', '$timeout', 'ExpenseReportService', 'CategoriesService', 'ProductService', 'InvoiceService', 'CustomerService', '$sessionStorage', 'ngDialog',
    function ($scope, $rootScope, $routeParams, $timeout, ExpenseReportService, CategoriesService, ProductService, InvoiceService, CustomerService, $sessionStorage, ngDialog) {

        $scope.DateRangeViewModel = {
            UserId: $sessionStorage.UserId,
            StartDate: new Date(),
            EndDate: new Date(),
            ReportName: 'Expense'
        };

        $scope.visibility = {
            noData: false
        };

        $scope.currencies = [
            { index: 0, name: 'LIRA', rate: 1, symbol: 'la la-turkish-lira' },
            { index: 1, name: 'USD', rate: 2, symbol: 'la la-dollar' },
            { index: 2, name: 'EUR', rate: 3, symbol: 'la la-euro' },
            { index: 3, name: 'GBP', rate: 4, symbol: 'la la-gbp' }
        ];

        $scope.expenseCategories = [];
        $scope.supplierCategories = [];
        $scope.productCategories = [];

        $scope.expenseHeaderList = [];
        $scope.suppliersList = [];
        $scope.productsList = [];

        //pie chart data
        $scope.expensePieChart = [];
        $scope.suppliersPieChart = [];
        $scope.productsPieChart = [];


        $scope.setDate = function (dateType) {
            if (dateType == undefined) {
                $('#startDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                var date = new Date();

                var startDate = new Date(date.getTime() - 30 * 24 * 60 * 60 * 1000);

                $('#startDate input').datepicker('setDate', startDate);


                $('#endDate input').datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true
                });
                $('#endDate input').datepicker('setDate', new Date());
            }

            $scope.DateRangeViewModel.StartDate = $('#startDate input').val();
            $scope.DateRangeViewModel.EndDate = $('#endDate input').val();

            $scope.DateFilterValue = $scope.DateRangeViewModel.StartDate + " - " + $scope.DateRangeViewModel.EndDate;

            //initializePieCharts();
        };

        $scope.initialize = function () {

            $scope.setDate();

            //getting all expense Categories
            CategoriesService.getAllExpenseCategories($scope.DateRangeViewModel, function (response) {

                $scope.totalAmountInExpenseCategories = 0;

                angular.forEach(response, function (value, key) {

                    $scope.totalAmountInExpenseCategories = $scope.totalAmountInExpenseCategories + value.BalanceAgainstThisCategory;

                    $scope.expenseCategories.push(value);
                });

                angular.forEach(response, function (value, key) {

                    $scope.expensePieChart.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / $scope.totalAmountInExpenseCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });
            });

            //getting all supplier Categories
            CategoriesService.getAllSupplierCategories($scope.DateRangeViewModel, function (response) {

                $scope.totalAmountInSupplierCategories = 0;

                angular.forEach(response, function (value, key) {

                    $scope.totalAmountInSupplierCategories = $scope.totalAmountInSupplierCategories + value.BalanceAgainstThisCategory;

                    $scope.supplierCategories.push(value);
                });
                angular.forEach(response, function (value, key) {

                    $scope.suppliersPieChart.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / $scope.totalAmountInSupplierCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });
            });

            //getting all product Categories
            CategoriesService.getAllProductCategories($scope.DateRangeViewModel, function (response) {

                $scope.totalAmountInProductCategories = 0;

                angular.forEach(response, function (value, key) {

                    $scope.totalAmountInProductCategories = $scope.totalAmountInProductCategories + value.BalanceAgainstThisCategory;

                    $scope.productCategories.push(value);
                });
                angular.forEach(response, function (value, key) {

                    $scope.productsPieChart.push({
                        label: value.CategoryName,
                        data: ((value.BalanceAgainstThisCategory / $scope.totalAmountInExpenseCategories) * 100),
                        color: value.CategoryColor,
                        balanceAgainstThisCategory: value.BalanceAgainstThisCategory
                    });
                });
            });

            //getting all expenses
            InvoiceService.getAllInvoices($scope.DateRangeViewModel, function (response) {

                angular.forEach(response, function (value, key) {

                    value.Date = parseDate(value.Date);

                    //value.DueDate = parseDate(value.DueDate);

                    if (value.VoucherType == '0002') {
                        $scope.expenseHeaderList.push(value);
                    }
                });
            });

            //getting all BPs
            CustomerService.getAllBusinessPartnersByType($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.suppliersList.push(value);
                });
            });

            //getting all Products
            ProductService.getAllProducts($sessionStorage.UserId, function (response) {

                angular.forEach(response, function (value, key) {

                    $scope.productsList.push(value);
                });
            });

            setTimeout(function () {
                initializePieCharts();
            }, 2500);
        };

        $scope.initialize();


        var initializePieCharts = function () {

            var plotObj = $.plot($("#pie_chart_expense"), $scope.expensePieChart, {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    show: false
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            });
            var plotObj1 = $.plot($("#pie_chart_suppliers"), $scope.suppliersPieChart, {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    show: false
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            });
            var plotObj2 = $.plot($("#pie_chart_products"), $scope.productsPieChart, {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    show: false
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            });

        };

        $scope.compareDate = function (dueDate) {

            return new Date(parseInt(dueDate.replace('/Date(', ''))).getTime() < new Date().getTime();
        };


        var parseDate = function (date) {
            var dateFormat = new Date(parseInt(date.replace(/(^.*\()|([+-].*$)/g, '')));

            var day = dateFormat.getDate() + "/";
            var month = (dateFormat.getMonth() + 1);
            if (month < 10) {
                month = "0" + month + "/";
            }
            else {
                month = month + "/";
            }
            var year = dateFormat.getFullYear();

            return day + month + year;
        };

    }]);






