﻿var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str.name)) {
                matches.push(str.name);
            }
        });

        cb(matches);
    };
};


var loadAutoComplete = function (controlId, source) {

    $('#' + controlId).typeahead({
        hint: true,
        highlight: true,
        minLength: 0
    },
    {
        name: 'states',
        source: substringMatcher(source)
    });

};

var setLoading = function (loading) {
    var body = $('body');
    if (loading) {
        $('.preloader-backdrop').fadeIn(200);
    } else {
        $('.preloader-backdrop').fadeOut(200);
    }
};

var bodyClass = function (Class) {
    $('body').attr('class', Class);
};

var initLayout = function () {
    // SIDEBAR ACTIVATE METISMENU
    $(".metismenu").metisMenu();

    // SIDEBAR TOGGLE ACTION
    $('.js-sidebar-toggler').click(function () {
        if ($('body').hasClass('drawer-sidebar')) {
            $('#sidebar').backdrop();
        } else {
            $('body').toggleClass('sidebar-mini');
            if (!$('body').hasClass('sidebar-mini')) {
                $('#sidebar-collapse').hide();
                setTimeout(function () {
                    $('#sidebar-collapse').fadeIn(300);
                }, 200);
            }
        }
    });

    // QUICK SIDEBAR TOGGLE ACTION
    $('.quick-sidebar-toggler').click(function () {
        $('.quick-sidebar').backdrop();
    });

    // SEARCH BAR ACTION
    $('.js-search-toggler').click(function () {
        $('.search-top-bar').backdrop().find('.search-input').focus();
    });

    // Session timeout

    var idle_timer;
    (function () {
        $('#timeout-activate').click(function () {
            if (+$('#timeout-count').val()) {
                activate(+$('#timeout-count').val());
            }
        });

        $('#timeout-reset').click(function () {
            reset();
        });

        function reset() {
            $(document).idleTimer("destroy");
            if (idle_timer) clearTimeout(idle_timer);
            $('#session-dialog').modal('hide');
            $('.timeout-toggler').removeClass('active');
            $('#timeout-reset-box').hide();
            $('#timeout-activate-box').show();
        }

        function activate(count) {
            $('#session-dialog').modal('hide');
            $('#timeout-reset-box').show();
            $('#timeout-activate-box').hide();
            $(document).idleTimer(count * 60000);

            setTimeout(function () {
                $('.timeout-toggler').addClass('active');
            }, (count - 1) * 60000);

            $(document).on("idle.idleTimer", function (event, elem, obj) {
                // function you want to fire when the user goes idle
                toastr.warning('Your session is about to expire. The page will redirect after 15 seconds with no activity.', 'Session Timeout Notification', {
                    "progressBar": true,
                    "timeOut": 5000,
                });
                idle_timer = setTimeout(timeOutHandler, 5000);
            });

            $(document).on("active.idleTimer", function (event, elem, obj, triggerevent) {
                // function you want to fire when the user becomes active again
                clearTimeout(idle_timer);
                $(document).idleTimer("reset");
                toastr.clear();
                toastr.success('You returned to the active mode.', 'You are back.');
            });

            function timeOutHandler() {
                reset();
                alert('Your session has expired. You can redirect this page or logout.');
            }
        }
    })();
}


var initPage = function () {

    // Activate bootstrap select
    if ($(".selectpicker").length > 0) {
        $('.selectpicker').selectpicker();
    }

    // Activate Tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Activate Popovers
    $('[data-toggle="popover"]').popover();

    // Activate slimscroll
    $('.scroller').each(function () {
        $(this).slimScroll({
            height: $(this).attr('data-height') || '100%',
            color: $(this).attr('data-color') || '#71808f',
            railOpacity: '0.9',
            size: '4px',
        });
    });

    $('.slimScrollBar').hide();


    // Pre Copy to clipboard

    if ($(".clipboard-copy").length > 0) {
        new Clipboard('.clipboard-copy', {
            target: function (t) {
                return t.nextElementSibling;
            }
        }).on('success', function (e) {
            e.clearSelection();
            e.trigger.textContent = 'Copied';
            window.setTimeout(function () {
                e.trigger.textContent = 'Copy';
            }, 2000);
        });
    }

    // PANEL ACTIONS
    // ======================

    $('.ibox-collapse').click(function () {
        var ibox = $(this).closest('div.ibox');
        ibox.toggleClass('collapsed-mode').children('.ibox-body').slideToggle(200);
    });
    $('.ibox-remove').click(function () {
        $(this).closest('div.ibox').remove();
    });
    $('.fullscreen-link').click(function () {
        if ($('body').hasClass('fullscreen-mode')) {
            $('body').removeClass('fullscreen-mode');
            $(this).closest('div.ibox').removeClass('ibox-fullscreen');
            $(window).off('keydown', toggleFullscreen);
        } else {
            $('body').addClass('fullscreen-mode');
            $(this).closest('div.ibox').addClass('ibox-fullscreen');
            $(window).on('keydown', toggleFullscreen);
        }
    });
    function toggleFullscreen(e) {
        // pressing the ESC key - KEY_ESC = 27 
        if (e.which == 27) {
            $('body').removeClass('fullscreen-mode');
            $('.ibox-fullscreen').removeClass('ibox-fullscreen');
            $(window).off('keydown', toggleFullscreen);
        }
    }

    setTimeout(function () {
        $('.minicolors').minicolors({
            control: $(this).attr('data-control') || 'hue',
            defaultValue: $(this).attr('data-defaultValue') || '',
            format: $(this).attr('data-format') || 'hex',
            keywords: $(this).attr('data-keywords') || '',
            inline: $(this).attr('data-inline') === 'true',
            letterCase: $(this).attr('data-letterCase') || 'lowercase',
            opacity: $(this).attr('data-opacity'),
            position: $(this).attr('data-position') || 'bottom left',
            swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
            change: function (value, opacity) {
                if (!value) return;
                if (opacity) value += ', ' + opacity;
                if (typeof console === 'object') {
                    console.log(value);
                }
            },
            theme: 'bootstrap'
        });

        $('#datatable').DataTable();

        //incase there are two datatables on the same page then datatable1 id is being used
        $('#datatable1').DataTable();

        //incase there are three datatables on the same page then datatable2 id is being used
        $('#datatable2').DataTable();

        //incase there are four datatables on the same page then datatable3 id is being used
        $('#datatable3').DataTable();

        $(document).ready(function () {

            $('#date_1 input').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true
            });
            $('#date_1 input').datepicker('setDate', new Date());

            $('#date_99 input').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true
            });

            $('#date_99 input').datepicker('setDate', new Date());

            $('#date_customerAllocation input').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true
            });

            $('#date_customerAllocation input').datepicker('setDate', new Date());


            $('#date_2 input').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true
            });
            var todayDate = new Date();
            $('#date_2 input').datepicker('setDate', todayDate);

            $("#set_date_today").click(function () {
                $('#date_2 input').datepicker('setDate', todayDate);
            });
            $("#set_date_7").click(function () {
                var newDate = new Date(todayDate.getTime() + 7 * 24 * 60 * 60 * 1000);
                $('#date_2 input').datepicker('setDate', newDate);
            });
            $("#set_date_14").click(function () {
                var newDate = new Date(todayDate.getTime() + 14 * 24 * 60 * 60 * 1000);
                $('#date_2 input').datepicker('setDate', newDate);
            });
            $("#set_date_30").click(function () {
                var newDate = new Date(todayDate.getTime() + 30 * 24 * 60 * 60 * 1000);
                $('#date_2 input').datepicker('setDate', newDate);
            });
            $("#set_date_60").click(function () {
                var newDate = new Date(todayDate.getTime() + 60 * 24 * 60 * 60 * 1000);
                $('#date_2 input').datepicker('setDate', newDate);
            });

        });


    }, 1000);
}




