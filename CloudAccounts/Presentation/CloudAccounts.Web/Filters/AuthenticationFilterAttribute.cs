﻿using CloudAccounts.SharedModel;
using CloudAccounts.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CloudAccounts.Web.Filters
{
    public class AuthenticationFilterAttribute : ActionFilterAttribute
    {
        public bool Disabled { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Disabled)
            {
                return;
            }

            if (SessionInfo.UserId == 0)
            {
                //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Home" }, { "action", "NullUserException" } });

                ResponseObject response = new ResponseObject();

                response.IsSuccess = false;
                response.ResponseCode = "0002";
                response.ErrorMessage = "User session ended on server";

                filterContext.Result = new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}