USE [CloudAccountsDb]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](50) NOT NULL,
	[AccountCode] [varchar](10) NOT NULL,
	[AccountType] [varchar](50) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Nature] [varchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Allocation]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Allocation](
	[AllocationId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[Account] [nvarchar](50) NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Explanation] [nvarchar](max) NULL,
	[AllocationType] [nvarchar](5) NOT NULL,
	[AllocationMode] [nvarchar](5) NOT NULL,
	[InvoiceHeaderId] [int] NULL,
	[CashBankId] [int] NOT NULL,
 CONSTRAINT [PK_Allocation1] PRIMARY KEY CLUSTERED 
(
	[AllocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuthorizedPerson]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorizedPerson](
	[APId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](50) NULL,
	[Notes] [varchar](max) NULL,
	[CompanyId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_AuthorizedPerson] PRIMARY KEY CLUSTERED 
(
	[APId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CashBank]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashBank](
	[CashBankId] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](50) NOT NULL,
	[CurrencyName] [varchar](50) NOT NULL,
	[CurrencyRate] [decimal](18, 4) NOT NULL,
	[OpeningBalance] [decimal](18, 4) NULL,
	[OpeningBalanceDate] [datetime] NULL,
	[BankName] [varchar](50) NULL,
	[Branch] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[IBAN] [varchar](50) NULL,
	[CompanyId] [int] NOT NULL,
	[AccountType] [varchar](10) NOT NULL,
	[AccountCode] [varchar](10) NOT NULL,
 CONSTRAINT [PK_CashBank] PRIMARY KEY CLUSTERED 
(
	[CashBankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[CategoryColor] [varchar](50) NOT NULL,
	[CategoryType] [varchar](50) NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CommercialTitle] [varchar](50) NULL,
	[Email] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[OpenAddress] [varchar](max) NULL,
	[Logo] [varchar](max) NULL,
	[District] [varchar](max) NULL,
	[Province] [varchar](max) NULL,
	[Wire] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[VD] [varchar](50) NULL,
	[VNo] [varchar](50) NULL,
	[CompanyTypeCode] [varchar](5) NOT NULL,
	[FeatureTypeCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompanyType]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyType](
	[CompanyTypeId] [int] NOT NULL,
	[CompanyTypeCode] [varchar](5) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Icon] [varchar](50) NULL,
 CONSTRAINT [PK_CompanyType] PRIMARY KEY CLUSTERED 
(
	[CompanyTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[currency]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[currency](
	[curr_ID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](10) NULL,
	[title] [varchar](30) NULL,
	[symbol] [varchar](10) NULL,
	[created_On] [datetime] NULL,
	[created_By] [varchar](50) NULL,
	[Machine_Created] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[curr_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[CompanyTitle] [varchar](50) NOT NULL,
	[ShortName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[FaxNumber] [varchar](50) NULL,
	[IBANNumber] [varchar](50) NULL,
	[District] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[CustomerType] [varchar](5) NOT NULL,
	[VKN] [varchar](50) NULL,
	[Tax] [varchar](50) NULL,
	[OpeningBalanceDate] [datetime] NULL,
	[OpeningBalanceAmount] [decimal](18, 4) NULL,
	[CategoryId] [int] NULL,
	[Address] [varchar](max) NULL,
	[AccountCode] [varchar](10) NOT NULL,
	[BusinessPartnerType] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IdentificationNumber] [varchar](50) NULL,
	[IBAN] [varchar](50) NULL,
	[CategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Email] [varchar](50) NULL,
	[AccountCode] [varchar](10) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExpenseList]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpenseList](
	[bill_Id] [int] IDENTITY(1,1) NOT NULL,
	[explaination] [varchar](max) NULL,
	[bill_Date] [varchar](10) NULL,
	[bill_Num] [varchar](50) NULL,
	[totalAmount] [decimal](18, 4) NULL,
	[total_VAT] [decimal](18, 4) NULL,
	[paymentStatus] [varchar](20) NULL,
	[payment_Date] [varchar](10) NULL,
	[supplierName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[bill_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feature]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feature](
	[FeatureTypeId] [int] NOT NULL,
	[FeatureTypeCode] [varchar](5) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Icon] [varchar](50) NULL,
 CONSTRAINT [PK_Feature] PRIMARY KEY CLUSTERED 
(
	[FeatureTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GLDetail]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GLDetail](
	[GLDetailId] [int] IDENTITY(1,1) NOT NULL,
	[GLHeaderId] [int] NOT NULL,
	[AccountCode] [varchar](10) NOT NULL,
	[Debit] [decimal](18, 4) NOT NULL,
	[Credit] [decimal](18, 4) NOT NULL,
	[Explanation] [varchar](150) NULL,
 CONSTRAINT [PK_GLDetail] PRIMARY KEY CLUSTERED 
(
	[GLDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GLHeader]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GLHeader](
	[GLHeaderId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionType] [varchar](50) NOT NULL,
	[TransactionNumber] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[CompanyId] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GLHeader] PRIMARY KEY CLUSTERED 
(
	[GLHeaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceDetail]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetail](
	[InvoiceDetailID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceHeaderID] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [decimal](18, 4) NOT NULL,
	[DiscountPerc] [float] NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[TaxPerc] [float] NULL,
	[Description] [nvarchar](50) NULL,
	[Unit] [nvarchar](50) NULL,
 CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED 
(
	[InvoiceDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceHeader]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceHeader](
	[InvoiceHeaderId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[TotalAmount] [decimal](18, 4) NOT NULL,
	[InvoiceNo] [varchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[Description] [varchar](100) NULL,
	[CategoryId] [int] NULL,
	[CurrencyRate] [decimal](18, 4) NOT NULL,
	[BaseRate] [decimal](18, 4) NOT NULL,
	[VoucherType] [varchar](10) NOT NULL,
	[CurrencyId] [int] NOT NULL,
	[LabelId] [int] NULL,
 CONSTRAINT [PK_InvoiceHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceHeaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](50) NULL,
	[Barcode] [varchar](50) NULL,
	[StartingQuantity] [int] NULL,
	[Unit] [varchar](50) NULL,
	[PurchasePrice] [decimal](18, 4) NULL,
	[PurchaseCurrencyRate] [decimal](18, 4) NULL,
	[SalesPrice] [decimal](18, 4) NULL,
	[SalesCurrencyRate] [decimal](18, 4) NULL,
	[KDVPerc] [decimal](18, 4) NULL,
	[BaseRate] [decimal](18, 4) NULL,
	[AccountCode] [varchar](10) NULL,
	[SalesCurrencyId] [int] NULL,
	[PurchaseCurrencyId] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[supplierDetail]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplierDetail](
	[supplierDetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[supplierHeaderID] [int] NULL,
	[supplierName] [varchar](100) NULL,
	[email] [varchar](50) NULL,
	[telephone] [varchar](20) NULL,
	[Notes] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[supplierDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupplierHeader]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierHeader](
	[supplierID] [int] IDENTITY(1,1) NOT NULL,
	[companyName] [varchar](50) NULL,
	[shortName] [varchar](30) NULL,
	[category] [varchar](20) NULL,
	[emailAddress] [varchar](50) NULL,
	[phoneNum] [varchar](20) NULL,
	[faxNum] [varchar](20) NULL,
	[IBAN_Num] [varchar](30) NULL,
	[openAddress] [varchar](max) NULL,
	[district] [varchar](50) NULL,
	[province] [varchar](50) NULL,
	[legalEntity] [bit] NULL,
	[realPerson] [bit] NULL,
	[TC_Identification] [varchar](50) NULL,
	[vkn_tckn] [varchar](30) NULL,
	[taxAdministration] [varchar](50) NULL,
	[companyDate] [datetime] NULL,
	[price] [decimal](18, 0) NULL,
	[currencySymbol] [varchar](3) NULL,
	[msg] [varchar](max) NULL,
	[openingBal] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[supplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 03/02/2020 1:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Title] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[SurName] [varchar](50) NULL,
	[UserStatusTypeCode] [varchar](5) NOT NULL,
	[Phone] [varchar](50) NULL,
	[Photo] [varchar](max) NULL,
	[CompanyId] [int] NOT NULL,
	[CashBankTransaction] [varchar](5) NOT NULL,
	[SalesTransaction] [varchar](5) NOT NULL,
	[ExpenseTransaction] [varchar](5) NOT NULL,
	[EmployeesTransaction] [varchar](5) NOT NULL,
	[guid] [varchar](max) NULL,
	[IsEmailConfirmed] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (1018, N'Opening Balance', N'0001', N'Opening Balance', 1, N'CAPITAL')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (1019, N'General Expense', N'0002', N'General Expense', 1, N'EXPENSE')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (2018, N'Cash', N'0003', N'CASH', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (3018, N'Bank', N'0004', N'BANK', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (9021, N'Salaries Expense', N'0042', N'Expense', 1, N'EXPENSE')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11025, N'Marvi Rice', N'0043', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11026, N'un processed rice', N'0044', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11027, N'new customer on fly', N'0045', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11028, N'a', N'0046', N'Employee', 1, N'EXPENSE')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11029, N'pro with dif cur', N'0047', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11030, N'Generic Solutions & Consultancy', N'0048', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11031, N'Generic Solutions & Consultancy', N'0049', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11032, N'Generic Solutions & Consultancy', N'0050', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11033, N'Generic Solutions & Consultancy', N'0051', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11034, N'Generic Solutions & Consultancy', N'0052', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11035, N'Generic Solution & Consultancy', N'0053', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11036, N'Generic Solution & Consultancy', N'0054', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11037, N'Generic Solutions', N'0055', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11038, N'Generic Solutions', N'0056', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11039, N'Test', N'0057', N'Account Receivable', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11040, N'Sella Rice', N'0058', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11041, N'Basmati Rice', N'0059', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11042, N'Basmati Super Rice', N'0060', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11043, N'Football Rice', N'0061', N'Inventory', 1, N'ASSET')
GO
INSERT [dbo].[Account] ([AccountId], [AccountName], [AccountCode], [AccountType], [CompanyId], [Nature]) VALUES (11046, N'Marvel Agro', N'0064', N'Account Receivable', 1, N'ASSET')
GO
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[AuthorizedPerson] ON 
GO
INSERT [dbo].[AuthorizedPerson] ([APId], [Name], [Email], [Telephone], [Notes], [CompanyId], [CustomerId]) VALUES (17, N'', NULL, NULL, NULL, 1, 5007)
GO
INSERT [dbo].[AuthorizedPerson] ([APId], [Name], [Email], [Telephone], [Notes], [CompanyId], [CustomerId]) VALUES (18, N'saad', N'hasnain.105@gmail.com', N'123123123', N'123123123', 1, 5009)
GO
INSERT [dbo].[AuthorizedPerson] ([APId], [Name], [Email], [Telephone], [Notes], [CompanyId], [CustomerId]) VALUES (20, N'saad', N'saad@gmail.com', N'123123123', N'123123123123', 1, 5011)
GO
SET IDENTITY_INSERT [dbo].[AuthorizedPerson] OFF
GO
SET IDENTITY_INSERT [dbo].[CashBank] ON 
GO
INSERT [dbo].[CashBank] ([CashBankId], [AccountName], [CurrencyName], [CurrencyRate], [OpeningBalance], [OpeningBalanceDate], [BankName], [Branch], [AccountNumber], [IBAN], [CompanyId], [AccountType], [AccountCode]) VALUES (1002, N'Cash In Hand', N'TRL – Turkish Lira', CAST(1.0000 AS Decimal(18, 4)), CAST(1000.0000 AS Decimal(18, 4)), CAST(N'2018-04-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 1, N'0001', N'0003')
GO
INSERT [dbo].[CashBank] ([CashBankId], [AccountName], [CurrencyName], [CurrencyRate], [OpeningBalance], [OpeningBalanceDate], [BankName], [Branch], [AccountNumber], [IBAN], [CompanyId], [AccountType], [AccountCode]) VALUES (1004, N'Bank', N'TRL - Turkish Lira', CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(N'2018-04-03T00:00:00.000' AS DateTime), N'Some Bank', N'Some Branch', N'351weg', NULL, 1, N'0002', N'0044')
GO
SET IDENTITY_INSERT [dbo].[CashBank] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (50, N'new cateeee', N'#c90404', N'0001', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (52, N'e', N'#a36d6d', N'0002', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (55, N'new ex', N'#456fad', N'0002', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (56, N'qqq', N'#8a9dba', N'0005', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (57, N'qwqwq', N'#5105eb', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (60, N'rt', N'#23c23d', N'0004', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (61, N'reve', N'#416ab5', N'0003', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (65, N'new catee1223', N'#120707', N'0001', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (67, N'Textile Customers', N'#055259', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (69, N'un-sales', N'#b9b7b5', N'0001', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (70, N'un-Business Partner', N'#b9b7b5', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (71, N'new product category', N'#946918', N'0004', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (1073, N'un-product', N'#b9b7b5', N'0004', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (1076, N'un-expense', N'#b9b7b5', N'0002', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3096, N'un-revenue', N'#b9b7b5', N'0003', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3097, N'un-workers', N'#b9b7b5', N'0005', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3098, N'un-customer', N'#b9b7b5', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3099, N'Test Customer', N'#ffffff', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3100, N'Rice Invoice', N'#C0C0C0', N'0001', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3101, N'Marvel Agro', N'#ffffff', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3102, N'Customer Invoice', N'#ffffff', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3103, N'Customer Invoice', N'#ffffff', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3104, N'Customer Invoice', N'#ffffff', N'0006', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3105, N'Customer Invoice', N'#808080', N'0001', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3106, N'ELECTRONIC', N'#FF0000', N'0004', 1)
GO
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [CategoryColor], [CategoryType], [CompanyId]) VALUES (3107, N'SPARE PARTS', N'#00FF00', N'0004', 1)
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Company] ON 
GO
INSERT [dbo].[Company] ([CompanyId], [Name], [CommercialTitle], [Email], [Phone], [OpenAddress], [Logo], [District], [Province], [Wire], [Fax], [VD], [VNo], [CompanyTypeCode], [FeatureTypeCode]) VALUES (1, N'Name of the Company', N'Commercial title of the Company', N'new@company.com', N'12312456', N'Address of the company', NULL, N'North', N'Sindh', N'02136640271', N'02136642724', N'0001', N'0002', N'0005', N'1')
GO
INSERT [dbo].[Company] ([CompanyId], [Name], [CommercialTitle], [Email], [Phone], [OpenAddress], [Logo], [District], [Province], [Wire], [Fax], [VD], [VNo], [CompanyTypeCode], [FeatureTypeCode]) VALUES (2, N'c', N'c', N'c', N'123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', N'1')
GO
INSERT [dbo].[Company] ([CompanyId], [Name], [CommercialTitle], [Email], [Phone], [OpenAddress], [Logo], [District], [Province], [Wire], [Fax], [VD], [VNo], [CompanyTypeCode], [FeatureTypeCode]) VALUES (3, N'aaa', N'aaa', N'hdfhdf', N'aaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0001', N'0001')
GO
INSERT [dbo].[Company] ([CompanyId], [Name], [CommercialTitle], [Email], [Phone], [OpenAddress], [Logo], [District], [Province], [Wire], [Fax], [VD], [VNo], [CompanyTypeCode], [FeatureTypeCode]) VALUES (4, N'qq', N'qq', N'asd', N'qqq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0001', N'0002')
GO
INSERT [dbo].[Company] ([CompanyId], [Name], [CommercialTitle], [Email], [Phone], [OpenAddress], [Logo], [District], [Province], [Wire], [Fax], [VD], [VNo], [CompanyTypeCode], [FeatureTypeCode]) VALUES (5, N'aaa', N'aaa', N'new@user.ocm', N'aaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0001', N'0001')
GO
SET IDENTITY_INSERT [dbo].[Company] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 
GO
INSERT [dbo].[Customer] ([CustomerId], [CompanyId], [CompanyTitle], [ShortName], [Email], [PhoneNumber], [FaxNumber], [IBANNumber], [District], [Province], [CustomerType], [VKN], [Tax], [OpeningBalanceDate], [OpeningBalanceAmount], [CategoryId], [Address], [AccountCode], [BusinessPartnerType]) VALUES (5007, 1, N'Marvi Rice', N'MR', N'mr@gmail.com', N'5q1wd', N'65a1f', N'65q1wd5q1', NULL, NULL, N'0001', N'651qwd', N'51qwd', NULL, NULL, 70, N'65q1wdq', N'0043', N'0001')
GO
INSERT [dbo].[Customer] ([CustomerId], [CompanyId], [CompanyTitle], [ShortName], [Email], [PhoneNumber], [FaxNumber], [IBANNumber], [District], [Province], [CustomerType], [VKN], [Tax], [OpeningBalanceDate], [OpeningBalanceAmount], [CategoryId], [Address], [AccountCode], [BusinessPartnerType]) VALUES (5008, 1, N'new customer on fly', N'new cust', NULL, N'65q1we', NULL, NULL, N'aaa', N'aaa', N'0001', N'aaa', N'aaa', NULL, NULL, 57, N'some weird address', N'0045', N'0001')
GO
INSERT [dbo].[Customer] ([CustomerId], [CompanyId], [CompanyTitle], [ShortName], [Email], [PhoneNumber], [FaxNumber], [IBANNumber], [District], [Province], [CustomerType], [VKN], [Tax], [OpeningBalanceDate], [OpeningBalanceAmount], [CategoryId], [Address], [AccountCode], [BusinessPartnerType]) VALUES (5009, 1, N'Generic Solutions', N'GSC', N'hasnain.105@gmail.com', N'02136640271', N'123123123', N'123123123', N'Gulshan', N'Sindh', N'0001', N'123123123', N'123123123', NULL, NULL, 3098, N'123123123', N'0055', N'0001')
GO
INSERT [dbo].[Customer] ([CustomerId], [CompanyId], [CompanyTitle], [ShortName], [Email], [PhoneNumber], [FaxNumber], [IBANNumber], [District], [Province], [CustomerType], [VKN], [Tax], [OpeningBalanceDate], [OpeningBalanceAmount], [CategoryId], [Address], [AccountCode], [BusinessPartnerType]) VALUES (5011, 1, N'Test', N'Test', N'Test@test.com', N'123123123', N'123123123', N'123123123', N'North', N'Sindh', N'0001', N'123123123', N'123123123', NULL, NULL, 3099, N'123123123', N'0057', N'0001')
GO
INSERT [dbo].[Customer] ([CustomerId], [CompanyId], [CompanyTitle], [ShortName], [Email], [PhoneNumber], [FaxNumber], [IBANNumber], [District], [Province], [CustomerType], [VKN], [Tax], [OpeningBalanceDate], [OpeningBalanceAmount], [CategoryId], [Address], [AccountCode], [BusinessPartnerType]) VALUES (5012, 1, N'Marvel Agro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0001', NULL, NULL, NULL, NULL, 3098, NULL, N'0064', N'0001')
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 
GO
INSERT [dbo].[Employee] ([EmployeeId], [Name], [IdentificationNumber], [IBAN], [CategoryId], [CompanyId], [Email], [AccountCode]) VALUES (5, N'a', N'a', N'a', 56, 1, N'a', N'0046')
GO
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[GLDetail] ON 
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9125, 9084, N'0043', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for ')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9126, 9084, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), N'Credit entry for ')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9127, 9085, N'0045', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for innnn')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9128, 9085, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), N'Credit entry for innnn')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9129, 9086, N'0043', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for invoi')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9130, 9086, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), N'Credit entry for invoi')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9131, 9087, N'0044', CAST(118.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for aaa')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9132, 9087, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(118.0000 AS Decimal(18, 4)), N'Credit entry for aaa')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9133, 9088, N'0043', CAST(127.4400 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for invoice with discount and tax')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9134, 9088, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(127.4400 AS Decimal(18, 4)), N'Credit entry for invoice with discount and tax')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9135, 9089, N'0044', CAST(118.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for new ex')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9136, 9089, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(118.0000 AS Decimal(18, 4)), N'Credit entry for new ex')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9137, 9090, N'0044', CAST(118.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for exlpajin')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9138, 9090, N'0045', CAST(0.0000 AS Decimal(18, 4)), CAST(118.0000 AS Decimal(18, 4)), N'Credit entry for exlpajin')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9139, 9091, N'0044', CAST(118.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for aaaa')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9140, 9091, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(118.0000 AS Decimal(18, 4)), N'Credit entry for aaaa')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9141, 9092, N'0043', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for xxx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9142, 9092, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), N'Credit entry for xxx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9143, 9093, N'0044', CAST(106.2000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for xx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9144, 9093, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(106.2000 AS Decimal(18, 4)), N'Credit entry for xx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9145, 9094, N'0044', CAST(106.2000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for xx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9146, 9094, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(106.2000 AS Decimal(18, 4)), N'Credit entry for xx')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9147, 9095, N'0043', CAST(106.2000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9148, 9095, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(106.2000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9149, 9096, N'0043', CAST(158.8751 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Customer Invoice')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9150, 9096, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(140.1800 AS Decimal(18, 4)), N'Credit entry for Customer Invoice')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9151, 9096, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(18.6900 AS Decimal(18, 4)), N'Credit entry for Customer Invoice')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9152, 9097, N'0043', CAST(18.6912 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Sale Invoice # 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9153, 9097, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(18.6900 AS Decimal(18, 4)), N'Credit entry for Sale Invoice # 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9154, 9098, N'0043', CAST(1869.1200 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Sale Invoice # 0002')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9155, 9098, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(1869.1200 AS Decimal(18, 4)), N'Credit entry for Sale Invoice # 0002')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9156, 9099, N'0043', CAST(40.4000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Sale Invoice # 0003')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9157, 9099, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(40.4000 AS Decimal(18, 4)), N'Credit entry for Sale Invoice # 0003')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9158, 9100, N'0043', CAST(18.6912 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Test Description')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9159, 9100, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(18.6900 AS Decimal(18, 4)), N'Credit entry for Test Description')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9160, 9108, N'0064', CAST(1495.2960 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Invoice')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9161, 9108, N'0044', CAST(0.0000 AS Decimal(18, 4)), CAST(1495.3000 AS Decimal(18, 4)), N'Credit entry for Invoice')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9162, 9109, N'0064', CAST(98.5600 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Invoice # 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9163, 9109, N'0059', CAST(0.0000 AS Decimal(18, 4)), CAST(98.5600 AS Decimal(18, 4)), N'Credit entry for Invoice # 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9164, 9110, N'0064', CAST(145.4400 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Marvel Agro')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9165, 9110, N'0059', CAST(0.0000 AS Decimal(18, 4)), CAST(145.4400 AS Decimal(18, 4)), N'Credit entry for Marvel Agro')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9166, 9111, N'0043', CAST(17.2800 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Debit entry for Invoice Test 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9167, 9111, N'0058', CAST(0.0000 AS Decimal(18, 4)), CAST(17.2800 AS Decimal(18, 4)), N'Credit entry for Invoice Test 0001')
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9168, 9112, N'0044', CAST(17.2800 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9169, 9112, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(17.2800 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9170, 9113, N'0000', CAST(1495.2960 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9171, 9113, N'0064', CAST(0.0000 AS Decimal(18, 4)), CAST(1495.2960 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9172, 9114, N'0044', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9173, 9114, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9174, 9116, N'0044', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9175, 9116, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9176, 9117, N'0044', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9177, 9117, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9178, 9118, N'0044', CAST(141.6000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL)
GO
INSERT [dbo].[GLDetail] ([GLDetailId], [GLHeaderId], [AccountCode], [Debit], [Credit], [Explanation]) VALUES (9179, 9118, N'0043', CAST(0.0000 AS Decimal(18, 4)), CAST(141.6000 AS Decimal(18, 4)), NULL)
GO
SET IDENTITY_INSERT [dbo].[GLDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[GLHeader] ON 
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9082, N'Invoice', NULL, N'', 1, CAST(N'2018-05-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9083, N'Invoice', NULL, N'', 1, CAST(N'2018-05-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9084, N'Invoice', NULL, N'', 1, CAST(N'2018-05-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9085, N'Invoice', NULL, N'innnn', 1, CAST(N'2018-05-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9086, N'Invoice', NULL, N'invoi', 1, CAST(N'2018-05-22T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9087, N'Bill', NULL, N'aaa', 1, CAST(N'2018-05-26T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9088, N'Invoice', NULL, N'invoice with discount and tax', 1, CAST(N'2018-05-29T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9089, N'Bill', NULL, N'new ex', 1, CAST(N'2018-05-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9090, N'Bill', NULL, N'exlpajin', 1, CAST(N'2018-05-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9091, N'Bill', NULL, N'aaaa', 1, CAST(N'2018-05-31T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9092, N'Invoice', NULL, N'xxx', 1, CAST(N'2018-06-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9093, N'Bill', NULL, N'xx', 1, CAST(N'2018-06-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9094, N'Bill', N'xx', N'xx', 1, CAST(N'2018-06-07T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9095, N'Allocation', NULL, N'xx', 1, CAST(N'2018-06-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9096, N'Invoice', NULL, N'Customer Invoice', 1, CAST(N'2018-07-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9097, N'Invoice', NULL, N'Sale Invoice # 0001', 1, CAST(N'2018-08-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9098, N'Invoice', NULL, N'Sale Invoice # 0002', 1, CAST(N'2018-08-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9099, N'Invoice', NULL, N'Sale Invoice # 0003', 1, CAST(N'2018-08-05T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9100, N'Invoice', NULL, N'Test Description', 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9102, N'Invoice', NULL, N'Invoice Description', 1, CAST(N'2018-09-03T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9104, N'Invoice', NULL, N'Invoice Desc', 1, CAST(N'2018-09-04T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9105, N'Invoice', NULL, N'invoice description', 1, CAST(N'2018-09-04T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9106, N'Invoice', NULL, N'Invoice Description', 1, CAST(N'2018-09-04T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9107, N'Invoice', NULL, N'Invoice Descr', 1, CAST(N'2018-09-04T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9108, N'Invoice', NULL, N'Invoice', 1, CAST(N'2018-09-04T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9109, N'Invoice', NULL, N'Invoice # 0001', 1, CAST(N'2018-09-10T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9110, N'Invoice', NULL, N'Marvel Agro', 1, CAST(N'2018-09-10T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9111, N'Invoice', NULL, N'Invoice Test 0001', 1, CAST(N'2018-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9112, N'Allocation', NULL, N'Invoice Test 0001', 1, CAST(N'2018-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9113, N'Allocation', NULL, N'Invoice', 1, CAST(N'2018-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9114, N'Allocation', NULL, N'', 1, CAST(N'2018-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9115, N'Invoice', NULL, N'', 1, CAST(N'2019-06-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9116, N'Allocation', NULL, N'', 1, CAST(N'2019-06-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9117, N'Allocation', NULL, N'', 1, CAST(N'2019-06-09T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[GLHeader] ([GLHeaderId], [TransactionType], [TransactionNumber], [Description], [CompanyId], [TransactionDate]) VALUES (9118, N'Allocation', NULL, N'', 1, CAST(N'2019-06-09T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[GLHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetail] ON 
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7090, 2017, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 0, CAST(141.6000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7092, 2019, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 0, CAST(141.6000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7094, 2021, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 10, CAST(127.4400 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7098, 2025, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 0, CAST(141.6000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7099, 2025, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 0, CAST(141.6000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7100, 2026, 6, 1, CAST(100.0000 AS Decimal(18, 4)), 10, CAST(106.2000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7101, 2027, 6, 1, CAST(100.0000 AS Decimal(18, 4)), 10, CAST(106.2000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7102, 2028, 6, 1, CAST(120.0000 AS Decimal(18, 4)), 1, CAST(140.1800 AS Decimal(18, 4)), 18, N'Sella Rice', NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7103, 2028, 7, 1, CAST(16.0000 AS Decimal(18, 4)), 1, CAST(18.6900 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7104, 2029, 7, 1, CAST(16.0000 AS Decimal(18, 4)), 1, CAST(18.6900 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7105, 2030, 7, 100, CAST(16.0000 AS Decimal(18, 4)), 1, CAST(1869.1200 AS Decimal(18, 4)), 18, N'Special Rice', N'Kg')
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7106, 2031, 7, 2, CAST(20.0000 AS Decimal(18, 4)), 0, CAST(40.4000 AS Decimal(18, 4)), 1, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7107, 2032, 7, 1, CAST(16.0000 AS Decimal(18, 4)), 1, CAST(18.6900 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7108, 2033, 6, 12, CAST(120.0000 AS Decimal(18, 4)), 12, CAST(1495.3000 AS Decimal(18, 4)), 18, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7109, 2034, 8, 1, CAST(100.0000 AS Decimal(18, 4)), 12, CAST(98.5600 AS Decimal(18, 4)), 12, NULL, N'100')
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7110, 2035, 8, 12, CAST(12.0000 AS Decimal(18, 4)), 0, CAST(145.4400 AS Decimal(18, 4)), 1, NULL, NULL)
GO
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailID], [InvoiceHeaderID], [ProductId], [Quantity], [UnitPrice], [DiscountPerc], [Amount], [TaxPerc], [Description], [Unit]) VALUES (7111, 2036, 7, 1, CAST(16.0000 AS Decimal(18, 4)), 0, CAST(17.2800 AS Decimal(18, 4)), 8, NULL, N'12')
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceHeader] ON 
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2017, 1, 5007, CAST(141.6000 AS Decimal(18, 4)), NULL, CAST(N'2018-05-21T00:00:00.000' AS DateTime), CAST(N'2018-05-21T00:00:00.000' AS DateTime), N'Unpaid', N'', 50, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2019, 1, 5007, CAST(141.6000 AS Decimal(18, 4)), NULL, CAST(N'2018-05-22T00:00:00.000' AS DateTime), CAST(N'2018-05-22T00:00:00.000' AS DateTime), N'Unpaid', N'invoi', 50, CAST(4.5932 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 1, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2021, 1, 5007, CAST(127.4400 AS Decimal(18, 4)), NULL, CAST(N'2018-05-29T00:00:00.000' AS DateTime), CAST(N'2018-05-29T00:00:00.000' AS DateTime), N'Unpaid', N'invoice with discount and tax', 50, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2025, 1, 5007, CAST(141.6000 AS Decimal(18, 4)), NULL, CAST(N'2018-06-05T00:00:00.000' AS DateTime), CAST(N'2018-06-05T00:00:00.000' AS DateTime), N'Unpaid', N'xxx', 50, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2026, 1, 5007, CAST(106.2000 AS Decimal(18, 4)), NULL, CAST(N'2018-06-07T00:00:00.000' AS DateTime), CAST(N'2018-06-07T00:00:00.000' AS DateTime), N'PAYABLE', N'xx', 52, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0002', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2027, 1, 5007, CAST(106.2000 AS Decimal(18, 4)), N'xx', CAST(N'2018-06-07T00:00:00.000' AS DateTime), CAST(N'2018-06-07T00:00:00.000' AS DateTime), N'PAYABLE', N'xx', 55, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0002', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2028, 1, 5007, CAST(158.8751 AS Decimal(18, 4)), NULL, CAST(N'2018-07-30T00:00:00.000' AS DateTime), CAST(N'2018-07-30T00:00:00.000' AS DateTime), N'Unpaid', N'Customer Invoice', 69, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2029, 1, 5007, CAST(18.6912 AS Decimal(18, 4)), NULL, CAST(N'2018-08-05T00:00:00.000' AS DateTime), CAST(N'2018-08-05T00:00:00.000' AS DateTime), N'Unpaid', N'Sale Invoice # 0001', 69, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2030, 1, 5007, CAST(1869.1200 AS Decimal(18, 4)), NULL, CAST(N'2018-08-05T00:00:00.000' AS DateTime), CAST(N'2018-08-05T00:00:00.000' AS DateTime), N'Unpaid', N'Sale Invoice # 0002', 50, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2031, 1, 5007, CAST(40.4000 AS Decimal(18, 4)), NULL, CAST(N'2018-08-05T00:00:00.000' AS DateTime), CAST(N'2018-08-05T00:00:00.000' AS DateTime), N'Unpaid', N'Sale Invoice # 0003', 3100, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2032, 1, 5007, CAST(18.6912 AS Decimal(18, 4)), NULL, CAST(N'2018-09-02T00:00:00.000' AS DateTime), CAST(N'2018-09-02T00:00:00.000' AS DateTime), N'Unpaid', N'Test Description', 50, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2033, 1, 5012, CAST(1495.2960 AS Decimal(18, 4)), NULL, CAST(N'2018-09-04T00:00:00.000' AS DateTime), CAST(N'2018-09-04T00:00:00.000' AS DateTime), N'Unpaid', N'Invoice', 3102, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2034, 1, 5012, CAST(98.5600 AS Decimal(18, 4)), NULL, CAST(N'2018-09-10T00:00:00.000' AS DateTime), CAST(N'2018-09-10T00:00:00.000' AS DateTime), N'Unpaid', N'Invoice # 0001', 3105, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, NULL)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2035, 1, 5012, CAST(145.4400 AS Decimal(18, 4)), NULL, CAST(N'2018-09-10T00:00:00.000' AS DateTime), CAST(N'2018-09-10T00:00:00.000' AS DateTime), N'Unpaid', N'Marvel Agro', 3105, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, 3096)
GO
INSERT [dbo].[InvoiceHeader] ([InvoiceHeaderId], [CompanyId], [CustomerId], [TotalAmount], [InvoiceNo], [Date], [DueDate], [Status], [Description], [CategoryId], [CurrencyRate], [BaseRate], [VoucherType], [CurrencyId], [LabelId]) VALUES (2036, 1, 5007, CAST(17.2800 AS Decimal(18, 4)), NULL, CAST(N'2018-09-30T00:00:00.000' AS DateTime), CAST(N'2018-09-30T00:00:00.000' AS DateTime), N'Unpaid', N'Invoice Test 0001', 3105, CAST(1.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0001', 0, 3096)
GO
SET IDENTITY_INSERT [dbo].[InvoiceHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [CompanyId], [Name], [Code], [Barcode], [StartingQuantity], [Unit], [PurchasePrice], [PurchaseCurrencyRate], [SalesPrice], [SalesCurrencyRate], [KDVPerc], [BaseRate], [AccountCode], [SalesCurrencyId], [PurchaseCurrencyId]) VALUES (6, 71, 1, N'un processed rice', N'65w12e', N'21dfw', 100, N'kg', CAST(100.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(120.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(18.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0044', 0, 0)
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [CompanyId], [Name], [Code], [Barcode], [StartingQuantity], [Unit], [PurchasePrice], [PurchaseCurrencyRate], [SalesPrice], [SalesCurrencyRate], [KDVPerc], [BaseRate], [AccountCode], [SalesCurrencyId], [PurchaseCurrencyId]) VALUES (7, 1073, 1, N'Sella Rice', N'SR01', N'10101010', 10000, N'kg', CAST(12.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(16.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(18.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'0058', 0, 0)
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [CompanyId], [Name], [Code], [Barcode], [StartingQuantity], [Unit], [PurchasePrice], [PurchaseCurrencyRate], [SalesPrice], [SalesCurrencyRate], [KDVPerc], [BaseRate], [AccountCode], [SalesCurrencyId], [PurchaseCurrencyId]) VALUES (8, 1073, 1, N'Basmati Rice', NULL, NULL, 0, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'0059', 0, 0)
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [CompanyId], [Name], [Code], [Barcode], [StartingQuantity], [Unit], [PurchasePrice], [PurchaseCurrencyRate], [SalesPrice], [SalesCurrencyRate], [KDVPerc], [BaseRate], [AccountCode], [SalesCurrencyId], [PurchaseCurrencyId]) VALUES (9, 1073, 1, N'Basmati Super Rice', NULL, NULL, 0, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'0060', 0, 0)
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [CompanyId], [Name], [Code], [Barcode], [StartingQuantity], [Unit], [PurchasePrice], [PurchaseCurrencyRate], [SalesPrice], [SalesCurrencyRate], [KDVPerc], [BaseRate], [AccountCode], [SalesCurrencyId], [PurchaseCurrencyId]) VALUES (10, 1073, 1, N'Football Rice', NULL, NULL, 0, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'0061', 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([UserId], [Email], [Password], [Title], [Name], [SurName], [UserStatusTypeCode], [Phone], [Photo], [CompanyId], [CashBankTransaction], [SalesTransaction], [ExpenseTransaction], [EmployeesTransaction], [guid], [IsEmailConfirmed]) VALUES (1, N'admin', N'admin', N'NewUser', N'Userrrrr', N'New', N'0001', N'0331123456', NULL, 1, N'0001', N'0001', N'0001', N'0001', NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [Email], [Password], [Title], [Name], [SurName], [UserStatusTypeCode], [Phone], [Photo], [CompanyId], [CashBankTransaction], [SalesTransaction], [ExpenseTransaction], [EmployeesTransaction], [guid], [IsEmailConfirmed]) VALUES (4, N'new@user.ocm', N'123456', NULL, N'aaa', NULL, N'0002', NULL, NULL, 5, N'0001', N'0001', N'0001', N'0001', NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [Email], [Password], [Title], [Name], [SurName], [UserStatusTypeCode], [Phone], [Photo], [CompanyId], [CashBankTransaction], [SalesTransaction], [ExpenseTransaction], [EmployeesTransaction], [guid], [IsEmailConfirmed]) VALUES (5, N'new@testuser.com', N'admin', N'test user', N'test user', N'test user', N'0002', N'123456', NULL, 1, N'0001', N'0001', N'0001', N'0001', NULL, NULL)
GO
INSERT [dbo].[User] ([UserId], [Email], [Password], [Title], [Name], [SurName], [UserStatusTypeCode], [Phone], [Photo], [CompanyId], [CashBankTransaction], [SalesTransaction], [ExpenseTransaction], [EmployeesTransaction], [guid], [IsEmailConfirmed]) VALUES (9, N'new', N'user', NULL, N'come', NULL, N'0003', NULL, NULL, 1, N'0001', N'0001', N'0001', N'0001', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Company]
GO
ALTER TABLE [dbo].[Allocation]  WITH CHECK ADD  CONSTRAINT [FK_Allocation_CashBank] FOREIGN KEY([CashBankId])
REFERENCES [dbo].[CashBank] ([CashBankId])
GO
ALTER TABLE [dbo].[Allocation] CHECK CONSTRAINT [FK_Allocation_CashBank]
GO
ALTER TABLE [dbo].[Allocation]  WITH CHECK ADD  CONSTRAINT [FK_Allocation_InvoiceHeader] FOREIGN KEY([InvoiceHeaderId])
REFERENCES [dbo].[InvoiceHeader] ([InvoiceHeaderId])
GO
ALTER TABLE [dbo].[Allocation] CHECK CONSTRAINT [FK_Allocation_InvoiceHeader]
GO
ALTER TABLE [dbo].[AuthorizedPerson]  WITH CHECK ADD  CONSTRAINT [FK_AuthorizedPerson_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[AuthorizedPerson] CHECK CONSTRAINT [FK_AuthorizedPerson_Company]
GO
ALTER TABLE [dbo].[AuthorizedPerson]  WITH CHECK ADD  CONSTRAINT [FK_AuthorizedPerson_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[AuthorizedPerson] CHECK CONSTRAINT [FK_AuthorizedPerson_Customer]
GO
ALTER TABLE [dbo].[CashBank]  WITH CHECK ADD  CONSTRAINT [FK_CashBank_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[CashBank] CHECK CONSTRAINT [FK_CashBank_Company]
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Category] CHECK CONSTRAINT [FK_Category_Company]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Category]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Company]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Category]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Company]
GO
ALTER TABLE [dbo].[GLDetail]  WITH CHECK ADD  CONSTRAINT [FK_GLDetail_GLHeader] FOREIGN KEY([GLHeaderId])
REFERENCES [dbo].[GLHeader] ([GLHeaderId])
GO
ALTER TABLE [dbo].[GLDetail] CHECK CONSTRAINT [FK_GLDetail_GLHeader]
GO
ALTER TABLE [dbo].[GLHeader]  WITH CHECK ADD  CONSTRAINT [FK_GLHeader_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[GLHeader] CHECK CONSTRAINT [FK_GLHeader_Company]
GO
ALTER TABLE [dbo].[InvoiceDetail]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_InvoiceHeader] FOREIGN KEY([InvoiceHeaderID])
REFERENCES [dbo].[InvoiceHeader] ([InvoiceHeaderId])
GO
ALTER TABLE [dbo].[InvoiceDetail] CHECK CONSTRAINT [FK_InvoiceDetail_InvoiceHeader]
GO
ALTER TABLE [dbo].[InvoiceDetail]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[InvoiceDetail] CHECK CONSTRAINT [FK_InvoiceDetail_Product]
GO
ALTER TABLE [dbo].[InvoiceHeader]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHeader_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[InvoiceHeader] CHECK CONSTRAINT [FK_InvoiceHeader_Category]
GO
ALTER TABLE [dbo].[InvoiceHeader]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHeader_Category1] FOREIGN KEY([LabelId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[InvoiceHeader] CHECK CONSTRAINT [FK_InvoiceHeader_Category1]
GO
ALTER TABLE [dbo].[InvoiceHeader]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHeader_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[InvoiceHeader] CHECK CONSTRAINT [FK_InvoiceHeader_Company]
GO
ALTER TABLE [dbo].[InvoiceHeader]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHeader_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[InvoiceHeader] CHECK CONSTRAINT [FK_InvoiceHeader_Customer]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Company]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company]
GO
USE [master]
GO
ALTER DATABASE [CloudAccountsDb] SET  READ_WRITE 
GO
