﻿
using CloudAccounts.Enums.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudAccounts.Enums
{
    public enum MembershipType
    {
        [StringValue("0001")]
        [StringDescription("Silver")]
        Silver,

        [StringValue("0002")]
        [StringDescription("Gold")]
        Gold,
        
        [StringValue("0003")]
        [StringDescription("Premium")]
        Premium,
        
        [StringValue("0004")]
        [StringDescription("Diamond")]
        Diamond
    }
}
