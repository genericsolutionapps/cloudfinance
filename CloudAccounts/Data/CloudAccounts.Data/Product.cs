//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CloudAccounts.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        public Product()
        {
            this.InvoiceDetails = new HashSet<InvoiceDetail>();
        }
    
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Barcode { get; set; }
        public Nullable<int> StartingQuantity { get; set; }
        public string Unit { get; set; }
        public Nullable<decimal> PurchasePrice { get; set; }
        public Nullable<decimal> PurchaseCurrencyRate { get; set; }
        public Nullable<decimal> SalesPrice { get; set; }
        public Nullable<decimal> SalesCurrencyRate { get; set; }
        public Nullable<decimal> KDVPerc { get; set; }
        public Nullable<decimal> BaseRate { get; set; }
        public string AccountCode { get; set; }
        public Nullable<int> SalesCurrencyId { get; set; }
        public Nullable<int> PurchaseCurrencyId { get; set; }
    
        public virtual Category Category { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual Company Company { get; set; }
    }
}
