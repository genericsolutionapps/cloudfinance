//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CloudAccounts.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoiceHeader
    {
        public InvoiceHeader()
        {
            this.InvoiceDetails = new HashSet<InvoiceDetail>();
            this.Allocations = new HashSet<Allocation>();
        }
    
        public int InvoiceHeaderId { get; set; }
        public int CompanyId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public decimal TotalAmount { get; set; }
        public string InvoiceNo { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime DueDate { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public decimal CurrencyRate { get; set; }
        public decimal BaseRate { get; set; }
        public string VoucherType { get; set; }
        public int CurrencyId { get; set; }
        public Nullable<int> LabelId { get; set; }
    
        public virtual Category Category { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual ICollection<Allocation> Allocations { get; set; }
        public virtual Company Company { get; set; }
        public virtual Category Category1 { get; set; }
    }
}
