//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CloudAccounts.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Company
    {
        public Company()
        {
            this.Accounts = new HashSet<Account>();
            this.AuthorizedPersons = new HashSet<AuthorizedPerson>();
            this.CashBanks = new HashSet<CashBank>();
            this.Categories = new HashSet<Category>();
            this.Customers = new HashSet<Customer>();
            this.Employees = new HashSet<Employee>();
            this.GLHeaders = new HashSet<GLHeader>();
            this.InvoiceHeaders = new HashSet<InvoiceHeader>();
            this.Products = new HashSet<Product>();
            this.Users = new HashSet<User>();
        }
    
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string CommercialTitle { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OpenAddress { get; set; }
        public string Logo { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Wire { get; set; }
        public string Fax { get; set; }
        public string VD { get; set; }
        public string VNo { get; set; }
        public string CompanyTypeCode { get; set; }
        public string FeatureTypeCode { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<AuthorizedPerson> AuthorizedPersons { get; set; }
        public virtual ICollection<CashBank> CashBanks { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<GLHeader> GLHeaders { get; set; }
        public virtual ICollection<InvoiceHeader> InvoiceHeaders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
