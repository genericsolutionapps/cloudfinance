//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CloudAccounts.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Priviledges
    {
        public int ID { get; set; }
        public string ApplicationCode { get; set; }
        public int User_ID { get; set; }
        public int Form_ID { get; set; }
        public bool Is_Insert { get; set; }
        public bool Is_Update { get; set; }
        public bool Is_View { get; set; }
        public bool Is_Delete { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public string Created_By { get; set; }
        public string Machine_Created { get; set; }
        public Nullable<int> Module_ID { get; set; }
    }
}
